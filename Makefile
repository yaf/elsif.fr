PANDOC := $(shell command -v pandoc 2> /dev/null)

all:
ifndef PANDOC
    $(error "pandoc is not available please install pandoc")
endif


build: ## Construit le site
	./pana.sh

deploy: ## déploie le site
	git push && rsync -avz content/ elsif@ssh-elsif.alwaysdata.net:www/

run: ## démarre un serveur local
	python3 -m http.server 8000 --bind 127.0.0.1 --directory content/

