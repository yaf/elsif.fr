---
title: Dimanche 13 février 2022
---


Pour commander des livres via des libraires indépendants https://www.lalibrairie.com/

---

[La restructuration de code, bonnes pratiques et stratégies](https://blog.tcweb.org/2022/02/06/la-restructuration-de-code-bonnes-pratiques-et-strat%C3%A9gies/)

« Afin d’améliorer la qualité de notre code en le restructurant, il y a un pré-requis : avoir un bon harnais de test ; Et 2 stratégies :
- Appliquer chaque règle de “clean code” une par une sur l’intagralité du code avant d’en changer.
- Suivre l’arbre de vos envies de modification et commencez par les feuilles. C’est la technique du mikado.
»

---

> il (le logiciel libre) a été coopté, intégré et récupéré par les colosses de la Silicon Valley, Google, Apple, Facebook, Amazon et Microsoft (Gafam). Au point que les logiciels open source (pour « code source ouvert », un terme adopté dans le milieu industriel pour parler du logiciel libre sans parler de… libertés ! se trouvent désormais au cœur de l’économie numérique.

> La communauté du logiciel libre peut-elle se constituer en entité politique qui réfléchit, au-delà du logiciel, sur la société dans son ensemble ? Peut-elle se confronter aux orthodoxies productivistes, au développement infini de la puissance de calcul ? Tout le passé indique le contraire. Son succès, pourtant, en dépend.

https://www.monde-diplomatique.fr/2022/01/O_NEIL/64221

---

Fish setup ?

From https://www.youtube.com/watch?v=KKxhf50FIPI 
devaslife

- Fisher (package manager)
- shellder shel theme
- nerd fonts powerline patched font / sourceCode pro
- z (directory jumping)
- peco interactive filtering -CTRL+r search history,  CTRL+f search config dir
- exa a modern ls
- ghq local git repo orga


---

> Les organisations d’éducation populaire reçoivent des subventions et les militants connaissent une professionnalisation qui conduit à une neutralisation progressive de l’éducation populaire.
>
> Le principe de contrôle du temps de loisirs de la jeunesse reste une constante.
>
> Mais, il se développe dans le cadre d’une société où la réduction du temps de travail, qui a été conquis par les luttes sociales, est colonisée par la société de consommation et en particulier les industries du divertissement.

> La notion de loisir change de sens. Elle ne désigne plus un temps dédié à l’étude et à l’engagement dans la vie politique, mais un temps dédié au divertissement.

> Bien souvent, ces pratiques fonctionnent parce qu’elles reposent sur la soumission des participants à l’autorité de l’animateur (ou formateur) de groupe et sur la base du conformisme de groupe. Les participant-e-s acceptent de se plier à un ordre qui n’a pas de sens, mais qui amène justement à faciliter l’obéissance de groupe à des activités certes divertissantes – voire parfois « infantilisantes » -, mais qui n’ont guère de sens.

> L’éducation populaire repose sur des pratiques qui ont du sens. Ce n’est pas tant la nature des techniques que le sens que l’on met derrière les techniques qui caractérise l’éducation populaire. Les pratiques ne sont pas là juste pour proposer de l’animation, mais elles visent à développer une démarche de prise de conscience critique tournée vers une action collective de transformation sociale.


https://lewebpedagogique.com/educationpopulaireconscientisante/2022/02/10/ne-pas-confondre-education-populaire-et-animation/

---


Passer par les bailleurs sociaux pour installer des paysans ?

https://reporterre.net/Premiere-en-France-une-ferme-en-logement-social-dans-le-Bearn

https://www.renouveau-paysan.org/


---

Fiche : Le psychisme des personnes opprimées (selon Freire)

> d) je me sens capable de m’engager collectivement pour essayer de changer ma situation et celles des personnes qui vivent les mêmes problèmes que moi. Oui : tout à fait/ assez bien/ pas vraiment/ pas du tout.

https://lewebpedagogique.com/educationsdesoi/2022/02/04/fiche-le-psychisme-des-personnes-opprimees-selon-freire/

---

https://www.linkedin.com/posts/henri-leleu-8267834_je-viens-dactiver-mon-espace-sant%C3%A9-%C3%A0-activity-6894754699325902848-NlU1/

---

Des endroits à visiter, des personnes à rencontrer ?

https://esprit-fablab.org/
https://grifon.fr/

---

[Dans les lycées et les collèges, la vie scolaire sous Pronote](https://www.monde-diplomatique.fr/2022/01/TOURETTE/64241)

> Utilisé dans plus de deux établissements du second degré sur trois, le logiciel de gestion de la vie scolaire Pronote rend bien des services aux familles et aux communautés éducatives, notamment pour affronter les dernières réformes du lycée. Mais, en faisant de l’immédiateté la norme, il change aussi les métiers en profondeur, ainsi que les relations entre parents, élèves et enseignants.

---

> Bā Duàn Jǐn (chinois traditionnel : 八段錦 ; litt. : Huit pièces de brocart1) est une gymnastique chinoise (ou qi gong, 氣功) visant à rendre l'organisme plus résistant et prolonger la vie. 

https://fr.wikipedia.org/wiki/Baduanjin

https://www.youtube.com/watch?v=ZfIf1pCii4Q

---

Python shell 
https://gist.github.com/davidbgk/51f38d1fc4ecc0c8778a9bab2804beb9

---

À lire ? [Aurélien Berlan, Terre et liberté. La Quête d’autonomie contre le fantasme de délivrance, La Lenteur, 2021](https://blog.ecologie-politique.eu/post/Terre-et-liberte)

---

CSS Speed run https://blog.shevarezo.fr/post/2022/01/20/css-speedrun-tester-connaissances-css
