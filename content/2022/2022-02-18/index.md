
> Precisely, [France’s 1968 law 68-1 about patents literally](https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000006281073/1979-07-01/) does not consider “les programmes d’ordinateurs” as patentable inventions. This has been changed since, but it set a strong precedent worldwide, as anything that France does in general.


https://deprogrammaticaipsum.com/the-conquest-of-code/

---



Alors, est-ce que l’arrivée du RGPD [1] l’an prochain mettra un frein à tout ça ?
Je vais peut-être paraitre rétrograde, mais si on me demande de protéger les données de santé à caractère personnel produite par mon établissement, personnellement je n’aurais pas envie de les laisser partir par SMS.
Je suis ouvert au débat et preneur si des juristes ont un avis sur le sujet.



https://www.dsih.fr/article/2386/rappel-des-rendez-vous-par-sms-un-risque-pour-la-confidentialite.html
