---
title: Dimanche 29 mai 2022
---

Contre les limaces, nous avons essayé 
- la cendre (ça fonctionne pas trop mal, mais dès qu'il pleut, c'est moins bien. Je crains également que ça abime le sol à long terme, alors je n'en met pas trop.

- Tuile renversé pret des cultures avec ramassage le matin (j'ai plus de mal à le faire le soir). Ça apporte la satisfaction de commencer la journée le nez dans les plantes, et d'observer la vie. Car en plus de ramasser sous les tuiles, je jette un coup d'œil aux plantes. Je déplace ensuite les limaces récolté vers un endroit où je vois souvent les oiseaux du coin chercher à manger. Le hic, c'est que souvent, les limaces ont bien mangées avent...

- Le citron, orange (agrume ?). Astuce évoqué hier, qui est en court d'expérimentation dans le jardin. Pour la première nuit, c'était plutôt pas mal. Mais peut-être un hasard. Voyons la suite.

- Purin de fougères. C'est le plan que j'aimerais mettre en place dans les jours à venir.


Pas de canard ou poule pour le moment. Nous sommes en location.

---

J'ai repris un peu l'usage de VSCodium. J'ai ajouté le plugin vim. C'est assez intéressant.

Par contre, je crois que je me retrouve à « vouloir » utiliser VSCodium quand ma PR contient trop de fichier. J'en suis presque à me dire que quand j'ai envie de quitter Vim pour VSCodium, c'est que j'ai fait trop de modification pour cette PR (je parle pas forcement de la taille des commits, mais du volume complet de modification de la PR).
