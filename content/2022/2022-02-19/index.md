---
title: 19 février 2022
---

> Salut Yannick,
> 
> 
> Arf, pas cool la réponse de Chrysalide, est-ce qu’ils ont dit ça par loyauté territoriale ?? Et une CAE de Rennes ou Nantes qui fait un peu du conseil ?
> 
> Si tu pries et que tu leurs dis que tu n’as vraiment vraiment pas envie d’aller à FILEO, ça passe ? Après, tout dépend, si tu veux juste un statut pour un statut, FILEO peut te convenir, le mieux serait d’aller les rencontrer et de poser des questions clés pour voir si tu pourrais te sentir bien.
> 
> Et le portage salarial type cadre en mission ?
> 
> 
> Moi je suis en auto-entreprise donc je ne me suis pas posée toutes ces questions. Et je n’ai pas eu vent de création d’autres CAE, et pourtant le territoire est mûr pour voir fleurir autre chose, avec des valeurs ESS. Avec moi ça fait déjà 2 personnes intéressées ! J’aimerais bientôt avoir un autre statut donc il y a quelque chose à réfléchir !
> 
> Sinon tu as demandé à la BGE, à la Colloc ? A Optimism (rien à voir avec une CAE mais Max le directeur est toujours dans les bons plans, quand il ne les créé par lui-même…), à C2Sol ?
> 
> Mais du coup il y a un détail qui m’échappe, et Scopyleft, c’est une coopérative… tu en es parti ?
> 
> Au plaisir d’échanger !
> A bientôt,
> Marie

J'ajouterais que ça serai intéressant de voir https://www.astrolabe.coop/ le fablab de rostrenen et les divers lieux que l'on m'a déjà conseiller.
