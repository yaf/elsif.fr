---
title: Lundi 11 avril 2022
---

À lire ? [Le procès des étoiles de Florence Trystram](https://www.babelio.com/livres/Trystram-Le-proces-des-etoiles/983192)
À lire ? [Diony coop : Des coopératives alimentaires autogérées dans le 9-3. Apprendre en faisant.](https://editions-libertaires.org/produit/diony-coop-des-cooperatives-alimentaires-autogerees-dans-le-9-3-apprendre-en-faisant-2017-de-jean-claude-richard/)


https://coopaparis.wordpress.com/2015/09/06/visite-de-la-dionycoop/

---

Parce que je n'arrive plus à retrouver l'article qui m'a donné l'envie de ne plus utiliser `let` dans rspec, je vais coller ici les ressources que je retrouve aujourd'hui :

- [ My issues with Let ](https://thoughtbot.com/blog/my-issues-with-let)
- [Code duplication in unit tests](https://evgenii.com/blog/code-duplication-in-unit-tests/)

Et l'avertissement de la doc

>  Note:
>
> let can enhance readability when used sparingly (1,2, or maybe 3 declarations) in any given example group, but that can quickly degrade with overuse. YMMV.



https://www.rubydoc.info/github/rspec/rspec-core/RSpec%2FCore%2FMemoizedHelpers%2FClassMethods%3Alet

https://mtlynch.io/good-developers-bad-tests/

https://kentcdodds.com/blog/avoid-nesting-when-youre-testing

http://blog.institut-agile.fr/2010/11/la-danseuse-et-la-dette-technique.html


---

> Voici, encore une fois, la devise d’Alan Kay : “Les choses simples devraient être simples et les choses compliquées possibles”. (Lorusso, 2021)

https://www.quaternum.net/2022/04/10/vim-maitriser-son-environnement/

> Avec un éditeur de texte comme Vim, j’apprends à écrire, le texte sous les doigts.
