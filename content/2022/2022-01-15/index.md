---
title: Samedi 15 janvier 2022
---

Un apprentissage actif, en programmation, ça ne signifie pas « rechercher sur google seul ». Ça signifie construire une connaissance, une compréhension de la situation.

Pour le faire, il faut donner des clefs, certaines en tout cas, attirer l'attention sur un point plutôt qu'un autre.


