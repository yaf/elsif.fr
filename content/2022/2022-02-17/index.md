---
title: Jeudi 17 février 2022
---

Avoir un rythme bien défini pour travailler en « sprint » un peu comme en agilité. Histoire de faire de l'agilité pour l'apprendre.

[qrop La planification libre pour le maraîchage](https://qrop.frama.io/)


https://bouchecousue.com/blog/wifi-ouvert-dans-un-espace-accueillant-du-public-quelles-obligations-sappliquent/

> si les autorités peuvent imposer des obligations de conservation, cela ne peut déboucher sur des obligations de collecte d’informations qui ne sont pas pertinentes pour l’exploitation d’un service.

> ne jamais laisser sans réponse une réquisition judiciaire, et une réponse même négative expliquant que vous ne pouvez transmettre que les données que vous collectez, et non celles que vous ne collectez pas, sera de nature à établir votre bonne foi et vous éviter bien des tracas.



---

https://empreintedigitale.fr/


---

Finished in 46 minutes 24 seconds (files took 1.99 seconds to load)
1562 examples, 0 failures

