---
title: Jeudi 28 avril 2022
---

L'éducation, le savoir-faire, la main et la tête...


> l'intelligence de la main 

> Frédéric Joulian "Le wasa, ça correspond aux arts de faire, à des habiletés qui sont à la fois incarnées et apprises, c'est les deux à la fois. Ça renvoie à des savoirs formels, celui du forgeron, celui du marionnettiste qui va développer un wasa très aigu, très précis. Mais ça correspond aussi à des savoirs courants."

Ça fait réver. Un des secrets du japon sans doute ?

Pas de hierarchisation des savoirs. Combinaison de l'esprit et de la main.

> Un outil pour penser !

