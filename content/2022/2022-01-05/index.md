---
title: Mercredi 5 janvier 2021
---

Laura THAY  9 h 22
Helloooo :visage_légèrement_souriant: bonne année a tous ! j'étais dans la premiere promo des descodeuses et depuis jai intégré un master en developpement web chez Hetic ! J'aimerai rattraper mon retard en terme d'algorithme et de structure de donnée ! Auriez-vous des bouquins a me recommander en mode débutante svp ? Merci à vous :clin_d'œil:






Yannick  10 h 51
alors, il y a Grokking Algorithms qui est vraiment chouette https://www.manning.com/books/grokking-algorithms (je crois qu'il existe en français si besoin)
10 h 52
il y a aussi une référence en la matière : Introduction to Algorithms, Third Edition https://mitpress.mit.edu/books/introduction-algorithms-third-edition
10 h 53
Ensuite, dans ceux que je n'ai pas lu mais dont on m'a beaucoup parlé en bien , il y a Pearls of Functional Algorithm Design https://www.cambridge.org/core/books/pearls-of-functional-algorithm-design/B0CF0AC5A205AF9491298684113B088F
10 h 54
et Purely Functional Data Structures https://www.cambridge.org/core/books/purely-functional-data-structures/0409255DA1B48FA731859AC72E34D494
10 h 55
Algorithms, 4th Edition https://www.pearson.com/us/higher-education/program/Sedgewick-Algorithms-4th-Edition/PGM100869.html
10 h 56
et puis quelques référence que j'avais noté mais sans avoir de retour d'autres personnes dessus :
10 h 56
Advanced Data Structures https://www.cambridge.org/core/books/advanced-data-structures/D56E2269D7CEE969A3B8105AD5B9254C
10 h 58
Data Structures And Algorithms Made Easy (voir sur le site de l'auteur http://careermonk.com/ )
11 h 01
The Algorithm Design Manual https://link.springer.com/book/10.1007/978-1-84800-070-4 (modifié)


Yannick  11 h 08
J'oubliais le fameux Structure and Interpretation of Computer Programs https://mitpress.mit.edu/sites/default/files/sicp/index.html

et j'avais aussi ce bout de truc dans la liste des livres qui me font envie : Poems That Solve Puzzles https://global.oup.com/academic/product/poems-that-solve-puzzles-9780198853732?cc=fr&lang=en&
