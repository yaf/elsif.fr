---
title: Lundi 2 mai 2022
---


Mikael De Juillac
Hier, à 09:43  ·
"Saviez-vous qu'il existe un antidépresseur naturel dans la nature ?  C'est la bactérie Mycobacterium vaccae trouvée dans le sol que les humains ingèrent ou inhalent lorsqu'ils passent du temps dans la nature et le jardinage.  Cette bactérie stimule la partie du cerveau responsable de la production de sérotonine.  Les jardiniers les plus passionnés vous diront que leur jardin est leur "endroit heureux" et que l'acte physique de jardiner réduit le stress et améliore l'humeur.  Le Mycobacterium vaccae améliore également les fonctions cognitives, il a un effet sur la maladie de Crohn et même la polyarthrite rhumatoïde.  Les effets naturels de ces bactéries antidépressives dans le sol peuvent se faire sentir jusqu'à 3 semaines.  Asseyez-vous, salissez vos mains et soyez en meilleure santé !"

https://www.facebook.com/dodiermikael/posts/10159809868617692
