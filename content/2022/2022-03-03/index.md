---
title: Jeudi 3 mars 2022
---

> La conscientisation, comme théorisée par le pédagogue brésilien Paulo Freire durant les années 1960, est le processus socioconstructiviste par lequel les groupes opprimés acquièrent une compréhension des injustices systémiques qu’ils vivent et par lequel ils coconstruisent les moyens de s’émanciper de leur oppression (Freire, 1974). L’apprentissage dépend d’une relation horizontale entre la personne enseignante, qui sert de guide, et les personnes apprenantes. Le modèle emploie le dialogue, le questionnement et la formation de jugement afin que les apprenant.es développent une conscience critique de leurs circonstances de vie et trouvent des solutions pour améliorer leur condition collective (Freire, 1971).

> Suivant notre revue de littérature et en respectant la définition de la conscientisation de Freire, nos recherches nous amènent à proposer dix critères nécessaires à un jeu pour représenter un espace de conscientisation 

> À travers nos recherches, nous n’avons repéré aucun jeu vidéo répondant aux dix critères simultanément

> Nous avons toutefois identifié que la création de jeux pourrait en elle-même représenter un espace de conscientisation : grâce à la présence d’un.e agent.e facilitateur.trice, un groupe marginalisé ou minorisé pourrait se rencontrer, coopérer, partager leurs perspectives et s’outiller ensemble afin de créer un jeu. Par exemple, un Women’s Game Jam serait un espace analogue à un contre-espace (counterspace) (Case et Hunter, 2012) dans lequel un collectif de femmes conceptrices pourrait défier l’oppression systémique en créant un jeu qui s’adresse à d’autres femmes vivant des difficultés dans l’industrie du jeu vidéo (de Castell et Skardzius, 2019) ou dans les études universitaires en jeu (Humphrey, 2017).

http://vectis.ca/2022/02/21/dix-criteres-pour-faire-du-jeu-un-espace-de-conscientisation/

---

Pour me replonger dans VIM

- https://github.com/amix/vimrc
- https://www.guckes.net/vim/setup.html
- https://www.reddit.com/r/vim/comments/t0rc1t/vim_minimal_setup_explained/
- https://vimawesome.com/
- https://www.reddit.com/r/vim/
- https://vim.fandom.com/wiki/Best_Vim_Tips
