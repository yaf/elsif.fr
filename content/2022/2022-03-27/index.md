---
title: dimanche 27 mars 2022
---


> Alors bienvenues à toutes ! Bienvenues à toutes les femmes et meufs dans nos campagnes, bienvenue à tous les membres de la communauté LGBTQI+ [Lesbienne, gay, bi, trans, queer, intersexe et plus], bienvenue à toutes les personnes racisées, les handi et toutes celleux qui ont envie de faire bouger les normes en milieu rural. Dégenrons le monde agricole et rural. Ensemble dégenrons et dérangeons !

https://reporterre.net/Femmes-rurales-nous-ne-laisserons-pas-la-campagne-aux-hommes

---

C'est cool d'avoir un cahier pour faire mon journal du jardin. Mais est-ce que c'est vraiment la meilleure option ?

Je l'utilise pour deux objectifs : 
- apprendre, prendre conscience que j'apprend
- noté ce que je plante et où

Pour être franc, je crois que je ne prends que le temps de faire le deuxième point. Faut-il que j'envisage un autre espace pour mes apprentissages ?

Est-ce que ce n'est pas ce qui pousse qui me permet de vérifier que j'apprends ?

---

> la correction de dysfonctionnements dans notre code est une compétence très importante pour gagner en autonomie.

https://blog.nicolas.brondin-bernard.com/le-guide-pour-apprendre-a-debugger-du-code/

---

> s’inspirer des communs comme formes institutionnelles pour repenser l’administration des biens collectifs. Cela entraîne deux transformations majeures. D’une part, ces services publics ne pourront plus être l’objet de privatisations. D’autre part, leur gouvernance devra intégrer les usagers, les collectifs organisés et les agents publics dans leur conception, leur mise en œuvre et leur fonctionnement. 

> Cela nécessite un changement de paradigme dans l’organisation de l’action publique : passer de commanditaires décisionnaires à des pratiques de coopération et des modes d’action partenariaux, sortir des postures habituelles et des rapports trop souvent binaires financeur-financé ou commanditaire-prestataire de service public. 

https://acteurspublics.fr/articles/pour-une-societe-des-communs-face-a-la-privatisation-du-service-public
