---
title: mercredi 11 mai 2022
---

Pour le projet d'atelier d'auto-réparation de vél, les personnes de l'atelier Syklett mon conseillé le wiki [Wiklou](https://wiklou.org/wiki/Accueil). Il a également été évoqué l'[Heureux Cyclage](https://www.heureux-cyclage.org/)

J'ajoute quelque liens trouvé par-ci par-là

https://www.ari-recyclage.com/
http://www.laptiterustine.fr/atelier-velo/recuperation-vieux-velos/


---

Très heureux également d'avoir rendu visite à la pépinière du Saint [Des fruits des fleurs](http://desfruitsdesfleurs.fr/), membre de la coopérative [Graine de liberté](https://www.grainesdeliberte.coop).

J'ai pris des pieds de persil, coriandre, basilic et un truc asiatique (oublié le nom).

Il y a des grosses attaques de limaces et d'escargots dans le potager. Nous avons perdu toutes nos premiers semis de salade, une bonne partie des carottes et des navets. J'avais acheté 2 pieds de courgette qui ont fini complétement bouffé, et deux pieds de melon de rennes, pareil...

Je crois que des tournesols ont aussi été grignoté...

J'ai tenté la cendre. Ça semle fonctionné, mais je ne pense pas que ce soit durable, et à mon avis, ça abime le sol d'en mettre tout le temps...

Pour les canards, c'est compliqué, il n'y a pas de marre, et je ne pense pas pouvoir en faire tant que nous sommes en location. Les poules serait une option intéressante... Peut-être l'an prochain ?

En attendant, je vais jouer avec des plantes, pourquoi pas quelques purins et autres potions. Et mettre en place des plaques d'œufs, des tuiles et faire un petit tour le matin pour les récolter et les proposer aux oiseaux du coin...

En attendant, voici quelques infos sur les plantes anti-limaces que j'ai récolté ce soir

> Absinthe
>
> L’absinthe, cette plante connue depuis l’Antiquité, est une vivace aromatique dont la culture facile présente de multiples avantages. Elle s’adapte à tous types de sols, quelle que soit l’exposition, et elle est très rustique. Elle dégage une odeur très forte qui a un effet répulsif sur les limaces et escargots. On peut aussi l’utiliser pour la création d’insecticide. En plus de tout cela, c’est une plante médicinale.

> Ail
>
> L’odeur de l’ail va également repousser les limaces de façon immédiate. Il est donc possible de préparer votre répulsif à base d’ail. Pour cela, rien de plus simple, préparez un mélange à base d’eau et d’ail broyé, puis laissez infuser au moins une heure. Vous pouvez mettre votre préparation dans un vaporisateur et la pulvériser sur les plants attaqués. Néanmoins, il sera nécessaire de renouveler régulièrement l’opération.

> Armoise
>
> L’armoise est une autre plante médicinale. Elle est rustique et résiste aux épisodes de sécheresse. C’est une plante facile à cultiver. En plus d’offrir de nombreux bienfaits sous forme de tisanes, elle peut repousser les limaces de votre jardin. Pour ce faire, il faudra préparer un purin d’armoise en faisant infuser 1 kg de tiges et de feuilles récemment coupées dans 10 l d’eau.


> Begonias
>
> Les bégonias peuvent être plantés à proximité des plantes à protéger. Ils auront un effet répulsif en plus d’être jolis à regarder. L’objectif est alors, dans votre jardin, de remplacer les plantes qui éventuellement attirent les limaces par d’autres qui les repoussent.

> Bourrache
> 
> On n’a presque plus besoin de vanter les nombreux avantages qu’offre la culture de la bourrache dans un jardin. Elle s’étale facilement et permet de profiter rapidement d’un parterre de petites fleurs bleues. Peut-être trop facilement parfois, car elle peut devenir envahissante, mais on peut alors la manger dans une salade. Elle permet d’enrichir le sol et de protéger le potager des limaces, chenilles, etc..

J'adore la bourrache. J'ai des graines, mais j'ai fait un semi qui ne semble pas terrible... deux graines ont levé :-/ Est-ce que je n'ai pas trouvé les bonnes conditions pour le semis ? Est-ce trop tard ?

> Capucine
>
> Les capucines sont faciles à semer et elles égaieront votre jardin avec leurs fleurs colorées et ce, de juin jusqu’aux premières gelées. En plus de leur aspect esthétique, elles font partie des fleurs qui vont contribuer à tenir les limaces à distance de votre potager ou de votre jardin.

> Cassis
>
> Les plants de cassis feront office de barrière naturelle pour protéger vos plants contre des indésirables tels que la limace. Avec des feuilles de cassis, on peut également préparer des décoctions à répandre sur les cultures.

> Cerfeuil
>
> Le cerfeuil est une plante condimentaire aux vertus médicinales connues depuis le Moyen-Âge. Mais pas seulement, car lui aussi cumule les propriétés. Celle que nous retiendrons aujourd’hui est son effet répulsif sur les limaces du fait de son odeur.

Ah, j'en ai pris deux pieds aussi.. Nous verrons bien s'ils sont attaqué ou pas :)

> Consoude
>
> La consoude est une plante-engrais qui trouvera sa place dans votre jardin. Elle présente de nombreux atouts pour un entretien minime. C’est une plante robuste qui se développera aisément sur un terrain humide. Elle possède des vertus médicinales : ses feuilles et ses tiges contenant de l'allantoïne, elle fut beaucoup utilisée pour stimuler la multiplication cellulaire. Outre cela, elle peut aussi être cuisinée ou encore utilisée comme répulsif sous forme de purin.

La plante que je n'ai pas encore cotoyé mais que j'aimerais beaucoup avoir et travailler !

> Geranium
>
> Le géranium, l’incontournable de nombreux jardins et balcons, est une plante vivace qui profite d’une longue floraison. Les limaces ne l'appréciant pas, il contribue à former une barrière esthétique et protectrice contre ces indésirables.

> Osmonde royale
>
> Cette plante rustique résiste à des températures pouvant descendre jusqu’à -15°C. Comme elle est de type fougère, il est préférable de lui réserver un espace à la fois ombragé et humide. Ce sont ses frondes, c’est-à-dire ce qui joue le rôle de feuille, qui vont être utilisées contre les limaces. Elles peuvent être broyées pour être utilisées en paillage autour des plantes sensibles, par exemple.

> Persil
> 
> Qui ne connaît pas le persil ? Mais saviez-vous qu’il fait partie des plantes que les limaces détestent par-dessus tout ? Pour cette raison, on peut le planter en large plate-bande autour d’une culture à protéger. C’est une plante bisannuelle, qui se plaira dans un emplacement au soleil, mais bénéficiant quand même d’une ombre légère. De plus, la terre doit être riche et rester fraîche.

> Rue officinale
>
> Cette plante condimentaire aux propriétés analgésiques est à aborder avec méfiance. En effet, si elle se développe sous le soleil, elle devient irritante pour la peau et peu appréciable. Néanmoins, cultivée avec soin, elle peut devenir agréable en bouche et au regard. En plus de cela, son odeur plutôt forte aura tendance à repousser les escargots, les limaces, les rongeurs et autres indésirables. Pour bénéficier au maximum de cet effet répulsif, vous pouvez également préparer une macération : avant sa floraison, il vous faut alors cueillir 800 g de feuilles de rue et les mettre à tremper dans 10 l d’eau, puis vous devez laisser macérer pendant 10 jours avant de vaporiser sur vos plants.

> Sauge ornementale
> 
> Cette plante robuste est facile à cultiver. Peu exigeante, elle s’adapte à de nombreuses situations. Sa floraison a lieu en automne, avant cela son feuillage aromatique décorera votre jardin. En plus de cela, vous profiterez d’une barrière protectrice contre les parasites.

> Tanaisie
>
> La tanaisie est une plante médicinale cultivée depuis très longtemps qui a réussi à s'acclimater dans de nombreuses régions. Son port touffu peut atteindre jusqu’à 1,50 m. En été, elle va se couvrir d’une floraison abondante dont l’odeur va repousser de nombreux indésirables, dont les limaces. Avec ses feuilles, vous pouvez aussi préparer un purin à répandre sur vos cultures. À l’inverse, elle va attirer des insectes tels que les coccinelles.

> Thym
>
> Le thym regorge lui aussi de propriétés. Grâce à son odeur très forte, il repousse les ravageurs tels que les limaces. Même ses racines diffusent des substances qui sont repoussantes. On aura alors tout à gagner à utiliser cette plante aromatique qui pousse facilement dans différents types de sol dès lors qu’elle profite d’un bon ensoleillement.

