---
title: Yannick François
---

<img src="/images/portrait-2021-petit.png" id="portrait" style="float: left; margin: 0 1em;">

Bonjour, je m'appelle **Yannick**. Je travaille chez [Pix](https://pix.fr) depuis le centre ouest Bretagne, au Faouët.

Le plus simple pour me contacter est de m'envoyer un email à à
[yannick@kaz.bzh](mailto:yannick@kaz.bzh).

<hr style="clear:both">

J'écrit quelques [billets de blogs](/carte) ([flux atom](/feed.xml)) que j'essaie maintenant de constituer en histoire.

* [Journal d'un formateur en bootcamp](https://yaf.github.io/journal-d-un-formateur-en-2015/) en 2015
* [Petite histoire d'une mission bancale](une-mission-bancale) en 2015

Je partage une [liste trop grande de trucs que j'aimerais faire](/boite-a-idee).

Ma présence ailleurs

- [\@yaf\@framapiaf.org](https://framapiaf.org/@yaf)
- [linkedin/yannick-françois](https://www.linkedin.com/in/yannick-fran%C3%A7ois-52896a180/)
- [pixelfed.social/ya_f](https://pixelfed.social/ya_f)
- [github/yaf](https://github.com/yaf)
- [gitlab/yaf](https://gitlab.com/yaf)
- [framagit.org/yaf](https://framagit.org/yaf)

Quelques structures, communautés ou associations auxquelles je participent

- [Le Grand Manger](https://www.legrandmanger.bzh/)
- [Terre de liens Bretagne](https://terredeliens.bzh/)
- [Fédération Régional des CIVAM de Breatgne](https://www.civam.org/fr-civam-bretagne/)
- [April](https://www.april.org/)


