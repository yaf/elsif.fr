---
title: Semaine 40
---

_Du 4 au 10 octobre 2020_

Est-ce que mon journal peut être un pense-bête, un endroit où je pose des
liens, des citations, des bouts de phrase sans forcement y mettre un sens, une
explication ?

Je pourrais aussi définir les grands axes sur lesquels j'ai envie d'avancer,
les sujets qui me donne de l'énergie, et regarder si je rempli ou pas chaque
semaine ? L'inverse fonctionne aussi. Ceci étant, je n'ai pas forcement le même
découpage d'une semaine à l'autre.

---


## Transition

P'tit boulots ce matin sur l'éco-hameau [Brangoulo](https://www.brangoulo.fr/).
Bonne équipe, bonne ambiance. Des gros travaux de déblayage à faire, des
enfants partout, et des travaux de jardin aussi (récolte de graines principalement).

Je suis resté sur le déblayage, mais la participation aux activités de
maraîchage me botte bien. Il semble y avoir des ruches aussi. Je n'ai pas
encore tout exploré, et je ne suis pas resté pour la visite.

J'apprécie la joie du groupe, le fait que les enfants aient une
vraie place. J'ai également beaucoup apprécié entendre « si rien a avancé à la
fin de la journée, c'est pas grave, ce qui compte c'est que nous puissions nous
parler ». Très chouette.

Je me suis senti avec des personnes que je connais. Ça fait un peu entre soi.
Et en même temps, je pense que c'est une forme de protection que recherche ce
genre de groupe.

Est-ce que ma famille pourrait s'installer tranquillement dans ce genre de
milieu ? Je me demande. Ça fera parti des trucs à explorer justement.


Une participante parisienne explorait divers éco-lieu. Elle était passé voir
[Demain en main](https://demainenmain.fr/), et [le village du bel
air](https://villagedubelair.org/). Ce dernier est un super endroit d'après
elle. La particularité c'est que toutes les personnes y habite en habitat
léger. Un peu trop _roots_ pour moi sans doute.  Mais ça me ferais plaisir de les
rencontrer malgré tout !

> Le Village du Bel-Air, ainsi nommé, se développe depuis maintenant plus d’un
> an sur un terrain de jeu de 15 hectares entremêlé de prairies et de forêts.
> 
> En face des ruines d’un château du 11ème siècle, nous avons investi les deux
> bâtiments existants en lieu collectif. Parallèlement, chacun.e des
> villageois.es possède un habitat léger individuel pour nourrir son intimité.
> Nous partageons le reste du terrain avec des animaux domestiques et sauvages.
> 
> Ici, nous expérimentons une autre manière de vie collective en tentant d’y
> faire vivre 6 grandes valeurs qui nous animent.  Celles-ci nous rassemblent
> et nous donnent une vision commune. Les voici ci-dessous :

---

Première réunion avec [Alpha-B](https://www.brangoulo.fr/alpha-b/)
(l'association de l'éco hammeau Brangoulo). C'est sur Zoom :-/

C'est un peu rapide pour moi, tout juste le lendemain de ma première visite.
Ceci étant, il faut bien y aller un moment. Après tout, je peux m'investir un
peu dans cette histoire, ne serait-ce que pour aider à faire vivre le projet,
apprendre, découvrir.

---

[Des cours pour devenir
agriculteur](https://www.youtube.com/channel/UCUaPiJJ2wH9CpuPN4zEB3nA). Merci
Ver de terre production !


J'apprends que sont fondateur à faire un passage dans le monde du libre. Ça
explique cette approche de partage de connaissance !

> Diffusion libre du savoir
>
> Formations intégrales en accès libre sur YouTube

À quand un passage sur PeerTube ? Il faut peut-être en parler à Framasoft ?

[Wiki du sol vivant](https://wiki.solvivant.org/index.php/Wiki_Sol_Vivant)

---


## Botanique & potion

[_Zanthoxylum piperitum_](https://fr.wikipedia.org/wiki/Zanthoxylum_piperitum),
poivrier du Sichuan. Rutacae Feuille et graine en consommation. Épines qui se
lignifient ! :D

---

[Oxymel, une boisson à préparer !](http://www.lesjardinsdalice.ch/2020/09/oxymel-preparez-le-maintenant.html)

---

Découverte du [verger citoyen à Hennebont](http://www.verger-citoyen.fr/) via l'[annonce de la 3eme fête du verger citoyen](https://www.hennebont.bzh/evenement/3eme-fete-du-verger-citoyen/)

Je rate la fête ce week-end. Tant pis, au moins j'ai découvert leur existence.

---

## Programmation


À tester ?

> Quickstrom is a new autonomous testing tool for the web. It can find problems
> in any type of web application that renders to the DOM. Quickstrom
> automatically explores your application and presents minimal failing
> examples. Focus your effort on understanding and specifying your system, and
> Quickstrom can test it for you.

[docs.quickstrom.io](https://docs.quickstrom.io/)

---

> Donc si, de nos jours, on veut lire un article sur ce blog, il faut soit
> accepter de se balader à poil et d’offrir ses données privées à plein de gens
> (voir ci-dessus), soit passer par une gymnastique amusante, par exemple :

[nota-bene.org/je-suis-un-vieux-con](https://nota-bene.org/Je-suis-un-vieux-con)


---

> Les impacts du numérique
> 
> Les conséquences d’une conception numérique sont multiples :
> 
> - Les problèmes d’accessibilité, qui ne permettent pas aux personnes en
>   situation de handicap, incluant les personnes sourdes et malentendantes
>   d’avoir un accès à de nombreux services numériques.
> - Les problèmes éthiques, comme la récupération ou l’utilisation des données
>   personnelles sans en avertir les utilisateurs.
> - Les problèmes de sécurité : de nouvelles failles sont découvertes tous les
>   jours et de nombreux développeurs négligent la protection des données. Il
>   est impératif de protéger son application, nos données et surtout celles de
>   nos utilisateurs.
> 

[iso.org/standard/33020.html](https://www.iso.org/fr/standard/33020.html)


> Une fois la liste des fonctionnalités clairement identifiées, qu’un designer
> est passé par là pour gérer l’UX en prenant en compte un parcours utilisateur
> simple et accessible, dépourvu de “Dark Pattern” et de défilement infini,
> nous pouvons choisir l’hébergeur qui va accueillir notre site !

> Mais il ne faut pas perdre de vue qu’en général, le plus gros impact que vous
> pouvez atténuer est côté client, vu qu’une politique de mise en cache côté
> serveur permet de réduire drastiquement la génération du code. C’est le
> client qui téléchargera les données, effectuera des centaines de requêtes, et
> sera forcé de mettre à jour son navigateur ou même son ordinateur si jamais
> il sent que tout devient plus « lent », vu que vous n’avez pas rendu votre
> application assez performante en pensant aux plus anciennes contraintes
> matérielles.

> Réduire sa dépendance aux modules externes est une bonne astuce au niveau
> éco-conceptuel, mais aussi au niveau de la qualité logicielle

> garder la plus grande compatibilité possible avec les anciens périphériques

[new.infomaniak.com/web-ecologique](https://news.infomaniak.com/web-ecologique)

---

J'ai changer le nom du [rookie club](https://rookieclub.org/) pour [les pieds
dans le code](https://lespiedsdanslecode.org/) J'avais le nom de domaine, et
très envie de faire un truc avec.

- rapatrier les informations du [Discourse](https://www.discourse.org/) dans un site statique (ou un dépôt git ?)
- commencer à imaginer un parcours avec des éléments à chaque étapes (liens vers
  des exercices, vers des outils d'auto-évaluation)
- inviter sur le [Discord](https://discord.com/)
- fermer la mailling list
- annoncé à /ut7 que j'ai plus besoin du serveur, ni du nom de domaine :)

Nouveau groupe sur github [github.com/lespiedsdanslecode](https://github.com/lespiedsdanslecode)

J'aimerais bien faire un point avec Rémy à propos de tout ça.

---


## Découvertes

Une vidéo dont j'ai entendu parlé à Brangoulo : [entretient avec Isabelle
Padovani](https://la-ferme-des-enfants.com/demasquer/). Très intéressante
Isabelle Padovani. Ça laisse présager une bonne approche à Brangoulo :)

---

Durant mon premier _lundivisio_, j'ai également entendu parlé de [Forum Zegg](https://www.zegg-forum.org/en/). J'ai trouvé d'autres référence sur [flowlandmontreal/forum-de-zegg](https://www.flowlandmontreal.com/fr/evenements/2019/9/25/forum-de-zegg) et [agapae.fr/forum-de-zegg](http://agapae.fr/forum-de-zegg/).

Il a été évoqué les [cercles restauratifs](https://www.cerclesrestauratifs.org/wiki/Pr%c3%a9sentation_des_Cercles_Restauratifs) avec d'autres références aussi sur [matransformationintérieure.fr/cercles-restauratifs-interieurs](https://matransformationintérieure.fr/cercles-restauratifs-interieurs/) et [apprendreaeduquer.fr/cercle-restauratif-a-lecole](https://apprendreaeduquer.fr/cercle-restauratif-a-lecole/).

---

Après [DataDock](https://www.data-dock.fr/), voilà [QualiOpi](https://travail-emploi.gouv.fr/formation-professionnelle/acteurs-cadre-et-qualite-de-la-formation-professionnelle/qualiopi). [Souad](https://twitter.com/SouadSoR) semble trouver ça intéressant. À regarder
donc

---

> Le code social est un concept développé initialement par la SAS
> [ChezNous](https://cheznous.coop/). C’est un texte qui décrit les fondements
> de son action. Dans sa version initiale, il est constitué de 6 chapitres
> principaux qui incluent un certain nombre de valeurs et de procédures dans un
> même document, reliées de manière cohérente à une pratique (actions). 

[Une définition du code social](https://contributivecommons.org/le-code-social-une-definition/)

> La communauté étant évolutive, le code social est naturellement en constante
> transformation selon un processus décrit dans le code social lui-même, de
> manière à maintenir une cohérence et un dialogue constant entre les textes et
> les actes.

[Exemple de code social](https://contributivecommons.org/le-code-social-des-exemples/)

C'est beau. J'ai envie de faire pareil, j'ai envie de participer !

---

Le [site personnel de Nils](https://ichimnetz.com/) est vraiment très original.
Il y fait un usage intéressant un type range (c'est bien une des premières fois
que je vois cet input HTML utilisé de façon intéressante, voir utilisé tout court) !

J'aime bien ce genre de site.

La dimension pédagogique de la manipulation du CSS est fabuleuse. Préciser le
nombre de lignes du CSS est vraiment top. Ça permet d'être très clair sur ce
qu'il se passe.

