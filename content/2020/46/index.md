---
title: Semaine 46
---

_Du dimanche 16 au samedi 21 novembre_

## Lundi 16 novembre

[Méthode Agile: la justice rappelle l'importance du cahier des charges](https://www.lemondeinformatique.fr/actualites/lire-methode-agile-la-justice-rappelle-l-importance-du-cahier-des-charges-80976.html). Tiens, je ne suis pas surpris...

---

[Bienvenue sur le site de la distribution éducative PrimTux, destinée aux élèves du primaire](https://primtux.fr/)

À tester pour les enfants ?

---

Utiliser TCR (Test Commit Revert) est plus simple si j'ai un [git](https://git-scm.com/) propre. Je pense à la commande `git status` qui, dans mon cas affiche souvent des fichiers que je garde temporairement, le temps de bricoler.

La commande pour moi serait un truc comme `bundle exec rspec spec/services/rdv_updater_spec.rb && git add -A && git commit`. Ça me permettrais d'écrire le pas franchi.

Sans doute devrais-je _squasher_ avant de pousser sur le dépôt distant.

[My opinion about test && commit || revert with Kent Beck](http://testingandlearning.home.blog/2019/01/30/my-opinion-about-test-commit-revert-with-kent-beck/)


Testé un peu [TCR](https://medium.com/@kentbeck_7670/test-commit-revert-870bbd756864).

C'est violant de supprimer le code écrit, et en même temps il y a une vertu à ne pas s'attacher au code, et surtout, l'obligation de ne pas en écrire beaucoup entre deux tests.

Il y a cependant une nouvelle inversion : il faut réfléchir un peu plus avant. C'est peut-être mieux, plus naturel.

J'utilise la commande `(bundle exec rspec spec/services/rdv_updater_spec.rb && git add -A && git commit) || git checkout .`

Et je découvre qu'en fait, certaines personnes font le reset uniquement sur le code source !

> For TCR, we need a specialization of TDD. Where we never come to the Red state or are at least as short as possible
> - We start Green (do: `test && commit || reset`)
> - Write a test (do: `test && commit || reset src`)
> - Fake the implementation (do: `test && commit || reset src`)
> - Refactor, where you replace n Fakes with a real implementation (do: `test && commit || reset src`)

Assez surprenant je trouve, mais ça évite sans doute de tricher comme je le faisait en gardant Vim ouvert, et en écrasant la suppression via le tampon de vim...

[TCR (test && commit || revert). How to use? Alternative to TDD?](https://medium.com/@tdeniffel/tcr-test-commit-revert-a-test-alternative-to-tdd-6e6b03c22bec)

---

Discussion passionnante autour du fait de raller sur une techno, un langage, un style de musique ou autre. Se rappeler que certaines personnes apprécie, utilise ces _choses_ là, et ça peut leur faire mal. Il y a des techno, langages, musique plus accessible que d'autres en fonction de l'histoire de chaque personne

J'en retient que je ne prends pas assez soin des autres, particulièrement dans les discussions sur l'informatique.

J'emporte également une réflexion sur une documentation de ce qui est, en informatique, dangereux à encourager ou pas. Je m'explique : utiliser [Github](https://github.com/), me semble moins aliénant que de se mettre à MacOs. Ça me semble assez facile de changer de github pour autre chose, sans doute lié à la technologie sous-jacente, alors que sortir de MacOS semble beaucoup plus dur (habitude ? Logiciel spécifique?).

Est-ce qu'il existe déjà une sorte de documentation sur le sujet ? J'y vois des problématiques d'interopérabilités. Est-ce que ça rentrerais dans un cadre de [BlueHat](https://www.numerique.gouv.fr/actualites/la-communaute-blue-hats-hackers-dinteret-general-est-lancee-rejoignez-nous/) ? De [Framasoft](https://framasoft.org/) ? À voir.

---

De l'art de chercher d'abord localement une réponse à sa question, plutôt que de tout de suite aller sur internet...

La commande [`grep`](http://fr.manpages.org/grep) est un outil que je connais mal, mais que j'utilise beaucoup. Dans ce cadre, il me permet c'est d'explorer le code existant. C'est une source d'inspiration, de documentation à porté de main, accessible, et très lié au contexte.

_Mon disque dur est une super encyclopédie du code, à condition que les logiciels soient libre_

---

Rencontre avec une éleveuse de champignon sur la presqu'île de Crozon. Très intéressant. Le BPREA reviens encore sur la table. C'est l'outil indispensable pour acquérir de la terre agricole parait-il...

- [le site des CFA et CFPPA du réseau des établissements publics agricoles en Bretagne](https://formation-agricole-bretagne.fr/)
- [BPREA Maraîchage biologique](https://www.kerliver.com/index.php/bprea-maraichage-biologique)
- [BPREA, formation sur formation.gref-bretagne.com](https://www.formation.gref-bretagne.com/formation/responsable-dentreprise-agricole/1502085F)
- [BPREA en FORMATION A DISTANCE, sur le site formation-guingamp.fr](https://www.formation-guingamp.fr/index.php/12-formations-longues/86-formation-longues)
- [BP REA à distance : Brevet Professionnel Responsable d’Entreprise Agricole sur le groupe ESA](https://www.groupe-esa.com/formation/bp-rea-distance/)

Est-ce que j'arriverais à suivre une formation en plus du code ?
Est-ce que j'arriverais à suivre une formation à distance ?
Est-ce qu'il y a une partie (gestion d'entreprise) sur laquelle je pourrais faire valoir des acquis pro ?

D'autre formation intéressante en plante aromatique et herboristerie :
- [Formation en herboristerie sur pole emploi](https://labonneformation.pole-emploi.fr/toutes-les-formations?motscles=herboristerie)
- [Formation en aromathérapie sur pole emploi](https://labonneformation.pole-emploi.fr/toutes-les-formations?motscles=aromatherapie)
- [Formation en plantes sur pole emploi](https://labonneformation.pole-emploi.fr/toutes-les-formations?motscles=plantes)

[Calendrier de formation des de l'école bretonne d'herboristerie](http://www.capsante.net/wordpress/?page_id=357)

---

[Minga](https://fr.wikipedia.org/wiki/Minga_(tradition_sud-am%C3%A9ricaine))

> De manière générale, une minga est une action, éventuellement festive et souvent conviviale, de travail collectif à des fins sociales. Le mot dans cette acception a une origine sud américaine.

---

Toutes les plantes utilisé pour la teinture porte une espèce en **[Genre] tinctorum**

[_Rubia tinctorum_](https://fr.wikipedia.org/wiki/Garance_des_teinturiers) couleur rouge/orangé

---

[The Mursalski](https://mursalski-the.com/), [la tisane des montagnes de Bulgarie](https://www.courrierdesbalkans.fr/The-Mursalski-la-tisane-des-montagnes-de-Bulgarie), un village qui s'appelle [Trigrad](https://fr.wikipedia.org/wiki/Trigrad).

Via une émission du [Génie des plantes](https://www.arte.tv/fr/videos/052377-002-A/le-genie-des-plantes/)

---

> Pendant la révolution, la devise, c'est liberté, égalité, fraternité. Mon idée c'est de changer l'ordre : je commencerais par la fraternité, ensuite, liberté, et ensuite on aura l'égalité. C'est la conséquence l'égalité.

Les milles chemins, extrait de « Chemin faisant » de [Eileen Caddy](https://www.souffledor.fr/auteur/16-eileen-caddy)

[Histoire d'accueil inconditionnel et de fraternité. Kerfland 1/2](https://www.youtube.com/watch?v=yCFKUCqiDVE)

Vidéo par Les Petits Nuages

> Si des paysans/maraichers bio souhaitent en savoir plus sur le projet de relance de la ferme, ils peuvent nous contacter sur notre page facebook, ou par mail : lespetitsnuages.contact@gmail.com

Ce Bernard me fait tellement penser au Bernard qui m'a fait découvrir la taille des arbres, la greffes, l'amour du jardin...

---

À lire ? [LA PETITE COMMUNISTE QUI NE SOURIAIT JAMAIS](https://www.la-petroleuse.com/romans-litterature/4201-la-petite-communiste-qui-ne-souriait-jamais.html)
À lire ? [LES BOULETS ROUGES DE LA COMMUNES](https://www.la-petroleuse.com/romans-litterature/1387-les-boulets-rouges-de-la-communes.html)
À lire ? [L’INSURRECTION DE LA VIE QUOTIDIENNE](https://www.la-petroleuse.com/livres-situationnisme/4767-livre-l-insurrection-de-la-vie-quotidienne-vaneigem.html)
À lire ? [OFF THE MAP](https://www.la-petroleuse.com/livres-anarchisme/4768-livre-off-the-map.html)
À lire ? [Éric Sadin](https://fr.wikipedia.org/wiki/%C3%89ric_Sadin) un écrivain et philosophe français, principalement connu pour ses écrits technocritiques
À lire ? The weather machine [Fiche de lecture : The weather machine](https://www.bortzmeyer.org/weather-machine.html)

---

[Les espaces-tests agricoles : expérimenter l’agriculture avant de s’installer - Analyse n°92](https://agriculture.gouv.fr/les-espaces-tests-agricoles-experimenter-lagriculture-avant-de-sinstaller-analyse-ndeg92)
[Les espaces-tests agricoles : expérimenter l'agriculture avant de s'installer](https://agriculture.gouv.fr/sites/minagri/files/cep_analyse_92_espaces_tests_agricoles-3.pdf)

Il n'y en a pas beaucoup en Bretagne...

Accompagnement entre une personne morale (entreprise, association, coopérative, collectivité) et une personne physique, non salariée à temps complet. La structure d’accompagnement s’engage à mettre à disposition de la person Comme en CAE ... Oh wait ! :D (ben oui, c'est la même chose, à ceci prêt que les espaces tests agricoles sont sur des durées délimitées :) et que ce ne sont pas souvent des coopératives.

Des exceptions :
- [CAE Rhizome](https://www.cae-rhizome.com)
- [Les champs des possibles](https://www.leschampsdespossibles.fr/)

---

> À appliquer le « Principe de Postel » : « Soyez tolérant dans ce que vous acceptez, et pointilleux dans ce que vous envoyez ». C’est une règle employée par les gens du W3C depuis des années pour communiquer. Elle est très pratique dans un environnement tout-distant et composé de gens de cultures très différentes.

[Le Principe de Postel appliqué aux conversations](https://nota-bene.org/Le-Principe-de-Postel-applique-aux-conversations)

---

Échange autour de tumblr

> Je ne suis pas sur de savoir ce que Tumblr a de différent qu'un moteur de blog ? Qu'est-ce qui est particulier à Tumblr que nous pourrions essayer de reprendre ?
>
> (je dis ça parce que ça m'amusais pas mal aussi à un moment, et je n'arrive plus à savoir ce qui faisait la diff)
>
> -- @joachim

> @yaf @joachim mhhh bonne question... Tumblr est un peu intermédiaire entre un blog classique (posts au format assez libre, avec des metas comme les tags) et du microblog (avec son système de reblog assez similaire à un retweet).
>
> Ce qui le différencie des deux, c'est les divers formats de posts ("text", "link", "video", ...) et que tous les commentaires ajoutés à un reblog s'affichent (contrairement au quote tweet qui n'affiche que la dernière ref)
>
> -- @yhancik@octodon.social

Des pistes pour remplacer Tumblr en libre ?
- [Zotlabs](https://zotlabs.com/)
- [Hubzilla](https://the-federation.info/hubzilla)

---

> pour que ces bactéries vous aident à lutter contre celles pathogènes, il faut leur fournir un environnement favorable. Les prébiotiques sont la « nourriture » des bonnes bactéries (ou probiotiques). Ils se trouvent essentiellement sous forme de fibres non digestibles. Les aliments riches en prébiotiques sont : l'ail, l'artichaut, l'asperge, la chicorée, le poireau, l'oignon... Les aliments les plus riches en probiotiques : yaourts et laits fermentés, choucroute, miso, légumes lacto-fermentés.

[Soutenir son système immunitaire](http://kristellcorre-sante-naturelle.fr/blog/soutenir-son-systeme-immunitaire)

