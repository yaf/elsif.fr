---
title: dimanche 27 décembre 2020
---

Et si [les pieds dans la code](https://lespiedsdanslecode.org/) c'était pour construire une série d'outils pour apprendre à programmer (lecture de code, analyse de pattern, ...) et que je gardais le nom « rookie club » pour le club des personnes qui se retrouve pour apprendre la programmation ensemble ?

Je pourrais libérer le serveur de /ut7 et leur demander un transfert du nom de domaine...

Laissons quelques temps passer là dessus avant de prendre une décision. De toute façon, j'imagine que c'est un peu les vacances chez /ut7 :)

---

À lire ? [Vivre sans ? institutions, police, travail, argent...](https://www.lalibrairie.com/livres/vivre-sans----institutions-police-travail-argent-----conversation-avec-felix-boggio-ewanje-epee_0-5447895_9782358721714.html) Vivre sans ? de Frederic Lordon

---

Note de lecture [Penser par soi-même](https://www.lelabo-ess.org/penser-par-soi-meme-guide-de-resistance-par.html) de Harald Welzer

> Dans son livre _Effondrement_, Jared Diamond montrait à quoi est dû l'échec historique de sociétés telles que celles des Mayas, des Vikings du Goenland ou des Pascuans. De tels échecs avaient une caractéristique commune qui réside dans le fait qu'au moment où l'on a pris conscience que les conditions de survie devenaient précaires, on a commencé à  mettre en place toutes les stratégies d'intensification qui avaient fonctionné _jusqu'à là_.

> On procédait d'après l'expérience, mais celle-ci n'est d'aucune aide quand les conditions ont changé. L'expérience devient alors un piège. De nouvelles conditions de survie appellent de nouvelles stratégies de survie.

> L'objectif de ce livre est de remédier à cette vision en tunnel. Son titre « Penser par soi-même » fait naturellement référence au programme de Kant de « la sortie de l'homme hors de l'état de minorité dont il est lui même responsable » ; pour cela, l'homme doit penser par lui-même.

> Si l'on demande où les systèmes totalitaires ont puisé les forces qui leur ont permis d'organiser le monde, au moins pour un temps, selon leur idéologie, c'était avant tout dans la destruction de relations sociales établies.

> Il se pourrait que le totalitarisme d'aujourd'hui se manifeste justement sous les traits de la liberté : pouvoir, à tout instant, avoir et être tout ce que l'on croit vouloir avoir et être. Il n'existe qu'un seul système de régulation qui limite cette liberté : le marché. Car contrairement à ce que présente 1984, on n'a plus besoin d'une instance de surveillance qui contrôle et intervient sur les désirs et les mouvements des individus quand ils deviennent dangereux. Nul besoin de Gestapo ni de Tchéka ; à l'ère de Google et de Facebook, chaque internaute livre délibérément toutes les information s nécessaire sur lui, sans que personne ne l'y force.

> Aujourd'hui, nous n'avons pas encore affaire à la destruction du contexte social, mais le potentiel est énorme. Et si ce diagnostic se révèle juste, alors la résistance est une nécessité absolue. Il faut s'opposer à la destruction des bases de notre subsistance future, à l'extractivisme, et, surtout, à l'appropriation du tissu social. Résister à l'abandon volontaire de la liberté. Résister à la bêtise. Résister à la tentation de dire tout simplement : « CE n'est pas bien grave, ça ne dépend pas de moi. »

> Tandis que les familles moyennes économisaient autrefois pendant longtemps avant de pouvoir s'offrir une nouvelle armoire, qu'il faisaient faire ou qu'ils achetaient chez un marchand de meubles, il s'agit à présent d'articles à emporter et à jeter. D'un point de vue écologique, ces pseudo-meubles à durée de vie courte ne sont pas une catastrophe uniquement parce qu'on les jette après usager : pour les fabriquer, on utilise beaucoup plus d'énergie, de matériaux et de transports que pour n'importe quelle armoire menuisée.

> On a utilisé au cours du seul XXe siècle plus d'énergie qu'au cours de toute l'histoire de l'humanité. Pendant ce même laps de temps, l'économie est devenue quatorze fois plus importante et on a multiplié la production industrielle par quarante.

> La différenciation fonctionnelle des sociétés fondées sur la division du travail a fait émerger un type d'individu très adaptable, capable de se conformer facilement à des exigences de rôles changeantes et souvent très contradictoires, que ce soit dans le cadre de son travail, de sa famille, d'un club, de ses relations amicales, etc.

_après avoir évoqué des soucis sociaux et une histoire de circuit de formule 1 au Bahreim_
> Le Bahreim est un microscope. Il présente la mécanique des relations sociales fondamentales à l'ère de la mondialisation : un consumérisme totalitaire dénué de tout besoin naturel, dont les instigateurs sont capables d"'influencer des économies ou des États entiers et poussent les gouvernements à organiser ou soutenir l'empêchement ou la répression de contre-pouvoirs.

> Les contestataires sont des consommateurs en puissance, et c'est pourquoi aucun acteur de l'économie n'a fondamentalement quoi que ce soit à leur reprocher.

> Cela fait longtemps que les conflits idéologiques n'ont plus d'objet, ce qui explique que ces indignées ne sont pas des opposants, et n'ont pas besoin d'être combattus. Il n'y a aucun motif d'hostilité.

> L'esthétique de la fragilité a été un opposant chétif à la conception consumériste de la liberté : c'est lorsque le bloc de l'Est s'est effondré de manière si peu spectaculaire, comme si l'histoire n'avait fait que lâcher du vent, que la mondialisation a vraiment démarré sous forme de l'universalisation de l'économie de croissante capitaliste. Et jusqu'à maintenant, elle entraîne une surexploitation des ressources si terrible qu'il semble évident que, dans deux ou trois décennies, elle aura détruit ses propres conditions de fonctionnement. Une telle économie est au plus haut point non économe, car pour se maintenir en vie, elle a besoin de toujours plus de matériaux pour produire toujours plus de produits toujours plus chers pour toujours plus de gens avec toujours plus de besoins.

> Mais l'histoire nous enseigne que la vérité est une question de consensus social et que les hommes croient encore aux choses les plus absurdes à partir du moment où tout le monde y croit.

> La seule ressource qu'il reste pour produire de la valeur ajoutée à l'échelle mondiale, c'est l'avenir. La culture du «toujours de tout » exploite l'avenir de ceux qui auront la malchance de naître après vous.

> C'est ainsi que les concepts d'une croissance fondamentalement illimitée et de l'importance de l'énergie, nés de la révolution industrielle, se sont aussi traduits dans notre conception de nous-mêmes.
> On peut appeler cela « l'industrialisation profonde » : tout comme les sites de production, les routes, les centrales, les centres commerciaux, l'alimentation électrique, etc. structurent notre monde extérieur, les concepts de l'expansion illimitée déterminent notre vie intérieur.

> L'industrialisation profonde implique également une modification de la perception du temps. La mesure industrielle a créé un rythme régulier entre temps de travail et de repose, indépendant des saisons et des cycles agricoles. Au XXe siècle, les moyens de locomotion à vapeur provoquèrent une industrialisation du temps et de l'espace - une accélération constante du déplacement dans l'espace, une augmentation permanente de la mobilité et de la vitesse qui se poursuit encore aujourd'hui.

> L'invention de l'école en tant qu'institution d'éducation et de formation pour tous les membres d'une société représente également un développement des premiers pays industrialisés dans lequel la fonction éducatrice et disciplinaire était mise en avant au même titre que la transmission du savoir. On y enseignait chaque vertu qui, comme la ponctualité, la propreté, le soin, l'ordre, etc. était imprégnée d'un caractère social exploitable dans les sociétés basées sur la division du travail,, c'est-à-dire qui soit compatible en toutes circonstances à une cadence imposée.
> Il ne faut également pas sous-estimer un des effets de la scolarisation qui résidait dans la reproduction des rapports de concurrence et de compétitivité, ainsi que la mesure des performances individuelles par les systèmes de notation. Et tout cela survit encore aujourd'hui : non seulement les taux de scolarisation et d'alphabétisation sont devenus l'indicateur principal du «développement », mais la déstructuration de tous les aspects de l'apprentissage et de la formation par des critères de performance mesurables sont depuis Bologne et le G8, plus que jamais en vigueur. Aujourd'hui, les écoliers, écolières, étudiants et étudiantes ne peuvent même plus imaginer qu'il puisse exister des modes de formation sans objectif et sans évaluation, ni des carrières sans compétition ni points de performance.

> La révolution industrielle, la division du travail, la pédagogie, l'individualisation et la biographisation, l'universalisation de la notion d'énergie... Tout cela contribue finalement à transformer de façon extraordinaire le substantiel en une simple succession d'étapes : chaque processus de fabrication n'est que le précédent du suivant, chaque produit n'est qu'un premier avant un deuxième, chaque phase de fabrication n'est qu'une étape d'une interminable chaîne de répétition. On n'atteint jamais d'objectif, mais l'argent est multipliable à l'infini et le potentiel de productivité illimité.

> Il ne s'agit pas simplement d'un problème cognitif que l'on pourrait résoudre par l'explication et la raison, mais de l'inertie de l'histoire et de l'environnement.

> C'est précisément là que la raison se heurte à ses propres limites, car elle n'a accès qu'à la partie cognitive de notre capacité d'orientation. L'autre partie, bien plus vaste, qui s'articule autour de routines d'interprétations et de références inconscientes - sociologiquement parlant : l'habitus - en est parfaitement protégée.

> Tant que nous ne pourrons pas enclencher quelque chose contre cette histoire, nous ferons toujours partie d'elle.

> Le fait que le problème provienne de l'acheteur du carburant fossile, et non du producteur, qui espère que la matière première qui représente son fonds de commerce dure encore longtemps, s'efface dans la routine de la gestion de crise. Cela se produit selon un schéma répétitif dans lequel les dégâts _visibles_ estompés par l'intervention d'aides professionnelles comme bénévoles et, enfin, bien que le scandale d'après et celui d'encore après aient déjà éclaté, l'affaire débattue dans des bataille juridiques sur la responsabilité et le dédommagement.

> Changer pour un autre fournisseur n'est donc pas une stratégie ; la vrai stratégie réside uniquement dans le renoncement au tout dernier téléphone portable. Mais pour la très grande majorité des consommateurs et consommatrices, ce renoncement est hors de question, ce qui montre bien que la moralisation et la politisation du produit et de ses fabricantes sont limitées au scandale et n'altèrent le comportement de consommation que sur le court terme.

> La concurrence règne aussi dans l'économie de l'attention, même parmi les organisations à qui on délègue la protestation (Green Peace, Food Watch, etc.), et certaines protestent tout simplement mieux que d'autres.

> La théorie selon laquelle la régulation des marchés, et donc probablement leur orientation vers davantage de durabilité, dépendrait du consommateur se révèle alors logiquement erronée. Dans la mesure où le citoyen consommateur _doit_, dans l'exercice même de son pouvoir stratégique, se soumettre aux lois du marché, il ne pourra jamais occuper une position d'organisation. C'est la raison pour laquelle il devrait plutôt être citoyen politique et fixer les règles du marché.

> On voit alors que le consumérisme a le potentiel d'absorber toute contestation - mais le capitalisme en a toujours été capable, et la seule chose qui m'étonne est qu'on l'ait en grande partie oublié jusqu'à aujourd'hui. À titre d'exemple : le très déterminé mouvement anarchiste punk et son groupe phare, les Sex Pistols, ont dû faire, il y a quelques dizaines d'années, l'expérience déterminante de l'immense adaptabilité de l'appropriation capitaliste lorsqu'ils ont constaté l'épatante vitesse à laquelle leur symbole a été transformé en accessoires de mode estampillés Gucci ou Versace, rendu commercialisable et donc tout à fait dénué de critique.

> Plus la balance penche en défaveur du demandeur, plus le prestataire peut se réjouir de ma situation pour son commerce. C'est pourquoi la faim est aussi bonne pour les affaires.

> Un mouvement social ne peut gagner que s'il rassemble des représentants de tous les groupes sociaux, même si à l'origine ils n'avaient pas d'intérêt crucial pour les revendications particulières de l'initiateur. En d'autres termes : le mouvement pour les droits civiques aux États-Unis a gagné à l'instant où tous les groupes sociaux se sont approprié ses revendications ; l'abolition de l'esclavage devient possible à partir du moment où elle devient l'affaire de ceux qui ne sont pas esclaves eux-mêmes.

> La sensation de détenir un pouvoir, renforcée par l'appartenance à une communauté, et l'expérience de l'insubordination à l'autorité permettent de prendre conscience que _tout peut être autrement_ quand on s'en donne les moyens. Une telle expérience est un poussant bastion contre l'indifférence. En psychologie, on appel cela l'auto-efficacité.

> De cet éloignement de l'utopie découle un manque de réflexivité total : si je ne vois que les dégâts causés par les entreprises de pêches ou chimiques, j'oublie très vite que les poissons qu'on capture et les savons qu'on produit sont destinés à un marché dont je fais partie. Cette vision des choses a l'avantage de me permettre d'identifier les problèmes _là où je ne me trouve pas_, ce qui m'autorise, après avoir démonté les anomalies, à poser très dignement des exigences qui ne remettent pas ma propre position en cause.

> Une fois de plus, la foi en la technique se révèle être une partie du problème qu'elle prétend résoudre. Et une fois de plus, cela ne vient pas à l'encontre d'une utilisation sensée de la technique, assurément nécessaire à une modernité durable, mais cela montre que le sens et l'utilisation de la technique dépendent de la culture au sein de laquelle on y recourt.

> On ne peut pas mettre fin à l'extactivisme avec des accords internationaux, pas même avec la géo-ingénierie ou l'ouverture d'un nouveau marché : on ne peut le combattre qu'en réduisant la consommation. Comme il s'agit d'une pratique sociale, elle ne pourra être remplacée que par une autre pratique sociale. Et c'est précisément là que le problème devient politique.

> C'est pourquoi le travail dans ces organisations (celles qui doivent apprendre à gérer l'inattendu) consiste principalement à faire en sorte que ces événements _ne se produisent pas_. C'est aussi pur cela que toute une série de phénomènes auxquels on accorde de la valeur dans d'autres organisations deviennent ici problématiques : toute forme de routine est un problème, car elle réduit la sensibilité aux difficultés qui se présentent. L'expérience est contre-productive car elle induit l'anticipation d'un événement par assimilation à quelque chose qui s'est déjà produit et qu'on va donc considérer et traiter comme d'habitude.

> L'expérience en soi n'est pas une source de compétences car les hommes répètent très souvent les mêmes expériences sans véritablement essayer de faire évoluer ces répétitions en nouveautés. -- Karl Weick

> L'expérience peut devenir un piège si l'on rencontre une situation qui ressemble à un événement vécu, mais dont la nature est en réalité tout à fait différente.

> L'expérience est donc utile si elle est appliquée à des événements similaires à ceux dont on a déjà fait l'expérience. La plupart du temps elle se révèle en revanche trompeuse si l'on souhaite anticiper avec précision des événements sans précédent.

> Pour gérer l'imprévu, il faut avant tout développer des capteurs qui signalent l'arrivée d'un événement qui dépasserait immédiatement les mesures routinières. Cela veut dire qu'il faut se méfier de l'expérience et toujours considérer ce qui arrive d'un œil nouveau.

> Il s'agit également de ne pas gérer l'inattendu en ayant recours à des recettes éprouvées, mais de rassembler le plus rapidement possible les compétences les plus variées qui pourraient participer à l'analyse et à la description d'un problème. Car bien souvent il s'agit simplement de savoir à quel problème on est confronté.


> tout comme l'expérience peut s'avérer un désavantage et les plans problématiques, les erreurs ne sont plus mauvaises mais deviennent des sources d'information cruciales.

> Alors qu'on tente habituellement d'éviter l'erreur et, une fois qu'elle s'est produite, de la dissimuler, on va considérer ici qu'elle a beaucoup de valeur : les employés des organisations à haute fiabilité qui pointent des erreurs ne se voient plus mis au placard, mais distingués.

> La précaution est fondée sur le constat que le savoir et l'ignorance se développent ensemble. Lorsque l'un grandit, l'autre grandit également. Les personnes précautionneuses acceptent leur propre ignorance et s'efforcent d'en combler les lacunes, car elles savent bien que chaque nouvelle réponse soulève une multitude de questions. Le pouvoir d'une orientation précautionneuse réside en ce qu'elle fait passer son attention de l'attendu à l'insignifiant, de l'information confirmée à la preuve du contraire, de l'agréable au désagréable, de la certitude à l'inconnu, de l'explicite à l'implicite, du factuel au probable, du consensus à l'opposition. Précaution et actualisation effacent les angles morts qui se développent dans la perception, car les hommes ont trop confiance en leurs attentes -- Karl Wieck & Kathleen Sutcliffe


J'apprécie l'histoire, un peu longue à retranscrire ici, d'un hypothétique site de vente en ligne de matériel de bricolage qui, au lieu d'essayer de vous vendre la perceuse plus au moins 3 ou 4 autres produits se retrouve à vous faire louer une perceuse à un voisin :)

_Une histoire courte à publier à part ?_

> Un art de vivre pour bientôt

> Votre perceuse à percussion est cassée. Vous allumez votre ordinateur et cliquez sur otto.de pour voir les offres du moment. À votre grande surprise, pas un produit ne s'affiche après que vous avez tapé « perceuse » dans le moteur de recherche. Au lieu de cela, on vous demande : « Pourquoi voulez-vous acheter une nouvelle perceuse ? » Étonné, vous répondez : « Parce que la mienne ne fonctionne plus. » Question suivante : « Quel est le problème ? Il est sûrement possible de la réparer. » Quand vous répondez que l'appareil ne marche tout simplement plus du tout, otto.de vous indique une liste d'adresses : « Nous vous recommandons les mécaniciens suivants qui se trouvent dans les environs et travaillent en partenariat avec Otto. Souhaitez-vous que nous prenions contact avec un réparateur ? » Vous répondez : « Non ! Je voudrais voir les produits. » Sur ce, voilà enfin a liste des perceuses disponibles, comme vous l'aviez demandé dès le début.
> Celle qui semble la meilleure est un tout nouveau modèle de chez Bosch, un perforateur avec bonne force de percussion, avec les plus basses classes acoustique et énergétique. 319 euros. Bon, on n'achète pas ce genre de choses tous les jours. On la prend ? On la prend. Mais au lieu d'envoyer l'outil dans votre panier après avoir cliqué, otto.de vous demande à nouveau : « Combien de fois par an en moyenne avez-vous besoin d'une perceuse ? » Vous réfléchissez, c'est une bonne question. Eh bien, quatre ou cinq fois, peut-être. Doucement, vous vous laissez intriguer par la suite. Otto vous fait savoir que « nos conseillers et conseillères estiment qu'il n'est pas intéressant d'acheter un tel outil pour l'utilisation que vous en faites. Quelqu'un dans votre voisinage a récemment acheté une machine similaire et s'est inscrit sur la liste des prêteurs. Vous pouvez lui emprunter sa perceuse ». De mieux en mieux, pensez-vous. Mais qui prête donc sa machine ? « Souhaitez-vous choisir cette option ? Souhaitez-vous obtenir ses coordonées ? » Bien sûr que vous voulez ses coordonnées, par simple curiosité maintenant. Otto acture 3,95 euros pour la médiation, un prix honnête. Il propose également de venir chercher le produit défectueux gratuitement. Vous venez d'économiser 315,05 euros. Et on vous débarasse d'un objet encombrant. Sur votre écran s'affichent les coordonnées du prêteur. Näumann ! Je savais bien qu'il avait tout, pensez-vous. C'est parfait, cela fait un moment que je voulais papoter avec lui. Sa femme est si gentille. Vous cliquez sur « Finaliser ». L'écran affiche : « Merci d'avoir choisi Otto ! Les personnes qui n'ont pas acheté la perceuse que vous avez consultée n'ont pas non plus acheté les articles suivants : visseuse électrique Bosch PX 17, disqueuse Black & Decker WS 34/3, boîte à outils Konfix XL. »
> Elle est bien cette histoire, non ? Elle pourrait se passer dans un monde qui ne concentre plus son intelligence sur la multipication des produits, mais dans leur utilisation : l'intelligence sociale.

J'ai un petit doute sur « elle est gentile sa femme ». Soit il y a une forme d'humour, de dérision de notre société patriarcale, soit c'est déplacé et inutile. Ça n'enlève rien à l'intérêt du scénario heureusement.

> Cette capacité d'adaptation évolutive si pratique des hommes les empêches pourtant de se rendre compte du déclin : ils nont pas de point de référence qui leur permettrait de remarquer automatiquement que, _maintenant_, quelque chose a changé radicalement. Car la perception s'ajuste constamment pour en arriver à ce résultat qui se confirme de lui-même : pas de danger, rien n'a changé.

J'apprécie ce dernier tier du livre où l'on explore deux scénarios d'anticipation l'un positif, l'autre négatif... C'est intéressant de se projeter.

> Dans la mesure où le mode de vie humain est absolument coopératif, l'idée que l'homme se fait de l'économie, du behaviorisme, mais aussi de la théorie philosophique de l'altérité, est fausse. (...) Le cerveau humain est fait pour fonctionner en coopération, c'est un _organe relationnel_ bioculturel qui ne se développe qu'en interagissant avec d'autres individus. Alors que la plupart des êtres vivants non humains naissent avec un cerveau presque mature, avec des programmes comportementaux et des modèles de réaction prédéfinis qui leur permettent de survivre, les hommes viennent au monde avec un cerveau tout sauf complet. La maturation organique d'une seule région ou d'une seule fonction du cerveau dure chez l'homme jusqu'au début de l'âge adulte. Le développement de l'architecture de transmission neuronale se fait tout au long de la vie. Cela signifie que le développement du cerveau humain se fait toujours dans des circonstances culturelles. L'environnement dans lequel a lieu le développement et les capacités que les sociétés ont développées à un moment donné entrent dans l'organisation des connexions synaptiques d'un cerveau humain en développement. Les cerveaux humains ne se conjuguent pas au singulier, ils ne se forment qu'au sein d'un réseau composé d'autres cerveaux.

> L'image que nous avons actuellement de nous-mêmes est la conséquence d'un malentendu culturel : évidemment, une société fondée sur la division du travail, qui transforme ses membres en spécialistes super productifs, a besoin d'individus qui ont une image égocentrique, compétitive et individualiste d'eux-mêmes. Plus ces images deviennent individualistes dans le processus de la civilisation capitaliste, plus la culture s'axe sur la consommation, car consommation est distinction.

> Les communautés de pratique sont des groupes qui partagent un intérêt ou une passion pour quelque chose et qui apprennent ensemble à mieux la pratiquer. -- Définition du créateur du terme « communauté de pratique » Étienne Wenger
> Tous les groupes qui travaillent sur un exercice déterminé dans le cadre d'un processus d'apprentissage commun peuvent donc être désignes par le terme de communauté de pratique.

> Les communautés de pratique apportent à leurs membres un profonds sentiment positif d'_auto-efficacité_, un sentiment qu'on ne ressent que lorsque l'on a réussi à faire bouger quelque chose.

> Moins un groupe repose sur l'approvisionnement par autrui pour affronter les problèmes, les situations d'urgences ou les catastrophes, plus il est résilient, et plus il lui sera facile de développer de tels systèmes d'assistance et de soutien. Cela se traduit par l'exemple d'une coopérative rurale du Costa Rica qui compte parmi les prestations sociales qu'elle fournit aux agriculteurs un accès à Internet et à des logiciels gratuits, qui leur permettent de contrôler leurs stocks, de trouver des fournisseurs ou sous-traitants, ou leur évite des déplacements aux bureaux administratifs qui leur coûtent du temps de travail. Et tout cela sans que les agriculteurs disposent de connaissances formelles ou informatiques : « Quelqu'un qui travaille aux champs comprend très vite qu'il est bien plus intéressant de gérer lui-même ou de faire gérer par la communauté ses semailles et ses logiciels plutôt que par un tiers. L'idée de travailler avec des outils qui ne réduisent pas notre potentiel, qui sont gratuits et qui fonctionnent bien et rapidement même sur de vieux ordinateurs reste comme toujours une option séduisante »

> La philologue Adriana Sanchez, qui travaille pour la coopérative, ajoute la combinaison à première vue inhabituelle du simple travail de la terre et du développement de logiciels aux connaissances locales, ce qui contribue encore une fois à la résilience de la communauté. C'est également un bon exemple de la manière dont les formes de sociétés traditionnelles peuvent poursuivre leur développement grâce à des outils modernes. On notera que c'est la culture locale qui détermine l'usage de la technique, pas l'inverse.

> Le retour des coopératives est une étape importante vers une modernité durable car elles sont fondées non pas sur une utilisation privée des ressources, mais sur une utilisation commune.

> Les 12 principes d'une resistance réussie
> 1. Tout pourrait être autrement.
> 2. Il ne dépend que de vous de faire bouger les choses.
> 3. Alors prenez-vous au sérieux.
> 4. Arrêtez d'être d'accord.
> 5. Résistez à chaque fois que vous n'êtes pas d'accord.
> 6. Vous disposez d'une énorme marge de manœuvre.
> 7. Élargissez votre marge de manœuvre là où vous êtes et là où vous avez de l'influence.
> 8. Contractez des alliances.
> 9. Attendez-vous à essuyer des revers, surtout ceux qui viennent de vous.
> 10. Vous n'êtes pas responsable de la terre entière.
> 11. La façon dont vous résistez dépend de vos possibilités.
> 12. Et de ce que vous aimez faire.



