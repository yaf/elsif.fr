---
title: 19 décembre 2020
---


Découverte du [verger citoyen Pom d'amis à Stang Er Gat](https://www.optim-ism.fr/evenement/mission-benevole-au-verger-citoyen-4/). C'est vraiment chouette ! Quel belle association [Optim'ism](https://www.optim-ism.fr/optimism-qui-sommes-nous) !

> Optim’ism construit et teste la transition écologique par la transformation de l’économie vers un modèle plus résilient et plus inclusif.

Ça donne envie de faire une boite dans le genre, ou de travailler avec eux !

Une première piste : entreprise d'insertion par l'activité eco ?

- [Travailler chez Optim'ism](https://www.optim-ism.fr/travailler-chez-optimism/)
- [Des jardins partagés](https://www.optim-ism.fr/dans-les-jardins/)
- [Bénévolats ?](https://www.optim-ism.fr/devenir-benevole/)
- [Adhérent ?](https://www.optim-ism.fr/adherer-a-optimism/)

---

[Peer to peer chat plateforme](https://cabal.chat/)

