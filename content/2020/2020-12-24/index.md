---
title: 24 décembre 2020
---

[HotWire](https://hotwire.dev/)

Intéressante découverte via [un message de sunny](https://mamot.fr/@sunfox/105428995037015232). Merci !

C'est encore un peu trop magique par contre. Combien de JavaScript chargé ? Est-ce que c'est l'évolution de turbo-link ?

---

Quelque lien et outil pour l'accessibilité et les couleurs.

- https://paletton.com ne propose pas des set de couleur assez contrasté pour l'accessibilité.
- https://color.a11y.com
- http://a11yrocks.com/colorPalette/
- http://clrs.cc/a11y/


