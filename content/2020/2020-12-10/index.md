---
title: Dimanche 10 décembre 2020
---

J'ai entendu parlé de [Caddy server](https://caddyserver.com/) hier, et aujourd'hui, David me partage un lien qui en parle un peu plus.

Intéressant. Encore du Go. Est-ce que c'est un langage a tester ?

---

Partie d'un livre sur les ver de terre, je me retrouve à commande l'ensemble des 8 livres des auteurs du [jardin vivant](http://www.lejardinvivant.fr/). Je n'aurais peut-être pas du ? J'aurais pu prendre le temps de lire un peu leur site avant :)

---

[Hyperbonus 8 - Association Ping - La documentation dans les tiers-lieux, ça sert à quoi ?](https://www.youtube.com/watch?v=nrHoNe2Ql3U&feature=youtu.be)

- Documenter pour partager, pour prendre du recul, pour aider les autres.
- Inspirer par des histoires, raconter des échecs.
- La documentation technique est un élément clef, mais pas que.

---

[Trouble du Déficit d'Attention avec ou sans Hyperactivité](http://www.tdah-adulte.org/)

---

[Cueillir les plantes sauvages : une interview avec Thomas Echantillac](https://www.altheaprovence.com/cueillir-les-plantes-sauvages-une-interview-avec-thomas-echantillac/)

- [Association française des professionnels de la cueillette de plantes sauvages](http://www.cueillettes-pro.org/)
- [Syndicat professionnel de productrices et de producteurs de plantes aromatiques et médicinales](https://www.syndicat-simples.org/)

Devenir cueilleur !?!?? Voir Paysan-Cueilleur ? Avec un peu d'artisanat.
Prélever les bougeons, un impact plus fort sur la plante.
Coopérative de mise en commun de matériel de séchage

[La Sicarappam : une coopérative pas comme les autres !](http://www.sicarappam.com/)

> Il y a de la place pour des coopératives sur les plantes sauvages en France.

Faire une culture d'arnica. C'est une plante difficile à récolter ces dernières années.
