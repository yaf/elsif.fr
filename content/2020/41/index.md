---
title: Semaine 41
---

_Du 11 au 17 octobre 2020_


Et pourquoi mon site ne serais pas un wiki ? Un wiki en markdown ? Ça
m'apporterais l'aspect jardinage que j'aimerais (construction de contenu
enrichi avec le temps) et de pouvoir malgré tout écrire une sorte de journal.

Qu'est-ce qu'un site statique m'offre par rapport à un wiki ? La facilité de
déploiement (pas de backoffice), et une facilité de maintenance. D'autant que
le wiki serait intéressant s'il y avait d'autres contributions. Or sur mon
site, c'est pas gagné.  Par contre, pour le site du rookie club / les pieds
dans le code, la question est intéressante ! Et si j'essayais un wiki ?

---

Et si notre projet c'était surtout de vivre pas trop loin de la mer, en
essayant de vivre de ce qu'on produit, trouve, bricole, en prenant soin des
liens entre nous (famille), entre vous (voisinage, village), entre humains
(gens de passages) ? Faire le paysan, c'est très très lié à un statut demandé
par la société, celle qui à besoin de créer des cases pour savoir les ranger
dans un ordre d'importance, de pouvoir. J'ai peut-être pas besoin de ça. Nous
n'avons peut-être pas besoin de ça.

Un éco-hameau rentre dans ce schéma. Mais c'est pas obligatoire.


---

> Important de transmettre de manière assumé, d'y passer du temps, d'y mettre
> de l'énergie.

> Transmettre l'identité sans avoir honte, et en même temps, il faut que
> l'enfant puisse remettre en question ces identité, en changer s'il le veut.
> C'est cette transmission qui n'est pas définitive que j'essaie de mettre en
> avant dans le livre.

[La puissance des mères (2020, La
Découverte)](https://play.acast.com/s/kiffe-ta-race/-50casdecoles-lesmeresaucreneau)

---

Comment partager ma vision du monde avec les personnes autour de moi ? Sans
doute en étant franc et en étant moi même. En affirmant mon décalage. Oui,
parfois je me sens en décalage, et je ne fais rien. Je suis figé. Je n'arrive
pas à dire quelque chose. Ça ne semble pas grave sur le moment, et en même
temps, j'ai l'impression que c'est là que je pourrais faire la différence,
amorcer un changement chez certaines personnes autour de moi.

Et je repense à [cette vidéo d'Isabelle
Padovani](https://la-ferme-des-enfants.com/demasquer/) qui parle d'aller sur la
colline de l'autre pour le faire bouger. Est-ce que j'ai le courage de faire ce
chemin ? Sans doute pas avec tout le monde, ni dans toutes les conditions.

Dur d'exprimer un désaccord. J'ai du mal à passer du temps avec mon frère. Nous
ne sommes pas du tout accordé. Je crois que nous n'arrivons pas à nous
comprendre. J'essaie de l'écouter. Je crois que je n'y arrive pas. Je n'arrive
pas à aller sur sa colline. Je n'ai jamais réussi.

C'est sans doute pour ça que je ne réagis pas en général. C'est aussi sans
doute pour ça que je cherche la compagnie de certaines personnes, et moins
d'autres.

Ça ressemble à de l'entre-soi, ça me dérange tiens.

---

> Maintenant que je me suis habitué à travailler moins pour gagner moins, je
> suis rentré dans un cycle bénéfique de travail libre et volontaire qui me
> réjouit bien plus qu’auparavant. Je lis plus, je stresse moins, je n’achète
> plus rien de neuf, je répare avec mon temps libre. Je regarde pousser mes
> enfants, et j’apprends. A être papa, à faire du pain, de la bière, à
> m’occuper littéralement de mon potager.

[Fabien lamarque dans son Biland de l'année 2019](https://fabien-lamarque.eu/Bilan-de-l'ann%C3%A9e-2019/)

Pas le même cheminement, mais une conclusion similaire :)


## Botanique, potions et jardins


[Liste d'herbes communes utiles aux problèmes du système respiratoire (poster
constitué par l'association Cueillir)](https://www.facebook.com/452291938180007/photos/a.453176281424906/3334714349937737/)

- Bouillon blanc _Verbascum thapus_
- Lierre terrestre _Glechoma hederacea_
- Thym _Thymus vulgaris_
- Bourrache _Borago officinalis_
- Ronce _Rubus fruticosus_
- Plantain _Plantago lanceolata_


## Programmation

[Coder pour changer de vie](https://coder-pour-changer-de-vie.com/)

Je suis tomber sur la chaine Youtube de ce monsieur. C'est une approche plutôt
intéressante. Il évoque une méthode pour apprendre à programmer. Je me dit que
ça pourrais ressembler ça le nouveau rookie club, une chaine Youtube. Mais on y
ferais du code et des schémas.

---

Une grande liste de tutorial, dans divers langage : [Flavio
Copes](https://flaviocopes.com/)

Avec un projet au nom amusant [The valley of
code](https://thevalleyofcode.com/), un projet de tutoriel.

---

> Réussir un projet informatique c’est difficile, bien plus que de le rater.
> Mais ce qui est encore plus difficile c’est de l’arrêter au beau milieu en
> osant reconnaître que l’on s’est trompés. 

[Health Data Hub : du recadrage de la CNIL à l’arrêt prévisible du
service](https://www.dsih.fr/article/3925/health-data-hub-du-recadrage-de-la-cnil-a-l-arret-previsible-du-service.html)

---

J'aimerais essayer de faire du `(Test && Commit) || Revert` pour voir.

C'est une pratique un peu déroutante, qui semble partir du TDD pour aller un cran plus loin ? Ou un cran de coté ?

Kent Beck en parle très bien dans [test && commit || revert](https://medium.com/@kentbeck_7670/test-commit-revert-870bbd756864).

J'aime beaucoup la façon qu'il a eu d'approcher cette nouvelle façon de travailler

> I’m not arguing for “test && commit || revert” nor even describing its trade-offs. I’m saying it seems like it shouldn’t work at all, it does, and I hope you try it (if you’re the sort of person who just tries new programming workflows).


Dans un autre article, le voilà qui évoque le build incassable que j'avais vu en action il y a bien longtemps. C'est basé sur le même genre de principe [test && git commit -am working](https://medium.com/@kentbeck_7670/limbo-on-the-cheap-e4cfae840330).

---

Un [gist sur la mise en place d'une base PostGreSQL pour Rails](https://gist.github.com/yaf/7a96916f609a3e8a69cd6e991b29c4af)

---

Un [gist sur l'usage d'un template de commit avec Co-author](https://gist.github.com/yaf/9022c6eaf5d760d8cd2536ddc54345be)

---

[Thomas propose une matinée sur Twich pour coder une mise en place sur MesAides](https://twitter.com/1h0ma5/status/1316360041272954880)

Faire tu twich comme Thomas ? Dommage, je ne pourrais pas voir cette scéance
pour voir comment il l'organise. Une prochaine fois sans doute ?  Créer un
compte et s'abonner à certaines personnes...

---

Antoine Augusti à lu et extrait des phrases du rapport de la cour des comptes à
propos des projets informatiques de l'état. Il a choisi de regarder
particulièrement les aspects « ressources humaines ».

Après un passage chez Etalab et le MTES, il est maintenant au Canada au CDS.

[Antoine Augusti sur twitter à propos du rapport de la Cour des Comptes](https://twitter.com/AntoineAugusti/status/1317083255770877957)

Le rapport [la conduitedes grands projets numériquesde l’état](https://www.ccomptes.fr/system/files/2020-10/20201014-58-2-conduite-grands-projets-numeriques-Etat.pdf)

> J'ai lu ce rapport et je me suis attardé sur la partie "Les ressources
> humaines, un déficit critique à combler rapidement" Plongée dans les
> ressources numériques de l'État français, en ne gardant que des citations du
> rapport.

> Les difficultés de recrutement et de fidélisation du personnel de la filière
> numérique au sein de l’État sont l’un des facteurs majeurs d’échec des grands
> projets numériques L’État ne connaît donc pas avec précision les effectifs
> affectés à des fonctions numériques.

> Les effectifs de la filière numérique sont estimés à 18 500 auxquels
> s’ajoutent environ 20 000 personnels du ministère des armées Hors ministère
> des armées, plus de la moitié des « informaticiens » dépendent des ministères
> économiques et financiers et de l’intérieur

> Les chefs de projet au sein de l’État sont plutôt âgés
(cf un tableau... Tiens, comment j'ajoute une image dans mes journaux ?)

> L’État dispose en permanence de 500 postes ouverts dans le numérique de
> statut fonctionnaire ou contractuel, de bac +3 à bac +5 et plus.  Le métier
> de chef de projet MOE présentait en 2018 un taux de vacance de 17 % soit une
> centaine de postes

> À la douane, des périodes de vacance de poste de 18 mois ont été constatés
> dans le bureau « étude et projets du système d’information » Au service des
> technologies et des systèmes d’information de la sécurité intérieure, 30
> emplois civils vacants en 2018, 86% des postes

> Ceci nécessite de disposer d’une expertise interne minimale pour piloter les
> prestataires chargés de la réalisation. Or, l’État ne dispose pas aujourd’hui
> de ressources internes de pilotage suffisantes.

> la Dinum pour formuler ses avis sur les grands projets de l’État [...]
> recourt également à des prestataires extérieurs. Le taux d’externalisation de
> cette activité s’éleverait en 2019 à 40 %.

> À l’issue de cette analyse, il ressort qu’un plan de recrutement de l’ordre
> d’au moins 400 ETP serait nécessaire pour couvrir a minima cette impasse de
> ressources Le ministère des armées compte 20 à 25 % de postes vacants en
> sortie de concours de catégorie A, et 75 % en B

> Les Isic sont majoritairement recrutés en seconde partie de carrière : 41 %
> des admis sont âgés de 30 à 39 ans et seulement 20 % ont entre 20 et 29 ans
> [...] La moyenne d’âge des personnes recrutées est de ce fait de 40 ans.

> Le pourcentage de contractuels varie donc fortement entre les ministères : de
> 54 % au sein de la filière numérique du ministère de la culture à une quasi
> inexistence au sein du ministère de l’environnement 92% des chefs de projets
> sont contractuels à la culture et 2% au MTES

> Bien qu’il soit possible depuis 2016190 de recruter les contractuels
> directement en CDI, cette disposition est encore trop peu utilisée [...] À
> titre d’exemple, la DGFiP n’a recruté aucun de ses contractuels en CDI

> il ressort que 59 % des compétences « clés » sont détenues par des agents
> contractuels.  Les délais de recrutement par concours restent longs : six
> mois [...] Ils laissent peu de chance face à la concurrence du secteur privé
> qui peut recruter ces compétences en 15 jours

> 31 % des contractuels et 21 % des fonctionnaires ne sont pas certains de
> rester dans l’administration à cause de la faiblesse de la rémunération Au
> sein du ministère de l’éducation nationale, le salaire moyen d’un chef de
> projet est d’environ 20 % inférieur au privé

> Pour les contractuels, il n’y a pas de parcours de carrière organisé.  l’État
> employeur dans le domaine du numérique indique que « le portage au plus haut
> niveau est jugé encore insuffisant »

> Dans les administrations françaises, le niveau de compréhension des sujets
> numériques est faible.

Je partage ici la conclusion d'Antoine.

> Ce ne sont que des extraits choisis. Je vous recommande la lecture de ce
> rapport. La partie sur les ressources humaines est à partir de la page 101
> (n'ayez pas peur)

> Remarque personnel : s'il est si difficile de recruter du monde, que les
> personnes trouvent le salaire trop faible et les conditions d'emploi pas
> adaptées, intéressez-vous aux attentes. Le rapport comporte 1 seul fois le
> mot "télétravail". C'est pourtant un levier pour recruter

> en dehors de la région parisienne, où le vivier est important, les salaires
> sont plus faibles, tout en offrant une qualité de vie différente. Jetez un
> oeil sur les attentes des professionnels du numérique quand ils envisagent de
> rejoindre le service public

[Résultats du sondage professionnels du numérique : vos motivations pour rejoindre le service public](https://www.etalab.gouv.fr/resultats-du-sondage-professionnels-du-numerique-vos-metiers-pour-rejoindre-le-service-public)

C'est vrai que j'accepterais de travailler en contractuel, voir en
fonctionnaire peut-être, mais il faudrait que je puisse continuer à travailler
à distance, et à temps partiel (4/5 au moins, si ce n'est 3/5), en maintenant
mon salaire.

---

Les premiers pas d'Élodie en mode padawan avec moi sur RDV-Solidarités nous
amène à nettoyer les fichiers `README.md` et `CONTRIBUTING.md`. Nous nous retrouvons à
regarder ce que d'autres font

- [Atom/cONTIBUTING.md](https://github.com/atom/atom/blob/master/CONTRIBUTING.md)
- [DJango/contributing](https://docs.djangoproject.com/en/dev/internals/contributing/)
- [NodeJS/CONTRIBUTING.md](https://github.com/nodejs/node/blob/master/CONTRIBUTING.md)
- [ExoressJS/Contributing.md](https://github.com/expressjs/express/blob/master/Contributing.md)
- [TravisCI/CONTRIBUTING.md](https://github.com/travis-ci/travis-ci/blob/master/CONTRIBUTING.md)
- [Flask/CONTRIBUTING.rst](https://github.com/pallets/flask/blob/master/CONTRIBUTING.rst)
- [Devise/CONTRIBUTING.md](https://github.com/heartcombo/devise/blob/master/CONTRIBUTING.md)
- [ReadTheDocs/contribute.html](https://docs.readthedocs.io/en/latest/contribute.html)

Ça nous permet de voir qu'une partie de ce que nous voulons mettre dans ce
fichier concerne surtout un guide de programmation (coding style).

---

J'ai amorcé [une liste d'outils qui permettent de gérer la prise de rendez-vous](https://pad.incubateur.net/662LptE1S3yw7xZ4ijix4A#). C'est sur un pad de l'incubateur, inaccessible à l'extérieur (il me semble ?). Faut-il le publier ?
J'imagine que ça pourrais nous servir de documentation pour [RDV-Solidarités](https://www.rdv-solidarites.fr).
Pourquoi sommes nous parti sur un outil spécifique ?

Il y a tellement de demande autour de ce sujet

Je devrais peut-être en parler plus largement, et me servir un peu des réseau
sociaux ?

Nous avons commencé à discuter des orientations possible pour généraliser
l'usage de RDV-Solidarité à d'autres domaines. Ça sera pas simple. Mais c'est
très intéressant comme tournure :)


## Découvertes

> Naguère, les libristes femmes de l’April marquaient le Ada Lovelace Day, en
> se photographiant avec des barbes postiches, comme pour neutraliser l’absence
> de l’ attribut qui fait défaut aux filles d’Eve et aux filles d’Ada, pour se
> fondre aux « barbus », surnom des geeks mâles.

[Citation de Véronique Bonnet, présidente de
l'April](https://www.april.org/ni-d-eve-ni-d-ada-tribune-de-veronique-bonnet-a-l-occasion-du-ada-lovelace-d)

---

- À lire ? [L'innovation ordinaire de Norbert
  Alter](https://www.cairn.info/l-innovation-ordinaire--9782130583530.htm#)
- À lire ? [Donner et prendre. La coopération en entreprise de Norbert
  Alter](https://journals.openedition.org/lectures/815)
- À lire ? [Une carte pour l'enfer de Miyuki
  Miyabe](https://www.babelio.com/livres/Miyabe-Une-carte-pour-lenfer/25332)


---

Une belle association qui tente de faire bouger le mammouth.

[Fait École
Ensemble](https://wiki.faire-ecole.org/wiki/Association_Faire_Ecole_Ensemble)

Il y a du beau monde là dedans. Est-ce que j'ai des choses à y apporter ?

---

Une belle association à découvrir à Nantes, mais en ligne aussi : [ping
base](https://www.pingbase.net/). Beaucoup de ressource en ligne.

---

[Son de la forêt à travers le
monde](https://timberfestival.org.uk/soundsoftheforest-soundmap/).

Avec quel matériel faire ce genre de prise de son ? Il manque une documentation
sur comment participer. Il y a aucun enregistrement sur la Bretagne.


---


> Parce que nous en avions marre…
>
> – de ne lire que des histoires de garçons et d’hommes à qui il arrive mille
> aventures – que seuls les hommes résolvent des enquêtes et sauvent le monde
> dans la plupart des films – que les étagères de nos bibliothèques soutiennent
> si peu de romancières, poétesses, essayistes, bédéastes, philosophesses… – de
> connaitre si peu de femmes qui ont marqué notre histoire, alors qu’elles
> méritent évidemment d’être connues
>
> … nous avons décidé de partir à la recherche des héroïnes ! 

[1001heroines.fr](https://1001heroines.fr/a-propos/)


---

> « Sur les questions écologiques, de justice sociale, d’égalité femmes-hommes,
> de politique : ce n’est pas Thomas Sankara qui était en avance sur son temps,
> c’est nous qui sommes en retard »

> « Le racisme peut passer par les personnels, mais aussi par les contenus
> d’enseignements, dont certains sont très problématiques. Par exemple, pour
> un·e jeune Noir·e ayant grandi en France, il est difficile d’avoir un rapport
> sain à son Histoire quand, à l’Ecole, les seules manières de l’aborder sont
> le fameux tryptique esclavage/colonisation/famine. Comme s’il n’y avait rien
> eu avant, et comme si pendant il n’y avait pas eu de résistances, de luttes.
> Tout cela participe à altérer la construction identitaire des jeunes issu·e·s
> de l’immigration, notamment post-coloniale ».

[Goundo Diawara, la Sankarette fidèle à la cité
rose](http://www.lallab.org/goundo-diawara-la-sankarette-fidele-a-la-cite-rose/)

---

Je retombe encore sur [Riposte Créative
Bretagne](https://ripostecreativebretagne.xyz/?PagePrincipale) à l'occasion
d'un partgae à propos de [coopération dans le monde
pédagogique](http://www.cooperations.infini.fr/). Riposte Créative propose [un
site pour tout ce qui touche à la
pédagogie](https://www.ripostecreativepedagogique.xyz/?PagePrincipale).

---

Fédération des café-libraires de Bretagne

http://www.lafederationdescafeslibrairiesbretagne.fr/guide/

Le plus proche actuellement semble être [La Dame
Blanche](http://www.la-dameblanche.fr/) à Port-Louis

---

Vraiment classe ! [Slow Ways](https://slowways.uk/)

> We’re creating a network of walking routes that connect all of Great
> Britain’s towns and cities.

À quel point c'est l'équivalent des « chemins de randonnées et grande
randonnées » ? C'est peut-être autre chose justement ?

---

Merci Stéphane pour la découverte de ce site sur la [résilience alimentaire](https://crater.resiliencealimentaire.org/).

Ça mériterais de regarder un peu les critères d'évaluation utilisé.

- [Le Faouët semble plutôt bien](https://crater.resiliencealimentaire.org/?idCommune=C-56057)
- [Hennebont est à la ramasse](https://crater.resiliencealimentaire.org/?idCommune=C-56083)
- [Guidel à du travail aussi](https://crater.resiliencealimentaire.org/?idCommune=C-56078)
- [Rostronen est sur la bonne voie](https://crater.resiliencealimentaire.org/?idCommune=C-22266)

C'est intéressant de voir que dans le centre Bretagne, le chemin du changement
est déjà amorcé. À moins que ce soit de vielle habitude qui perdure ? Ou bien le fait qu'il y a sans doute moins d'habitant.

Est-ce qu'il vaut mieux s'installer dans une endroit où ça serait intéressant
de se lancer en tant que paysan, ou bien là où il y a déjà du monde ? Ça
dépends sans doute du projet que nous avons...

