---
title: mardi 29 décembre 2020
---

Journal de mon activité sur [RDV-Solidarités](https://www.rdv-solidarites.fr/)

début rdv à 9h

- reprise du travail sur les couleurs de motifs

Pause de 1h pour Aydan et cours

- reprise de l'explo. Finalement, ce qu'on cherche c'est le contrate. Pas facile de trouver uen solution ok pour choisir dans une palette. Le mieux est sans doute le random à la github. Je vais essayer https://24ways.org/2010/calculating-color-contrast/ 

Pause à 12h30

Reprise avec Élodie à 14h30

- On relit un peu le code, on nettoie et on finalise la PR
- réponse à une agent qui se demande pourquoi elle a reçu une invitation

Arrêt à 16h

---

SIROP ANTITUSSIF À LA MAUVE 🤒 ❄️

La mauve sylvestre (Malva sylvestris) contient beaucoup de mucilages. Ces molécules gonflent au contact de l'eau pour former une sorte de gel. Ce "gel" réduit activement la toux d'irritation. Une bonne nouvelle en cette période de froid ! ☃️
Pour réaliser un sirop antitussif maison :
- Découpez 15 g de feuilles de mauve.
- Versez-y 150 ml d'eau bouillante dans un récipient avec couvercle. Laissez infuser durant 24 heures, puis filtrez.
- Ajoutez 150 g de sucre par 100 ml de liquide obtenu.
- Portez le mélange à ébullition jusqu'à une consistance sirupeuse.
- Laissez refroidir avant de conserver dans une bouteille pasteurisée.
Pour maîtriser diverses préparations phytothérapiques à reproduire à la maison, découvrez le module 'Phytothérapie' de notre NOUVELLE FORMATION EN CUISINE DES PLANTES SAUVAGES. Cette formation de 2 ans débute en avril prochain, et vous permettra de devenir experts en cuisine sauvage !
Informations : https://cuisinesauvage.org/ => onglet 'Formation longue' / joffrey.chalon@cuisinesauvage.org

[Recette original trouvé dans le groupe Plantes sauvages et médicinales de nos jardins et chemins sur Facebook](https://www.facebook.com/groups/530773417857627/permalink/726781531590147/)


Et si je me lançais dans une formation de deux ans avec [cuisine sauvage](https://cuisinesauvage.org/) ? Par contre, c'est à [Namur](https://www.openstreetmap.org/search?query=namur#map=13/50.4571/4.8663) :-/

