---
title: Semaine 45
---

_Du dimanche 8 au samedi 14 novembre_

## Lundi 9 novembre

> On 23/10/2020 15:20, Ludovic Rocher wrote:
> Bonjour Yannick,
> en effet. Pour faire des bons manches solides, je privilégie le frêne. J'en ai mis à sécher en quartier car le bois se rétracte en séchant ce qui laisse du jeu si on emmanche lorsque c'est "vert".
> L'idée serait de faire un atelier avec 4 à 8 participants, sur des petits manches (hachettes, marteau, etc.). Les participants pourront venir avec leur outils à ré-emmancher.
> Je ne travaille qu'avec des outils à main (hache - plane - couteaux - racloir).
> En fonction de l'habitude du travail du bois, je pense qu'il faudra prévoir entre une demi-journée et une journée. Je n'ai pas encore de recul sur ce stage car ce sera le 1er.
>
> N'hésites pas à poser des questions plus précises si tu en as.
> Cordialement,

> Ludovic Rocher
> Sculpteur & Affûteur
> Pédagogue de la perception <https://fepapp.fr/praticien/ludovic-rocher/>
> /Co-fondateur de/ Demain en main <https://demainenmain.fr>
> /Consultant pour le monitoring de la Facilité Energie ACP-UE <https://www.dem.dk/en/services/monitoring-evaluation/energy-facility/>/
> //rocher.ludovic@gmail.com <mailto:rocher.ludovic@gmail.com>/   -  +33 (0)6 69 91 40 19 

## Mardi 10

Note de lecture La Société du Spectacle de Guy Debord.

Je n'avais jamais lu ce livre. Je n'ai pas trouvé ça très facile. Je pense que je n'ai pas tout compris. Ça arrive. Pour mieux comprendre, il faudrait que je prenne plus de temps sur chaque paragraphe, et je n'en ait pas le courage. Pas pour l'instant.

Est-ce ça aussi le problème de la bibliothèque : il faut rendre l'ouvrage en 4 semaines ?

> 7.
> La séparation fait elle-même partie de l'unité du monde, de la praxis sociale globale qui s'est scindée en réalité et en image. La pratique sociale, devant laquelle se pose le spectacle autonome, est aussi la totalité réelle qui contient le spectacle. Mais la scission dans cette totalité la mutile au point de faire apparaître le spectacle comme son but. Le langage du spectacle est constitué par des _signes_ de la production régnant, qui sont en même temps la finalité dernière de cette production.

> 9.
> Dans le monde _réellement renversé_, le vrai est un moment du faux

> 16.
> Le spectacle se soumet les hommes vivants dans la mesure où l'économie les a totalement soumis. Il n'est rien que l'économie se développant pour elle-même. Il est le reflet fidèle de la production des choses, et l'objectivation infidèle des producteurs.

> 24.
> (...) La scission généralisée du spectacle est inséparable de la scission dans la société, produit de la division du travail social et organe de la domination de classe.

> 26.
> Avec la séparation généralisée du travailleur et de son produit, se perdent tout point de vue unitaire sur l'activité accomplie, toute communication personnelle directe entre les producteurs. Suivant le progrès de l'accumulation des produits séparés, et de la concentration du processus productif, l'unité et la communication deviennent l'attribut exclusif de la direction du système. La réussite du système économique de la séparation est la _prolétarisation_ du monde.

> 52.
> Au moment où la société découvre qu'elle dépend de l'économie, l'économie, en fait, dépend d'elle. Cette puissance souterraine, qui a grandi jusqu'à paraître souverainement, a aussi perdu sa puissance. Là où était le _ça_ économique doit venir le _je_. Le sujet ne peut émerger que de la société, c'est-à-dire de la lutte qui est en elle-même. Son existence possible es suspendue aux résultats de la lute des classes qui se révèle comme le produit et le producteur de la fondation économique de l'histoire.

> 68.
> Sans doute, le pseudo besoin imposé dans la consommation moderne ne peut être opposé à aucun besoin ou désir authentique qui ne soit lui-même façonné par la société et son histoire. Mais la marchandise abondante est là comme la rupture absolue d'un développement organique des besoins sociaux. Son accumulation mécanique libère un _artificiel illimité_, devant lequel le désir vivant reste désarmé. La puissance cumulative d'un artificiel indépendant entraîne partout _la falsification de la vie sociale_.

> 108.
> Quand l'idéologie, devenue absolue par la possession du pouvoir absolu, s'est changée d'une connaissance parcellaire en un mensonge totalitaire, la pensée de l'histoire a été si parfaitement anéantie que l'histoire elle-même, au niveau de la connaissance la plus empirique, ne peut plus exister. La société bureaucratique totalitaire vit dans un présent perpétuel, où tout ce qui est advenu existe seulement pour elle comme un espace accessible à sa police.

> 122.
> Quand la réalisation toujours plus poussée de l'aliénation capitaliste à tous les niveau, en rendant toujours plus difficile aux travailleurs de reconnaître et de nommer leur propre misère, les place dans l'alternative de refuser _la totalité de leur misère, ou rien_, l'organisation révolutionnaire a dû apprendre qu'elle ne peut plus _combattre l'aliénation sous des formes aliénées_.

_chapitre VI. le temps spectaculaire, une citation pour démarrer le chapitre_

> Nous n'avons rien à nous que le temps, dont jouissent ceux mêmes qui n'ont point de demeure.
-- Baltasar Gracian

> 152.
> Dans son secteur le plus avancé, le capitalisme concentré s'oriente vers la vente de blocs de temps «tout équipés », chacun d'eux constituant une seule marchandise unifiée, qui a intégré un certain nombre de marchandises diverses. C'est ainsi que peut apparaître, dans l'économie en expansion des « services » et des loisirs, la formule du paiement calculé « tout compris », pour l'habitat spectaculaire, les pseudo-déplacements collectifs des vacances, l'abonnement à la consommation culturelle, et la vente de la sociabilité elle-même en «conversations passionnantes » et « rencontres de personnalités ». Cette sorte de marchandise spectaculaire, qui ne peut évidemment avoir cours qu'en fonction de la pénurie accrue des réalités correspondantes, figure aussi bien évidemment pari les articles-pilotes de la modernisation des ventes, en étant payable à crédit.

> 167.
> Cette société qui supprime la distance géographique recueille intérieurement la distance, en tant que séparation spectaculaire.

> 207.
> Les idées s'améliorent. Le sens des mots y participe. Le plagiat est nécessaire. Le progrès l'implique. Il serre de près la phrase d'un auteur, se sert de ses expressions, efface une idée fausse, la remplace par l'idée juste.

---


[Inclusive Components](https://inclusive-components.design/)

> A blog trying to be a pattern library. All about designing inclusive web interfaces, piece by piece.

À lire ? [Inclusive Components: The Book](http://book.inclusive-components.design/)

Merci David. Encore du boulot pour faire les choses bien.

---

Après m'en avoir parlé une deuxième fois, je me décide à jeter un coup d'œil à [backmarket.fr](https://www.backmarket.fr). Sitina souhaite une imprimante pour ces cours. Ça fait longtemps que je bloque un peu sur ce sujet. Mais là, elle semble souffrir à regarder ces cours, et les imprimantes ne sont pas accessible en bibliothèque... Ok, go pour une imprimante alors.

---

Facebook est vraiment un lieu pour l'organisation de groupe. Je n'avais pas intégré cette aspect lors de mes premières utilisation autour des années 2010. De retour sur la plateforme pour justement pouvoir participer à certains groupes, et en découvrir d'autres, je suis vraiment impressionné par la quantité de chose qu'il s'y passe. Je suis un peu triste aussi bien sur.

---

Une adresse email familiale. Après un compte commun, c'est vrai que nous n'avons pas encore d'adresse commune pour la famille. Pourtant bien pratique pour partager l'information envoyé par la crèche, l'école, la mairie et les autres activités extra-scolaire...

Que faire ? Utiliser le nom de domaine [elsif.fr](https://elsif.fr/) que j'ai depuis un moment ? Je penche pour en prendre un nouveau. Deux options :
- prendre un nom de domaine qui combine nos deux noms. Le hic, c'est que ça fait long à dire à l'orale si jamais nous devons donner notre adresse :-/
- trouver un nom un peu originale mais simple à épeler, et dont on se souviendrait.

Pas simple :)

---

Nous parlons de [test commit revert](https://medium.com/@kentbeck_7670/test-commit-revert-870bbd756864) avec Élodie. Ça me repasse par la tête comme une pratique que je n'ai pas encore essayé. J'y vois un moyen de coder proprement, en respectant l'aspect psychologique de rester en vert tout le temps. Le soucis c'est que j'ai peur du coup d'écrire du code non testé ou d'aller un peu trop vite dans l'implémentation. J'ai parfois l'impression que le test rouge me permet de « doser » la granularité, mon rythme.

---

> L'effet Hawthorne, ou expérience Hawthorne, décrit la situation dans laquelle les résultats d'une expérience ne sont pas dus aux facteurs expérimentaux mais au fait que les sujets ont conscience de participer à une expérience dans laquelle ils sont testés, ce qui se traduit généralement par une plus grande motivation.

[Effet Hawthorne](https://fr.wikipedia.org/wiki/Effet_Hawthorne)

---


Parce qu'il n'y a pas que des gros framework en JavaScript.

[The Vanilla JavaScript Toolkit](https://vanillajstoolkit.com/)

> A collection of JavaScript methods, helper functions, plugins, boilerplates, polyfills, and learning resources.

---

> Au Low-tech Lab, nous essayons de réorienter la recherche et l’innovation vers un pro- grès durable pour l’Homme et la planète : d’un côté, un meilleur équilibre entre citoyenneté, travail, apprentissage et bien-être collectif ; de l’autre, une compréhension plus fine de nos écosystèmes pour une meilleure cohabitation avec eux.

> Mais nous voulons nous inspirer d’organisations sociales et économiques fondées sur l’entraide et le temps long, qui regorgent d’ingéniosité, cultivent une grande connaissance de leurs milieux, et témoignent le plus souvent d’un bonheur entier. Cette autre voie possible serait donc celle d’une société plus juste, plus sobre et plus solidaire. Pour transformer cette possibilité en réalité à grande échelle nous devons, dès maintenant et collectivement, explorer, prototyper et expérimenter des modèles de société fondés sur les principes de la low-tech, mais pas que !

[Le manifeste du low-tech lab](https://lowtechlab.org/fr/actualites-blog/le-manifeste-du-low-tech-lab)

---

Parce que j'oublie tout le temps les éléments qui font que https n'est pas une si bonne idée, je vais les poser ici.

> Encouraging everybody to switch to HTTPS promotes strong dependency to a third-party mafia, increases load time,

[HTTPS considered harmful](https://larlet.fr/david/stream/2018/01/06/)

Une série de contre proposition exploré ici

[Re: HTTPS considered harmful](https://larlet.fr/david/stream/2018/01/10/)
[Re: Re: HTTPS considered harmful](https://larlet.fr/david/stream/2018/01/24/)

_il faudra que je me demande si le fait que ces articles soit tous de David ne fragilise pas le point de vue_

---

> En effet, on est aliéné par rapport à un état idéal auquel on cherche à revenir, ou que l’on cherche à atteindre enfin. La notion désigne le processus par lequel le capitalisme suscite des besoins artificiels qui nous éloignent de cet état. En plus d’être aliénants, la plupart de ces besoins sont écologiquement irréalistes.

> Dans les pays du Sud, et même du Nord, certains de ces besoins élémentaires ne sont pas satisfaits. D’autres, qui l’étaient autrefois, le sont de moins en moins. Jusqu’à récemment, respirer un air non pollué allait de soi ; c’est devenu difficile dans les mégapoles contemporaines. Il en va de même pour le sommeil. Aujourd’hui, la pollution lumineuse rend l’endormissement difficile pour nombre de personnes, l’omniprésence de la lumière dans les villes retardant la synthèse de la mélatonine (surnommée « hormone du sommeil »).

> La division du travail enferme l’individu dans des fonctions et des compétences étroites tout au long de sa vie, lui interdisant de développer librement la gamme des facultés humaines. De même, le consumérisme ensevelit les besoins authentiques sous des besoins factices. L’achat d’une marchandise satisfait rarement un vrai manque. Il procure une satisfaction momentanée ; puis le désir que la marchandise avait elle-même créé se redéploie vers une autre vitrine.

> Selon André Gorz, la société capitaliste a pour devise : « Ce qui est bon pour tous ne vaut rien. Tu ne seras respectable que si tu as “mieux” que les autres (6). » On peut lui opposer une devise écologiste : « Seul est digne de toi ce qui est bon pour tous. Seul mérite d’être produit ce qui ne privilégie ni n’abaisse personne. » Aux yeux de Gorz, un besoin qualitatif a ceci de particulier qu’il ne donne pas prise à la « distinction ».

>  La garantie, c’est la lutte des classes appliquée à la durée de vie des objets.


[Ce dont nous avons (vraiment) besoin](https://www.monde-diplomatique.fr/2017/02/KEUCHEYAN/57134)

Ça me fait penser que je n'ai jamais lu du Gorz...

À lire ? André Gorz, Stratégie ouvrière et néocapitalisme, Seuil, Paris, 1964, 
À lire ? Ágnes Heller, La Théorie des besoins chez Marx, 10/18, Paris, 1978.
À lire ? André Gorz, La Morale de l’histoire, Seuil, Paris, 1959.


---

> Ajuster sa prédation, réévaluer ses propres besoins, réduire son impact écologique, ses pollutions, sa consommation énergétique, observer, accompagner ou soutenir la dynamique des organismes (jardin, corps, rivière, arbres, insectes, « mauvaises herbes », etc.), apprendre, chercher, se questionner pour comprendre : les voies pour la santé « One Health » sont absolument passionnantes et fascinantes. Elles peuvent procurer beaucoup de joie et d’émerveillement qui sont sans doute les principaux moteurs de la santé.

> Une même ferme menée en agro-écologie paysanne peut assurer en même temps la souveraineté alimentaire, mais aussi l’autonomie sanitaire et en plus, améliorer le fonctionnement de l’écosystème et les capacités de l’environnement.


[Les médecines terrestres face au coronavirus](https://www.terrestres.org/2020/09/30/les-medecines-terrestres-face-au-coronavirus)

---


Plutôt que de planter des trucs qui seront coupé, broyé, brulé, autant en faire du courant :)

> Coppicing cannot compete in an economic system that fosters the replacement of human labour with machines powered by fossil fuels

> Sustainable forest management is essentially local and manual. This doesn’t mean that we need to copy the past to make biomass energy sustainable again. For example, the radius of the wood supply could be increased by low energy transport options, such as cargo bikes and aerial ropeways, which are much more efficient than horse or ox drawn carts over bad roads, and which could be operated without fossil fuels. Hand tools have also improved in terms of efficiency and ergonomics. We could even use motor saws that run on biofuels – a much more realistic application than their use in car engines. 

J'aime bien l'idée que le vélo cargo et que les « téléphérique » (sans moteur je suppose) sont de super évolution par rapport au cheval.

> These numbers confirm that it is not biomass energy that’s unsustainable. If the whole of humanity would live as the 40% that still burns biomass regularly, climate change would not be an issue. What is really unsustainable is a high energy lifestyle. We can obviously not sustain a high-tech industrial society on coppice forests and line plantings alone. But the same is true for any other energy source, including uranium and fossil fuels. 

[How to Make Biomass Energy Sustainable Again](https://www.lowtechmagazine.com/2020/09/how-to-make-biomass-energy-sustainable-again.html)

---

> Ti Miam est le résultat de la collaboration des Universités de Bretagne Occidentale (UBO) et de Bretagne Sud (UBS) avec les équipes de Lorient Agglomération et d’ALOEN, l’Agence Locale de l’Energie de Bretagne Sud.
> 
> Il s’agit d’un outil mis à disposition des habitants du Pays de Lorient pour s’alimenter de façon plus locale et plus durable.

[Ti Miam](https://timiam.bzh/)

---

Habiter un [Zome](https://www.blog-habitat-durable.com/zome/) ? Pour une famille de 5 personnes ? Certainement en combinant plusieurs Zome.

---
Je redécouvre certains basique du test et de la lisibilité.

Toujours à la recherche d'argument et d'élément factuel pour exprimer mon inconfort face à l'écriture de spec avec beaucoup de `let` bien souvent éloigné du test à modifier, créer, lire.

[Le pattern Arrange Act Assert](http://wiki.c2.com/?ArrangeActAssert)

> "Arrange-Act-Assert" : a pattern for arranging and formatting code in UnitTest methods:

> Benefits:
> - Clearly separates what is being tested from the setup and verification steps.
> - Clarifies and focuses attention on a historically successful and generally necessary set of test steps.
> - Makes some TestSmells more obvious:
>   - Assertions intermixed with "Act" code.
>   - Test methods that try to test too many different things at once.

Une autre forme évoque le [Assemble Activate Assert](http://wiki.c2.com/?AssembleActivateAssert). Est-ce qu'il y a une différence ?

Il y a aussi, introduit par Cucumber il me semble, le Given-When-Then.

And [Why Good Developers Write Bad Unit Tests](https://mtlynch.io/good-developers-bad-tests/)

> Good test code is no different. It should produce clear results without forcing the reader to jump through multiple levels of indirection. Developers often lose sight of this because it differs from how they learned to write production code.

> When you write a test, think about the next developer who will see the test break. They don’t want to read your entire test suite, and they certainly don’t want to read a whole inheritance tree of test utilities.

>  Accept redundancy if it supports simplicity.

> To write excellent tests, a developer must align their engineering decisions with the goals of test code. Most importantly, tests should maximize simplicity while minimizing abstraction. A good test allows the reader to understand intended behavior and diagnose issues without ever leaving the test function.

---

[La NASA propose une série d'activités sciences et techniques à faire à la maison !](https://www.nasa.gov/stem/nextgenstem/commercial_crew/crew-1-stem-mission-toolkit)

---

Pourquoi écrire un journal ? Pourquoi partager tout ça sur un site ? Qui lit ? Pourquoi faire ? Est-ce que ce n'est pas beaucoup de bruit au final ?

À un moment ça m'aide moi. Mais pourquoi partager ? Parce que d'autres personnes pourraient se retrouver dans une situation similaire ?

Est-ce que je lit des blogs ou journaux de personnes qui partagent un peu comme ça ? Oui, mais elles écrivent bien mieux. Est-ce une raison pour ne pas écrire moi même ?

---

Je note ici des auteurs pour les enfants... Aydan s'ennuie en lecture, ou plutôt il dévore les BD comme Anatola Latuile, Mortelle Adèle, et autre dans le même genre... J'aimerai lui suggérer des romans, des trucs plus épais. Là, on va à la médiathèque, et le soir même il a lu les 5 ouvrages emprunté :-/

Je crois que nous allons tenter les Harry Potter. Je lui ait suggéré, il a dit ok pour le premier. On verra bien.

Sinon, Anne-Sophie et une autre personnes ont évoqué [Astrid Lindgren](https://fr.wikipedia.org/wiki/Astrid_Lindgren) et [Roald Dahl](https://fr.wikipedia.org/wiki/Roald_Dahl). À explorer.

---

[Gros gros respect pour Josine et Gilbert Cardon pour tout ce que ces personnes font !](https://www.youtube.com/watch?v=vx8uOAYr3hI&feature=youtu.be)

Jardin ouvrier, éducation populaire, préservation graines, ... Un modèle.

> C'est d'abord les gens qui comptent. Et les gens tels qui sont, pas comme on les voudraient, tel qui sont.

---

Réunion du groupe de travail « journal de décision » de l'[association Alpha-B](https://www.brangoulo.fr/alpha-b/) (lié à l'éco-hameau de Brangoulo).

Partage du [le journal de décision de l'échapée belle](https://lechappeebelle.team/journal-de-d%C3%A9cisions)

Découverte de l'article de trello [Decision Journal](https://blog.trello.com/decision-journal)

> Instead, it’s something you’ll use for reflection. By documenting and periodically reviewing the decisions you make over time, you’ll get a better grasp on your state of mind and identify things like trends or common traps you find yourself falling into. 
> (...)
> 1. Don’t Use It For Everything
> 2. Keep It Simple
> 3. Create A Simple Template For Yourself
> 4. Review Your Journal Frequently


Découverte de Guy Kawasaky https://www.diateino.com/blog/2017/03/03/guy-kawasaki-tenez-un-journal-de-vos-decisions/

> nous sommes bien souvent victimes de biais cognitifs, et plus particulièrement du biais de confirmation. Ce biais nous empêche de tirer les leçons de nos décisions passées et d’améliorer notre processus de prise de décision. En effet, il nous incite à ne repérer que ce qui vient confirmer notre choix initial et à minimiser tout ce qui vient le contredire.


Dans la discussion, j'entends à nouveau parlé de la méthode des 6 chapeaux. Je n'ai pas encore eu l'occasion d'expérimenter. Ça serait sans doute intéressant un jour ?

[Méthode des six chapeaux](https://fr.wikipedia.org/wiki/M%C3%A9thode_des_six_chapeaux)

Est-ce que mon journal est une sorte de journal de décision personnel ?

Tout ça me donne envie de reprendre un journal de décision pour l'équipe Lapin ! C'était [une chouette expérience sur DossierSCO](https://github.com/betagouv/dossiersco.fr/tree/master/collections/_decisions)

---

[¢valuer ma consommation foyer](https://www.toutsurmoneau.fr/evaluer-ma-consommation-foyer)

Apparemment, 63% de notre consommation d'eau est sur les toilettes. Déjà que ça me dérange un peu de faire mes besoins dans de l'eau potable, si en plus c'est le plus gros poste de consommation, ça va devenir un sujet de préoccupation numéro un pour moi.

Est-ce que ce test est fiable ? Sur quoi se est-il construit ?

Ceci étant, je ne suis pas forcement surpris. J'ai une crainte sur la vaisselle, que nous faisons à la main. Encore que nous avons une technique qui me semble économe en eau.

---

Parce qu'un petit rappel de temps en temps ne fait pas de mal

> « Voici la philosophie d'Unix :
>
> - Écrivez des programmes qui effectuent une seule chose et qui le font bien.
> - Écrivez des programmes qui collaborent.
> - Écrivez des programmes pour gérer des flux de texte [en pratique des flux d'octets], car c'est une interface universelle. »

[Pilosophie Unix](https://fr.wikipedia.org/wiki/Philosophie_d%27Unix)

Et que je me suis retrouvé à y faire référence dans une lettre de recommandation.

---

Un gros paquets de référence aux cueillettes et plantes médicinales.

- [Association française des professionnels de la cueillette de plantes sauvages](http://www.cueillettes-pro.org/)
- [Syndicat professionnel de productrices et de producteurs de plantes aromatiques et médicinales](https://www.syndicat-simples.org/)
- [Syndicat professionnel de productrices et de producteurs de plantes aromatiques et médicinales - Massif de Bretagne](https://www.syndicat-simples.org/les-productrices-et-producteurs-simples/massif-bretagne/)
- [Cour : Botanique 1 - Initiation pratique au Museum d'Histoire Naturel](http://formation.mnhn.fr/fr/formation-continue/nos-formations/formations-qualifiantes/botanique-niveau-1)
- [Des ressources externes et des cours en ligne pour découvrir et se former en botanique](https://www.tela-botanica.org/ressources/ressources-pedagogiques/sur-le-web/)
- [La Plateforme Tela Formation](https://www.tela-botanica.org/outils/plateforme-telaformation/)
- [Formation : Conseiller en Plantes médicinales](https://www.cerfpa.com/formation/correspondance/conseiller_plantes_medicinales.htm)
- [Site de la Fédération Française des écoles d' herboristerie](http://www.ffeh.fr/enseignement-ecoles-francaises-herboristerie.php)

