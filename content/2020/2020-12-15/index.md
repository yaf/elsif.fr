---
title: Mardi 16 décembre 2020
---

J'ai lu le (?) livre de Marie Kondo à propos du rangement. Quelques éléments m'ont un peu surpris, assez pour avoir envie d'essayer.

- Pas de pile. Les vêtements s'abiment quand ils sont empilé, ceux du bas. On plie en rectangle. Plutôt dans des tiroirs pour les vêtements.
- Pas d'approche de rangement pièce par pièce, mais plutôt par groupe, famille d'objet.
- **Optimisé le rangement, pas l'utilisation**. Je crois que c'est le truc le plus évidant et pourtant auquel je n'avais pas du tout été habitué.
- L'ordre ? D'abord les livres, ensuite les vêtements, ...

---

Journal d'équipe.

Traitement d'une demande d'un usager qui est surpris (déçu ?) de ne pas pouvoir prendre un rendez-vous. Je lui ait demandé sa commune pour ensuite donner l'info à la référente pour qu'elle fasse suivre à la bonne organisation.

Est-ce que cette personne ne pouvait pas prendre rendez-vous parce qu'il n'y avait pas de créneau ? Ou bien parce que le créneau n'est pas ouvert ? Ou bien est-ce qu'il y a un soucis avec la secto ? C'est un département qui est en train de mettre en place la secto.

[Échange téléphonique avec le 22 pour un problème de fusion d'usager](https://pad.incubateur.net/n4PW84P1TOaSpH4wgpS9ag#).

Quelques tickets déplacé dans le Github ou dans le forum. Encore un regroupement. Cette fois, autour de la notion de département.

J'ai fini l'enquête que les tests qui ne passaient pas depuis hier.
- dans les tests sur le service de construction de créneaux, nous mockons la date du jour pour simuler que nous sommes le jeudi de la semaine suivante;
- Nous sommes le 14 et 15 décembre quand j'ai la barre rouge. Le jeudi de la semaine suivante est jeudi 24 décembre. Le jour d'après est le 25, un jour férié. J'ai l'impression que nous prenons en compte les jours fériés (j'ai un doute, ça mériterais d'être vérifié)
- Je fige la date à jeudi dernier (10 décembre 2020, c'est encore rouge
- Apparemment, la plage d'ouverture est considéré fermée
- je découvre l'optimisation via le champs `expired_cached` qui est mis à jour dans un callback `after_save`. Je ne suis vraiment pas confortable avec ces callbacks
- Dans ce callback, on appel la méthode `expire?` pour mettre la valeur à jour.
- Dans cette méthode, on vérifie que la date de début de plage (`first_day` est après aujourd'hui (`Date.today`)
- Le `travel_to(now)` ne semble pas avoir d'effet ici. Je précise un `before(:each) { travel_to(now) }` plutôt que de laisser `before{travel_to(now)}` mais ça ne change rien
- Je force la valeur de `expired_cached` à false. Le scope utilisé dans le calcul de créneau se base sur la valeur de se champs, sans faire toute la vérification de récurrence (c'est pratique pour mon cas). Je force cette valeur dans le « subject ». Je ne suis pas sur du moment où ce code est exécuté, autant le faire le plus prêt possible de l'endroit où nous allons nous en servir.

Finalisation de la feature de navigation ente la fiche RDV et la fiche Usager en proposant les prochains RDV d'un coté, et l'historique de l'autre.


---

Croping, scrapping... Manipuler les fichiers HTML, et les requêtes HTTP. Voilà un exercice que je fais peu. J'ai envie de me pencher sur le sujet. J'ai l'impression que David et Carl s'en servent beaucoup. J'aimerais construire mon flux RSS/Atom à partir des pages existantes...
