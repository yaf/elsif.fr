---
title: Mercredi 16 décembre 2020
---

Note de lecture « L'éveil de la petite grenouille » d'Eline Snel

> Calme-toi.
> Assieds-toi un instant, prends une tasse de thé et laisse retomber tes épaules. La paix du monde n'est pas en danger si tu t'arrêtes un moment.

> Aux émotions donnez de l'espace, aux comportements donnez des limites

Le conseil tourne ici autour de garder le lien avec les enfants, être à l'écoute de leurs émotions.

> Si à tout moment vous remplissez la vie de vos enfants, ils n'ont plus d'espace pour être eux-mêmes. Si vous exercez constamment de la pression sur eux, ils se briseront. Si vous les submergez de jouets, le goût de la possession rétrécira leur cœur. Si vous les servez au moindre signe, vous devenez leur prisonnier au lieu d'être leur parent.
> -- William Martin, le Tao Te King des parents. Éduquer avec Lao-Tseu

Intéressant. À lire ?


---

Les personnes de Code Lutin s'intéressent aux clones libre de Gather.town ; C'est chouette de savoir qu'il y en a !

> Je cherche une #AlternativeLibre à https://gather.town
> 
> J'ai déjà
> 👉 [https://workadventu.re/](https://workadventu.re/) (merci  @arthurlutzim pour la découverte)
> 👉 [https://www.calla.chat/](https://www.calla.chat/)
> 
> Est-ce que vous en connaissez d'autres ? Des avis ?
> 
> #ProximityChat

_source https://mastodon.libre-entreprise.com/@lutindiscret/105390262407036540_

---

[Hyperliens S02E05 - La Ferme des Volonteux, coopérative agricole ultra locale](https://www.youtube.com/watch?v=SqHEYhv1rz4&feature=emb_logo)

Une CAE agricole (ou agricool ? Voir paysanne, ça serait plus propre :p)

> Nos fermes sont bien des outils, mais c'est ce que ça a toujours été.On a une facheuse tendance à voir les choses comme des propriétés. Et quand tu travail al terre, des fois tu trouve des vielles chose qui vienne de la terre, des antiquité. Et là tu vois bien que tu n'es que de passage en fait. C'est en ça que nous on est propriétaire de rien du tout, je remercie juste les anciens de nous avoir laissé de la terre, et nous on est là pour la transmettre.

> Au lieu d'utiliser quelque chose, on peut en bénéficier, c'est autre chose.

---

_Rumex acetosa_

Feuille ovale, plutôt allongé, deux petites oreilles pointues en bas, du coté du petiole (qui est long). Oreilles pour ne pas confondre avec les autres rumex, en touffe pour ne pas confondre avec le liseron (en liane). Attention à le ne pas confondre aussi avec l'_Arua maculatom_. Plutôt en sous bois, mais possible que ça soit proche. Surtout jeune. L'arum à une nervure qui fait tout le tour de la feuille, contrairement à l'oseille.

Recette culinaire : flan, sauce pour pomme de terre, 

- 30g Graine de tourn trempé la veille
- 15g noisettes
- 8cl d'huile (tournesol ou autres)
- 1 gousse d'ail
- 1càcafé de purée de sésame
- 1cac de tamari (sauce soja fermenté
- une pincé de sel
- 75g d'oseille

