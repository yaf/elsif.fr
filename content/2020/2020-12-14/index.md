---
title: 14 décembre 2020
---

La plante du jour _Saponaria officinalis_ que je crois avoir croisé... Mais en fait non. Pas sur que ça pousse en Bretagne d'ailleurs.

[Bien-être au naturel : la saponaire, un savon naturel](https://www.rustica.fr/bien-etre/bien-etre-naturel-saponaire-savon-naturel,14843.html)

[Tuto : Comment réaliser du savon à partir de la saponaire](https://www.lapousseverte.fr/2020/11/20/tuto_savon_saponaire_plante/)

---

> Mais alors sur la forme…
>
> > Le rapport avec la convivialité est que cela implique que les logiciels libres sont, d’une certaine manière, plus égoïstes que les logiciels fermés (CSS). Une démangeaison personnelle implique de concevoir des logiciels pour ses propres besoins. Les exigences explicites sont donc moins nécessaires. Au sein d’un logiciel libre, ces exigences sont ensuite partagées avec une communauté de même sensibilité et l’outil individuel est affiné et amélioré pour le bénéfice de tous – au sein de cette communauté.
>
>    La facilité d’utilisation des logiciels open source
>
> Je crois que je suis de plus en plus sensible à cette forme d’ascendance de la technique sur l’utilisabilité et ce protocole semble en être le parfait exemple avec des techniciens qui s’en gargarisent et des clients tous plus inutilisables les uns que les autres. (Et hideux.)
>
> Cela m’attriste et ne m’incite guère à rejoindre une petite bulle élitiste. Le choix des espaces que l’on occupe est plus politique que jamais (cache).

[Un paragraphe sur le protocole Gemini](https://larlet.fr/david/2020/12/13/#gemini)

Ce qui est dommage c'est de devoir « construire » un autre outil, plus conviviale, plus ouvert, plus facile d'accès. Il y a de la perte d'énergie à vouloir rester entre soi

---

> Mais au-delà de la technique, cette routine me permet de prendre du recul sur ce que je fais, ce que ça vaut, où on va ensemble. Dans un contexte de distance forcée, je trouve cela d’autant plus pertinent. Je peux y partager une humeur, un questionnement a posteriori, ce qu’il me reste à faire pour démarrer la fois suivante, synthétiser mon souvenir d’une discussion, faire des blagues, partager des liens à moitié innocents, etc.

[à propos de la journalisation](https://larlet.fr/david/2020/12/13/#journal)

Ça fait bien longtemps que je journalise maintenant. Et, au delà de l'intérêt pour moi, au moment de le faire, je m'interroge sur l'intérêt pour les autres. C'est pour ça que j'expérimente le fait de publier.

Ça ajoute un peu de boulot, et je n'ai rien de spécial qui me permet de savoir si c'est lu par une personne quelque part. Peut-être devrais-je chercher à savoir, sans tracker ? Ou pas.

Par contre, David fait une claire distinction entre ces journaux pro, partagé avec l'équipe, et ces billets hebdo, partagé publiquement. Je me demande s'il ne serait pas intéressant de faire de même ? Nous faisions un journal d'équipe relativement collectif sur DossierSCO. Nous faisions beaucoup de MobProgramming. Et les jours où nous n'étions pas ensemble, et bien nous journalisions aussi dans un fichier commun. Où poser mon journal d'équipe, mon journal pro ? La meilleur place serait sans doute proche du projet. C'est un journal pour le projet, pour l'équipe. Pas pour moi.

Après avoir essayé plusieurs axes d'organisation, je me demande si la thématique à priori ne serais pas une facilité pour la publication. Ouvrir une histoire, y journaliser, puis, un jour, la fermer. J'ai l'impression de chercher à faire ça (avec [le journal d'un formateur en 2014-2015](https://yaf.github.io/journal-d-un-formateur-en-2015/) et [une mission bancale, en 2015 également](https://elsif.fr/une-mission-bancale), mais à posteriori, ce qui m'amène plus de travail.

Dans quel histoire ranger ces notes là ? Celles que j'écris ? Un journal _meta_ à propos de mon site ?

Liste possible de journaux (qui serait en fait des sujets qui m'intéresse, sur lesquelles j'ai envie de prendre du recul ?)
- plante, botanique et nature
- Apprentissage du code, voir plus
- politico-anachisme-feminisme ?
- ma mission en cours
- livres ? Articles ? Découvertes ? Une sorte de flux commenté de mes lectures autres
- un meta ?

Et si j'améliorerais mon outil de journal, pour l'appeler de n'importe où, en fonction d'un deuxième paramètre ?
`journal bota`, `journal barcode`, `journal rdv`, ...

---

Nous avons fini par envoyer un email pour préparer la bascule de [Merci Edgar](https://www.merciedgar.com/). Le début de la suite ?
