---
title: Vendredi 3 décembre
---

> Tela Botanica a fait le pari du web collaboratif et du partage de connaissances botaniques, et en 20 ans, ce sont plus de 54 000 botanistes amateurs et experts qui s’engagent collectivement pour la flore. Pour mieux la connaitre, pour la sauvegarder, pour comprendre et agir face aux pressions liées aux activités humaines et au changement climatique, notre action collective est nécessaire !

[Soutenez Tela Botanica](https://www.tela-botanica.org/2020/11/soutenez-tela-botanica-2/)

Et si j'ouvrais un [carnet botanique en ligne](https://www.tela-botanica.org/outils/carnet-en-ligne/) ?

[Tellement de vidéo à regarder](https://www.youtube.com/c/tela-botanica/videos), de [Projets](https://www.tela-botanica.org/projets/). Je n'ai pas encore trouvé le temps et l'énergie d'y participer.

---

[Un article qui explique très bien ce qu'est Gemini](https://www.bortzmeyer.org/gemini.html). J'ai bien vu un ami joué avec (après avoir fait du Gopher).

[Gopher](https://fr.wikipedia.org/wiki/Gopher) me branchais bien, mais [Gemini](https://gemini.circumlunar.space/) attire très fortement mon attention également.

Merci !

---

> L’homme derrière l’édit de 1667 est Jean-Baptiste Colbert, ardent défenseur du mercantilisme, courant économique basé sur la stricte réglementation étatique du commerce ainsi que la maximisation des exportations. Principal ministre d’État sous Louis XIV, en charge de l’industrie et du commerce, Colbert supervisa l’expansion de l’empire colonial français en Amérique du Nord et dans les Caraïbes, fondant en 1664 la Compagnie française des Indes orientales. Il rédigera plus tard la première version du Code noir, décret régissant le statut juridique des captifs asservis africains jusqu’en 1848. Le texte prévoyait notamment les mesures punitives en cas de marronnage :
>
> >  « L’esclave fugitif qui aura été en fuite pendant un mois, à compter du jour que son maître l’aura dénoncé en justice, aura les oreilles coupées et sera marqué d’une fleur de lys sur l’épaule ; s’il récidive un autre mois pareillement du jour de la dénonciation, il aura le jarret coupé, et il sera marqué d’une fleur de lys sur l’autre épaule ; et, la troisième fois, il sera puni de mort »[7].

Nous partons de loin, et il y a encore beaucoup de chemin à parcourir, et beaucoup de réparation à faire !

> Le discours de Senghor, relayé par de nombreux journaux étrangers, alerta les autorités françaises. Il y déclarait notamment :
>
> > « L’oppression impérialiste que nous appelons colonisation chez nous, et que vous appelez impérialisme ici, c’est la même chose, camarades : tout cela n’est que du capitalisme ; c’est lui qui enfante l’impérialisme chez les peuples métropolitains » (extrait de Lamine Senghor, David Murphy, La violation d’un pays : et autres écrits anticolonialistes, Paris, L’Harmattan, 2012, p. 63).

[Les forces du désordre, de la répression coloniale aux violences policières
](http://www.contretemps.eu/controle-france-afrique-police-colonialisme-violences-policieres/)

---

> Les "Lettres sur la botanique" de Jean-Jacques Rousseau, précédées d'un Traité élémentaire de cette science de L. Girault, sont disponibles sur Gallica, la bibliothèque numérique de la Bibliothèque nationale de France.

[Lettres sur la botanique de J.-J. Rousseau](https://www.tela-botanica.org/2020/12/jean-jacques-rousseau-lettres-sur-la-botanique/)

---

> La hiérarchie hospitalière doit évoluer vers moins de strates, de transversalité et vers un recentrage sur les services. La gouvernance doit pouvoir redonner la parole et l’expertise aux professionnels de terrain aux personnels médicaux et paramédicaux, avec une direction tripartite administrative, médicale et paramédicale. 

> L’État est le garant de la Sécurité sociale mais non le gestionnaire. La «gouvernance» doit être revue en donnant une place aux professionnels de santé et aux usagers qui devront siéger de plein droit au conseil de la Caisse Nationale de l’Assurance Maladie (CNAM) et dans les instances et comités qui en dépendent

[Santé : le contre plan du collectif Inter-hôpitaux](https://autogestion.asso.fr/sante-le-contre-plan-du-collectif-inter-hopitaux/)

Trop bien ce [Collectif Inte Hopitaux](https://www.collectif-inter-hopitaux.org/)

---

![Tout les articles lu !](capture-du-2020-12-04 22-43-20.png)

---

> Le télétravail n'est pas une question de lieu (c'est bien pour cela que je n'aime pas le mot télétravail), c'est une technique d'échange et de gestion de l'information au sein d'une entreprise.

[travail à distance, télétravail, travail alocalisé](https://www.la-grange.net/2020/11/29/remote-work)

