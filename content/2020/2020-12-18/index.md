---
title: Vendredi 18 décembre 2020
---

Journal de RDV-Solidarités.

- Analysé un soucis de secto de Christelle 64. Apparemment, il y a des lignes vides, 7 dans sont fichiers. Comment sont-elles là ?
- création d'un libellé de motif pour le social pour Christelle
- fini de retrouver le ou les rendez-vous qui sont en cours, avec un schéma : je prend une heure avant le début de rdv, une heure après la fin du rdv pour avoir de la marge, puis je regarde si « maintenant » apparait dans la fourchette. Reste à nettoyer un peu le code (le linter s'affole sur le fichier de test de user)
- réponse à Virginie du 80 à propos du blocage de suppression de lieu ayant une plage d'ouverture, même si elle est dans le passée. Amusant d'ailleurs, pourquoi ne peut-on pas supprimer les lieux lié à des plages closes ? Sans doute à cause du lien. Mais pourquoi garder les plages d'ouvertures qui sont passées ?
- réponse à Benoit, nous n'avons pas de contact autre pour la Drôme
- réponse à Amandine à propos de l'affichage des plages d'ouvertures dans la vue mois. Est-ce qu'il serait intéressant d'avoir une série de checkbox (ou de radio bouton ?) pour afficher/masquer les informations sur l'agenda ? Et si la meilleure représentation graphique n'était pas un calendrier comme nous avons l'habitude de voir ?
- Forward d'un email usager qui souhaite être désincrite (en réponse au mail d'invitation). J'imagine qu'elle a reçu ce mail sans être au courant. C'est le soucis de la création de compte usager par les agents. Faut-il changer le texte du mail d'invitation pour le faire en se plaçant du coté de l'usager qui reçoit ce message sans autre information ? Pourquoi « inviter » l'usager s'il n'y a pas de motif ouvert en ligne ?
- Réponse à Vaéa à propos de la réu référentes de mardi prochain

---

Un collège associatif ? En voilà une riche idée !

> Au début des années 2010, des habitant·es de la Montagne limousine ont pensé et tenu un collège associatif. L’élaboration et l’organisation collectives de l’enseignement portées dans ce projet, lui confèrent une dimension singulière. Ce livre propose de suivre les trois années de cette expérimentation à partir de témoignages sans détour. À travers un questionnement continu, l’école soutient ici des pensées et des pratiques complexes, sensibles, sur le fil.

À lire ? [Faire (l')école - Un collège associatif sur la Montagne limousine](https://www.editionsducommun.org/collections/all/products/faire-l-ecole-collectif-les-archeologues-d-un-chemin-de-traverse)
