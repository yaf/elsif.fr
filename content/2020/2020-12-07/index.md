---
title: Lundi 7 décembre 2020
---

Durant une discussion avec Michaël, une question est vite arrivée sur la table : un [airtable](https://airtable.com/) en libre ?

Partant du principe qu'Excel est un super outil d'autonomie pour beaucoup de personne.
Les limitations d'Excel sont l'accès et le partage à plusieurs.
Avoir un airtable-like, mais en mode auto-hébergé. Que voudrait dire de faire de la fédération dans ce genre d'outils ? Peut-être complétement inutile...

Est-ce qu'Airtable est un Excel like ? Pas si sur. C'est plutôt l'évolution des wufoo, typeform & co. C'est le fait de faire un formulaire et d'avoir un tableau de consultation.

Qu'est-ce qui rends Airtable si particulier par rapport à typeform ?

Un point à explorer.

Ça pourrait être un projet intéressant.

Nous avons aussi parlé de [Notion](https://www.notion.so/). J'ai cru voir passé un bout de journal de Noémie dessus. Je vais aller explorer.

Avec Michaël nous avons aussi parlé de RGPD forcement, et d'analyse de code pour en extraire les fonctions-traitement. C'est la clef. Cartographier les données, c'est pas suffisant. Oui, il faut lister les données à caractères personnelles, mais il faut aussi savoir ce qu'on en fait, et ce qu'on fait des autres données, c'est intéressant aussi.

En y réfléchissant un peu, une bonne base pourrait être les outils d'analyse de couverture de code, qui parcours le code justement. Ou encore les outils comme [strace](https://fr.wikipedia.org/wiki/Strace) qui analyse les variables et autres éléments de la mémoire durant l'exécution.

---

> Les occasions de se taire et celles de parler se présentent en nombre égal, mais nous préférons souvent la fugitive satisfaction que procurent les dernières au profit durable que nous tirons des premières.
> - Arthur Schopenhauer, Aphorismes sur la sagesse dans la vie (1851)

