---
title: Semaine 47
---

_Du dimanche 22 au samedi 28 novembre_

## Dimanche 22

> Le manifeste des remixeurs :
>
> - La culture s’appuie toujours sur le passé.
> - Le passé essaie toujours de contrôler l’avenir.
> - Notre avenir est de moins en moins libre.
> - Pour construire des sociétés libres, il faut limiter le contrôle du passé.

[Rip! A Remix Manifesto](https://www.brettgaylor.com/#/rip-a-remix-manifesto/)

> La documentation en pratique coopérative et/ou collaborative est-elle élitiste ?

> Une interprétation de la documentation en qualité d’objet à concevoir pourrait postuler d’inclure et de structurer celle-ci autour de quatre fonctions différentes : tutoriels, guides pratiques, explications et références techniques.

> - La forme tutorielle, qui est axée sur l’apprentissage, permet au nouvel arrivant de commencer, telle une leçon. Elle est similaire à l’acte d’apprendre à planter des légumes ou d’apprendre à faire la cuisine à un individu.
> - La forme de guide pratique, qui est axée sur les buts, montre comment résoudre un problème spécifique, tout comme une série d’étapes. Elle est semblable à l’acte de cultiver des légumes ou à une recette dans un livre de cuisine.
> - La forme d’explication, qui est axée sur la compréhension, explique, fournit des renseignements généraux et le contexte. Elle est comparable à un article sur l’histoire sociale de la tomate ou l’histoire sociale culinaire.
> - La forme d’un guide de référence, qui est axé sur l’information, décrit la conception réalisée. Elle vise l’exactitude, donc la vérification et l’ajout de sources, et recherche à être complète. Elle est semblable à un article d’encyclopédie de référence.


> L’homme a besoin d’un outil avec lequel travailler, non d’un outillage qui travaille à sa place. […] Or, il est manifeste aujourd’hui que c’est l’outil qui de l’homme fait son esclave. (Ivan Illich, sur la crise actuelle de la technique (la convivialité, paris : seuil))

Sa vie entière est menée par une espérance : permettre aux hommes de mieux se connaître, de ne plus avoir peur les uns des autres et donc de vivre en paix. Le document joue un rôle déterminant pour ce Pacifiste. [Paul Otlet par Marie-France Blanquet](https://www.reseau-canope.fr/savoirscdi/index.php?id=524)

[De l’hypothèse de la documentation comme technique de résistance et du wiki comme objet de ces résistances](https://sens-public.org/articles/1375/)

Tout ça me donne envie de bâtir les pieds dans le code sous forme d'un wiki pour l'apprentissage de la programmation. Le site où j'envisage d'exposé une « méthode » pourrait être à coté ? Ou bien un axe éditorial du wiki ?

---

[Obligation réelle environnementale](https://www.ecologie.gouv.fr/obligation-reelle-environnementale)

---

> Je suis profondément convaincu que la pleine autonomie ne devrait pas être une chose pour quelques-uns mais pour le plus grand nombre et que le concept présente beaucoup plus de potentiel que ce qui a été exploré à ce jour.

[Le Canada a ses agents libres — que fait-on maintenant?](https://medium.com/@plpilon_34114/le-canada-a-ses-agents-libres-que-fait-on-maintenant-781322920efe)

---

> On m’a beaucoup répété qu’il fallait trouver l’équilibre entre le « je », le « nous deux » (lorsque l’on vit en couple) et le « nous du collectif » pour ne pas s’oublier et ne vivre qu’au service du collectif.

[La fatigue dans les oasis, parlons-en !](https://cooperative-oasis.org/fatigueoasis/)

---

> Je me demande pourquoi Pixel­fed n’a pas été conçu comme un front-end aux serveurs Mastodon existants, plutôt qu’en réimplémentant un serveur moins complet à côté.

[Un peu de Pixel­fed](https://n.survol.fr/n/un-peu-de-pixelfed)

Tiens, une idée de projet amusant :)

---

Tiens, ça fait longtemps que je n'avais pas entendu parlé de Perl... Et pourtant il se passe des trucs intéressant on dirait 

[Raku en 2020](https://linuxfr.org/news/raku-en-2020)

Je note qu'il y a deux femmes sur la liste des personnes important pour ce langage :
-  Elizabeth Mattijsen : très impliquée dans Perl 6 ;
-  Audrey Tang : actuellement ministre du numérique à Taïwan (!), anciennement développeuse principale de Pugs (implémentation de Perl 6 en Haskell, voir [vieille interview](http://www.perlcast.com/2006/03/interview-with-audrey-tang.html) ou encore cette [interview](https://andrewshitov.com/2015/05/05/interview-with-audrey-tang/));

Est-ce que je fais plus attention à ça maintenant ou bien la présence de femme autour de la création d'un langage est « nouveau » ?

---

Je peine à reprendre mes journaux pour les publier ; en plus de revoir un peu les formulations, l'orthographe, je prends le temps de vérifier un peu les sources, les liens... Et surtout, j'essaie de « classer » les contenues... Et ça, sa me semble pas forcement une bonne idée.

Je crois que je préfère, dans un premier temps, publier comme ça, en laissant les dates, un fichier par semaine. Puis, plus tard, je pourrais construire une histoire qui regrouperait divers notes pour les consolider dans une autre page, un peu comme [mission bancale](https://elsif.fr/une-mission-bancale) et le [journal d'un formateur](https://yaf.github.io/journal-d-un-formateur-en-2015/). Ça aurait sans doute plus de sens, et ça serait plus simple pour moi.

Il y aurait le temps présent, avec la publication assez rapide de mes journaux. Puis le temps long où je consolide certaines histoires.


## Lundi 23

J'ai l'impression de pousser un chariot à roue carré. Et de ne pas prendre le temps d'installer des roues rondes.

Mon installation wifi n'est pas optimum, et je ne prends pas le temps de l'améliorer ; nous avons acheté une imprimante (ou bien [Scopyleft](http://scopyleft.fr/) ?) mais je ne l'ai toujours pas installé (faut dire, avec linux, c'est pas simple les imprimantes :p)...

Et par dessus ça, il me reprend l'envie de basculer sur [OpenBSD](https://www.openbsd.org/), avec [windowmaker](https://www.windowmaker.org/)... À se demander si je n'ai pas une sorte d'envie de surcharge mentale en ce moment :-/

Il me manque sans doute un peu de méditation, d'exercice physique, de promenade...

Les orientations possible pour [RDV-Solidarités](https://www.rdv-solidarites.fr/) sont vraiment sympa. Je pense notamment à l'aspect construction d'un outil de prise de rendez-vous généraliste.

C'est triste d'apprendre que d'autres se sont lancé à coder un truc de leur coté, mais c'est le jeu. Tant pis.

---

- Recenser des usages, des recettes ;
- Reconnaitre les plantes ;
- Rencontrer les guérisseuses, les shamans, les druides ;
- Documenter les pratiques, les plantes.

Mais comment tester ? Vérifier ? Est-ce que le temps qui passe me soigne aussi bien que la plante ?

[La médecine verte : médecine magique ou scientifique ?](https://www.franceculture.fr/emissions/les-nuits-de-france-culture/la-medecine-verte-medecine-magique-ou-scientifique)

---

> Ce texte, issu d’un long dialogue de travail, présente le savoir ethnobotanique de Maoulida Mchangama, habitant de Mayotte, mis en forme par Pascale Salaün en poste pendant plusieurs années dans l’île. Cette présentation part d’une soixantaine de maladies courantes dans la nosographie locale et cite les plantes et la manière dont elles sont utilisées pour les soigner.

[Recueil d’une pharmacopée à Mayotte](https://journals.openedition.org/oceanindien/1770)

---

[Une approche de la Médecine traditionnelle à Mayotte: Des plantes en question](http://www.ethnopharmacologia.org/prelude2016/pdf/biblio-hl-21-lartigau-roussin.pdf)


## Mardi 24 novembre

Note de lecture de [L'art d'habiter la terre - La vision biorégionale](https://wildproject.org/livres/lart-dhabiter-la-terre) de Kirkpatrick Sale

Dans l'avant propos de Mathias Rollot, il cite Lewis Mumford :

> si quelques chose peut arrêter la désintégration totale de la civilisation mondiale aujourd'hui, ça passera par un miracle : la redécouverte de l'échelle humaine décrite dans l'ouvrage encyclopédique de Kirkpatrick Sale

Est évoqué un ouvrage dont le titre m'interpelle
[Boundaries of Home: Mapping for Local Empowerment (The New Catalyst Bioregional)](https://www.goodreads.com/book/show/1456476.Boundaries_of_Home)

> Pour ces peuples premiers, le milieu environnant et ses composants - rivières, arbres, nuages, sources, montagnes - étaient perçus come vivants, dotés d'esprits et de sensibilités en tous points similaires à ceux humains : cette présence est ce que les Iroquois appelaient _orenda_, l'invisible force inhérente à toutes les parties de l'univers, c'est ce que certaines langues bantoues nomment _mata_.

> Les animaux avaient un esprit, évidemment, de sorte que dans toutes les sociétés de chasseurs, des formes de rituels d'excuses et de pardon étaient nécessaires avant la chasse : ainsi, les Navajos priaient le cerf avant de le tuer, les Mbutis se purifiaient par la fumée chaque matin, les Innus promettaient à l'animal chassé « toi et moi nous avons le même esprit et la même âme ». Les plantes, les fleurs et les arbres avaient des esprits, e étaient eux aussi tout aussi capables de sentir les choses, de sorte que la plupart de ces peuples premiers avaient élaboré des cérémonies liées à la coupe et à la récolte où ils demandaient à être pardonnées pour avoir retiré à la Terre Mère quelques-uns de ses enfants.

> D'une manière ou d'une autre, pourtant, la situation dégénéra. Peut-être est-ce venu de l'_extérieur_, par la faut de ceux qu'on appela les Doriens venus des steppes d'Europe, ces Indo-Européens qui vénéraient des divinités mâles. Comme l'énonça l'historien de l'art Vincent Scully, ces derniers semblent en effet avoir éliminé «le vieux concept de la domination de la déesse de la Terre elle-même, récupérant le pouvoir souverain par le biais de leur propre dieu du ciel, Zeus, le lanceur d'éclair », et détruisirent «le rapport simple et ancien, presque végétal, qui existait entre l'humain et la nature ».

> Ou bien alors est-ce venu de l'_intérieur_, par la faut de la décadence et du manque de soin, de la pure arrogance de la culture mycénienne, qui comme toutes celles qui dans leurs derniers jours ont trop grandi, se sont trop étendues, devenant alors plus intéressées par l'exploitation et la domination que par l'entretien et la durabilité, par la richesse offerte par les minéraux précieux plus que par la richesse des sols, par la préservation de la bureaucratie et de la hiérarchie plus que par les écosystèmes et les habitats.

Une dent contre les scientifiques 

> Doucement mais sûrement, les idées portées par le paradigme scientifique se développèrent et transformèrent complètement les attitudes de la société occidentale à l'égard de la nature et du cosmos. La nature n'était plus belle ou effrayante, elle était simplement _là_, non pour être célébrée ou adorée, mais le plus souvent pour être _utilisée_, avec tout l'ingéniosité et l'instrumentalité de la culture scientifique - avec précaution parfois, sans réserve d'autres fois, de façon limitée s'il le fallait, quoi qu'à choisir plutôt de manière illimitée, bref, elle était utilisée _par_ les humains, _pour_ les humains.

> On considère généralement, à raison, que la révolution scientifique débuta avec la mise au point du premier microscope (...). Tandis que les premières sociétés arrivèrent probablement à l'idée d'une Terre vivante suite à la contemplation des entrelacs du cosmos, des mystères du ciel rempli d'étoiles, et du miracle des saisons, les expérimentations menées par la science moderne se firent les yeux baissés, penchés sur les plus minuscules et simple formes possibles, observées par le biais d'une petite machine aisément manipulable. **Il y a quelque chose de troublant dans cette image - d'autant pus quand nous réalisons que le scientifique penché sur le microscope est en vérié à moitié aveugle. C'est dans la naissance de cette instrument que nous pouvons voir la mort de Gaea**.

> Impossible toutefois de rester aujourd'hui inconscients des lacunes, des échecs et des dangers fondamentaux inhérents à la science occidentale. **C'est dans la mort de Gaea et la transformation de nos relations à la nature que se trouvent les menaces les plus dangereuses**.

Référence à l'école de Francfort:
> la domination de la nature implique la domination de l'être humain (Horkheimer)

> Et si aujourd'hui nous voyons la Terre comme une zone statique et neutre, altérable à souhait par nos produits chimiques et contrôlable à loisir par nos technologies ; si nous nous regardons comme une espèce supérieure ayant le droit de tuer autant de centaines d'autres espèces que nous le souhaitons et de dominer le reste ; si nous croyons avoir le pouvoir de réorganier les atomes de la Nature et de réassembler les gènes, d'inventer des armes et des machines nourries avec une énergie créée de toutes pièces et capables de détruire la plupart des formes de vie pour toujours ; si nous construisons des technologies capables de piller les ressources de la planète, souillant ses écosystèmes, empoissonnant son air, et altérant ses processus vieux de plusieurs éons pour convenir à nos désirs ; si c'est cela, notre condition actuelle, c'est avant tout **parce que, plutôt que de remettre en cause la vision scientifique, nous l'avons presque entièrement acceptée**. Nous sommes les produits évidents de cette expérimentation de 400 ans qui a laissé le monde sens dessus dessous - et nous a conduits à la crise actuelle

> Non, la tâche n'est pas d'extirper la science, mais de l'incorporer, non pas de la mettre à l'écart mais de la contenir, non pas d'ignorer ses moyens, mais de mettre en question les fins aux services desquelles nous la mettons en œuvre. La tâche est d'utiliser ses outils indéniablement utiles au service d'une intention différente : **au service de la préservation plutôt que de la domination de la nature**.

> l'autonomie - pas tant à l'échelle de l'individu qu'à celle de la région - est inhérente au concept de biorégion.

> Savoir, apprendre, développer, se libérer - voilà quelques-uns des processus centraux de l'idée biorégionale.

> Le seul moyen pour que les gens adoptent un « bon comportement »et agissent de manière responsable, c'est de mettre en évidence le problème concret, et de leur faire comprendre leurs liens directs avec ce problème - et cela ne peut être fait qu'à une échelle limitée. C'est-à-dire que cela peut être fait lorsque les forces du gouvernement et de la société sont encore reconnaissables et compréhensibles, lorsque les relations entre les choses sont encore intimes et les effets des actions individuelles encore visibles ; lorsque l'abstrait et l'intangible s'effacent pour laisser place à l'ici et au maintenant, à ce que l'on voir et ce que l'on sent, au réel et au connu.

> Si je commence cette discussion sur e paradigme biorégional avec le concept de l'échelle, c'est parce que je suis persuadé qu'il est, au fond, le seul déterminant critique et décisif dans toutes les constructions humaines, qu'il s'agisse de bâtiments, de systèmes ou de sociétés.

> À la bonne échelle, le potentiel humain est libéré, la compréhension humaine magnifiée, les accomplissements humains multipliés. J'argumenterais que l'échelle optimale est biorégionale, ni trop petite pour risquer d'être impuissante ou appauvrie, ni trop grande pour devenir pesante ou impérieuse, une échelle à laquelle enfin le potentiel humain peut fonctionner de pair avec la réalité écologique.

> Un aspect fondamental au sujet de cette mosaïque fut suggéré il y a une décennie par les auteurs Britanniques de l'ouvrage qui devint par la suite un _best-seller_ dans le monde entier, _Changer ou disparaître. Plan pour la survie_
> Bien que nous pension que les petites communautés devraient être l'unité de base de la société, et que chaque communauté devrait être aussi autonome que possible, nous voudrions souligner ce fait que nous ne poposons nullement qu'elles soient centrées sur elle-mêmes, égocentrées, ni en aucun cas fermées au reste du monde. Il devrait y avoir un réseau de communication efficace et sensible entre toutes les communautés, de manière à ce que les préceptes de ase de l'écologie, comme l'entrelacement des choses et les effets lointains des processus écologiques et de leurs perturbations, puissent influer sur les prises de décisions communes.

> Si l'économie biorégionale devait commencer quelque part, ce serait logiquement là : elle devrait _en premier lieu_ chercher à maintenir le monde naturel plutôt qu'à l'utiliser, à s'adapter à l'environnement plutôt qu'à essayer de l'exploiter ou le manipuler, à _conserver_ non seulement les ressources, mais aussi les relations et les systèmes de ce monde naturel. Et c'est seulement _en second lieu_ qu'elle devrait chercher à établir des moyens de productions et d'échanges _stables_, en abandonnant ceux toujours en mouvement, dépendant d'une croissance et d'une consommation perpétuelles au service de ce que l'on nomme « le progrès », un dieu faux et illusoire pour peu qu'il en soit un.

> La _durabilité_ et non la _croissance_ serait son objectif (à l'économie biorégionale)

On évoque aussi la monnaie locale. Un point sur lequel j'aimerais me pencher aussi !

> La leçon à retenir pour l'économie biorégionale est assez évidente : doivent disparaître le marché de notre économies capitaliste conventionnelle, de même que l'accent mis sur la compétition, l'exploitation et le profit individuel.

> La notion d'économie coopérative plutôt que compétitive est si différente de l'expérience occidentale des 500 dernières année qu'elle est en fait difficile à imaginer.

> Considérer la valeur des biens selon leur utilité ou leur beauté plutôt que sur leur coût ; échanger des produits selon le besoin et non selon la valeur de l'échange ; assurer une distribution à l'ensemble de la société sans se préoccuper du travail effectué par chacun ; accomplir le travail indifféremment des idées de rendement, de salaire ou de bénéfice individuel et même souvent indifféremment de la notion même de travail.

> Le célèbre avertissement **« Laisse les choses tranquilles »** n'était pas seulement une mise en garde à l'égard des humains, les incitant à montrer un certain respect pour les travaux de la nature, mais, dans le contexte du _Tao te king_, il s'agissait d'un véritable conseil pour les princes et seigneurs de guerre chinois du 6e siècle avant J.-C. : la meilleure option de gouvernement ne serait pas seulement l'adoption d'un gouvernement le moins actifs possible, semble dire Lao Tseu, mais serait l'absence complète de gouvernement.

Je n'avais pas suivi que Lao Tseu était un anarchiste d'avant-garde !

> Une vision politique qui serait fondée sur les lois et mécanismes évidents du monde biotique ne célébrerait pas la coordination centralisée, ni l'efficacité hiérarchisée ou la force monolithique - les vertus apparentes de l'État-nation moderne -, mais plutôt l'exact opposé, à savoir la décentralisation, l'interdépendance et la diversité. Au sein de n'importe quel parc, sur n'importe quel rivage, dans n'importe quel bois, les principes naturels se réalisent essentiellement sans coercition, sans pression organisée, sans autorité reconnue. Ils sont, pour choisir le mot le plus proche au sein de notre vocabulaire inadapté, _libertaires_.

> « Le roi de la jungle » correspond à _notre_ description du statut du lion et elle est à la fois anthropomorphique et perverse ; lion et lionne sont dans l'ignorance profonde de ce rôle.

> le premier lieu des prises de décisions, de contrôle politique et économique devrait être la communauté, c'est-à-dire un groupement plus ou moins intime, soit à l'échelle du village dense d'environ 1 000 habitants, soit probablement plus souvent, à l'échelle de la communauté étendue de 5 000 à 10 000 habitants qui s'avère être très régulièrement l'unité politique fondamentale. C'est dans un tel lieu - où les ens se connaissent entre eux et connaissent l'essentiel de l'environnement qu'ils partagent, où, au minimum, les informations les plus basiques relatives à la résolution de problèmes sont connues ou facilement disponibles - que la gouvernance devrait prendre racine.

> La loi corrélée sur le plan écologique - et logique - à la loi de la force centrifuge est celle de la complémentarité ou de la mutualité. Selon cette loi, les membres d'une même espèce appartenant à une éco-niche donnée agissent de manière réciproque et non hiérarchique pour promouvoir et défendre leur communauté. Le terme employé par les écologistes pour décrire ce phénomène est « hétarchie » (_hétarchy_). Ce terme a été choisi en opposition à celui de hiérarchie et signifie _distrinction sans ordre de priorité_, confirmant, par exemple, que le bleu est clairement différent du jaune, mais qu'il ne lui est pas supérieur pour autant.

> La reine des abeilles après tout n'est qu'un prodigieux organe reproducteur qui sert là ruche un temps donnée et elle n'a de « reine » que son titre, que nous lui avons octroyé, tandis que les abeilles ventileuses ou manutentionnaires la considèrent certainement bien autrement.

> Il en est de même avec de nombreux mots de notre vocabulaire politique que nous utilisons pour décrire certains animaux - mâle « dominant », « roi de la colline », « castes dominantes », « esclaves », « ouvriers » et bien d'autres - qui en disent plus sur la culture occidentale que sur les comportement animaliers.

> Comme le souligne Murray Bookchin dans son importante étude philosophique, _The Ecology of Freedom_, « les semblants de traits hiérarchique qu'on pourrait penser trouver chez de nombreux animaux sont à comprendre comme les relations variables d'une chaîne plutôt que comme des stratifications organisées du type de celles que l'on trouve au sein des sociétés et des institutions humaines ».

> L'histoire de Geronimo illustre merveilleusement cette question. Il s'agissait d'un brave et courageux guerrier, vainqueur de nombreuses batailles contre les Mexicains, respecté en tant que chef militaire. En réalité, il n'était pas un « chef » puisqu'un tel statut n'existait pas chez les Apaches et il n'était en rien une personnalité politique, pas plus qu'il n'avait le pouvoir de commander quoi que ce soit en dehors des champs de bataille. Jusqu'au jour où après une victoire il essaya de s'élever au rang de chef permanent de la tribu : il fut immédiatement rejeté et même chassé de la tribu.
> Pourquoi est-il donc présenté dans les livres d'histoire comme « un chef des Apaches » ? Parce que les immigrants et leur rigidité ont supposé que n'importe quelle personne menant un commando contre eux était forcément une figure détenant un pouvoir politique, un équivalent indien du roi Arthur ou de Richard Ier et ils lui appliquèrent sans réfléchir leur titre habituel de commandant pour les « sauvages » : rien de plus compliqué que cela.

> Dans notre monde contemporain, bien que la complexité des sociétés mondiales puisse parfois paraître étourdissantes, la tendance flagrante dans les sphères culturelles, économiques et politiques est à l'uniformité et au monolithique.

> La culture industrielle, au nom de l'efficacité, de la modernité ou de l'économie vise l'uniformisation, l'interchangeabilité, la routinisation et la conformité ; elle aspire à rendre unique la langue, la monnaie, la bourse, le gouvernement, le système de mesure, le type de musique populaire, la manière de pratiquer la médecine, l'architecture des immeubles de bureaux, le type d'université. Le courant semble absolument non durable et pourtant ses signes sont visibles partout : dans des nations entières qui s'en remettent à un produit unique, des villes à une industrie unique, des fermes à une culture unique, des usines à un article unique, des personnes à un métier unique et des métiers à un geste unique.

> Il s'agit du meilleur moyen pour arriver non pas à la stabilité mais à la précarité, non pas à l'autonomie mais à l'esclavage, non pas à la bonne santé mais à la maladie.

> De belle symbiose sont aussi à l'œuvre dans l'une des associations les plus complexes du monde animal : la famille humaine. D'ordinaire, la femelle aînée se consacre souvent à plusieurs années d'accouchements, d'allaitements et d'attentions, pendant que le mâle se consacre à l'approvisionnement et à la protection de sa famille, de manière à élever une progéniture qui met plus d'une décennie à parvenir à maturité, et dont on attend aussi qu'elle rende la faveur de cette éducation soigneuse en prodiguant à ses parents, dans les dernières années de leurs vies, soutiens et provisions, accompagnés de ce mystérieux élément rendu parce qu'il a été donnée - l'amour.

> Pour ce qui est de la taille de la ville écologique idéale, j'ai entendu diverses estimations, de 25 000 à 250 000 habitants. La variations dépend aussi, bien sûr, de la quantité de terres arables dans les environs immédiats que la ville pourrait utiliser pour l'alimentation et la biomasse, et de sa dépendance à la campagne.

> Pour trouver salubrité et équilibre, la nature divise plutôt qu'elle n'unit, elle réduit ls échelles plutôt qu'elle ne les agrandit. Comme le dit l'économiste Leopold Kohr dans son analyse attentive de ce phénomène : 
>         Si la petitesse est un des mystérieux signes naturels de la santé et la grandeur un signe de maladie, la division doit donc nécessairement représenter un principe de soin. La division représente non seulement le principe de soin mais aussi de progrès. La seule façon de restaurer un équilibre sain face aux conditions désastreuses semble donc de passer par la division des unités sociales qui ont grossi dans des échelles ingérables.

> Un mouvement politique ne peut pas aller contre le courant de sa société bien longtemps : s'il essaye, il peut au mieux créer un contre-courant temporaire, produire quelques brefs remous de contrariété ou s trouver coincé dans des tourbillons inoffensifs et contraignants à la fois. Il peut se détourner du courant principale, il peut faire son propre chemin et même créer ses propres canaux, mais il ne peut inverser ou nier le courant dans lequel il vit. C'est pourquoi la plupart des révolutions échouent et celles qui semblent réussir ont toujours un petit quelque chose de la société qui les a engendrées - certaines seulement réussissent à créer plus que ce qu'elles ont eu prétention à dissoudre -, les Bourbons pour Napoléon, le tsar pour Staline, l'empereur pour Mao.

> Il (le projet politique du biorégionalisme) est ancré et, en un certain sens, inspiré par la recrudescence féministe mondiale des dernières décennies, exprimant les traits nourriciers, holistiques, de communauté et de spiritualité qui ont marqué sa philosophie

Autant je suis plutôt ok sur le fait que je retrouve des valeurs féministe dans cette histoire de biorégionalisme, autant, je ne suis pas certains d'être en accord avec cette histoire de traits nourriciers et holistiques...

> Puisque la tâche consiste à faire connaître aux gens leur biorégoin et les menaces qui pèsent sur elle, les organisateurs peuvent demander aux classes de sciences du lycée d'examiner la rivière locale et d'étudier son biote ; aux groupes de conservation locaux de faire une enquête sur les arbres dela région ; ...

> Si le biorégionalisme semble réalisable, c'est ensuite parce que ses concepts sont si fondamentalement accessible qu'il peut toucher chacun par-delà les frontières habituelles des luttes sociales.


Un peu long, mais pour un livre prit « au hasard » à la médiathèque, je suis plutôt agréablement surpris, une belle découverte. Beaucoup de chose me parle malgré un petit fond qui me laisse méfiant. Et c'est peut-être sain :)


---

Et si j'arrêtais Twitter ? Et si j'arrêtais Linkedin ?

Autant je trouve une utilité à Facebook dans la participation à des groupes, autant, sur Twitter, je me retrouve souvent à lire des contenu énervant, frustrant, révoltant. Et linkedin me berce d'illusion que je pourrais trouver une mission plus facilement, alors que je sais que ça ne se passe pas comme ça. Linkedin n'est qu'un outil permettant d'exposer son CV en ligne, d'exposer son parcours. Et si je le faisait sur mon site, que je m'arrangeais pour que mon site soit dans les premier liens qui apparaissent dans les résultat de recherche ?

---

Tellement vrai

> Emilien Pecoul @Ouarzy
> My job as a #SoftwareDeveloper is to understand someone else job in order to explain it to a computer.

[évoqué sur sur twitter](https://twitter.com/Ouarzy/status/1331000647202779136)


## Mercredi 25 novembre

Et voilà, je n'ai pas supprimer mon compte Twitter, mais je n'y suis plus personne, et je vais arrêté d'aller voir ce qu'il se passe dessus. Nous verrons bien. Prochaine étape Linkedin ?

---

Une association de logiciel libre à Mayotte [lagom](http://lagom.mayotte.free.fr/index.html). Toujours en vie ? J'envoie un message pour voir.

---

Les types me semble une bonne chose en programmation.

Cependant, l'évolution de certains langages, allant vers un changement de système de typage me fait un peu bizarre. J'ai peur de dénaturer le langage.

- Php, devenue objet à perdu de sa simplicité ;
- Java, intégrant une dimension fonctionnelle créer une grande confusion de lecture ;
- Ruby, avec un système de type statique qui pointe le bout de son nez va lui faire perdre sa dimension fun et malléable.

Alors ce n'est que mon avis, et peut-être que c'est très bien.

Peut-être que c'est important pour les langages d'évoluer dans ce genre de direction. Mais je trouve aujourd'hui, vu le nombre de langages disponible, qu'il est un peu dommage de vouloir élire le meilleure système de typage et le glisser dans tout les langages, élire le meilleur paradigme, et transformer chaque langage pour qu'il l'intègre.

En gardant sa spécificité, un langage garde un intérêt particulier par rapport aux autres. Est-ce que ruby continuera à être un bon langage pour faire de la méta-programmation avec un système de typage statique ?

_Merci Pierre pour l'article [Experiments with Gradual Typing in Ruby – Part 1](https://kemenaran.winosx.com/posts/experiments-with-gradual-typing-in-ruby-part-1/) qui m'a ouvert les yeux sur une direction que prends Ruby._


---

NextCloud et autre CozyCloud c'est chouette pour s'émanciper de GoogleDrive. Mais pour faire un truc bien décentralisé, j'aime bien [Syncthing](https://syncthing.net/) Ça avait plutôt bien marché pour moi quand j'étais chez [/ut7](http://ut7.fr/).

J'aimerais bien le remettre en place pour ma famille, et voir comment ça se passe pour des personnes qui passe moins de temps sur leurs ordinateurs.

---

Un article intéressant à propos du [Ph du sol](http://www.monjardinenpermaculture.fr/pages/le-ph-du-sol)

---

> Le cynnorhodon, le « fruit » de l’églantier (_Rosa canina s.l._) est extrêmement riche en vitamine C, un composé important dans le fonctionnement des défenses naturelles de notre organisme. On le prépare en infusion à température inférieure à 70° C à raison de 4 ou 5 « fruits » par bol, pas plus.
>
> Les tisanes de bourgeon de pin (_Pinus sylvestris_), la sommité fleurie d’hysope (_Hyssopus officinalis_), de lierre terrestre (_Glechoma hederacea_), la feuille de plantain lancéolé (_Plantago lanceolata_), la feuille d’eucalyptus globuleux (_Eucalyptus globulus_) facilitent la respiration et l’expectoration lorsque les bronches sont encombrées. Une pincée par tasse de 20 cl d’eau bouillante laissée 10 mn à infuser à raison de 3ou 4 tasses par jour.
>
> Enfin la tisane de feuille de mélisse (_Melissa officinalis_) est sans danger ni contre-indication, à raison d’une pincée par tasse d’eau frémissante deux ou trois fois par jour, (ou bien quelques feuilles de mélisse ajoutées aux exemples de tisanes précédentes) peut nous aider à gérer le stress qui nous atteint toutes et tous.
>
> Nous vous donnons volontairement ces quelques exemples pour que vous puissiez vous orienter vers des ressources locales.
>
> Le plantain, le bourgeon de pin, le lierre terrestre sont des plantes assez souvent communes et pour lesquelles il n’y a pas de confusion probable.
>
> Les sites suivants permettent d’identifier ces plantes et de savoirs si elles sont éventuellement rares ou protégées dans votre région :
> - [tela-botanica.org](www.tela-botanica.org)
> - [florealpes.com](www.florealpes.com)
>
> Toutefois, prenez votre temps, essayez de faire valider vos cueillettes éventuelles par un producteur-herboriste (envoyez des photos) il ou elle vous dira si oui ou non vous pouvez consommer en sécurité. En cas de doute abstenez-vous.
>
> Prenez soin de vous et des autres,

[herbes de vie](https://www.herbesdevie.com/)

_avec en prime un ascii art dans les commentaires du code source de la page (photo prise)_

---

[Open UI - The missing industry standard definition of UI](https://open-ui.org/)

> The purpose of Open UI to the web platform is to allow web developers to style and extend built-in web UI controls, such as <select> dropdowns, checkboxes, radio buttons, and date/color pickers.

---

[Ils produisent du lait « qui ne tue pas la chèvre »](https://www.ouest-france.fr/economie/agriculture/ils-produisent-du-lait-qui-ne-tue-pas-la-chevre-7006326)
Où je découvre que manger du fromage de chèvre tue (en général) la chèvre :-/

---

## Jeudi 26

[Usage Commun](https://usagecommun.fr/)

J'ai posé ça en marge, mais peut-être que j'aurais pu le partager sur #general et parler stratégie Scopyleft
Nous pourrions rejoindre cette SCIC en tant que personne morale
Je me suis inscrit pour participer à la constituante, à titre individuel pour commencé (ils font chier à mettre ça un mercredi par contre, ça sent les personnes sans enfants ou qui ne s'en occupe pas le mercredi ...)
J'ai l'impression que c'est ce que pourrait ou aurait pu devenir beta.gouv pour s'émanciper des dimensions politique qui mine un peu la situation actuelle de certaines équipes

---

_À propos de mettre en commun un outil de prise de rendez-vous_

> En fait, c’est pas si simple d’avoir un brique commune.
>
> Bien sur, la base est la même : un usager qui prend un rendez-vous sur une plage d’ouverture d’un agent.
> La difficulté va porter sur la « personnalisation » de chacune de ces parties. En faire une structure commune nécessiterais un paramétrage très complexe. Alors que mettre en place ce système basique est assez simple / rapide.
>
> Peut-être que le plus simple, c’est de rester sur une base de code séparé, et de discuter des points communs, pour éventuellement, extraire et réutiliser des briques communes : par exemple, l’agenda, les notifications…
>
> – Yannick


---

[Union Bretonne pour l’Animation des PAys Ruraux](https://www.ubapar.bzh/)

[Infini - Hébergeur libre, associatif, solidaire, non marchand, militant](https://www.infini.fr/)

---

[Ecole Lyonnaise de Plantes Médicinales](https://www.ecoledeplantesmedicinales.com/) Je crois que c'est l'école qu'a fait Christine.

[École Bretonne d'Herboristerie - Capsanté](http://www.capsante.net/wordpress/?page_id=357)
[Institu François d'Herboristerie](https://www.institut-francais-herboristerie.fr/)
[Fédération Française des écoles d'herboristerie](http://www.ffeh.fr/enseignement-ecoles-francaises-herboristerie.php)

---

Pour ne pas perdre la trace de ce super logiciel libre : Codimd change de nom et deviens [HedgeDoc](https://demo.hedgedoc.org/)

---

Après Miro, je découvre aujourd'hui [Klaxoon](https://klaxoon.com/fr/) lors de l'atelier CNV.

Très chouette atelier d'ailleurs. Je voulais voir si je connaissais la CNV comme j'en ait le sentiment, ou si j'en était loin. Bref, j'avais envie, besoin, de vérifier un truc.

J'ai appris des choses nouvelles, mais je n'était pas trop dans le faux.

La structure est un truc qui n'était pas clair pour moi. Oui, j'avais en tête l'observation des faits, la recherche des sentiments, et l'analyse des besoins (souvent ce point m'échappe par contre), ainsi que la demande.

Ceci étant, je n'avais pas forcement en tête l'ordre structuré, les listes permettant de s'aider à retrouver un sentiment ou un besoin.

Et particulièrement, je n'avais pas forcement en tête les structures de demande. Particulièrement, j'ai pu vérifier que c'était OK que je formule des demandes à moi même. Il y a part contre quelque chose à creuser sur le faire que je n'arrive pas vraiment à faire des demandes au autres, que je les tournent toujours vers moi.

Une piste peut-être, nous ne sommes pas obligé de faire des demandes d'actions. C'est peut-être un point qui me bloquait. J'envisage plus facilement de partager mon état, mon ressenti, et de demander à ce qu'il soit entendu que de formuler une demande où il faudrait que la personne en face de moi se mette en action.

J'ai également pu me rassurer sur le fait que la CNV c'est surtout un état d'esprit, une façon de vivre, plus qu'un protocole strict. C'était une intuition, ça me fait plaisir de la voir confirmé.

Et c'était sympa de revoir Julie rapidement :)

[Marshall Rosenberg](https://fr.wikipedia.org/wiki/Marshall_Rosenberg) créateur de la CNV. Une découverte pour moi :)

---

[Hacking Social](https://www.hacking-social.com/), le retour. Lors d'une discussion avec Élodie à propos d'empathie versus compassion. [Qu’est-ce que la compassion ?](https://www.hacking-social.com/2020/05/25/quest-ce-que-la-compassion/)

Du coup, retour de ce site dans mon lecteur de flux.

## Samedi 28

> Luc : Voilà ! Tu n’assumes pas ! Aujourd’hui une page moyenne sur Internet est plus lourde que ce jeu qui a occupé des gens et sur lequel on a bossé, etc. Récemment, il y a eu un autre exemple où des gens ont réussi à faire tourner Doom, c’est pour ça que c’est une unité de mesure, sur un test de grossesse.
>
> Manu : Un test… Le petit bâton qu’on doit mettre sous l’urine pour vérifier…
>
> Luc : C’est ça, dans lequel il y a une petite puce, je ne sais pas comment ça marche, mais il y a un petit peu d’électronique embarquée et les gens ont mis dessus et ont dit « est-ce qu’on peut faire tourner Doom ? » Il y a un petit écran, minuscule, du coup ils ont réussi à faire tourner Doom dessus.
>
> Manu : Waouh ! On fait vraiment n’importe quoi avec son temps aujourd’hui.
>
> Luc : Ce qui démontre qu’aujourd’hui on a évidemment une puissance de calcul très importante, mais la taille des logiciels a explosé. Par exemple, je me souviens qu’au début des années 2000, il y avait toute la question de la gestion des bugs. C’est-à-dire qu’avant, quand les logiciels étaient plus petits, on était dans un mode de développement plutôt artisanal.

[L'informatique est-elle devenue trop compliquée à développer ?](https://www.april.org/l-informatique-est-elle-devenue-trop-compliquee-a-developper-decryptualite-du-26-octobre-2020)

---

Emprunt et lecture de la BD [Le Tirailleur](https://www.futuropolis.fr/9782754808927/le-tirailleur.html). J'ai beaucoup apprécié.

Ça rend visible une bonne partie de ce qui n'est jamais dit. Ça raconte l'histoire d'un paquet de personnes qui sont montré du doigt, et dont les enfants grandissent dans un environnement qui ne les aides pas.

Nous pourrions raconter ces histoires au collège, en cours d'histoire. Voir d'éducation civique. Ça me semble nécessaire d'aborder cette autre histoire., Celle des colonies et de l'enrôlement de force.

> la seule chose dont on ne peut être privé, c'est du temps qui reste


