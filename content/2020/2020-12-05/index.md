---
title: Samedi 5 décembre 2020
---

[Nombril de vénus (_Umbilicus rupestris_)](https://fr.wikipedia.org/wiki/Umbilicus_rupestris)

> Plante vivace, regorgeant d'eau, le nombril de Vénus ou Umbilicus rupestris pousse en abondance dans des terrains ombragés, rocheux et rocailleux.

> Propriétés thérapeutiques
>
> - Reins : diurétique, calculs rénaux
> - Peau : adoucissant, émollient, cicatrisant (brûlures)
> - Résolutif : cors aux pieds
> - Régulation hormonale : ménopause

> Usages populaires
>
> Affections cutanées - anti-inflammatoire : ulcères, plaies, furoncles, hémorroïdes, brûlures
>
> Utilisations et posologie
>
> Jus de Nombril de Vénus - diurétique, adoucissant, émollient : prendre une cuillère à café par jour.
> Usage externe - cataplasmes : application du suc pressé sur les plaies, les brûlures, les furoncles, les ganglions lymphatiques enflés.


> Une plante comestible
>
> Plante aromatique, le nombril de Vénus est comestible. Les feuilles, crues, permettent de relever la saveur de certains plats froids (salades).

> Principes chimiques actifs
>
> Les feuilles renferment du nitrate de potassium.
>
> La plante contient également des tanins et des sels minéraux (fer) en abondance.


[Nombril de vénus (Umbilicus rupestris): vertus et propriétés sur le site Doctonat](https://doctonat.com/nombril-venus-vertus/)



> Attention : une autre plante, Omphalodes cappadocica est également connue sous le nom vernaculaire de nombril de Vénus. Elles n'ont pourtant pas grand-chose en commun.

[Nombril de Vénus sur Ooreka](https://jardinage.ooreka.fr/plante/voir/1651/nombril-de-venus)


> - Récolte : 10 min
> - Cuisson : 5 min
> - Préparation : 10 min
>
> Ingrédients :  (pour 1 bocal d'un litre et demi)
>
> - 1 l d'eau
> - 25 cl de vinaigre d'alcool
> - 500 g de Nombril de Vénus
> - 1 branche de Thym commun
> - 1 branche de Romarin
> - 4 gousses d'ail
> - 10 grains de Poivre noir
>
> Préparation :
>
> Récolter les jeunes pousses de nombril de Vénus à la fin de l'hiver quand elles sont encore vertes.
> Faire bouillir l'eau et le vinaigre.
> Remplir un bocal avec les nombrils de Vénus, l'ail, le romarin, le thym et le poivre en tassant un peu.
> Verser l'eau bouillante par-dessus, secouer le bocal pour faire remonter l'air encore emprisonné, remettre de l'eau jusqu'au bord et fermer hermétiquement.
> Attendre un mois avant de déguster.

[Recette de nombril de vénus au vinaigre](http://garrigue-gourmande.fr/index.php?option=com_content&view=article&id=2008&Itemid=209)

---


> Les interfaces alternatives pour consulter des sites populaires dans de meilleures conditions se multiplient.
> https://teddit.net/about vous permet de lire Reddit sans être pisté⋅e et sans avoir de réclames. C’est léger, ne nécessite pas JavaScript et naturellement libre et auto-hébergeable 😻
>
> (rappel : vous pouvez aussi utiliser invidious pour Youtube, nitter pour Twitter, bibliogram pour instagram…

[clochix 😾 @clochix@mastodon.social](https://mastodon.social/@clochix/105326353784451383)

---

C'est marrant comme il y a des idées qui fleurissent simultanément :)

Élodie voulais postulé à une offre en binôme avec Antoine. Et Thomas fait de même avec Valentine (lire [Candidater à deux](Journal : Candidater à deux)).

C'est amusant de mettre ça en perspective avec l'histoire de /ut7 où nous essayons de nous « vendre » en binôme. Ça ne fonctionnais jamais.

Qu'est-ce qui rends la proposition plus acceptable ici ? Les mentalités ont sans doute un peu évoluées. Je crois surtout que c'est un schema financier. C'est ok pour moi de payer un prix un peu élevé pour une personne si en fait elles sont deux (ou presque deux).

Ce que j'apprécie dans cette proposition, c'est aussi que ça rassure un peu tout le monde je crois. Les personnes accueillants qui du coup aurons un appuie pour prendre le temps de dépatouiller (si besoin) la personne accueilli. Et cette dernière, souvent en manque de confiance, aura une personne de confiance justement pour s'appuyer dessus.


