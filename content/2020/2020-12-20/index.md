---
title: 20 décembre 2020
---

À propos de la taille et de l'élagage des arbres.

[J’ai 2, 3 arbres à élaguer… prestataire, formateur ou artiste ?](https://lezarbres.wordpress.com/2020/12/19/jai-2-3-arbres-a-elaguer-prestataire-formateur-ou-artiste/)

À lire ? [La taille des arbres d'ornements](https://inventaire.io/entity/inv:4519939c6c5fb623a53e04e57a30ac9d)

---

Note de lecture du livre « Cueillettes sauvages sans risques , Bais, planges, champignons... » de Sylvis Hampikian

10% de la flore sauvage de métropole est toxique.

> Les dix commandements du cueilleur de plantes sauvages
>
> 1. Ne consommez une plante que si vous êtes sûr de l'avoir identifiée (y compris au jardin)
> 2. Emportez un guide botanique bien illustré lors de vos cueillettes pour vous aider en cas de doute, ou consultez-le à votre retour. Et n'hésitez pas à faire des sorties botaniques, des stages, des visites accompagnées de jardins médicinaux pour améliorer vos connaissances.
> 3. Ne consommez jamais de plante à odeur désagréable, fétide voir puant ! Au mieux elles sont non comestibles, au pire elles sont toxiques (cette règle est également valable pour les champignons).
> 4. Si vous avez ingéré des baies ou plates très amères, recrachez-les immédiatement et si possible reincez-vous la bouche rapidement. Là encore, au mieux elles sont non comestibles, au pire elles sont toxiques. Apprenez cette règle aux jeunes enfants, car ils sont souvent tentés de porter à la bouche de jolies petites baies rouges.
> 5. Méfiez-vous des plantes sauvages (ou ornementales) à feuillage tacheté de noir, de marron, brun-rouge : elles sont souvent toxiques. Ces taches peuvent également être signe de maladies.
> 6. Méfiez-vous des plantes qui laissent écouler un lait (généralement blanc ou jaune) lorsqu'on les coupe : la plupart sont toxiques et/ou irritantes. Attention  notamment aux euphorbes et aux hellébores, y compris les variétés ornementales (rose de noël).
> 7. Soyez particulièrement méfiant lorsque vous cueillez une plante sauvage de la famille des Apiacées (ancienne Ombellières), c'est-à-dire apparentée à la carotte ou au persil. En effet, nombre d'entre elles sont irritantes et/ou toxiques. Dans cette famille, la morphologie des plantes est assez homogène et il est parfois difficile de distinguer certaines espèces les unes des autres. Par ailleurs, les Apiacées sont souvent photosensibilisantes, y compris les variétés potagères.
> 8. Ne consommez jamais de plantes sauvage de la famille des Solanacées, car la majorité sont toxiques. Il est conseillé d'apprendre à reconnaître les plantes de cette famille.
> 9. Ne cueillez pas de plante fanée, à moitié sèche, présentant des traces de moisissure, ni de fruit abîmés. En revanche, ne pensez pas que le fait qu'une feuille ait été mangée par les chenilles, un champignon par les limaces ou qu'un fleur soit butinée par les insectes signifie qu'il s ne sont pas toxiques.
> 10. Et si un doute persiste encore, ou si vous ne possédez pas de guide de botanique montrez votre récolte à un pharmacien ou à un spécialiste parmi vos connaissances (botaniste, mycologue, amateur averti ou professionnel).

Les principales familles botanique de notre flore

Les Apiacées (anciennes Ombilifères)

_Nombreuse espèces toxiques_

Exemple typique, la carotte.
Les risques de confusion entre la carotte sauvage et la cigüe sont réels.
Les potagères sont ok (carotte, fenouil, aneth, persil, angélique, cerfeuil, ...), mais les sauvages sont à éviter.
Possible pour la carotte sauvage, mais attention. À la limite, angélique, fenouil, criste-marine, mais à condition de les identifier formellement.
Une piste : l'odeur du feuillage froissé. Agréable, aromatique pour les « bonne » apiacées, inexistant ou désagréable pour les toxiques.

Les Astéracées (anciennes Composées)

_Le seul réelle problème et l'allergie au pollen de cette famille_

C'est les marguerite, pissenlit ,chardon et autres fleur à capitule (type d'inflorescence où les fleurs n'ont pas de pédoncule et sont regroupées sur un receptacle, entourées de bractées).

Contient quelques espèces comestible, mais surtout beaucoup de médicinale. À consommer avec modération.

Les Brassicacées (anciennes Crucifères)

_la seule non comestible (légèrement toxique) est la Giroflée_ que l'on reconnait à son parfum de clou de girofle.

La plupart de graines peuvent être utilisé en condiments, les feuilles en salades.

Identification simple : 4 pétales en croix, 4 sépales et 6 étamines (4 longues et 2 plus courtes). Les feuilles forment en général des rosettes au centre desquelles se dresse la tige florale. La plupart d'entre elles dégagent, quand on les froissent, une odeur de chou plus ou moins marquée, due à la présence de composés soufrés.

Les Fabacées (ancienne papillonacées ou légumineuses)

_Pour les sauvages, se limiter aux fleurs d'accacia et aux Fabacées médicinales_ pour ceux qui savent les reconnaitre trèfle rouge, mélilot vulnéraire

Identification : fleur en forme de papillon, fruits en forme de gousse.

Cool pour le sol, moins pour nous (dans les sauvages en tout cas :))

Les Euphorbiacées

_Aucune sauvage comestible, usage médicinal délicat_

La plupart ont un lait blanchâtre qui s'écoule des tiges.

Les Lamiacées

_Aucune lamiacées en france métropolitaine n'est vraiment toxique_. Mais la haute teneur en composés volatils actifs (huiles essentiels) impose une consommation raisonnée (condiment ou médicinal).

En tisane, éviter les cures prolongées.

Famille de plante très mellifères.

Les fleurs forment un receptacle d'où émergent de longues étamines.

Les Liliacées

_En dehors de l'asperges sauvage, aucune n'est consommable_

Le lis, le muguet..

Les Aliacées

Groupe de plantes ayant échappées de la famille des Liliacées. Contient l'ail, l'oignon, ...

Dans les sauvages, on peut récolter l'ail des ours et le poireau des vignes.

Les Malvacées

Pas très répandu en france métropolitaine, mais _ne contient aucune plantes toxiques_.

Les Mauves et la Guimauves, ainsi que les roses trémières.

Les poacées (anciennes Graminées)

Souvent poreuses de parasites. C'est assez déconseillé d'en récolter.
_L'usage à des fins alimentaires et médicinales des Poacées sauvages nécessite certaines connaissances et doit être laissé aux spécialistes._

Petite exception pour la racine de chiendent qui constitue un excellent diurétique et dépuratif.

Les Renonculacées

_Toutes plus ou moins toxiques_

Fleurs en forme de casque antique, 5 pétales, feuilles très découpées.

Les Rosacées

5 pétales réparis symétriquement autour d'un cœur d'étamines bien dressées. Les feuilles sont généralement ovales , peu ou pas découpées.

_Aucune représentante toxique_

Fraisier, framboisier, ronces, rosiers sauvages, sorbiers...

Les Solanacées

_Aucune solanacées sauvages comestibles. Toutes sont toxiques voire mortelles_

Et pourtant nous en mangeons beaucoup. Mais ce sont des variétés cultivées, importé d'amérique (tomates, aubergine, poivron, pomme de terre, ...)

C'est amusant de noter que les anciens, à l'époque de l'importation de ces plantes, se méfiait et n'en mangeait pas.

Fleur à 5 pétales plus ou moins soudées formant soit une étoile, soit une trompette. Fruits à pépins, forment soit des baies globuleuses, soit des capsules. En général, colorés et brillants.



Les plantes très toxiques.
J'ai déjà rencontré et identifié quelques plantes listés:
- Belladone (je crois...)
- Datura (sur !)
- Morelle noire (sur !)
- Digitale pourpre (est-ce qu'elles étaient pourpre ?)
- Cigüe (à vérifier, s'entrainer)
- Arum tacheté


Le laurier rose et le ricin semble être les plus toxiques.

Pourquoi l'huile de ricin est-elle malgré tout utilisé parfois ?

> Usage interne
> En pharmacie, cette huile a longtemps été utilisée pour ses effets laxatifs et des générations d'enfants ont dû en absorber quand on en ignorait encore les dangers. Mais c'est un purgatif violent et son emploi est formellement proscrit pour cette indication.
>
> Elle reste néanmoins présente actuellement, en faible quantité, comme excipient dans de nombreuses spécialités pharmaceutiques.
>
> Usage externe
> L'huile de ricin était par ailleurs recommandée en usage externe (cataplasmes tièdes) par le botaniste Leonart Fuchs au XVIe siècle et plus tard, par le voyant américain Edgar Cayce, qui prétendait avoir obtenu ainsi de nombreuses guérisons et dont des naturopathes reprennent les conseils. Aucune étude scientifique n'a validé ce genre d'effet thérapeutique. 

Par contre, industrie, cosmétique et horticulture...

[utilisation de l'huile de Ricin](https://fr.wikipedia.org/wiki/Huile_de_ricin#Utilisation)


Le top des plantes comestibles sauvages

- Ail des ours
- Asperge sauvage (déjà fait)
- Bourrache (déjà fait)
- Chénopode bon-henri (épinard sauvage)
- Criste-marine (je devrais pouvoir en trouver en bord de mer en Bretagne... ?)
- Fenouil sauvage (déjà fait)
- Mauve sauvage
- Ortie (déjà fait)
- Oseille sauvage
- Pimprenelle
- Plantain (déjà fait)
- Pourpier
- Salicorne (à faire en bord de mer ?)

Le dessus du panier des plantes médicinales sauvages

- Achillée Millefeuille
- Bardane
- Bouleau
- Camomille sauvage ou matricaire
- Gentiane jaune
- Millepertuis offinical ou millepertuis commun
- Pensée sauvage
- Prêle des champs
- Reine-des-prés
- Ronce (déjà fait)
- Tilleul (déjà fait)

Amusant de voir que j'ai fait plus de comestible que de médicinale. Est-ce une spécialité de fait ou bien une forme de hasard ?

Un petit rappel sur les fruits sec comme la châtaigne, et une découverte pour moi : le faîne. Fruit du Hêtre. Dans une bogue, 3 graines triangulaire. Possible d'en consommer un peu cru. Attention aux trouble si on en mange trop. Cuite par contre, elles perdent leur toxicité.

J'apprends également que la plupart des algues sont consommables. Il faut les récolter sur le rocher, encore vivante et attaché au caillou, les garder propre, et les faire sécher. Il y a sans doute à explorer de ce coté là.

