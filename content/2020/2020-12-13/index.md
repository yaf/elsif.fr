---
title: Dimanche 13 décembre 2020
---

Pourquoi faire le tour de la maison ? Pourquoi, lorsque je me promène avec une personne qui nous visite, je commente chaque élément que nous croisons « là l'école des enfants, là c'est ceci qu'il se passe, là c'est ça ».

Après coup, j'y vois beaucoup d'énergie et d'informations étalées, sans avoir été sollicité en plus.

C'est, j'imagine, une forme de joie personnel de montrer toutes ces choses qui me plaise, avec lesquelles je suis heureux. Mais peut-être que la personne avec moi n'a pas envie de savoir tout ça, ou plutôt que ça fait beaucoup de bruit pour elle.

---

Une recette pour les nombrils de vénus

- 1 dl d'eau
- 25 cl de vinaigre
- 500 g de nombril de vénus
- 1 branche de Thym
- 1 branche de Romarin
- 4 gousses d'ail
- 10 grains de poivre noir

- Bouillir eau + vinaigre
- Remplir un bocal hermétique avec le reste, tasser
- Remplir avec l'eau et le vinaigre

Attendre 1 mois.

_Je me demande si je ne l'ai pas en double celle-ci_

---

[La posture de l’artisan·e de justice sociale](https://www.education-populaire.fr/posture-artisan-justice-sociale/)

> De la rigueur, de l’empathie et de l’optimisme !

> - L’artisan·e partage une commune humanité.
> - L’artisan·e est optimiste.
> - L’artisan·e est courageux·courageuse
> - L’artisan·e est partie prenante de la situation
> - L’artisan·e doit se défaire de ses propres enjeux
> - L’artisan·e évite deux écueils : être devant ou derrière les gens


