--- 
title: Une mission bancale 
--- 


C'est l'histoire d'une mission qui ne se passe pas très bien, surtout à la fin.
Personne n'a été blessé, personne n'a vraiment souffert. C'est un environnement
pas des plus agréable pour travailler que j'ai traversé durant ces quelques
jours de 2015. Il me semble intéressant de partager aussi ce genre d'aventure.

La mission commence en mai 2015. Je venais de quitter Simplon, et j'étais
retourner m'inscrire chez Port Parallèle. L'objectif étais de rentrer chez
/ut7, de faire comme si j'étais chez /ut7, de travailler chez ds clients comme
si j'étais chez /ut7, sans y être administativement. Pendant ce temps, nous
allions constituer la trésorerie nécessaire à mon entrée dans cette SCOP.

La mission s'annonçait plutôt pas mal pourtant. C'est dans la boite de
connaissance (peut-être d'amis pour certaines personnes chez /ut7) ayant
participé à Agile Open France au moins une ou deux fois. Pourtant, je vais
découvrir que nous n'avons pas du tout la même façon de voir l'agilité, de
pratiquer notre métier.

_je n'ai pas commencé à journalisé au début de la mission, ou bien j'ai perdu
des journaux, je ne sais plus_

## [#](#mardi-19-mai-2015)Mardi 19 mai 2015


> Il faut faire un point

Réaction très forte de Thibaut, un des membres de l'équipe. France voulais
faire une redirection après la création d'une ressource : retourner à l'index.
Ça semble logique, mais comme ce n'est pas écrit dans les specs... J'ai mal à
mon esprit agiliste.

Le repas du Midi avec Étienne et un des responsable chez ce client nous permet
d'exprimer nos difficultés face à certaines pratique de chez eux.

Je suis sensé être invité pour boire une bière avec les Acrhi (les grands boss
expérimenté de cette boite). C'est noté !

Je suis content de la journée. Début de reprise de certaines partie du code que
je ne trouvais pas agréable. Et l'application commence à ressembler à quelque
chose.

## [#](#mercredi-20-mai-2015)Mercredi 20 mai 2015

Ce matin j'ai binomé avec France pour nettoyer un peu encore le code
[node](https://nodejs.org/), ajouter des tests, supprimer des duplications et
ajouter une règle de gestion. Ça fait du bien :-)

Cette après midi, cérémonie. Le client final arrive en retard.

> **Cérémonie de l'après midi, avec un estomac bien rempli, resultat non garanti**.

Je suis un peu surpris par la démo. C'est un peu bordelique. Le client final
n'aide pas. Perte de temps, dispersion. Celui qu'on nome ScrumMaster chez mon
client avoue ne pas avoir beaucoup regardé l'appli. La rétrospective me semble
un peu plate. Nous évoquons quand même des craintes sur le code sale.

Comme bien souvent, j'ai le sentiment que les raisons qui ont poussé à choisir
cette stack technique sont douteuses. En plus de node, nous utilisons
[AngularJS](https://angularjs.org/), et une base
[postgresql](https://www.postgresql.org/).

Une bonne discussion pour la plannification. La recette chez mon client permet
de calculer des points et de choisir des US. C'est bien commode, c'est bien
codifi. Ceci dit, je crois que nous en avons encore trop prit. Nous verrons.
C'est peut-être pas si bon que ça d'avoir ce genre d'abaque, ça se saurait si
ça fonctionnait :)

Je suis fatigué, on a fini tard: 20h.

J'ai acté avec le commercial du projet que je n'ai que les 16h30 provisionné
sur le projet pour le moment. Je peux consommer comme je veux, mais pas plus.
Je vais donc adapter ma présence avec l'équipe. Dans un premier temps je vais
venir un peu plus (le vendredi en plus), et je viendrais moins en juin.


## [#](#vendredi-22-mai-2015)Vendredi 22 mai 2015

Stand up qui va bien. Nous avons du retard, c'est principalement du au client
final qui n'a pas validé les tickets. Il est loin ce client.

Nous partons ensuite chacun sur nos tickets. Pas de pair programming. C'est
triste. Surtout que je pense que c'est pour un soit disant gain d'efficacité.
Mais comment gagner en efficacité alors que deux des trois développeurs sont
des stagiaires ?

Un des stagiaires part sur le ticket d'upload de photo. C'est un peu la galère:
la lib angular envoie le fichier en mode streaming (intéressant pour ne pas
bloquer l'interface utilisateur), mais du coup node le recoit bizarrement. Il
manque peut-être un truc pour cloturer la reception ?

L'autre stagiaire part elle sur l'éditeur WYSIWYG. Je n'ai pas assez insisté
pour ne pas faire cette feature, j'aurais peut-être due ? Après quelques
essais, elle à finalement réussie à le mette en place.

De mon coté, j'ai voulu faire du nettoyage du code coté client (organiser un
peu les repertoires angular). C'était galère. Je faisait des pas trop grand.
J'ai fait plusieurs fois des retour en arrière pour avancer d'un plus petit
pas. Au final, je n'ai pas fait tout ce que je voulais. J'ai ensuite fait un
premier pas sur le système de recherche: un champ avec une query avec un `where
= `. J'ai ensuite exploré la recherche en full text search. Je crois que nous
allons faire tout ça à la main.

Après une journée un peu hard. J'aide le premier stagiaire à trouver un
contournement au fonctionnement de l'upload. Nous avons faire un formulaire or
angular pour envoyer le fichier en direct à node, puis nous le stockons dans un
repertoire local, avec enregistrement du chemin...

Entre les difficultés à maintenir une organisation clair des fichiers, les
galères sur l'upload, et le fait que les deux stagiaires ont déjà beaucoup de
chose à apprendre, je me demande si AngularJS est un choix judicieux ? De plus
je ne vois pas trop quel est l'intérêt pour l'usage du client ?

En fait je me demande si ce projet n'aurait pas été plus simple avec du
html/css/jquery/nodejs/mongodb plutôt que du html/css/angular/node/postgres.
Est-ce que le changement est encore faisable ? Je pense que pour mongo c'est
moins indispensable... Mais nous pourrions gagner du temps de réalisation en
supprimant angular :-)


## [#](#mardi-26-mai-2015)Mardi 26 mai 2015

J'ai démarré la journée en pensant qu'il fallait que nous passions le projet
sur html/css/jquery, nodejs et mongodb, et je la termine en me disant, "ok tant
pis, mais du coup mettont tout dans angular. Ce sont les stagiaires qui insiste
pour garder Angular. Je ne vois toujours pas l'intérêt de ce truc, mais autant
s'en servir à fond pour savoir.

Le client final commence à chouiner. La fin du projet va être difficile avec
lui.

Je test une nouvelle configuration de vim: numérotation de lignes relative et
ligne courante souligné. Pas mal.

J'ai craqué le soir, j'ai fusioné les deux repertoire client et server du
projet. Ça m'ennuie d'avoir travaillé le soir pour eux, mais j'en avait
vraiment envie.

## [#](#mercredi-27-mai-2015)Mercredi 27 mai 2015

Ce matin, nous nous sommes dépéché pour finir les derniers points. Du coup nous
avons livré une régression... Retour arrière aussi vite que possible... J'ai
vraiment le sentiment que nous courons après les points.

C'est d'ailleurs évoqué en retro, lors de la cérémonie de l'après midi. Il y a
aussi des grandes discussion sur la célérité. Le stagiaire voudrais la doubler
pour pouvoir utiliser plus de couleurs pour définir les tickets. Le scrum
master semble contre, il préfère que l'on découpe plus les tickets... Je fais
l'innocent en demandant pourquoi au stagiaire (je ne comprenais vraiment pas ce
qu'il voulais faire pendant la cérémonie). Après la cérémonie, il m'a expliqué.
La célérité représente une sorte de ticket possible par jour (et par
personne... ), et le *standard* ici c'est de finir un ticket tout les jours.
C'est donc interdit d'avoir un ticket à 3 points si la célérité est de 3 ou
moins.

Tout ça pour faire du code pour un client... Je ne suis pas sur que ces
*standard* aide vraiment à faire fonctionner les équipes. Mon intuition c'est
que c'est utilisé pour savoir comment l'équipe avance en cours de sprint. **Ça
sent le contrôle à fond**. Dommage pour l'efficacité.

J'ai fait le taf du scrum master (sans le vouloir) pendant la cérémonie: j'ai
demander au client de bloquer une demi journée toutes les semaines, histoire
qu'il soit dispo toujours à cette date.

C'est à ce demander si des formations dev en mode Gym Club pour les aider à
faire monter des dev en archi + des formations scrum pour les scrum master ne
serait pas intéressant pour eux. Le hic est qu'ils ne font rien si ce n'est pas
facturé (le principe des boite de services qui essaient d'être rentable sur
chaque minutes...).

## [#](#vendredi-29-mai-2015)Vendredi 29 mai 2015

Nous avons encore fait le lapin blanc... Nous sommes en retard. Bruno, le scrum
master n'est pas là, alors quand nous faisons le standup, Antoine (le
commercial, scrum master sur d'autre projet normalement) viens faire un passage
en mode "mais pourquoi"...

Thibaut à essayé de mettre en place des outils de specs executable ET travisCI.
Travis fait tourné les tests unitaires que j'ai écrit sur la partie nodejs,
mais pour les specs executables, ça semble plus difficile. WebDriver ou je ne
sais quel protractor ne semble pas fonctionner. J'espère qu'il à réussi à faire
quelque chose dans l'après midi.

France à fait des essais de librairie pour faire du PDF. Mais elle essaie d'en
faire trop peut-être. J'apprend que l'équipe à décidé hier (jeudi) de dire que
ce ticket de "recherche" devait avoir un livrable dans l'application. *Je ne
suis pas sur de bien comprendre ce que cette boite entend par ticket de
recherche du coup ?*. Alors elle aussi est en *retard*.

De mon coté, je démarre avec Ansible. C'est amusant et pas trop compliqué. De
la configuration yaml. Dommage que ça ne soit pas du code ? **Je me demande
comment faire du TDD sur ces sujets, comment automatiser les vérifications que
je fait manuellement**, Il faudra que j'en parle avec la super équipe qui a mis
ça en place chez Mourinex ! :-D

Je commence aussi à modifier le script de déployement pour livrer l'application
sur le serveur de prod. Ça me permet de voir qu'il manque git. Je finirais
mardi.

## [#](#mardi-2-juin-2015)Mardi 2 juin 2015

Aujourd'hui, nous sommes encore en retard... Apparement France n'est pas en
forme. J'ai appris que ça mère viens d'arriver de Chine. Cause-Effet ?
Peut-être. Du coup elle dit que c'est elle qui est en retard. *Esprit d'équipe
bizarre ici par rapport à ce genre de sujet*.

Je continue sur Ansible et Capistrano. Un peu galère d'avoir la prod et la
préprod sur le même serveur. *C'est là que je pense très fort à des container*.
Mais au final je m'en sors (avec du retard par rapport à la prévision). En
plus, je me rappel sur la fin que pour générer du PDF, nous avons besoin de la
dernière version de wkhtmltopdf, une installation hors paquet debian du coup.
C'est un peu galère au dernier moment. Je finirais demain matin. Tant pis pour
l'upload de photo qui ne fonctionnera surement pas.

Quelques questions d'archi avec Tibaut. Il me parle ce qui pour lui est normal:
créer une table avec l'id (mais rien d'autre) ET une table de jointure avec le
même id plusieurs fois, lié à des id de documents... Je lui propose de tout
mettre dans la même pour le moment... ça semble lui faire bizarre.

Journée un peu rude (des échecs sur la mise en production), mais globalement
tout semble rouler un peu mieux...

## [#](#mercredi-3-juin-2015)Mercredi 3 juin 2015

Ce matin, je fini de réparer le ansible/capistrano. Je regrette d'avoir fait le
déployement en capistrano. Nous aurions pu le faire avec ansible, et ainsi
utiliser le même outil pour le provisionning ET le delpoiement.

France n'est toujours pas dedans. Mais elle me signale un soucis quand elle
essaie de déployer. Nous regardons. Effectivement, y'a une truc qui coince avec
Forever (l'outil nodejs qui s'occupe du monitoring du process node).

Thibaut est toujours empétré dans les test fonctionnel. Apparement, quand c'est
pas protractor qui merde, c'est travis. Il faut que je trouve une strategie
pour mettre plue de test unitaire. Ça m'aidera à avoir un peu plus confiance
dans notre code, et ça me permettra de relativiser un peu plus le manque de
tests fonctionnel.

Cette après midi, cérémonie. Le SM est speed. Il a un train pour Londres ce
soir. J'ai appris ce midi que lui aussi après le projet, il part à Londres.
**C'est sur, pas de rab pour moi, ou alors ça sera avec une autre équipe !**.
Je regle aussi un soucis de date, disons une mauvaise compréhension. Je vais
donc finir les deux dernières semaines à 1 jour par semaine.

La cérémonie se passe plutôt bien. Mais France est encore à coté du sujet. Pas
concentré. A la fin, il nous reste un peu de temps à nous, l'équipe. Du coup on
remet un peu les doigts dans le code. J'en profite pour parler un peu avec
France. Effectivement, elle a des soucis lié à son stage à Londres et ça mère
qui est aussi ici... et peut-être d'autre truc. Je lui dit que ce n'est pas
grave, et que je préfère savoir que c'est ça plutôt qu'un truc avec l'équipe.

J'ai vraiment un soucis avec leur cérémonie. Mais comment faire autrement ?
Avec des stagiaires, un po qui est client, un sm qui est commercial... Dur de
trouver le bon ton, le bon rythme, la bonne façon de procéder. *Peut-être à la
fin du projet, j'aurais trouvé une proposition à leurs faire :-p*.


## [#](#mardi-9-juin-2015)Mardi 9 juin 2015

Ce matin, après un update (git pull et revue/discussion), j'essaie de voir les
soucis de deploiment dont l'équipe m'a parlé hier par email. Et là, paf le
chien, je me retrouve coincé, impossible de déployer.

Je peux me connecter au serveur en ssh, mais le capistrano ne fonctionne pas,
un soucis de ssh. Je me rappel que Capistrano utilise un *forwarding agent*.
C'est ça qui coince. Je prend quelques renseignements, j'essaie de vérifier
étape par étape si un truc coince...

L'équipe ayant bien avancé, ils continuent sur leur lancé, moi j'essaie de me
dépatouiller.

Je binome un peu avec eux. Ils ont du mal à apréhender la notion de callback
JS. Idem pour les problématique de SQL.

Antoine est venu mettre un coup de *pression*: _Vous n'avez pas encore fait le
standup, il faut le faire à heure fixe_. Nous attendions Bruno, notre scrum
master. Mais du coup, tant pis nous avons fait le daily sans lui.

L'après midi, finalement un autre archi me donne la solution, il fallait que je
re-donne ma clef privée à l'agent: `ssh-add .ssh/id_rsa`... Ça fonctionne.

J'en profite pour faire un peu de shell pour rendre le processus de deploiement
un peu plus robuste.


## [#](#mercredi-10-juin-2015)Mercredi 10 juin 2015

Ce matin, la cérémonie devait commencé à 8h. J'avais pris des disposition pour
pouvoir être là vers 8h30 au plus tard. Je suis arrivé à 8h15, le PO était en
retard.. (alors que c'est pour lui que nous avons fait ça très tôt).

Nous ne faisons pas vraiment de demo, mais plutôt la validation de tickets qui
traine. Il se trouve qu'un nouveau cas non prévu bousille tout une partie des
features que nous avons fait. Du coup plein de ticket reparte dans le backlog.
Nous avons oublié qu'une affaire doit absoluement avoir un interlocuteur...

Ce raté est en partie du à nous qui n'avons pas rappelé au PO ces sujets, et à
lui qui est vraiment en mode: "ok, l'agilité, c'est j'écris mon cahier des
charges sur des petits tickets dans un outils en ligne, cool". Mais
l'application, dont il n'est pas et ne sera pas utilisateur apparement, n'est
toujours pas dans les mains d'un utilisateur... Il y aura d'autres surprises !

A la pause, il reçoit un coup de fil qui le met dans une drole d'humeur... A
son retour, il commence à s'impatienter, il est nerveux. Il nous reproche de ne
pas aller assez vite, et malheureusement l'équipe lui donne raison (sans
vraiment le vouloir je pense ?) en exprimant un sentiment d'aller trop
doucement...

J'essaie d'attirer leur attention sur le nombre de chose que nous avons fait,
sur le fait que ce sont des features un peu masqué (sans interface graphique).

Le PO part avant la fin, nous essayons de faire le planning sans lui, mais il y
a trop de question pour pouvoir le faire correctement. Gros sentiment d'échec.
Bruno, le SM, m'en fait par le midi: "C'est un gros échec pour moi". Nous
discutons un peu sur les trucs possible qui pourrait rattraper le coup.

En même temps, Bruno et France vont partir à Londres à la fin du projet, ça se
ressent.

L'après midi, Thibaut est tout feu tout flamme, comme d'habitude, et veux coder
12 trucs en même temps. Mais il oublie de verifier que cela fonctionne,
d'écrire des tests, de nettoyer son code... Bref.

Ils ont rendez-vous au téléphone ce soir avec le PO pour essayer de finir le
planning. De mon coté, j'ai essayé de faire une feature ET de faire du
refatoring... peut-être un peu trop. Du coup, au moment de push, je pull et
lance les rare tests d'intégrations, *paf*, j'en ai pété un. Je vais être en
retard au JSLDD, mais tant pis, j'essaie de corriger. Je n'y arrive pas, C'est
pas très simple, nous n'avons pas commecé sur de bonne base l'utilisation
d'angular, et les refactoring nécessaire sont parfois délicat. Le débugage
aussi ! Je pousse ma branche et laisse un message. J'espère qu'ils vont trouver
la solution demain.


## [#](#mardi-16-juin-2015)Mardi 16 juin 2015

Je me retrouve à réparer le build, mais pas suite à ce que j'avais laissé la
dernière fois. En fait hier, il y a eu une boulette, et peut-être une boulette
par dessus... bref. Une histoire de variable, de setup, d'oublie de function et
autres bricole.

Pendant le daily, Benjamin est venu surveiller... Il me redemande (avec Bruno)
si je peux être en ligne (skipe/hangout) pendant les Daily. J'explique que
c'est compliqué de prendre du temps sur d'autres client... Benjamin me demande
un postmortem sur les histoires de test qui sont rouge. Il insiste aussi sur
leur questionnaire devops, il essaye de voir si nous avons bien compris ce que
ça signifie (nous sommes sous le standard depuis le début, ça les chatouillent
on dirait ! :-)).

L'équipe à vraiment bien avancé, le backlog du sprint et vide. Comme cette
après-midi France et Bruno vont chez le client pour faire un tour de l'appli
avec un oeil UX/Design, nous avons décidé de ne pas prendre des tickets en
plus. Nous optons pour une phase de nettoyage du code. Thibault met en place un
outil de qualité de code, et suite à ça, se lance dans le refactoring. De mon
coté, j'essaie de mettre en place des outils pour pouvoir bénéficier d'un
environnement de test propre, un environnement de dev et un de prod. Ça nous
permettrais d'au moins mettre en place un reset de la base avant de lancer les
tests. Le mieux serais de pouvoir mettre en place une sorte de factory-girl.

Après mon email _post-mortem_, Benjamin me répond en parlant de ne faire que
des choses qui apporte de la valeur pour le client, des choses que le client
verra, pour qu'il se rende compte de ce que je fait... Je trouve que leur mode
de travail laisse peut de place à la qualité. Ce matin il évoquait le fait que
le client pouvait décider de la non qualité de son application... **beurk**

Ce midi je suis allez manger avec 6 petits jeunes *développeurs* d'ici (c'est
Thibaut qui m'a emené avec ces copains). Et bien ils sont tous stagiaires. L'un
d'entre eux, arrivé hier, à fait une réflexion: "mais y'a que des stagiaires ?"
:-)

## [#](#mercredi-17-juin-2015) Mercredi 17 juin 2015

Ce matin c'est cérémonie chez le client. Une société qui controle des travaux,
avec des gros sous donc. Avenue Iéana... J'arrive en retard, je suis orienté en
sous-sol vers la salle de réunion. Grande pièce, fauteils en cuir énorme, une
table en bois précieux énorme, bref, ça sent l'argent frais...

Le client se plaint des régressions, c'est vrai qu'il y en a pas mal. Hier en
refactorant nous avons cassé pas mal de truc... Il est content car il va s'en
servir pour négocier un tarif sûrement.

Le chrono est bien tenu, la cérémonie ne dépasse pas le temps imparti, par
contre la rétro est quasi passé à la trappe. **On voit bien les priorités**.
Bruno négocie le fait que je puisse être là au daily le matin. Je vais du coup
plutôt faire des demi-journées surement.

Après le repas, de gros échange avec Thibaut et France sur les tests, trop de
tests, pas assez, comment on fait... Une vrai négociation. Je n'arrive pas à
faire comprendre mon point de vue à Thibaut. Je me rend compte que j'ai peu
d'argument. *Est-ce vraiment important d'ailleurs ?* Jonhatan évoque sur slack
que george est codé sans tests. Cette idée fait un bout de chemin durant
l'après midi, après que j'ai décidé d'arreter de convaincre Thibaut.

Antoine, le commercial de l'affaire, a reçu un email du PO/client. Bien sur, il
se plaint. Il commence une négo. Mais Antoine vient nous voir avec Bruno pour
faire une réunion/point pour avoir des billes à lui opposer. La réunion est un
peu cliché, c'est un peu chiant de revenir encore sur ces histoires de tests.
Surtout que je suis en reflexion, prêt à justement faire une impasse dessus,
alors qu'Antoine et Bruno, insiste sur le fait de devoir les faire et de
présenter cela au client comme indispensable. Je vis ces remarques comme assez
dogmatique. *Est-ce que je vieilli sur ces sujets ?*. Je me rend compte que le
TDD m'aide à programmer proprement, mais que je ne suis pas pour une couverture
de tests à 100%. Je crois que Thibaut pense que je souhaite cela. En fait je
commence à proposer de faire des tests maunellement surtout, de les écrires et
les dérouler.

Je suis en retard pour la réunion avec les potentiels futurs élèves, je me
sauve avant de les avoir entendu statué sur le sujet. De toute façon je ne les
voient que lundi matin prochain, je verrais bien le résultat.

Bizarre de travailler dans une équipe de cette manière. Le plus bizarre et de
faire de la production pendant juste quelques jours...


## [#](#mardi-23-juin-2015)Mardi 23 juin 2015

Pas de courant chez le client aujourd'hui, je travail depuis la boutique.

Nous faisons le daily meeting sur Slack, comme hier matin, il n'y a pas Bruno
(hier je suis passé faire le daily seulement). C'est bizarre. En plus d'être
avec 2 stagiaires, je me retrouve sans un garant des pratiques du client. C'est
un coup à me faire faire tout changé :-p.

Nous sommes en retard d'après les standard défini ici. Il faut donc se fouetter
avec des orties. En gros, nous avons travaillé sur des sujets hors scope, hors
ticket hier, et nous n'avons pas comptabilisé l'atelier chez le client dans la
vélocitée. *Je trouve tout ce système bien lourd à la longue, qu'est-ce que le
client réclame vraiment ? Est-ce que tout cela n'est pas pour servir un discour
commercial ?* J'aimerais bien debriefer avec Étienne sur l'échange que nous
avons eu avec Antoine à Agile France.

Je commence un ticket qui a posé problème à Thibaut hier, un truc de date. Je
me focalise sur un point: ne pas pouvoir saisir une date inconnu: le 34 mai
2015. Il se trouve qu'en fait, Angular le gère déjà en glissant jusqu'au jour
      qui conviens, dans l'exemple, la date utilisé sera le 3 juin 2015 (34 -
      31 = 3). Du coup je comment et passe à un autre ticket en attendant le
      verdic du client. J'ajoute un footer dans le pdf, sans faire beaucoup de
      refactoring.

Le client répond dans l'après midi que c'est ok de ne rien faire sur ce sujet,
mais il rappel qu'il ne faut pas pouvoir saisir une date futur... J'ai mal lu
le ticket ou bien ce n'était pas précisé ?

Thibaut a encore faire une remarque: " j'ai passé le sprint à faire des tests,
il faut que nous fassions des points maintenant " ...

A suivre demain.

## [#](#mercredi-24-juin-2015)Mercredi 24 juin 2015

Ce matin, je suis de mauvaise humeur. Une histoire de clefs introuvable, un
plombier qui devait venir à 8h qui n'est pas là à 8h45, du coup je pars quand
même, je suis en retard à la cérémonie...

Nous partons encore dans des histoires de forme, de standard, de points,
d'estimations. Je suis un peu fatigué et ça ne passe pas bien. Je crois que ça
se voit. Le client n'est pas très heureux non plus. Est-ce qu'il joue pour
négocier ?

Thibaut me fatigue un peu parfois, il ne prend pas de recul sur sa pratique. Je
pense que France là dessus remet plus facilement en cause, et se pose plus de
question. Bruno lui essait de faire avancer le projet, mais en collant aux
standards définie.

*Quel est l'objectif de ces standard ?* Je me demande si c'est pour controller
les troupes/projets au sens large, ou bien juste pour être sur que les scrum
master ne soit pas inutile, ou encore si ce n'est pas un pseudo process pour
rassurer le client et pouvoir vendre des prestation. En tout cas, je n'ai pas
l'impression que ça m'aide à avance moi.

Je dois annuler un déjeuner car la cérémonie dur trop longtemps. Tant pis, je
le verrais plus tard. C'est un élève de la 2nd promo, celui embauché chez Octo,
dans la startup qui fait des app pour entreprise... Peut-être je vais lui
proposer de passer demain à la boutique ?

Cet après midi, Valentin, un archi en partance pour Londres, et le coach de
France, viens nous aider en mode sous marin (entendre, gratuit pour le client).
C'est cool. Du coup il binome avec Thibaut sur les soucis de travisCi et de
tests fonctionnels. De mon coté, je binome avec France pour régler des soucis
sur la numérotation de documents, et dans l'optique de nettoyer un peu le code.

C'est très sympa. Nous essayons des choses pour améliorer le code. Nous
enlevons des appels serveurs inutile, des procédures qui ne servent à rien,
déplaçons du code du serveur sur le client et inversement... Bref, un grand
nettoyage, tout en déroulant des scénarios utilisateurs autour de ces
documents, mais manuellement. Je n'ai vraiment pas envie de prendre du temps
pour écrire des scénarios automatisé. Je trouve cela trop douloureux pour le
moment. J'aimerais arriver à un code dans lequel nous pourrons faire du TDD (et
donc ajouter des tests unitaires sur le code existant) facilement.

France avait parlé d'un gros noeud de spageti, je pensais que nous pourrions
nettoyer, mais non, c'est effectivement un gros bordel.

Avant de partir, un rebase pour faire une pull request, et là, on se rend
compte que le refactoring des tests des autres ne fonctionne pas sur la machine
de france... Je les laissent essayer de régler cela.

J'ai chopé Benjamin vite fait pour voir les histoires de dispo. Pas eu le temps
d'aller chercher Étienne. Apparement, le client actuel souhaite ajouter 3
sprint début aout que je vais faire en 2 semaines puisque j'ai une semaine
prise. Et par contre, il voulait me prendre sur un autre projet, pour 3
semaines, mais c'est sur mes congés... tant pis.

Je suis bien fatigué, mais content du code produit avec France. Je ne la
reverrais pas tout de suite, elle part à Londres samedi. J'espère que le code
ira bien en prod et que j'arriverais à travailler avec Thibaut la semaine
prochaine.

## [#](#mardi-30-juin-2015)Mardi 30 juin 2015

Ce matin, après un point rapide avec Thibaut, je m'atèle à réparer le serveur
de staging (qui est aussi celui de prod). France, avant de partir pour
l'Angleterre à cassé un truc. Elle à voulu mettre nodejs à jour pour pouvoir
faire marché des promèses qu'elle à mis en place sur sont environnement... Mais
ne sachant comment faire elle a ajouté NVM... Une couche par dessus une
couche...

Je nettoie tout cela en supprimant toute l'installation, je modifie le script
ansible et zou, ça fonctionne ... presque. Le paquet Debian de node est à jour:
0.12.5, mais l'installation de NPM ne fonctionne pas. Je met un peu de temps à
découvrir qu'en fait, npm est inclu dans note à partir de la version 0.12. Ouf.

L'après midi, je prend un ticket que Thibaut ne sent pas. Après quelques
échanges avec le client, le ticket se simplifié. Je le fait tranquillement
pendant que lui nettoie la génération de PDF.

Vu les températures, ils ont le droit de travailler de chez eux pour le reste
de la semaine. Mais demain, nous ferons la dernière cérémonie chez le client,
et nous viendrons travailler l'après midi chez Theodo.

Bruno, le SM, était absent tout le matin, et vraiment pas présent l'après midi.
Il part en vacances demain soir, avant la fin du sprint qui a lieu vendredi.
Comme France, il a déjà la tête en Angleterre. Ça fait même quelques semaines.
Thibaut se lache et me faire part de son mécontentement par rapport à Bruno,
certaines pratiques ici, et la relation avec le client final qui, par interface
électronique interposé, n'est pas très fluide.

Est-ce que d'autres sont comme Thibaut, pas très enchanté de la façon dont les
projets sont géré ici ? C'est possible.

J'ai croisé plusieurs fois Julien et Benjamin, ils ne m'ont rien dit par
rapport à mes prochaines journées chez eux. Je déduis que je suis dispo la
semaine prochaine. En même temps, je suis en congé juste après.

## [#](#mercredi-1er-juillet-2015)Mercredi 1er juillet 2015

Ce matin, cérémonie chez le client. Thibaut à demandé à Antoine de venir. Il
est là, posé. C'est pas mal au début il prend un peu le stylo pour animer la
cérémonie, mais très vite ça tourne au consultant, donnant de grand conseil...
Bof.

Encore plein d'échanges autour du la façon dont on travail plus que sur la
façon de mettre de la valeur dans l'application. Un moment, Bruno me demande de
trancher. Je lui explique que non, je ne vais pas trancher sur une pratique qui
ne me va pas. Je note plusieurs chose qui me semble ne génante chez eux, les
voici un peu en vrac:

* Pas de démo, c'est dommage
* grosse perte de temps pour les estimations (avec poker et tout le décorum)
* toute la communication (avec le PO) est électronique, ça manque de fluidité
* pourquoi pas du flux continue ? (c'est peut-être un peu biaisais, lié à mes
  préférences ça)
* un proxy-po aurait peut-être pu aider ? en tout cas, une personne qui sais
  batir des produits, histoire de pouvoir dialoguer avec le client, qui est
  celui qui finance. Au lieu d'un SM, avoir une personne produit...
* tous les clients sont différents, toutes les équipes sont différentes =>
  pourquoi avoir des standard de fonctionnement valable pour tous ? y'a un
  relan de command&control dans cette histoire
* le sentiment que parfois ils oublient que le faire un logiciel et un
  processus de découverte

Je ne peux pas m'empécher d'en parler avec Bruno et Thibaut, mais je ne suis
pas sur d'être entendu. Sauf peut-être avec Bruno, nous avons des échanges
constructif sur ce point.

Antoine a décelé que Thibaut, Bruno et moi n'étions pas aligné (ce sont mes
mots, il a évoqué une mauvaise entente ou un truc dans le genre). C'est assez
vrai. J'évoque le fait que je suis d'accord, mais que je ne me sent pas du tout
de l'évoquer en rétro devant leur client, que cela ne me semble pas propice.
J'aurais aimé que eux l'aborde, pour me donner le signal que c'était possible.

Bref. Etant donné que c'est le dernier jour pour moi, et pour Bruno, tout cela
me semble bien inutile. C'est bizarre aussi ce fait de ne pas créer une équipe
qui dure un peu.

L'après midi, je code, répare des trucs, supprime une promise inutile et bugué
que France nous avais laissé dans le code. fait une petite correction par ci,
et un petit ajout de sécurité par là...

Je n'ai rien demandé à Benjamin ou Julien, que j'ai pourtant croisé plusieurs
fois, et eux non plus ne m'ont rien dit.

## [#](#lundi-27-juillet-2015)Lundi 27 juillet 2015

C'est la reprise. Le projet repart pour 3 sprints. Je retrouve Thibaut qui est
toujours Dev. S'ajoute Clément que j'ai déjà croisé, je crois qu'il a déjà un
ou deux projets dans les pattes (j'espère), et un autre Benjamin qui fera le
SM.

Vendredi dernier, Benjamin m'a appelé. Nous avons eu un échange sur le thème de
: « ton projet est le pire de chez Théodo, comment ça se fait ? » J'ai exposé
les points comme le départ en Angleterre de Bruno et France, le fait que
Thibaut cherche à *faire des points*... Nous verrons bien. Ce matin, Benjamin
(le SM) à remis un peu le couvert : *Comment ça se fait que vous n'avez pas de
définition du succès sur le projet ? Comment ça se fait que ce truc n'est pas
rempli, et que ça, sa ne fonctionne pas ...* Bref. Ambiance. Thibaut met ça sur
le dos de Bruno, je lui rappel que nous avons tous un peu merdé.

Benjamin et Thibaut sont ok pour utiliser une célérité de 4 ou je ne sais pas
quoi. Je ne comprend toujours pas leur usage de ce truc, mais tant pis, je les
laisse piloter. Je vais beaucoup moins m'impliquer sur leurs pratique pour
éviter de faire des vagues... Le projet se passera peut-être mieux ? En tout
cas, je serais sûrement moins affecté.

Nous essayons ensuite de débuguer le Travis qui est rouge (et laissé rouge)
depuis bien longtemps... Je part avant de le voir vert. J'espère que demain ça
sera réglé...

## [#](#mardi-28-juillet-2015)Mardi 28 juillet 2015

Ce matin, pour ne pas changer, le PO/client, est arrivé en retard...
Heureusement, nous n'avons pas fait de rétrospective (démarrage de projet).
Décision sur la célérité: elle sera haute... Ah ! Cela permet d'avoir plus
d'amplitude pour estimer les tickets. C'est vraiment bizarre, mais bon. La
cérémonie se passe plutôt bien. J'ai l'impression que tous le monde était très
attentif au fait que ça se déroule correctement et dans les temps.

Choix de petits tickets pour l'après midi, du coup ça dépotte. Enfin, je bouffe
tout les petits tickets. Clément et Thibaut avance sur leurs tickets à 2.
Est-ce la bonne stratégie ? Nous verrons bien.

En fin de journée j'attaque un refactoring histoire de supprimer quelques
duplication et d'aller un peu plus vers une archi où le nodejs sert d'API et le
client, avec angular contient beaucoup de logique métier.


## [#](#mercredi-29-juillet-2015)Mercredi 29 juillet 2015

J'attaque aujourd'hui un gros ticket. Gros parce qu'il touche des morceaux de
code pas très beau. L'idée est de faire un peu de refactoring... Mais la
motivation me manque. Je fais un peu de bricolage, rien de plus. Je fini le
ticket. J'entends pendant ce temps les deux autres en train de ce prendre la
tête sur l'ajout d'une librairie pour la gestion de l'autentification. Je n'ai
pas très envie de les aider.

Ce matin, il y a tellement de personnes qui viennent pour des entretiens que
Benoit, notre SM ne peux pas venir faire le Daily. Nous faisons sans lui. Comme
nous avons fait beaucoup de petits tickets, les points sont fait. Donc rien à
signaler. Pour combien de temps.

Gros manque de motivation aujourd'hui.

## [#](#vendredi-31-juillet-2015)Vendredi 31 juillet 2015

Ce matin, en arrivant, Clément et Thibaut sont en train de ce poser de grandes
questions apparement, mais à mon arrivée, ils décident de plutôt aborder un
soucis avec la preprod: elle ne fonctionne pas...

Après quelques questions, je comprend finalement que c'est la fonctionnalité
qui permet de s'authentifier, que nous devions finir mercredi, qui ne
fonctionne pas sur la preprod alors qu'en local cela fonctionne. Nous partons
en daily meeting sur ces échanges. Le Po/client à déplacé des tickets de "à
valider" à "backlog" parce qu'il n'était apparement pas bon. Il a fait des
retours sur des sujets qui apparement n'ont pas grand chose à voir...

Après analyse, il semblerais que sur 5 retour il y a bien 2 régression, deux
précision sur les tickets réalisé, et un truc qui n'a rien à voir... Personnes
ne souhaite l'appeler pour en parler, je m'en charge donc. De manière factuel
nous abordons ces points et il crée deux tickets (il dit "acheter") qu'il place
dans le backlog, puis nous discutons du reste, et je lui explique pourquoi la
preprod est coincé pour le moment.

Ensuite, Clément et moi essayons de voir ce qui pose problème. Apparement c'est
le nginx qui bloque les cookies (ou une partie ?), mais Clément à l'impression
qu'il y a autre chose qui ne fonctionne pas. C'est vrai qu'en passant dans
certains écrans, il y a "une déconnexion", c'est à dire que nous sommes
redirigé vers la page de login. Je lui dit de réparer d'abord la partie nginx
et nous verrons ensuite, mais non, il s'obstine. Ok. Un archi, demandé par
Benoit, notre SM, arrive et demande à Clément de lui epxliquer le problème...
Au lieu de partir de la situation actuel, il lui refait tout l'historique. Je
pars, c'est l'heure.

J'ai le sentiment de ne pas avoir beaucoup fait avancé le projet, et de
travailler avec deux sourds... Plus que 2 sprints, dont un seul chez eux ! :-)


## [#](#lundi-10-août-2015)Lundi 10 août 2015

Ce matin, en arrivant, j'apprend que la préprod est encore en vrac. Apparement
un truc aléatoire lors de la génération de certains documents...

J'ai passé un bon bout de journée dessus pour au final virer les promesses
qu'ils avaient décidé d'utiliser pour faire une bonne vieille requête SQL en
chaine de caractères. Du coup, ça fait une reuqête au lieu de x (x pouvant
aller facilement à 10). Nous regardons ensuite avec Clément comment améliorer
le code.  Je le force à regarder puisque j'ai laissé malgré moi des boulettes
dans le code.

Quelques échanges technique avec Clément surtout, Thibaut c'est lancé dans un
ticket "en plus" avec plein de refactoring... Ce soir, il dit ne pas encore
avoir attaqué le ticket mais fait du refactoring... Nous verrons demain la
quantité de régression (que j'espère proche de 0).

Pas de SM de la journée... Enfin si, nous le voyons débarquer vers 17h30. Il
nous demande comment ça va et repart.

Benjamin à interpelé Étienne à notre retour du repas. Étienne est parti ensuite
en réunion. J'ai resenti comme une exclusion, mais j'avoue que ça me va bien.
J'étais content de ne pas être invité. J'ai vraiment le sentiment que c'est ma
dernière semaine ici et ça me va bien. C'est même peut-être ce qui va m'aider à
passer la semaine.

## [#](#mardi-11-août-2015)Mardi 11 août 2015

Je suis arrivé avant que les portes soient ouvertes chez mon client... Le
client final est en mode pas content: le produit n'est pas *fini*. Il découvre
des régressions, râle, sans être trop agressif je trouve, mais je sens une
tension dans la pièce. En plus il y fait chaud.

Je reste assez silencieux: je n'ai pas participé la semaine dernière, et hier
j'ai juste reparé un soucis. Pas vraiment le sentiment d'avoir apporté une
influence sur ce sprint. Ça me va bien. Je laisse les autres dérouler leur
processus.

A la pause, le client final me prend à part pour me parler du projet, de la
deception qu'il a. Il évoque aussi le fait que l'équipe est meilleur maintenant
qu'au début. Je lui explique que c'est tout à fait vrai, que chez les éditeurs
il y a y soucis de ne pas avoir trop de turn over car les équipes deviennent
meilleurs avec le temps. Elles apprenent à travailler comme un groupe et en
plus apprennent le métier.

La cérémonie se déroule, la rétro passe presque inaperçu. J'apprend qu'il y
aura une cérémonie vendredi, en fin de journée, je n'y serais pas.

## [#](#mercredi-12-août-2015)Mercredi 12 août 2015

Ce matin, je ne suis pas bien motivé. Ça commence à être une habitude. Avec
l'équipe, sans le SM, nous décidons que je vais prendre le ticket qui revois
les pdf. Je n'ai pas vraiment mis le nez dedans depuis le début.

Je ne suis pas déçu ! De la duplication, des morceaux de code qui pourrait être
dupliqué, mais en fait non (ce qui explique des écarts entre un pdf d'un type
et un autre).

Derrière moi, Thibaut rale sur le client. C'est fatiguant. Je me demande si un
de mes soucis avec ce projet n'est pas son attitude. Il veut faire vite,
rejette les fautes sur le client. Du coup cette après-midi, nous ne le voyons
presque pas: un coup il est bénévole pour installer le wifi au 3ième étage, un
coup il a un entretien avec son *coach* (je crois).

Le SM lache une petite reflexion sur le fait que *ses deux projets* ne
fonctionne pas bien (en dessous du standard sur le questionnaire client, le
standard c'est 8/10, nous avons 4/10).

Heureusement, demain c'est jeudi, et il ne me reste plus que vendredi matin.

Je n'ai pas fini mon nettoyage de pdf, je finirais vendredi matin, J'espère que
j'arriverais à tout mettre au propre et qu'il n'y aura pas d'autres soucis.


## [#](#et_apres)Et après

Le client n'a pas voulu nous payer la dernière semaine de travail. Apparement,
le client finale ne voulais pas payer les deux dernières semaines... Au delà de
nous faire partager le soucis, j'ai été pointé comme étant le responsable du
fiasco de ce projet. Ça n'a pas été facile à entendre.

Ensuite, nous avons voulu reconstituer le fil de certains évènements. J'avais
malheureusement perdu mes mails durant l'été. Une belle leçon.

J'ai tiré un autre apprentissage de cette histoire : si ça ne va pas, il faut
partir vite, pas attendre la fin en se disant que ça ira mieux après.

Cette histoire ma également confirmé qu'il y a plein d'interprétation possible
de l'agilité, et que même chez les personnes qui semblent proche, les écarts
peuvent être dur à vivre.

