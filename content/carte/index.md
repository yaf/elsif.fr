---
title: Carte
---

## 2021

### Janvier 

* [2 janvier 2021](/2021/2021-01-02/)
* [3 janvier 2021](/2021/2021-01-03/)
* [4 janvier 2021](/2021/2021-01-04/)
* [5 janvier 2021](/2021/2021-01-05/)
* [7 janvier 2021](/2021/2021-01-07/)
* [10 janvier 2021](/2021/2021-01-10/)
* [14 janvier 2021](/2021/2021-01-14/)
* [15 janvier 2021](/2021/2021-01-15/)
* [16 janvier 2021](/2021/2021-01-16/)
* [19 janvier 2021](/2021/2021-01-19/)
* [24 janvier 2021](/2021/2021-01-24/)
* [25 janvier 2021](/2021/2021-01-25)
* [26 janvier 2021](/2021/2021-01-26)
* [27 janvier 2021](/2021/2021-01-27)
* [29 janvier 2021](/2021/2021-01-29)
* [30 janvier 2021](/2021/2021-01-30)
* [31 janvier 2021](/2021/2021-01-31)

### Février

* [1er février 2021](/2021/2021-02-01/)
* [2 février 2021](/2021/2021-02-02/)
* [3 février 2021](/2021/2021-02-03/)
* [4 février 2021](/2021/2021-02-04/)
* [5 février 2021](/2021/2021-02-05/)
* [7 février 2021](/2021/2021-02-07/)
* [8 février 2021](/2021/2021-02-08/)
* [9 février 2021](/2021/2021-02-09/)
* [10 février 2021](/2021/2021-02-10/)
* [12 février 2021](/2021/2021-02-12/)
* [14 février 2021](/2021/2021-02-14/)
* [16 février 2021](/2021/2021-02-16)
* [19 février 2021](/2021/2021-02-19)
* [20 février 2021](/2021/2021-02-20)
* [24 février 2021](/2021/2021-02-24)
* [25 février 2021](/2021/2021-02-25)
* [26 février 2021](/2021/2021-02-26)
* [27 février 2021](/2021/2021-02-27/)

### Mars

* [1er mars 2021](/2021/2021-03-01/)
* [3 mars 2021](/2021/2021-03-03/)
* [4 mars 2021](/2021/2021-03-04/)
* [5 mars 2021](/2021/2021-03-05/)
* [7 mars 2021](/2021/2021-03-07/)
* [9 mars 2021](/2021/2021-03-09/)
* [10 mars 2021](/2021/2021-03-10/)
* [12 mars 2021](/2021/2021-03-12/)
* [13 mars 2021](/2021/2021-03-13/)
* [15 mars 2021](/2021/2021-03-15/)
* [16 mars 2021](/2021/2021-03-16)
* [17 mars 2021](/2021/2021-03-17)
* [18 mars 2021](/2021/2021-03-18)
* [19 mars 2021](/2021/2021-03-19)
* [20 mars 2021](/2021/2021-03-20)
* [21 mars 2021](/2021/2021-03-21)
* [22 mars 2021](/2021/2021-03-22/)
* [23 mars 2021](/2021/2021-03-23/)
* [24 mars 2021](/2021/2021-03-24/)
* [25 mars 2021](/2021/2021-03-25/)
* [26 mars 2021](/2021/2021-03-26/)
* [27 mars 2021](/2021/2021-03-27/)
* [28 mars 2021](/2021/2021-03-28/)
* [29 mars 2021](/2021/2021-03-29/)
* [30 mars 2021](/2021/2021-03-30/)
* [31 mars 2021](/2021/2021-03-31/)

### Avril 

* [Mardi 13 avril 2021](/2021/2021-04-13)
* [Merecredi 14 avril 2021](/2021/2021-04-14)
* [Vendredi 16 avril 2021](/2021/2021-04-16)
* [Samedi 17 avril 2021](/2021/2021-04-17)
* [Dimanche 18 avril 2021](/2021/2021-04-18)
* [Mercredi 21 avril 2021](/2021/2021-04-21)
* [Vendredi 23 avril 2021](/2021/2021-04-23)
* [Samedi 24 avril 2021](/2021/2021-04-24)
* [Lundi 26 avril 2021](/2021/2021-04-26)
* [Mardi 27 avril 2021](/2021/2021-04-27)
* [Mercredi 28 avril 2021](/2021/2021-04-28)
* [Jeudi 29 avril 2021](/2021/2021-04-29)
* [Vendredi 30 avril 2021](/2021/2021-04-30)

### Mai

* [1er mai 2021](/2021/2021-05-01)
* [5 mai 2021](/2021/2021-05-05)
* [7 mai 2021](/2021/2021-05-07/)
* [8 mai 2021](/2021/2021-05-08/)
* [11 mai 2021](/2021/2021-05-11/)
* [12 mai 2021](/2021/2021-05-12/)
* [13 mai 2021](/2021/2021-05-13/)
* [15 mai 2021](/2021/2021-05-15/)
* [16 mai 2021](/2021/2021-05-16/)
* [19 mai 2021](/2021/2021-05-19/)
* [20 mai 2021](/2021/2021-05-20/)
* [21 mai 2021](/2021/2021-05-21/)
* [22 mai 2021](/2021/2021-05-22/)
* [23 mai 2021](/2021/2021-05-23)
* [25 mai 2021](/2021/2021-05-25)
* [26 mai 2021](/2021/2021-05-26)
* [28 mai 2021](/2021/2021-05-28)
* [29 mai 2021](/2021/2021-05-29)
* [30 mai 2021](/2021/2021-05-30)
* [31 mai 2021](/2021/2021-05-31/)

### Juin

* [2 juin 2021](/2021/2021-06-02/)
* [3 juin 2021](/2021/2021-06-03/)
* [4 juin 2021](/2021/2021-06-04/)
* [5 juin 2021](/2021/2021-06-05/)
* [6 juin 2021](/2021/2021-06-06/)
* [8 juin 2021](/2021/2021-06-08/)
* [9 juin 2021](/2021/2021-06-09/)
* [10 juin 2021](/2021/2021-06-10/)
* [11 juin 2021](/2021/2021-06-11/)
* [12 juin 2021](/2021/2021-06-12/)
* [14 juin 2021](/2021/2021-06-14)
* [15 juin 2021](/2021/2021-06-15)
* [16 juin 2021](/2021/2021-06-16)
* [26 juin 2021](/2021/2021-06-26)
* [29 juin 2021](/2021/2021-06-29)

### Juillet

* [3 juillet 2021](/2021/2021-07-03)
* [5 juillet 2021](/2021/2021-07-05/)
* [6 juillet 2021](/2021/2021-07-06/)
* [7 juillet 2021](/2021/2021-07-07/)
* [9 juillet 2021](/2021/2021-07-09/)
* [11 juillet 2021](/2021/2021-07-11/)
* [12 juillet 2021](/2021/2021-07-12/)
* [13 juillet 2021](/2021/2021-07-13/)
* [14 juillet 2021](/2021/2021-07-14/)
* [17 juillet 2021](/2021/2021-07-17/)
* [18 juillet 2021](/2021/2021-07-18/)
* [21 juillet 2021](/2021/2021-07-21/)
* [25 juillet 2021](/2021/2021-07-25)
* [27 juillet 2021](/2021/2021-07-27)
* [28 juillet 2021](/2021/2021-07-28)
* [29 juillet 2021](/2021/2021-07-29)
* [30 juillet 2021](/2021/2021-07-30)

### Août

* [1er août 2021](/2021/2021-08-01)
* [4 août 2021](/2021/2021-08-04/)
* [9 août 2021](/2021/2021-08-09/)
* [20 août 2021](/2021/2021-08-20/)
* [26 août 2021](/2021/2021-08-26/)
* [29 août 2021](/2021/2021-08-29/)

### Septembre

* [6 septembre 2021](/2021/2021-09-06/)
* [10 septembre 2021](/2021/2021-09-10/)
* [11 septembre 2021](/2021/2021-09-11/)
* [12 septembre 2021](/2021/2021-09-12/)
* [13 septembre 2021](/2021/2021-09-13/)
* [14 septembre 2021](/2021/2021-09-14/)
* [22 septembre 2021](/2021/2021-09-22)
* [30 septembre 2021](/2021/2021-09-30)

### Octobre

* [1er octobre 2021](/2021/2021-10-01)
* [2 octobre 2021](/2021/2021-10-02)
* [3 octobre 2021](/2021/2021-10-03)
* [5 octobre 2021](/2021/2021-10-05)
* [12 octobre 2021](/2021/2021-10-12/)
* [13 octobre 2021](/2021/2021-10-13/)
* [17 octobre 2021](/2021/2021-10-17/)
* [18 octobre 2021](/2021/2021-10-18/)
* [23 octobre 2021](/2021/2021-10-23/)
* [24 octobre 2021](/2021/2021-10-24/)
* [31 octobre 2021](/2021/2021-10-31/)

### Novembre

* [9 novembre 2021](/2021/2021-11-09/)
* [10 novembre 2021](/2021/2021-11-10/)
* [12 novembre 2021](/2021/2021-11-12/)
* [13 novembre 2021](/2021/2021-11-13/)
* [14 novembre 2021](/2021/2021-11-14)
* [18 novembre 2021](/2021/2021-11-18)
* [20 novembre 2021](/2021/2021-11-20/)
* [24 novembre 2021](/2021/2021-11-24/)
* [26 novembre 2021](/2021/2021-11-26/)
* [27 novembre 2021](/2021/2021-11-27/)

### Décembre

* [4 décembre 2021](2021-12-04/)
* [5 décembre 2021](/2021/2021-12-05/)
* [7 décembre 2021](/2021/2021-12-07/)
* [12 décembre 2021](/2021/2021-12-12/)
* [13 décembre 2021](/2021/2021-12-13/)
* [15 décembre 2021](/2021/2021-12-15/)
* [20 décembre 2021](/2021/2021-12-20/)
* [21 décembre 2021](/2021/2021-12-21/)
* [22 décembre 2021](/2021/2021-12-22/)
* [23 décembre 2021](/2021/2021-12-23/)
* [24 décembre 2021](/2021/2021-12-24/)
* [26 décembre 2021](/2021/2021-12-26/)
* [31 décembre 2021](/2021/2021-12-31/)


## 2020

* [Semaine 39 (du 27 septembre au 3 octobre)](/2020/39/)

* [Semaine 40 (du 4 au 10 octobre)](/2020/40/)
* [Semaine 41 (du 11 au 17 octobre)](/2020/41/)
* [Semaine 42 (du 18 au 24 octobre)](/2020/42/)
* [Semaine 43 (du 25 au 31 octobre)](/2020/43/)

* [Semaine 44 (du 1er au 7 novembre)](/2020/44/)
* [Semaine 45 (du 8 au 14 novembre)](/2020/45/)
* [Semaine 46 (du 15 au 21 novembre)](/2020/46/)
* [Semaine 47 (du 22 au 28 novembre)](/2020/47/)

* [Lundi 30 novembre 2020](/2020/2020-11-30/)
* [Dimanche 29 novembre 2020](/2020/2020-11-29/)

* [Mardi 1er décembre 2020](/2020/2020-12-01/)
* [Jeudi 3 décembre 2020](/2020/2020-12-03/)
* [Vendredi 4 décembre 2020](/2020/2020-12-04/)

* [Samedi 5 décembre 2020](/2020/2020-12-05/)
* [Lundi 7 décembre 2020](/2020/2020-12-07/)
* [Mercredi 9 décembre 2020](/2020/2020-12-09/)
* [Jeudi 10 décembre 2020](/2020/2020-12-10/)
* [Samedi 12 décembre 2020](/2020/2020-12-12/)
* [Dimanche 13 décembre 2020](/2020/2020-12-13/)
* [Lundi 14 décembre 2020](/2020/2020-12-14/)
* [Mardi 15 décembre 2020](/2020/2020-12-15/)
* [Mercredi 16 décembre 2020](/2020/2020-12-16/)
* [Jeudi 17 décembre 2020](/2020/2020-12-17/)
* [Vendredi 18 décembre 2020](/2020/2020-12-18/)

## 2018

* [Notes du 5 Septembre 2018](/2018/2018-09-05/index.html)
* <a href="/2018/2018-08-30/index.html">Notes du 30 Août 2018</a>
* <a href="/2018/2018-08-29/index.html">Notes du 29 Août 2018</a>
* <a href="/2018/2018-08-28/index.html">Notes du 28 Août 2018</a>
* <a href="/2018/2018-08-27/index.html">Notes du 27 Août 2018</a>
* <a href="/2018/2018-08-26/index.html">Notes du 26 Août 2018</a>
* <a href="/2018/2018-08-23/index.html">Notes du 23 Août 2018</a>
* <a href="/2018/2018-08-21/index.html">Journal du 21 Août 2018</a>
* <a href="/2018/2018-08-17/index.html">Journal du 17 Août 2018</a>
* <a href="/2018/2018-08-16/index.html">Journal du 16 August 2018 </a>
* <a href="/2018/2018-08-10/index.html">Notes du 10 août 2018</a>
* <a href="/2018/2018-08-02/index.html">Notes du 2 août 2018</a>
* <a href="/2018/2018-08-15/index.html">2018 08 15</a>
* <a href="/2018/2018-07-31/index.html">Journal du 31 juillet 2018</a>
* <a href="/2018/2018-07-29/index.html">Journal du 29 juillet 2018</a>
* <a href="/2018/2018-07-28/">Notes du samedi 28 juillet 2018</a>

## 2015

* [Fin de mission](une-mission-bancale#et-apres)
* [Dernier refactoring](une-mission-bancale#mercredi-12-août-2015) le 12 août
* [Roue libre](une-mission-bancale#mardi-11-août-2015) le 11 août
* [Dernière semaine sur place](une-mission-bancale#lundi-10-août-2015) le 10 août
* [Précision](une-mission-bancale#vendredi-31-juillet-2015) le 31 juillet
* [Sans motivation](une-mission-bancale#mercredi-29-juillet-2015) le 29 juillet
* [C'est parti](une-mission-bancale#mardi-28-juillet-2015) le 28 juillet
* [Nouveau départ](une-mission-bancale#lundi-27-juillet-2015) le 27 juillet
* [Dernière cérémonie](une-mission-bancale#mercredi-1er-juillet-2015) le 1er juillet
* [Déploiement](une-mission-bancale#mardi-30-juin-2015) le 30 juin
* [Refactoring](une-mission-bancale#mercredi-24-juin-2015) le 24 juin
* [Vélocité](une-mission-bancale#mardi-23-juin-2015) le 23 juin
* [Négociation](une-mission-bancale#mercredi-17-juin-2015) le 17 juin
* [Standard](une-mission-bancale#mardi-16-juin-2015) le 16 juin
* [Forcing](une-mission-bancale#mercredi-10-juin-2015) le 10 juin
* [Patouille](une-mission-bancale#mardi-9-juin-2015) le 9 juin
* [Cérémonie](une-mission-bancale#mercredi-3-juin-2015) le 3 juin
* [Rude journée](une-mission-bancale#mardi-2-juin-2015) le 2 juin
* [Lapin blanc](une-mission-bancale#vendredi-29-mai-2015) le 29 mai
* [Bricolage](une-mission-bancale#mercredi-27-mai-2015) le 27 mai
* [Craquer](une-mission-bancale#mardi-26-mai-2015) le 26 mai
* [Chacun de son côté](une-mission-bancale#vendredi-22-mai-2015) le 22 mai
* [Choix douteux](une-mission-bancale#mercredi-20-mai-2015) le 20 mai
* [Il faut faire un point](une-mission-bancale#mardi-19-mai-2015) le 19 mai

## 2009

<ul>

<li>21 Nov 2009 » <a href="/2009/2009-11-21-le-code-et-les-commentaires/">Le code et les commentaires</a></li>

<li>08 Nov 2009 » <a href="/2009/2009-11-08-bsd-ressources/">BSD ressources</a></li>

<li>04 Nov 2009 » <a href="/2009/2009-11-04-nouvelle-affiche-au-bureau/">Nouvelle affiche au bureau</a></li>

<li>03 Nov 2009 » <a href="/2009/2009-11-03-ann-jruby-1-4-0/">[ANN] JRuby 1.4.0</a></li>

<li>29 Oct 2009 » <a href="/2009/2009-10-29-haiku-r1-alpha-1/">Haiku R1/Alpha 1</a></li>

<li>14 Oct 2009 » <a href="/2009/2009-10-14-agile-tour-2009-paris/">Agile Tour 2009 - Paris</a></li>

<li>03 Oct 2009 » <a href="/2009/2009-10-03-green-it/">Green IT</a></li>

<li>29 Sep 2009 » <a href="/2009/2009-09-29-un-chti-dojo/">Un Chti dojo</a></li>

<li>21 Sep 2009 » <a href="/2009/2009-09-21-les-habitudes-ont-la-belle-vie/">Les habitudes ont la belle vie</a></li>

<li>16 Sep 2009 » <a href="/2009/2009-09-16-haikuos-alpha1/">HaikuOS alpha1</a></li>

<li>30 Aug 2009 » <a href="/2009/2009-08-30-openbsd-4-6-precommande/">OpenBSD 4.6 Précommande</a></li>

<li>27 Jun 2009 » <a href="/2009/2009-06-27-pourquoi-kanban/">Pourquoi Kanban ?</a></li>

<li>09 Jun 2009 » <a href="/2009/2009-06-09-ruby-file-open-w-acces-concurrent/">Ruby File.open &#39;w&#39; Acces concurrent</a></li>

<li>30 May 2009 » <a href="/2009/2009-05-30-retour-des-xpday-2009/">Retour des XPDay 2009</a></li>

<li>24 May 2009 » <a href="/2009/2009-05-24-xpday-france-2009/">XPDay France 2009</a></li>

<li>19 May 2009 » <a href="/2009/2009-05-19-apero-ruby-de-mai-2009/">Apéro Ruby de mai 2009</a></li>

<li>01 May 2009 » <a href="/2009/2009-05-01-openbsd-4-5-et-pickaxe-1-9/">OpenBSD 4.5 et PickAxe 1.9</a></li>

<li>21 Apr 2009 » <a href="/2009/2009-04-21-skinny-controllers-and-fat-models/">Skinny controllers and fat models</a></li>

<li>14 Apr 2009 » <a href="/2009/2009-04-14-openbsd-binary-upgrade/">OpenBSD binary upgrade</a></li>

<li>09 Apr 2009 » <a href="/2009/2009-04-09-ne-soyez-pas-un-developpeur-ruby/">Ne soyez pas un développeur Ruby</a></li>

<li>18 Mar 2009 » <a href="/2009/2009-03-18-jruby-1-2-0/">JRuby 1.2.0</a></li>

<li>08 Mar 2009 » <a href="/2009/2009-03-08-railscampparis-2/">RailsCampParis</a></li>

<li>04 Mar 2009 » <a href="/2009/2009-03-04-openbsd-4-5-current/">OpenBSD 4.5-current</a></li>

<li>20 Feb 2009 » <a href="/2009/2009-02-20-railscamp-paris-2-le-retour/">RailsCamp Paris 2 le retour</a></li>

<li>24 Jan 2009 » <a href="/2009/2009-01-24-apero-paris-rb-ruby-1-9-1/">Apero Paris.rb - Ruby 1.9.1</a></li>

<li>18 Jan 2009 » <a href="/2009/2009-01-18-ruby-if-unless/">Ruby if unless</a></li>

<li>10 Jan 2009 » <a href="/2009/2009-01-10-ruby-et-les-frameworks-web/">Ruby et les frameworks web</a></li>

<li>10 Jan 2009 » <a href="/2009/2009-01-10-developpement-dont-vous-etes-le-heros-mais-pilote-par-les-tests/">Développement dont vous êtes le héros mais piloté par les tests</a></li>

</ul>

## 2008

<ul>
<li>22 Dec 2008 » <a href="/2008/2008-12-22-git-ou-mercurial/">Git ou Mercurial</a></li>
<li>21 Dec 2008 » <a href="/2008/2008-12-21-jruby-1-1-6/">JRuby 1.1.6</a></li>
<li>14 Dec 2008 » <a href="/2008/2008-12-14-windowmaker-come-back/">WindowMaker come back</a></li>
<li>11 Dec 2008 » <a href="/2008/2008-12-11-shoes-vendange-tardive/">Shoes : vendange tardive</a></li>
<li>04 Dec 2008 » <a href="/2008/2008-12-04-retour-sur-un-bon-week-end-ruby-et-rails/">Retour sur un bon week-end Ruby et Rails</a></li>
<li>23 Nov 2008 » <a href="/2008/2008-11-23-april-pas-encore-adherent/">April, pas encore adhérent ?</a></li>
<li>22 Nov 2008 » <a href="/2008/2008-11-22-rails-party-le-30-novembre-2008/">Rails Party le 30 novembre 2008</a></li>
<li>02 Nov 2008 » <a href="/2008/2008-11-02-note-de-service-bricabox-upgrade/">Note de service: Bricabox upgrade</a></li>
<li>01 Nov 2008 » <a href="/2008/2008-11-01-1er-novembre-openbsd-4-4/">1er novembre : OpenBSD 4.4</a></li>
<li>16 Oct 2008 » <a href="/2008/2008-10-16-quel-type-de-developpeur-je-suis/">Quel type de développeur je suis ?</a></li>
<li>30 Sep 2008 » <a href="/2008/2008-09-30-openbsd-wicontrol/">OpenBSD wicontrol</a></li>
<li>28 Sep 2008 » <a href="/2008/2008-09-28-tdd-c-est-quoi/">TDD C&#39;est quoi ? (En ruby bien sur !)</a></li>
<li>13 Sep 2008 » <a href="/2008/2008-09-13-rest-maintenant-soap-quand-tu-veux/">REST maintenant, SOAP quand tu veux</a></li>
<li>05 Sep 2008 » <a href="/2008/2008-09-05-openbsd-4-4-precommande/">OpenBSD 4.4 precommande</a></li>
<li>30 Aug 2008 » <a href="/2008/2008-08-30-des-shoes-propres-avec-ajax/">Des shoes propres avec Ajax</a></li>
<li>29 Aug 2008 » <a href="/2008/2008-08-29-jruby-1-1-4/">JRuby 1.1.4</a></li>
<li>19 Aug 2008 » <a href="/2008/2008-08-19-shoes-change-de-pompe/">Shoes change de pompes</a></li>
<li>16 Aug 2008 » <a href="/2008/2008-08-16-choisir-un-ordinateur-portable/">Choisir un ordinateur portable</a></li>
<li>15 Aug 2008 » <a href="/2008/2008-08-15-ruby-test-unitaire/">Ruby : Test Unitaire</a></li>
<li>01 Aug 2008 » <a href="/2008/2008-08-01-typo-upgrade/">Typo Upgrade</a></li>
<li>17 Jul 2008 » <a href="/2008/2008-07-17-openbsd-4-4-beta/">OpenBSD 4.4 beta</a></li>
<li>17 Jul 2008 » <a href="/2008/2008-07-17-jruby-1-1-3/">Jruby 1.1.3</a></li>
<li>11 Jul 2008 » <a href="/2008/2008-07-11-fete-en-grande-pompes/">Fête en grande Pompe</a></li>
<li>08 Jul 2008 » <a href="/2008/2008-07-08-livre-les-design-patterns-en-ruby/">Les design patterns en ruby</a></li>
<li>01 Jul 2008 » <a href="/2008/2008-07-01-openbsd-jdk1-5-et-labre-des-ports/">OpenBSD, JDK1.5 et l&#39;abre des ports</a></li>
<li>29 Jun 2008 » <a href="/2008/2008-06-29-openbsd-livecd/">OpenBSD LiveCD</a></li>
<li>22 Jun 2008 » <a href="/2008/2008-06-22-drapeaux-en-pagaille/">Drapeaux en pagaille</a></li>
<li>10 Jun 2008 » <a href="/2008/2008-06-10-typo-et-le-jardinier/">Typo et le jardinier</a></li>
<li>10 Jun 2008 » <a href="/2008/2008-06-10-openbsd-et-openjdk/">OpenBSD et OpenJDK</a></li>
<li>08 Jun 2008 » <a href="/2008/2008-06-08-typogardenparty/">TypoGardenParty</a></li>
<li>06 Jun 2008 » <a href="/2008/2008-06-06-openbsd-installation-de-grub/">OpenBSD Installation de Grub</a></li>
<li>29 May 2008 » <a href="/2008/2008-05-29-socket-versus-port/">Socket versus Port</a></li>
<li>24 May 2008 » <a href="/2008/2008-05-24-courage-du-developpeur/">Courage du developpeur</a></li>
<li>20 May 2008 » <a href="/2008/2008-05-20-back-to-web-projet-brouette/">Back to web - projet brouette</a></li>
<li>03 May 2008 » <a href="/2008/2008-05-03-gem-les-packages/">Gem les packages</a></li>
<li>01 May 2008 » <a href="/2008/2008-05-01-openbsd-4-3-released/">OpenBSD 4.3 released</a></li>
<li>22 Apr 2008 » <a href="/2008/2008-04-22-dojo/">Dojo</a></li>
<li>19 Apr 2008 » <a href="/2008/2008-04-19-5e-apero-rubyfrance/">5e apéro rubyFrance</a></li>
<li>12 Apr 2008 » <a href="/2008/2008-04-12-openbsd-4-3/">OpenBSD 4.3</a></li>
<li>05 Apr 2008 » <a href="/2008/2008-04-05-methodes-agiles/">Méthodes agiles</a></li>
<li>02 Apr 2008 » <a href="/2008/2008-04-02-eurobsdcon-strasbourg/">EuroBSDCon - Strasbourg</a></li>
<li>30 Mar 2008 » <a href="/2008/2008-03-30-wmii-3-6/">Wmii 3.6</a></li>
<li>23 Mar 2008 » <a href="/2008/2008-03-23-foaf-ou-ldap/">Foaf ou LDAP</a></li>
<li>22 Mar 2008 » <a href="/2008/2008-03-22-suivre-le-flux-de-lhistoire/">Suivre le flux de l&#39;histoire</a></li>
<li>19 Mar 2008 » <a href="/2008/2008-03-19-et-pourquoi-pas-wmii-iiiiiiiii/">Et pourquoi pas Wmii iiiiiiiii</a></li>
<li>12 Mar 2008 » <a href="/2008/2008-03-12-slim/">SLiM</a></li>
<li>05 Mar 2008 » <a href="/2008/2008-03-05-ruby-vs-shell/">Ruby vs Shell</a></li>
<li>02 Mar 2008 » <a href="/2008/2008-03-02-self-migrate-5-0-3/">Self.migrate(5.0.3)</a></li>
<li>28 Feb 2008 » <a href="/2008/2008-02-28-openbsd-ports/">OpenBSD Ports</a></li>
<li>23 Feb 2008 » <a href="/2008/2008-02-23-openbsd-current-4-3/">OpenBSD Current -&gt; 4.3</a></li>
<li>07 Feb 2008 » <a href="/2008/2008-02-07-ruby-specs/">Ruby Specs</a></li>
<li>02 Feb 2008 » <a href="/2008/2008-02-02-openbsd-current/">Openbsd-current</a></li>
<li>02 Feb 2008 » <a href="/2008/2008-02-02-guyane-et-orpaillage/">Guyane et Orpaillage</a></li>
<li>01 Feb 2008 » <a href="/2008/2008-02-01-openbsd-usb-mount/">OpenBSD - USB Mount</a></li>
<li>16 Jan 2008 » <a href="/2008/2008-01-16-sqlite-backup/">SQLite Backup</a></li>
<li>12 Jan 2008 » <a href="/2008/2008-01-12-got-new-shoes-with-ruby/">Got new Shoes with Ruby</a></li>
</ul>

## 2007

<ul>
<li>18 Dec 2007 » <a href="/2007/2007-12-18-opendns/">OpenDNS</a></li>
<li>18 Dec 2007 » <a href="/2007/2007-12-18-les-methodes-agiles-contre-les-mamouths/">Les methodes agiles contre les mamouths</a></li>
<li>17 Dec 2007 » <a href="/2007/2007-12-17-a-lancienne/">A l&#39;ancienne</a></li>
<li>04 Dec 2007 » <a href="/2007/2007-12-04-retour-aux-sources/">Retour aux sources</a></li>
<li>01 Dec 2007 » <a href="/2007/2007-12-01-avenir-des-applications-web-libre/">Avenir des applications web libre</a></li>
<li>29 Nov 2007 » <a href="/2007/2007-11-29-openbsd-4-2-expat-missing/">OpenBSD 4.2 Expat missing</a></li>
<li>19 Nov 2007 » <a href="/2007/2007-11-19-changer-de-nom-de-domaine/">Changer de nom de domaine</a></li>
<li>18 Nov 2007 » <a href="/2007/2007-11-18-parisweb-2007-mon-topo/">ParisWeb 2007: mon topo</a></li>
<li>01 Nov 2007 » <a href="/2007/2007-11-01-openbsd-42-est-sorti/">OpenBSD 4.2 est sorti !</a></li>
<li>01 Nov 2007 » <a href="/2007/2007-11-01-le-calendrier-libre/">Le calendrier libre</a></li>
<li>14 Oct 2007 » <a href="/2007/2007-10-14-flash-format-proprietaire-et-developpement/">Flash, format propriétaire et développement</a></li>
<li>12 Oct 2007 » <a href="/2007/2007-10-12-les-causeries-de-lapril-jabber/">les causeries de l&#39;APRIL: Jabber</a></li>
<li>11 Oct 2007 » <a href="/2007/2007-10-11-gari-gtd-que-les-choses-soient-faites/">GARI, GTD, que les choses soient faites</a></li>
<li>06 Oct 2007 » <a href="/2007/2007-10-06-rubyfrance/">RubyFrance</a></li>
<li>02 Oct 2007 » <a href="/2007/2007-10-02-et-xul-alors/">Et XUL alors !</a></li>
<li>24 Sep 2007 » <a href="/2007/2007-09-24-un-parisien-en-province-moins-de-stress/">Un parisien en province - Moins de stress</a></li>
<li>17 Sep 2007 » <a href="/2007/2007-09-17-cool-uris-dont-change/">Cool URIs don&#39;t change</a></li>
<li>14 Sep 2007 » <a href="/2007/2007-09-14-enquete-utilisation-flux-rss/">Enquête sur l&#39;utilisation des flux RSS</a></li>
<li>24 Aug 2007 » <a href="/2007/2007-08-24-openbsd-4-2-cest-deja-demain/">OpenBSD 4.2 c&#39;est déjà demain</a></li>
<li>16 Aug 2007 » <a href="/2007/2007-08-16-statelessness-mon-job-cest-tout/">Statelessness: mon job un point c&#39;est tout</a></li>
<li>14 Aug 2007 » <a href="/2007/2007-08-14-microformats-on-commence-petit/">Microformats: on commence petit</a></li>
<li>09 Aug 2007 » <a href="/2007/2007-08-09-ruby-new-vs-initialize/">Ruby: new vs initialize</a></li>
<li>26 Jul 2007 » <a href="/2007/2007-07-26-openbsd-fondation/">OpenBSD Fondation</a></li>
<li>19 Jul 2007 » <a href="/2007/2007-07-19-standard-ouvert-pour-leurope/">Standard ouvert pour l&#39;europe</a></li>
<li>18 Jul 2007 » <a href="/2007/2007-07-18-un-parisien-en-province-le-cout-de-la-vie/">Un parisien en province - le cout de la vie</a></li>
<li>13 Jul 2007 » <a href="/2007/2007-07-13-nouvelle-machine-sous-le-bureau/">Nouvelle machine sous le bureau</a></li>
<li>12 Jul 2007 » <a href="/2007/2007-07-12-pour-garder-des-belles-mains/">Pour garder des belles mains</a></li>
<li>10 Jul 2007 » <a href="/2007/2007-07-10-relax-max/">Relax max</a></li>
<li>01 Jul 2007 » <a href="/2007/2007-07-01-changement-de-licence/">Changement de licence</a></li>
<li>22 Jun 2007 » <a href="/2007/2007-06-22-le-web-libre/">Le web libre</a></li>
<li>18 Jun 2007 » <a href="/2007/2007-06-18-cest-historique/">C&#39;est pas de ma faute c&#39;est historique</a></li>
<li>14 Jun 2007 » <a href="/2007/2007-06-14-opensolaris-vu-par-le-monde-libre/">OpenSolaris vu par le monde libre</a></li>
<li>13 Jun 2007 » <a href="/2007/2007-06-13-une-journee-sans-google-trop-tard/">Une journée sans google - trop tard</a></li>
<li>11 Jun 2007 » <a href="/2007/2007-06-11-gagner-sa-vie-librement/">Gagner sa vie librement</a></li>
<li>03 Jun 2007 » <a href="/2007/2007-06-03-jruby-1-0-0rc3/">JRuby 1.0.0RC3</a></li>
<li>27 May 2007 » <a href="/2007/2007-05-27-et-pourquoi-pas-opensolaris/">Et pourquoi pas OpenSolaris</a></li>
<li>24 May 2007 » <a href="/2007/2007-05-24-rendez-vous/">Rendez-vous</a></li>
<li>15 May 2007 » <a href="/2007/2007-05-15-microsoft-gnulinux-et-les-brevets/">Microsoft, GNULinux et les brevets</a></li>
<li>04 Mar 2007 » <a href="/2007/2007-03-04-url-parsing-with-ruby/">URL Parsing with Ruby</a></li>
<li>02 Mar 2007 » <a href="/2007/2007-03-02-note-to-self/">Note to self</a></li>
<li>24 Feb 2007 » <a href="/2007/2007-02-24-nouvelle-machine-que-choisir/">Nouvelle machine: que choisir</a></li>
<li>09 Feb 2007 » <a href="/2007/2007-02-09-self-join-april-org/">self.join(APRIL.ORG)</a></li>
<li>31 Jan 2007 » <a href="/2007/2007-01-31-reflexion-les-bases-de-donnees/">Reflexion : les bases de données</a></li>
<li>23 Jan 2007 » <a href="/2007/2007-01-23-ssh-avec-clefs/">ssh avec clefs</a></li>
<li>13 Jan 2007 » <a href="/2007/2007-01-13-mysql-user-privileges/">Mysql user privileges</a></li>
</ul>


## 2006


- 26 Dec 2006 » [RubyFrance.logo.new](/2006/2006-12-26-rubyfrance-logo-new/)
- 22 Dec 2006 » [OpenBSD hostname.change](/2006/2006-12-22-openbsd-hostname-change/)
- 13 Dec 2006 » <a href="/2006/2006-12-13-xp-france/">XP France</a>
- 12 Dec 2006 » <a href="/2006/2006-12-12-bsd-certification/">BSD Certification</a>
- 06 Dec 2006 » <a href="/2006/2006-12-06-tout-ce-bouscule/">Tout ce bouscule</a>
- 05 Dec 2006 » <a href="/2006/2006-12-05-rubyfrance-cherche-son-logo/">RubyFrance cherche son logo</a>
- 10 Nov 2006 » <a href="/2006/2006-11-10-freecycle-le-recyclage-simplement/">Freecycle, le recyclage simplement</a>
- 09 Nov 2006 » <a href="/2006/2006-11-09-java-en-gpl/">Java en GPL ?</a>
- 01 Nov 2006 » <a href="/2006/2006-11-01-openbsd-4-0-released/">OpenBSD 4.0 released !</a>
- 25 Oct 2006 » <a href="/2006/2006-10-25-vim-exploration-de-fichiers/">Vim exploration de fichiers</a>
- 19 Oct 2006 » <a href="/2006/2006-10-19-jdll-feed-back/">JDLL: feed back</a>
- 16 Oct 2006 » <a href="/2006/2006-10-16-demission/">Démission</a>
- 14 Oct 2006 » <a href="/2006/2006-10-14-openbsd-3-9-gestion-utilisateurs-environnements/">OpenBSD 3.9 : gestion utilisateurs environnements</a>
- 10 Oct 2006 » <a href="/2006/2006-10-10-explosion-de-boite-cranienne/">Explosion de boite cranienne</a>
- 09 Oct 2006 » <a href="/2006/2006-10-09-openbsd-3-9-gestion-utilisateur/">OpenBSD 3.9 gestion utilisateur</a>
- 04 Oct 2006 » <a href="/2006/2006-10-04-les-journees-du-logiciel-libre/">Les Journées du Logiciel Libre</a>
- 26 Sep 2006 » <a href="/2006/2006-09-26-radio_libre_attitude-entree_libre-saison2-start/">radio_libre_attitude.entree_libre.saison2.start</a>
- 16 Sep 2006 » <a href="/2006/2006-09-16-jai-craque/">J&#39;ai craqué</a>
- 13 Sep 2006 » <a href="/2006/2006-09-13-openbsd-un-systeme-complet-installation/">OpenBSD: un systême complet (installation)</a>
- 05 Sep 2006 » <a href="/2006/2006-09-05-openbsd-le-retour-du-retour-du-retour-du-retour/">OpenBSD Le retour du retour du retour du retour....</a>
- 31 Aug 2006 » <a href="/2006/2006-08-31-debian-etch/">Debian Etch</a>
- 25 Aug 2006 » <a href="/2006/2006-08-25-philosophie-libriste-de-comptoir/">Philosophie libriste de comptoir</a>
- 22 Aug 2006 » <a href="/2006/2006-08-22-microsoft-et-mozilla/">Microsoft et Mozilla</a>
- 21 Aug 2006 » <a href="/2006/2006-08-21-le-cout-dun-serveur-dedie-vs-un-serveur-maison/">Le coût d&#39;un serveur dédié vs un serveur maison</a>
- 18 Aug 2006 » <a href="/2006/2006-08-18-serveur-dedie-dedibox-autres/">Serveur dédié: Dédibox ? Autres ?</a>
- 16 Aug 2006 » <a href="/2006/2006-08-16-happy-birthday-debian/">Happy Birthday Debian</a>
- 15 Aug 2006 » <a href="/2006/2006-08-15-window-maker-theme-tango/">Window Maker Theme : Tango</a>
- 14 Aug 2006 » <a href="/2006/2006-08-14-gestionnaire-de-fenetre-windowmaker-come-back/">[Gestionnaire de fenêtre] WindowMaker come back !</a>
- 11 Aug 2006 » <a href="/2006/2006-08-11-a-propos-du-livre-sur-leconomie-des-logiciels/">A propos du livre sur &#34;l&#39;économie des logiciels&#34;</a>
- 09 Aug 2006 » <a href="/2006/2006-08-09-powerpc-la-fin-dune-alternative/">PowerPC: la fin d&#39;une alternative ?</a>
- 14 Jul 2006 » <a href="/2006/2006-07-14-openbsd-mon-bilan/">OpenBSD mon bilan</a>
- 11 Jul 2006 » <a href="/2006/2006-07-11-pc-recyclage-et-gnu-linux-de-bonne-idees/">PC recyclage et gnu/linux de bonne idées</a>
- 05 Jul 2006 » <a href="/2006/2006-07-05-openbsd-patch/">OpenBSD: Patch</a>
- 28 Jun 2006 » <a href="/2006/2006-06-28-openbsd-powerpc-mapping-clavier-mac-usb/">OpenBSD/PowerPC: mapping clavier mac usb</a>
- 21 Jun 2006 » <a href="/2006/2006-06-21-ecocircus-eco-festival-creabio/">Ecocircus: Eco festival CREABIO</a>
- 20 Jun 2006 » <a href="/2006/2006-06-20-stop-aux-racketiciels/">Stop aux racketiciels !</a>
- 16 Jun 2006 » <a href="/2006/2006-06-16-ruby-association-et-forum/">Ruby: Association et forum</a>
- 15 Jun 2006 » <a href="/2006/2006-06-15-le-web-passe-present/">Le web: passé, present</a>
- 09 Jun 2006 » <a href="/2006/2006-06-09-tsf-bon-pour-les-oreilles/">TSF: Bon pour les oreilles</a>
- 09 Jun 2006 » <a href="/2006/2006-06-09-et-encore-un-hack-tout-vilain/">Et encore un hack tout vilain</a>
- 08 Jun 2006 » <a href="/2006/2006-06-08-scite-le-bonus-indispensable/">SciTE: le bonus indispensable</a>
- 08 Jun 2006 » <a href="/2006/2006-06-08-openbsd-installation-sur-mac-mini/">OpenBSD: installation sur Mac Mini</a>
- 05 Jun 2006 » <a href="/2006/2006-06-05-rubyfr-org/">RubyFR.org</a>
- 02 Jun 2006 » <a href="/2006/2006-06-02-politique-arretons-de-vouloir-toujours-plus/">Politique: Arretons de vouloir toujours plus</a>
- 01 Jun 2006 » <a href="/2006/2006-06-01-pourquoi-choisir-openbsd/">Pourquoi choisir OpenBSD</a>
-
- 24 May 2006 » <a href="/2006/2006-05-24-vacances/">Vacances</a>
- 24 May 2006 » <a href="/2006/2006-05-24-la-coree-plus-dur-que-leurope/">La Corée plus dur que l&#39;Europe</a>
- 19 May 2006 » <a href="/2006/2006-05-19-documentation-dentreprise/">Documentation d&#39;Entreprise</a>
- 18 May 2006 » <a href="/2006/2006-05-18-open-discussion-day-drm/">Open Discussion Day, DRM</a>
- 17 May 2006 » <a href="/2006/2006-05-17-web-2-0/">Web 2.0</a>
- 16 May 2006 » <a href="/2006/2006-05-16-energie-avenir-chaos/">Energie, Avenir, Chaos</a>
- 12 May 2006 » <a href="/2006/2006-05-12-service-dinformation/">Service d&#39;information</a>
- 11 May 2006 » <a href="/2006/2006-05-11-logiciel-libre-et-license/">Logiciel libre et license</a>
- 10 May 2006 » <a href="/2006/2006-05-10-bibliotheque/">Bibliothêque...</a>
- 08 May 2006 » <a href="/2006/2006-05-08-openbsd-debian/">OpenBSD - Debian :-/</a>
- 04 May 2006 » <a href="/2006/2006-05-04-nouveau-centre-dinfluance/">Nouveau centre d&#39;influance ?</a>
- 04 May 2006 » <a href="/2006/2006-05-04-ie-mon-amour/">IE mon amour</a>
- 04 May 2006 » <a href="/2006/2006-05-04-format-ouvert-opendocument/">Format ouvert: OpenDocument</a>
-
- 28 Apr 2006 » <a href="/2006/2006-04-28-boulet-time/">Boulet Time</a>
- 20 Apr 2006 » <a href="/2006/2006-04-20-structure-dune-page-web/">Structure d&#39;une page web</a>
- 05 Apr 2006 » <a href="/2006/2006-04-05-tout-et-tout-bronze/">Tout et tout bronzé</a>
- 02 Mar 2006 » <a href="/2006/2006-03-02-les-premiers/">Les premiers</a>
- 01 Mar 2006 » <a href="/2006/2006-03-01-trinity-rescue-a-laide/">Trinity rescue : A l&#39;aide !</a>

