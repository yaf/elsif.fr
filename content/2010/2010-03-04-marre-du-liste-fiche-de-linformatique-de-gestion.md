---
layout: post
title: Marre du liste fiche de l'informatique de gestion
---

*Il y a quelque chose qui commence à être lourd dans les développements logiciel auquel je participe: l’ergonomie idiote. Je me demande comment enrayer cela. Voici une image du problème que je rencontre si souvent*.

En général, on écrit un logiciel pour faciliter la vie d’utilisateurs. Dans mon cas, c’est souvent des utilisateurs interne (je travail en *informatique de gestion*). Il y a déjà là une petite piste de reflexion: **pourquoi je traite des utilisateurs interne différement du grand public (ou utilisateur externe) ?**

Peut-être parce qu’on ne peut pas *imposer* une manière de travailler à monsieur tout-le-monde. Mais finalement, quand on y pense un peu, les divers services que l’on utilise nous impose un peu leur vision des choses, et quand c’est bien fait, c’est un service qui marche, parce que la façon dont sont proposé les fonctionnalitées nous convient. Sinon, le produit disparait.

Pourquoi ne pourrait-on pas imaginer la même chose en interne ? **Pourquoi ne pas forcer l’utilisation d’une application par son ergonomie, son design, ses fonctionnalitées ?**

Ensuite, au moment d’écrire le logiciel, on se retrouve souvent à faire un découpage. Souvent par grand domaine: gestion utilisateur, gestion vehicule, gestion tarif, gestion calendrier... gestion, gestion... C’est bon, on a compris qu’on travail en informatique de gestion ! Ca m’énerve !

Et quand on pose la question: **"c’est quoi la gestion d’un véhicule" ?** Et bien, on a souvent la réponse suivante: "un véhicule à un nom, une immatriculation, une marque (qu’on va gérer dans une table à part), un type (lien dans une table à part), un prix de location (il faudra boucler avec la gestion des tarifs et des plannings), et une date de mise en service". Super, **on parle de schéma de base de donnée (relationnel) sans avoir aborder les fonctionnalitées**...

Mais creusons un peu.

"au niveau fonctionnalité, on fait quoi réèllement ?" et voilà la réponse qui tue: "on fait une tableau avec des critères de recherche/filtre, si possible avec des colonnes triable, et puis un formulaire pour la création et la mise à jour". Génial !.

Pour les autres fonctionnalitées: reprendre le paragraphe précédent et remplacer “véhicule" et quelques champs par le nouveau domaine à étudier (utilisateur, tarif). Il n’y a que le calendrier éventuellement que l’on va gérer autrement...

Et on arrive à quelque chose de ce genre:

![Limmonde architecture n-tier de lappli de gestion](/files/ergo.jpg)

Y’a un truc qui cloche (outre le fait que je vais devoir apprendre a dessiner sur un ordinateur).

Habitué des bases de données relationnel, on se rend compte que c’est juste une **image des données**, avec un quelque règles métier placé entre les deux, plutôt que de les mettre sous forme de trigger de base.

Je me demande quel est l’interêt de ce genre d’application ? OÃ¹ est le plus qui fait la différence. Non parce que sinon, y’a moyen de faire **de beau trigger de base**, et d’**utiliser un outil de query comme [Squirrel-SQL](http://squirrel-sql.sourceforge.net/)** ou autre pour aller taper dedans. On pourrais même imaginer faire **quelques scripts sql à l’avance** pour faciliter la vie des utilisateurs. Très honnêtement, je pense que l’on irait plus vite en proposant cela, et **l’équipe apporterais tout autant de valeur au produit** (peut-être même plus) !

Comment faire pour empecher cela ? Comment changer les mentalités d’équipe entierement contaminé ? Je vois de mon angle de vue 2 pistes (qui ne s’exclu pas d’ailleurs):

* NoSQL (not only SQL) ou le grand mouvement des bases non relationnelle

Dur d’être passer à coté de ce genre d’article ces dernier temps. J’avoue ne pas avoir encore manipulé dans le cas d’une application concrete ce genre de base, mais juste fait quelques exploration. Cela me fait penser aux bases de donnée objet raté d’il y a quelque année. Enfin système de stockage d’objet voit le jour. Simple et semble-t-il efficace. Voilà peut-être un morceau de la solution pour arreter de penser les écrans sous forme de table de base de donnée relationnel.

* DDD (Domain Driven Design) approche du développement logiciel

Peut-être un peu plus confidentiel (surtout auprès des développeur français que je connais ?) mais j’ai l’impression que ça pourrais nous faire revenir à l’essentiel: la fonction d’un logiciel. Et non la structure des données (qui semble être le point le plus important aujourd’hui, malheureusement). Mais c’est un sujet un peu trop neuf pour moi pour l’instant. Si vous voulez en savoir plus, je ne peut que vous conseiller l’[article wikipedia sur le Domain Driven Design](http://en.wikipedia.org/wiki/Domain-driven_design)pour commencer (la version française manque d’ailleurs de richesse :p): puis le [site de la communauté ddd](http://domaindrivendesign.org/).

Je ne sais pas trop a quoi ressemblerons nos appli de demain, mais j’espère que celles que l’on voit dans les entreprises seront plus sympa à utiliser !

