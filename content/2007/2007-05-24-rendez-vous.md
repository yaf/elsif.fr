---
layout: post
title: Rendez-vous
---


Deux rendez-vous incontournable pour découvrir le monde libre:
####Journées du Libre de Montpellier 07

[Le programme sur le site de l’ALL](http://www.all.asso.fr/Realisations/2007/jlm07/ProgrammesJLM07/)

J’aimerais pouvoir assister à toutes les journées... mais jeudi pour les Collectivitées, vendredi pour les Entreprises c’est sur mes jours de boulot, et venant d’arriver, je ne suis pas sur de pouvoir me libérer. Les journées termine à 17h trop tôt pour que je passe après... Tant pis.

Par contre samedi, j’y serais !  Surtout car c’est pour moi l’occassion de voir Monsieur <span class="caps">RMS</span> en chair et en os, et car les conférences de l’après midi me semble passionnante:

* Formats libres", Th. Stoehr (président <span class="caps">AFUL</span> – [formats-ouverts.org](http://formats-ouverts.org/))
* “Promouvoir et défendre le Logiciel Libre dans un contexte politique et juridique changeant”, Frederic Couchet ([APRIL](http://www.april.org/))


Deux personnes que je lit régulièrement et que j’aurais plaisir à écouter pour une fois !

(La conf mozilla juste avant me parait bien interessante également ;-))

Une belle journée en perspective (et pour une fois aussi que je peu y aller avec mes pieds ;-)

####Rencontres Mondiales du Logiciel Libre 2007

C’est un grand classique, que je vais encore rater par manque de temps. (Pas de vacances pour le nouveau employé, c’est le revert de la médaille quand on change souvent d’employeur ;-/)

Mais j’espère bien qu’en 2008 j’arriverais à m’y rendre !

[Le site officiel des RMLL](http://www.rmll.info)

J’ajoute que [RubyFrance](http://rubyfrance.org) à prévu d’y tenir quelques conférences sur Ruby (et pas *que* RubyOnRails ! :) ).


