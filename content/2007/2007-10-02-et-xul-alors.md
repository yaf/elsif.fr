---
layout: post
title: Et XUL alors !
---

Non de XUL !

Ce matin en lisant ce billet du sur [lemondeinformatique.fr](http://www.lemondeinformatique.fr) au sujet des [risques liés à Ajax](http://www.lemondeinformatique.fr/actualites/lire-web-20-attention-aux-risques-lies-a-ajax-previennent-les-experts-24150.html) j’ai été surpris... Surpris par ce passage:

> Ted Farrell, architecte en chef et vice-président outils et middleware d’Oracle, rappelle déjà qu’en raison de la jeunesse des technologies, les entreprises doivent être prudentes quand au choix de leur solution : il y a deux grands frameworks sur le marché, et entre Microsoft et Adobe, il vaut mieux qu’elles ne se trompent pas car elles risqueraient alors de se retrouver coincées pour des années.

Hmmm. On parles ici de [Flex © Adobe Systems Inc.](http://flex.org/) et [SilverLight © Microsoft Corporation.](http://www.microsoft.com/silverlight/) je suppose.

Mais, arrêtez moi si je me trompe... Non... personne ? bon, Flex et Silverlight ne sont pas les pendants de [Xul](http://xulfr.org/) ? Alors pourquoi on en parle pas plus ? Surtout que là, pour la peine, avec Xul on reste *ouvert* !

Rappel: XUL c’est eXtensible User Language (il me semble). En gros c’est du <acronym title="eXtensible Markup Language">XML</acronym> pour la description d’interface, <acronym title="Cascading Style Sheet">CSS</acronym> pour la présentation et Javascript pour l’interaction *dans* l’interface. Que de la technologie existante, ouverte, spécifié, *standard*...

Alors le bon pour choix pour ne pas s’enfermer ne serais pas XUL ? Ah oui. LE pépin de XUL. Il faut soit [Firefox](http://www.mozilla-europe.org/fr/products/firefox/)  ou alors un [XulRunner](http://fr.wikipedia.org/wiki/XULRunner).

Quoique, On voit apparaître doucement des moteurs reprenant les spécifications de XUL comme [Lark](http://justlark.com/) (du XUL on Rails). Et a mon avis on a pas fini d’en voir... Je crois, et depuis un moment, que XUL est la bonne technologie pour les interfaces dit **riche**.

