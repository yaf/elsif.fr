---
layout: post
title: Message de service
---


Le week-end fut long. Et j’ai du faire des consessions pour que tout ce petit monde revienne en ligne.

La principale modification du serveur c’est un passage à [OpenBSD 4.2](http://www.openbsd.org) avec un changement de partage du disque. `/var` et `/home` font maintenant la même taille, et `/var` va accueillir la partie publique *enligne* du serveur.

Par contre, je pensais passer à Apache+FastCGI en environnement [chroot(2)](http://www.openbsd.org/cgi-bin/man.cgi?query=chroot&apropos=0&sektion=0&manpath=OpenBSD+Current&arch=i386&format=html) mais apache a vraiment un système de configuration un peut trop vieux et compliqué à mon gout...

Retour donc pour le moment à ce bon [lighttpd](http://www.lighttpd.net/) et un cluster de mongrel. Je pense que mongrel va finir par sauter pour laissé place à FastCGI (a voir) histoire de faciliter le *chrootage* de l’ensemble (plus facile de chrooté quand la communication se fait via des sockets que tcp je pense... à voir).

Voilà, pour le moment, c’est tout, vous pouvez retourner à une activitée normale.

![Xfce openbsd terminal](/files/xfce_openbsd_term.jpg)

*Un petit screenshot au coeur de l’action, avec un petit [uname(1)](http://www.openbsd.org/cgi-bin/man.cgi?query=uname&apropos=0&sektion=0&manpath=OpenBSD+Current&arch=i386&format=html) qui nous montre un bel OpenBSD 4.2 tout frais le tout avec mon archlinux clickaconvi comme on dit :)*


