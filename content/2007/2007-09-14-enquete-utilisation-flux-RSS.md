---
layout: post
title: Enquête sur l'utilisation des flux RSS
---

  
[Frederic de Villamil](http://fredericdevillamil.com/) lance une [grande enquête sur l’utilisation des flux RSS](http://fredericdevillamil.com/enqu%C3%AAte-combien-de-temps-passez-vous-chaque-jour-%C3%A0-lire-vos-flux-rss).

En voici les questions :
<ol>
* Quel Ã¢ge avez-vous ?
	* Quel est votre domaine dâ€™activités (informatique, marketing, modeâ€¦) ?
	* Combien de flux avez-vous dans votre agrégateur ?
	* Combien de temps passez-vous chaque jour à lire vos flux ?
	* OÃ¹ lisez-vous vos flux (maison, bureauâ€¦ )?
	* Quel agrégateur utilisez-vous (Google Reader, Bloglines, Netvibes, Lifereaâ€¦) ?
	* Vos lectures ont-elles principalement un rapport avec votre activité professionnelle ?
	* Classez-vous vos flux par thématiques dans votre agrégateur ?
	* Votre liste de lecture évolue-t-elle régulièrement ?
	* Pourquoi ?
</ol>

Pour répondre, ils vous suffit d’envoyer un mail à @sondage@de-villamil.com@ avec Sondage <span class="caps">RSS</span> en titre du mail (sinon elle risque de passer à la trappe).

Si vous aussi ça vous interesse, en plus de répondre, n’hésité pas à relayer l’info.

*part préparer sont petit mail de réponse*

  