---
layout: post
title: Welcome to Ruby 1.9.0
---


C’est le 25 décembre aujourd’hui... Et comme d’habtidue à cette période en hiver, c’est noÃ«l, ok, mais c’est surtout la sortie d’une version de [Ruby](http://www.ruby-lang.org/en/news/2007/12/25/ruby-1-9-0-released/) !

Que du bonheur ! Monsieur Matz non content  que certains test ne passe pas, a décidé de numéroter la version 1.9.0 au lieu de 1.9.1. Une autopunition :p M’enfin.

Pas mal d’avancé dans cette version: meilleur support d’unicode, rake et rubygem sont intégré à Ruby. Jean-François parle également d’un nouveau moteur d’expression rationelles sur le site de [rubyfrance.org](http://rubyfrance.org/articles/2007/12/25/sortie-de-ruby-1-9-0/).

Bon code !


