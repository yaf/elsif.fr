---
layout: post
title: Le web libre
---


Que ferait on aujourd’hui sans ces kilomètes de cable [RJ45](http://fr.wikipedia.org/wiki/RJ45), ces [Dorsales](http://fr.wikipedia.org/wiki/Backbone). Hein ? Et sans ce bon vieu (à l’échelle de l’informatique) protocole [HTTP](http://fr.wikipedia.org/wiki/Http) ? Déjà vous ne seriez pas ici, et moi non plus (sur ce blog bien sur).

Comme l’informatique bouge vite, avance rapidement, les premier documents fourni par ce protocole réseau au format textes on laissé leurs placent aux documents au [format ouvert](http://formats-ouverts.org/) [HTML](http://fr.wikipedia.org/wiki/HTML).

Mais depuis quelque temps le *web dynamique* nous gratifi d’_application en ligne_. On a même maintenant un [Web 2.0](http://fr.wikipedia.org/wiki/Web_2.0) et tout ça que c’est beau ça brille avec [Ajax](http://fr.wikipedia.org/wiki/Asynchronous_JavaScript_And_XML).

Alors je ne sais pas si je suis le seul, mais dans toutes ces applications *so web 2* et même les autres, plus simple (so web 1.0 ?) sont basé, pour la plupart, sur des outils opensource. A priori.

* en serveur web: [56% d’apache, 1,2% de lighttpd](http://www.journaldunet.com/solutions/dossiers/chiffres/technoswebserveurs.shtml)

* en base de données: [15% mysql, 4% postgresql, 12,8% firebird](http://www.01net.com/images/82111.gif) *(attention c’est global, pas uniquement web ici)*

* en langage de script: *pas de chiffres, j’ai pas trouvé* mais on le sait, PHP, Python, Ruby, Java

Alors c’est bien beau tout ça, ça donne l’impression d’être du coté clair de la force. Soit. Mais est-ce que les sources de toutes ces applications web sont disponible ? Où est le code source de [flickr](http://flickr.com/) ? Où sont les sources des plateforme de blog divers et varié (je ne parle pas des moteur de blog genre [Typo](http://typosphere.org/) mais plutôt des plateforme genre overblog et autres typepad) ?

Alors livrer un contenu en [Creative Common](http://fr.creativecommons.org/) c’est une chose, mais fournir les sources permettrais de ce placer réellement du bon coté de la force. *Promis si je fait une modif de Typo je fournirais les sources*. De toute façon la license nous y oblige dans la pluspart des cas. Mais qui le fait réellement ?

Pour remédier à cela, la [FSF](http://www.fsf.org/) ([FSF-France](http://fsffrance.org/)) planche sur une nouvelle license, dérivé de la [GNU GPL v3](http://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU), j’ai nomé la [GNU Affedro GPL v3](http://gplv3.fsf.org/agplv3-dd1.html)

Et pour finir, un petit extrait du pourquoi la réalisation de cette license est important:

> A secondary benefit of defending all users’ freedom is that improvements made in alternate versions of the program, if they receive widespread use, become available for other developers to incorporate. Many developers of free software are heartened and encouraged by the resulting cooperation. However, in the case of software used on network servers, this result may fail to come about. The GNU General Public License permits making a modified version and letting the public access it on a server without ever releasing its source code to the public.
>
> The GNU Affero General Public License is designed specifically to ensure that, in such cases, the modified source code becomes available to the community. It requires the operator of a network server to provide the source code of the modified version running there to the users of that server. Therefore, public use of a modified version, on a publicly accessible server, gives the public access to the source code of the modified version.
>

Alors messieurs du web 2.0.1_p35 merci d’arreter de proner les valeurs du libre sans les suivre.

*Bon d’un autre coté c’est peut-être moi qui n’est rien compris et qui me croyais dans le monde de oui oui où tout le monde il est beau, tout le monde il est gentil, et tout le monde fait du libre... C’est bien mon genre ça...*


