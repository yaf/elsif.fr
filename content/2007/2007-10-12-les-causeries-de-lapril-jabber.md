---
layout: post
title: "les causeries de l'APRIL: Jabber"
---


Depuis quelque temps, l’APRIL(Association pour la Promotion et la Recherche en Informatique Libre) ([april.org](http://www.april.org)) organise des [causeries](http://www.april.org/groupes/causeries/) :

> Les "Causeries APRIL" sont des interviews ou des discussions organisées régulièrement (avec un objectif d’une par semaine ou quinzaine), d’une durée d’une heure ou plus, sur un sujet donné.
>
> Les comptes-rendus sont publics ou privés suivant les sujets abordés.

La dernière date du 10 novembre 2007, elle concernais Jabber:

> "Jabber, les enjeux, et pourquoi la communauté du libre devrait Jabberiser" avec Nicolas Vérité, membre élu de la XSF (XMPP Standards Foundation) et adhérent APRIL.

Le compte rendu *publique* est très instructif. Alors medames et messieurs, si vous êtes en train de lire ça, j’aimerais bien que vous vous y mettiez ! Ajoutez moi en contact si vous voulez tout de suite remplir votre liste :-).

*Je me demande si je vais pas éradiquer mes autres comptes et ne garder que du jabber powered...*

ps: Je conseil fortement la lecture des autres comptes rendu des causeries, c’est souvent très instructif !

[![Banniere de soutien à l'April](http://www.april.org/association/documents/bannieres/banniere_horizontale_soutien_fulltext_486x60.png)](http://www.april.org/association/adhesion.html)

