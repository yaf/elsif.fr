---
layout: post
title: Message de service
---


Maintenant que la nouvelle version d’"OpenBSD":http://www.openbsd.org est sortie, la [bricabox](http://www.bricabox.info) va migrer. Mais non, finalement. Histoire de préparer l’arrivée de nouveaux services pour les colocataires ([Zifro et sont eternel teaser: Zlab.fr](http://www.zlab.fr) [Jean-mi et ces vaches](http://www.lacomte.net)) et de permettre une modification de l’environnement (passage prévu de lighttpd+mongrel à l’apacheChrootéD’OpenBSD + fastCGI), la bricabox va être re-installée.

Du coup, l’interruption de service pourrais être longue. Elle commencera quand nous aurons fait nos backup, et se terminera... Et bien vous verrez bien :)

D’ici là, soyez sage (oui j’ai remarqué qu’il y avait pas mal de spam en ce moment :-/)


