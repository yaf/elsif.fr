---
layout: post
title: Ngnix dans les ports OpenBSD
---

Sans vouloir faire de mauvais jeux de mots, on peut dire que ça tombe à pique !

Ce blog, ainsi que celui de [lacomte by Jean-mi](http://www.lacomte.net) et le *teaser infernal* j’ai nommé [zlab.fr](http://zlab.fr) tourne par le biais d’un frontal [Lighttpd](http://www.lighttpd.net/) et d’un cluster de serveur d’application [Mongrel](http://mongrel.rubyforge.org/). Lighttpd plutôt qu’apache car j’ai été séduit par la facilité de lecture et de confection du/des fichiers de configuration.

Mais voilà, cette petites phrase m’a fait réfléchir:

> I'm sad to say that I have to recommend people not use lighttpd anymore. The author hasn't updated the mod_proxy plugin and isn't providing too much support for the bugs it has. If you're running lighttpd and you constantly see 500 errors that are never recovered, then you should switch to Apache or another web server that can handle properly balancing backends.

([source sur le site de mongrel](http://mongrel.rubyforge.org/docs/lighttpd.html))

Du coup j’ai pendant un temps pensé à revenir sur [Apache](http://httpd.apache.org/). Mais en découvrant lighttpd je m’étais rendu compte que d’autre serveur existait derrière Apache et IIS. Il était temps de creuser la question.

Et là, ces dernier temps l’un d’entre eux sort du lot, un serveur *web* d’origine Russe: [ngnix](http://nginx.net/) \[engine x\]. Et ma fois, coté fichier de configuration c’est aussi clair que lighttpd, et apparemment c’est bien performant comme on l’aime.

Du coup, observation des [paquets OpenBSD](http://www.openbsd.org/4.1_packages/i386.html)... pas de ngnix... Regardage du coté des [ports OpenBSD](http://ports.openbsd.nu/) rien non plus :-(.

Alors une option:compile et construction de paquet maison ? hmmm. Même pas le temps de réfléchir à la question que voilà qu’une news tombe comme ça, un dimanche en plus, chez les morts-vivants: [ New Ports of the Week #36 (September 2)](http://undeadly.org/cgi?action=article&sid=20070909132009) et PAF! dans la liste voilà ti pas un [ports ngnix](http://ports.openbsd.nu/www/nginx) tout beau qui débarque.

C’est parfait. Le temps de faire quelque test et réglage, on vas voir si ça vaut le coup de changer.

*En ps, pour les fan de [TetriNet](http://www.tetrinet.org/), un ports pour le serveur [TetriNetX](http://www.openbsd.org/cgi-bin/cvsweb.cgi/ports/games/tetrinetx/) viens de voir le jour également.*


