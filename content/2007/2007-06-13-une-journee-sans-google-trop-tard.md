---
layout: post
title: Une journée sans google - trop tard
---


Je viens de lire sur [The Inquirer \[fr\] une nouvelle à propos d’une journée sans google](http://fr.theinquirer.net/2007/06/13/moteur_de_recherche_une_journe.html). Apparemment à l’initiative d’un blog anglophone, [altsearchengine.com](http://altsearchengines.com/2007/06/12/a-day-without-google/), la journée d’hier (le 12/06/2007) devait être consacré à la découverte d’autres moteur de recherche de google. C’est raté pour l’info fraiche.

Cela fait plusieurs jours que j’entends parlé de *accro a google, essayer d’utiliser autre chose* et tout un tas de reflexion de ce genre. Du coup j’essai, depuis bientôt 3 semaines, d’utiliser un autre moteur de recherche: [exalead](http://www.exalead.fr/search). C’est pas mal, c’est interessant. Ca reste un moteur de recherche de toute façon. Je pense que c’est surtout une question d’habitude.

Cet article de The inquirer m’a fait découvrir un autre moteur: [accoona](http://www.accoona.com/). Je pense que je vais l’essayer aussi. C’est important la diversité, le choix. On sait bien oÃ¹ mène un monopole, même quand il est de bonne foi (ou pas, hein Bill ;-)).

Il me faut maintenant trouver une alternative à gmail, ce qui je l’espère ne va pas tarder grace à [Gandi](http://www.gandi.net). Si on en crois [la page des services proposé par gandi](http://www.gandi.net/domain/detail) un  <acronym title="Gandi Mail"><span class="caps">GMAIL</span></acronym> devrais bientôt voir le jour. Alors certes, la version de base n’a pas les même fonctionnalitées (surtout en terme d’espace de stockage) mais on devrais pouvoir s’en sortir en archivant les vieux mails :-).

*A suivre...*


