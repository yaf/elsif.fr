---
layout: post
title: Mes plugins firefox
---


Malgrès les défauts que l’on peut lui trouver, j’utilise le navigateur [firefox](http://www.firefox.com) de la [fondation mozilla](http://www.mozilla.org/foundation/).

De temps à autres j’utilise aussi [Opera](http://www.opera.com/) (*bien qu’ils aient choisi QT pour le rendu graphique sous Xorg, mais bon...*).

Et j’ai quelque *outils* (plugins) dont je ne peut pas me passer.

*
[Colorzilla](http://extensions.geckozone.org/ColorZilla) indispensable pour vérifier, récupérer les couleurs du *web*.


*
[del.icio.us](https://addons.mozilla.org/en-US/firefox/addon/3615) (*la version de yahoo*). Outil génial pour la gestion et le partage des marques pages. J’avoue que celui là, je ne suis pas prêt de m’en passer. Ou alors faudra coder un truc vraiment génial ! :)


*
[Operator](https://addons.mozilla.org/en-US/firefox/addon/4106) Ca c’est parce que j’essai de me mettre au microformat et autres joyeuseté dans le genre. JE crois que ce genre d’outil ne demande qu’à être connu, mieux utilisé. A suivre...


*
[WebDeveloper](https://addons.mozilla.org/en-US/firefox/addon/60) alors celle là, comment faire sans ? En tout cas pour un développeur ? Ah si, j’ai entendu parlé de [Firebug](https://addons.mozilla.org/en-US/firefox/addon/1843), mais je n’ai pas encore essayé. Il faudrais peut-être.


Si vous en avez d’autres à me conseiller, on ne sais jamais. Mais je crois qu’avec ça, je suis paré pour surfer :)


