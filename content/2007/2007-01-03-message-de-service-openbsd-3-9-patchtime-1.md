---
layout: post
title: Message de service - OpenBSD 3.9  PatchTime #1
---

En préparation d’un migration à la version 4.0 du système du poisson lune, je vais appliquer les [patchs de sécurité de la 3.9](http://openbsd.org/errata39.html), histoire d’être le plus à jour possible et effectuer une migration douce.

Il se peut que le service de ce blog et de celui de [la comte](http://www.lacomte.net) soit perturbé. Désolé pour la gène occassionée.

*Depuis le temps que je doit faire ça... Ce qui est bête c’est que ce n’est pas grand chose, mais que ça prend du temps. Du temps de téléchargement (surtout au début pour récupérer les sources) et du temps de compilation. Mais maintenant que je suis dans une vrai société qui fait de la vrai informatique, j’ai un accès ssh à mon serveur et je peut tranquillement lancer un téléchargement, une compile, sans nuire à mon travail :)*



