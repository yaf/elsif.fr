---
layout: post
title: Pour garder des belles mains
---


*Enfin surtout pour les garder en bon état !*

Cela fait quelque temps que j’entends parler de la disposition de clavier [dvorak](http://fr.wikipedia.org/wiki/Clavier_dvorak). [Sunny](http://sunfox.org/) a même ecrit un billet avec [14 raisons pour passer au clavier dvorak](http://sunfox.org/blog/2007/04/14/14-raisons-pour-passer-au-clavier-dvorak/). Exellent, et je dirais qu’avant ce billet j’etais plutôt curieux, maintenant je suis convaincu.

Par contre son billet suivant regroupant [quelque question sur le clavier dvorak](http://sunfox.org/blog/2007/05/13/questions-sur-le-dvorak/) qui est censé nous donner le courage de s’y mettre n’a malheureusement pas eu cet effet sur moi. Du moins pas immediatement.

Alors depuis je tournicote autour de cette idée... et surtout autour d’un site sur [le clavier dvorak français](http://www.algo.be/ergo/clavier.htm). Et en parcourant ces pages, j’ai appris, et j’ai été surpris par certain des conseils fourni par ce site. Notament:

bq.L’arrière du clavier ne peut être plus haut que l’avant ce qui est malheureusement souvent le cas (l’avant du clavier est le côté du clavier près de la barre d’espacement).

Alors là, ça me bluf. Mais du coup **clac clac** j’ai rabattu les petits *élévateur* de l’arrière du clavier. On vas voir si à terme je m’y habitue. Pour le moment ce n’est pas désagréable.

Clavier Dvorak francophone:

!(/files/clavier-dvorak-fr.png)

*Maintenant il ne me manque plus qu’un bon coup de pied aux f\[es\]{5} pour m’y mettre... J’attends aussi avec impatience qu’un fabriquant sorte un clavier dvorak-fr qui, a mon avis, pourrais facilité sont utilisation.*


