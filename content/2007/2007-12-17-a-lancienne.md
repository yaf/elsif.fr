---
layout: post
title: A l'ancienne
---


Me voilà donc revenue dans le monde du service informatique. En mission chez un *grand compte* comme on dit. Une étape, pour me remettre dans le bain.

Mais voilà, chez ce client, comme chez beaucoup d’autres je pense, c’est **à l’ancienne** (ben oui c’est pas juste ma reprise de service qui est *à l’ancienne* ;)).

J’entends par là, vieille technologie propriétaire, vieille méthodologie non approprié.

D’un point de vue technologique d’abord. L’utilisation de cette technologie est une erreur et pour preuve. J’ai passé a peut prêt 75% de ma journée à essayer de remettre mon environnement de développement en route.

Forcement, j’ai une machine sous windows, assez récente, mais je ne l’utilise que pour me connecter en TSE à un serveur, sous windows également (déjà il y a du gÃ¢chis là, un [thin client](http://fr.wikipedia.org/wiki/Client_l%C3%A9ger) m’aurais suffis pour cela non ?). Bon passons pour les systèmes d’exploitation et le gÃ¢chis de matériel.

Alors forcement, la dessus, on se connecte tous on viens taper sur le même serveur de base de données et quand un petit malin (genre moi) fait tout planter sur un test de migration, ben c’est une 10aine de personnes qui sont au chomage technique. Mais il arrive aussi (caprice du monde propriétaire) que d’un coup, sans savoir, sans pouvoir analysé d’oÃ¹, un problème survient, vous empêche de vous connecter. Vous aller voir les voisins, eux aussi ne peuvent pas bosser... Alors on reboot, ça ne fait rien, on fait quelque danse *grigri* en priant pour que ça reparte (enfin ceux qui sont croyant, moi je vais prendre un café dehors en général :p).

Bref, matériel et technologie **à l’ancienne**

Pour ce qui est des méthodologies: Papiers, verrouillage des spécifications fonctionnelles.

> Tu sais ce que tu veux pour dans 2 ans pour des utilisateurs que tu n’as vu que 8 heures il y a deux mois ? ok ! Alors on verouille, tu pourras plus rien demander de plus pendant l’année qui s’écoule (si on a pas trop de retard genre voir chapitre précédent).

Horrible non ? Et bien c’est un peu ça que l’on souhaite mettre en place là, car forcement, *il y a trop de modification en court de développement*... Ben voyons.

C’est agile ça non ? Non effectivement, c’est **à l’ancienne** ...

Mais il ne faut pas croire c’est une bonne chose qui m’arrive ici. Je peut essayer d’imaginer comment je ferais autrement. Je peut aussi vérifier les arguments divers et varié pour ou contre tel ou tel méthode, technologie. On le sait, on le lit partout, mais s’y frotter pour de vrai c’est encore une autre affaire. J’ai déjà eu cette occasion y’a un moment, mais y revenir ça fait du bien, ça rafraÃ®chi la mémoire.

*Ceci dit, vivement la suite quand même :)*


