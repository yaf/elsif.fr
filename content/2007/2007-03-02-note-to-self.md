---
layout: post
title: Note to self
---

**Je n’oublierais plus d’installer mysql-dev avant d’essayer d’installer le binding ruby-mysql**

Il faut que je me marque ça sur un marbre, accroché en pendentif !

Ca doit faire au moins 3 fois que je me fais avoir. Bon ce soir, j’ai juste mis 10 minutes avant de m’en rendre compte. Donc histoire de ne plus oublier, je note ici la procédure. Elle se déroule sur macosX mais elle s’adapte à tout à peut de chose prêt.


<pre>
  {% highlight bash %}
    # Pour l’installation de mysql:
    pouype@pomme:pouype$ fink install mysql
    # pour la futur installation de ruby-mysql
    pouype@pomme:pouype$ fink install mysql-dev
    # Et après on peut sereinement executé:
    pouype@pomme:pouype$ gem install mysql
  {% endhighlight %}
</pre>

*[RubyGem](http://docs.rubygems.org/) c’est bien, c’est pratique. Il faudrait que j’en parle...*

__Dernière minutes__

Bon en fait j’avais pas vu, mais ça n’as pas marché, un problème pour trouver les librairies mysql on dirait. Bref, Gem c’est bien, mais on bonne installation de librairies à l’ancienne, avec un setup.rb, install.rb ça marche  mieux ;-)

