---
layout: post
title: Retour aux sources
---


Enfin, oui, c’est un peu ça. Disons que je suis de retour avec l’équipe (plus quelques personnes) qui m’a fait découvrir l’informatique et l’univers du développement.

La société qui m’avais proposé de passer de la mécanique à l’informatique à l’époque n’existe plus aujourd’hui. Des histoires de fusions mal digéré avait fait fermé la boite. Du coup, j’en avais profité pour aller voir l’herbe apparemment plus verte chez le voisin.

Mais la petite équipe à remonté une structure rapidement, des personnes qui avait envie de continuer à travailler ensemble pour les autres.

Et oui, c’est une petite <span class="caps">SSII</span> qui m’a montré le chemin, et c’est dans une <span class="caps">SSII</span> agrandi de 3 personnes que je reviens. Nous sommes une petite dizaine, étalé sur tout le territoire.

Revenir en <span class="caps">SSII</span> c’étais pour moi le moyen de me rapprocher du libre. Je crois que c’est plus facile aujourd’hui de travailler avec et pour le libre dans un <span class="caps">SSII</span> que chez un éditeur. Bon, c’est peut-être mon profil qui veux ça aussi... Sun ne m’a pas débauché pour travailler sur jruby :-/

Le but de mon retour chez eux est aussi d’apporter tout ce que j’ai récolté durant ces années de vadrouille (professionnelle et personnelle). J’espère pouvoir apporter les bonnes pratiques du web, la philosophie du libre, une vision plus *moderne*. En tout cas c’est l’objectif, pour eux comme pour moi. Alors en attendant de former une équipe pour se lancer dans l’aventure je reprend la main du *service* sur une mission de <span class="caps">MOE</span>.

Je ne préfère pas en dire plus pour le moment, mais ça fait du bien de retrouver un peu de dynamisme, de challenge, de défi, de rythme... J’avais l’impression de m’endormir là.

Je crois que j’avais besoin de passer un cap, prendre plus de responsabilité. J’essaie, je pense que ça va me plaire. Mais il faudra attendre la formation de l’équipe, et les première missions orienté *nouvelle technologie, web et autres* pour voir si tout colle.

*to be continued*


