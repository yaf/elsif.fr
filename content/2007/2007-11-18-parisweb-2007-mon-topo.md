---
layout: post
title: "ParisWeb 2007: mon topo"
---

Levé 6h30 histoire de pouvoir prendre un train pour me rendre à Paris (et une petite pensé pour tout ceux qui n’ont pas pu venir à cause de cette grève :-(). Cool y’a un train...

Ligne 14, j’adore, au moins celle là elle marche ! :) puis gare de lyon père lachaise, à pieds. Au moins je suis sur d’arriver, je suis même arrivé à l’heure.

Beaucoup de monde, café, croissants... Et viens le dur moment de choisir. Dans des moments comme celui-ci on aimerais bien avoir le don d’ubiquité... Bon il faut choisir, alors ça sera <span class="caps">XBL</span> par [Laurent Jouanneau](http://www.ljouanneau.com/blog/), depuis le temps que la techno [XUL](http://xulfr.org/) me titile.

![Fond d'écran des PC dans la salle de conférence: 'ParisWeb 2007 j'y suis'](http://zone.typouype.org/parisweb2007_jysuis.jpg)

Et bien, ce fut très interessant. Laurent nous a présenté <span class="caps">XBL</span> et nous a parlé d’XBL 2, spécification en cours de validation au W3C. J’avoue que même si l’utilisation des techno <span class="caps">XUL</span> me semble très interessante, je suis un peu réticent à travailler avec une techno qui ne fonctionne que sur une plateforme (même si celle-ci est disponible sur la plus part des OS). Or, si d’autres navigateur implémente XBL2, ma position par rapport à cette techno pourrais bien changer ! :)

Petit pause...

Bien, où je vais aller ensuite.. “Questions &amp; réponses techniques sur les bonnes pratiques” ? hmmm Non je vais aller voir ce qu’il se dit sur: “Un site web rapide ? c’est pas sorcier” !

Les deux compères de Téléfun sont d’humeur joyeuse. Celà me donne également l’occasion voir [Mat](http://blog.virgule.info/) en vivant et en direct

Conférence interessante, beaucoup de chose déjà connu, mais un rappel ne fait pas de mal, et quelques nouvelles pistes à explorer. Notamment le gzippage des pages statiques, et les headers de gestion de cache.

En sortant pour manger, un petit coucou à "Sunny"http://sunfox.org/ rapide, je ne voulais pas m’imposer pour le repas avec ça petite troupe :). Ca fait plaisir de voir tout ce beau monde du web réuni en un endroit.

Autant le matin à été orienté technique, autant l’apres-midi sera être accessible :)

Après avoir temp entendu parlé du <span class="caps">RGAA</span>, je suis allé voir l’éxplication de texte d’"Aurélien Levy":http://www.fairytells.net/ sur ce fameux Référentiel. Très interessant, on sent bien que l’homme est très impliqué dans ce qu’il fait. Nous avons pu voir qu’il reste beaucoup de boulot dans ce domaine, mais que la direction est bonne.

Petite pause encore...

Grosse hésitation pour la dernière conf de la journée... [Christophe Porteneuve](http://www.tddsworld.com/blogs/eapc/) fait une conférence sur les librairies javascript. J’ai entendu dire que c’etais un bon orateur, mais voilà, en me pointant devant la salle, en lisant “script.aculo.us”, “jQuery” &amp; co sur la porte, cela ma refroidi... Je crois que je suis comme Mat la dessus, je ne suis pas fan de toutes ces “librairies Javascript dont seul 3 ou 4 lignes nous suffisent bien souvent”...

J’ai donc opté pour la continuité et suis allé voir la conférence sur :“la conception d’un site accessible: démarche et bilan” présenté par Laurence Borne, Marie Destandau, Julie Landry et Emilie Sidoli.

Un description de leurs travaux de fin d’étude pour le [centre Georges Pompidou](http://www.handicap.centrepompidou.fr/handicapcp/site/index.php). Une sorte de mise en pratique de l’accessibilité après la théorie du <span class="caps">RGAA</span>.

Leur démarche m’a semblé être la bonne, une bonne étude pour comprendre l’objectif avant de ce lancer, le resultat technique est bonet beau. Il montre surtout que l’on peut faire de l’accessiblité tout en étant inovant(e :)) sur les designs et autres structure de site. Bravo. Ensuite, on se rend compte que la plus grosse problématique finalement n’est pas technique, mais plutôt le coté “social”. Il faut préparer les équipes à fournir un contenu accessible (film, band son, tableau), c’est sûrment ça qui est le plus délicat.

Une bonne journée bien rempli, le tout très bien organisé. Un petit point noir cependant, les salles informatique de l’"INSIA":http://www.insia.org/ ne sont pas vraiment pratique pour assister à des conférences, elles sont plutôt faites pour des ateliers pratique. Mais bon. C’etais très bien. J’y retournerais avec plaisir, puffy aussi :

!(http://zone.typouype.org/puffy_parisweb2007.jpg)

