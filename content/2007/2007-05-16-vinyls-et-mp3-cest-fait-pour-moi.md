---
layout: post
title: "Vinyls et mp3 : c'est fait pour moi !"
---


En voilà une idée qu’elle est bonne ! Un label Anglais a décidé de fournir avec ces vinyls un code permettant de télécharger l’équivalent du dit vinyl, mais au format mp3. Non DRMisé !

Bon c’est sur les morceaux ne sont pas fourni au format libre [OGG](http://fr.wikipedia.org/wiki/Ogg), mais c’est un point interessant voir ce que certain label sont prêt à proposer. Et puis ça évite d’avoir à faire la conversion (chose faisable, mais pas forcement des plus pratique dans certains cas).

**Le vinyl n’est pas mort !**

source: [The Inquirer](http://fr.theinquirer.net/2007/05/16/un_label_anglais_vend_des_viny.html)

