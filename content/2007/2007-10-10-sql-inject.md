---
layout: post
title: SQL Inject
---


Un très bon strip d’[XKCD](http://xkcd.com/about/) aujourd’hui. On y vois sûrement quelque chose de vécu, qui deviendra de plus en plus courant si l’éducation ne fait pas attention au développement de ces outils :-)

![xkcd n° 327](http://imgs.xkcd.com/comics/exploits_of_a_mom.png)


