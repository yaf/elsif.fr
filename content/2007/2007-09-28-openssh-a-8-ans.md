---
layout: post
title: OpenSSH a 8 ans
---

  
[Hier c’étais l’anniversaire d’OpenSSH](http://undeadly.org/cgi?action=article&sid=20070926182742). [OpenSSH](http://www.openssh.com/) est une implémentation Libre d’"SSH":http://fr.wikipedia.org/wiki/<span class="caps">SSH</span> un protocole de communication sécurisé. OpenSSH est l’implémentation la plus utilisé. Souvent copié, jamais égalé :-p

C’est évidemment un projet lié à [OpenBSD](http://www.openbsd.org).

Personnellement, je l’utilise tout les jours.

!(http://media.typouype.org/openssh.gif)

  