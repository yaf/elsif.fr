---
layout: post
title: OpenBSD hostname.change
---


**Pan**, oui je me tape sur les doigts. Déjà parce que ça fait un moment que j’ai pas blogué mais surtout parce que j’ai oublié comment on effectuais un changement de nom de machine sur un serveur OpenBSD.

Pour essayer de retenir cette procédure, et surtout la noter quelque part, en voici la teneur:

Pour commencer, il faut modifier le fichier **/etc/myname** (fichier lu au démarrage pour connaitre le nom de la machine).

Ensuite pour que les modifications soit prisent en compte on “redémarre” le démons en charge du réseau: **sh /etc/netstart foo0** **sh** est nécessaire pour l’execution de la comment netstart. Certaines commande (enfin, au moins celle là) ne sont pas executable telle quelle. Bien sur il faut remplacer le **foo0** par votre interface réseau (si vous en avez plusieurs) sinon ce paramètre n’est pas nécessaire.

*C’est pas grand chose, certain peuvent même rire tellement c’est bête comme manoeuvre mais j’arrive jamais à m’en souvenir :p. Il faudrais aussi aborder les démons unix qui sont bien plus propre et complet que les “taches” windows (le nom en dit long de toute façon :p ).  Mais là maintenant le plus important c’est de trouver un appartement à Montpellier !!*

