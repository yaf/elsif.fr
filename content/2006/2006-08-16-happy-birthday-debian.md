---
layout: post
title: Happy Birthday Debian
---

  

Et oui, c'est le 16 aout 1993 que Ian Murdock annonçait une nouvelle distribution GNU/Linux: [Debian](http://www.debian.org)



[Le message de Ian sur comp.os.linux.development](http://groups.google.com/group/comp.os.linux.development/msg/a32d4e2ef3bcdcc6?output=gplain)



13 ans... Une bien belle réussite quand on vois combien de distribution se base sur debian ! Même si ce n'etais pas le but premier de cette distribution.
D'ailleurs, [Etch devrais être là pour Décembre](http://www.fr.debian.org/News/2006/20060724).



<b>Longue Vie à debian !</b>


  