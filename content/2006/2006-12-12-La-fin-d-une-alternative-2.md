---
layout: post
title: La fin d'une alternative (2)
---

J’avais déjà énuméré [une série de machine sur une architecture ppc](/2006/08/09/powerpc-la-fin-dune-alternative.html) pouvant faire office d’alternative pour disposer de cette architecture depuis qu’Apple à décidé de fournir ces machines dans une architecture x86.

Mes finances allant un peu mieux, je me suis interessé recemment à l’une d’elle en voulant passer le cap... Et quelle fut ma deception de voir que c’etais fini, belle est bien fini. [L’alternative OpenDesktop proposé par pegasosppc (genesi)](https://www.pegasosppc.com/store.php?category=10) est en rupture de stock. Et si mon anglais approximatif n’est pas trop mauvais, j’ai l’impression que c’est plus qu’une rupture de stock. C’est belle est bien l’arrêt de cette alternative :’(

> The Pegasos motherboard and related products have sold out and been discontinued. Genesi would like to thank it’s customers for their support and sales of this product line.

*Pourquoi je n’ai pas craqué quand j’ai découvert ce bijou ? :’(*


