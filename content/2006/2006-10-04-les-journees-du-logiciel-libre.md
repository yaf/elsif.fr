---
layout: post
title: Les Journées du Logiciel Libre
---

  

Et bien depuis le temps que ça me travail, je vais enfin aller aux [journées du logiciel libre](http://www.jdll.org/)! J'ai pris les billets de train, c'est donc réglé. J'y serais le samedi 14. Je vais me taper un aller-retour dans la journée, ça me rappeleras mon passé de consultant :p



J'espère bien pouvoir y rencontrer quelque personne et surtout les quelques membres de [l'association rubyFR](http://rubyfr.org) dont [notre président Fredix](http://fredix.freemonk.org/) qui [apparement devrais être là (le 14 aussi ?)](http://fredix.freemonk.org/articles/2006/09/25/jdll-2006), et notre [secretaire de choc Zifro](http://hazzard.free.fr/zifro/), avec qui je partage aussi une passion pour OpenBSD ;).
J'imagine que notre trésorier est aussi dans les parages car il me semble qu'il est lyonnais... Et puis plein d'autres gens bien sur :p



Les [conférences s'annonces interessantes](http://www.jdll.org/2006/conferences). Par contre il faut faire des choix le samedi. Je pense que pour le matin c'est tout vu. Je veux en savoir plus sur [Xen](http://www.cl.cam.ac.uk/research/srg/netos/xen/), les [formats ouverts](http://formats-ouverts.org/) et DADVsi sont aussi des sujets qui m'interesse :). L'après midi s'annonce plus délicate dans les choix par contre... Xul, Wikipedia, Latex d'un coté, Clients légers et écologie de l'autres :-/. Je verrais sur place.



Dites moi si vous y passé :)


  