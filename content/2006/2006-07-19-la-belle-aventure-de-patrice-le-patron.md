---
layout: post
title: La belle aventure de Patrice le Patron
---

  

Comme bien souvent je porte aujourd'hui un ticheurte [lafraise.com](http://lafraise.com). Eh oui même au boulot ! Ils sont cool à mon boulot. C'est le ticheurte ["La belle aventure de Pierre la pierre"](http://www.lafraise.com/t-shirt-178p25-pierre-la-pierre.html).



Je ne veux pas vous parler directement de mon habit du jour, mais plutôt de Patrice, le boss de la fraise.com, le patron. Durant l'année que j'ai passé [en sa compagnie](http://www.lafraise.com/blog/) j'ai partagé ces moment de vie qu'il nous offre avec une belle plume. J'adore les ambiances qu'il donne, qu'il transpire sur la toile. J'avoue que plus d'une fois j'ai rêvé de me retrouver dans son univers.


Mais voilà, toute histoire à une fin, et [Patrice à decidé de se mettre en retraite](http://www.lafraise.com/blog/2006/07/ma_retraite_35.php). Il a vendu lafraise à [SpreadShirt](http://www.spreadshirt.net/?lang=fr&locale=FR)  qui est de la partie (vente de vetement personnalisé).



Ca fait bizarre, comme disent certain, lafraise n'aurais plus le même gout. Je pense que je continuerais à acheter des zolie ticheurte là bas si les graphistes sont toujours au rendez-vous, y'a pas de  raisons. J'espère aussi pouvoir repartager l'univers de Patrice à travers une autres expérience, un autre projet, voir juste un blog...



<b>Merci Patrice ! Merci pour tout ce bon temps ! Merci pour ces belles idées !</b>
Tu fais parti des gens qui sont, pour moi, des points de mire, des *modèles*, des sources d'inspirations pour avancer toujours plus loin. J'espère un jour avoir l'occassion de participer à un projet aussi agréable <s>qu'avais</s> qu'a l'air d'être  lafraise.com ;-)


  