---
layout: post
title: "OpenBSD: installation sur Mac Mini"
---

J’hésitais entre un post au sujet de [Ruby](http://www.ruby-lang.org/) et [OpenBSD](http://openbsd.org). Finalement je vais faire les deux.

Commençons par OpenBSD qui à la primeur car ça date d’hier soir

Vous le saviez peut-être déjà, mais après windows, gnu/linux (divers et varié), un petit tour rapide sous mac OS X, puis un retour sous GNU/linux, j’ai très envie d’apprendre au sujet des BSD.

Suite à plusieurs lecteurs et plusieurs post sur [pourquoi choisir OpenBSD](/2006/06/01/pourquoi-choisir-openbsd), [mes tout premier pas sous OpenBSD](/2006/05/06/openbsd-premier-pas) et [mon échec à faire ce que je souhaite sous OpenBSD](/2006/05/08/openbsd-debian), je m’étais décidé à attendre la fin de ma lecture [des cahiers de l’admin: BSD : Les dessous d’UNIX](http://www.amazon.fr/exec/obidos/ASIN/221211463X/171-6687168-0343433?%5Fencoding=UTF8) pour m’y remettre.

Je ne suis qu’à la moitié du bouquin que j’ai déjà craqué ! hier soir j’ai viré ma Debian pour y mettre un OpenBSD. Je pense avoir trouvé le système qui me plait.

## Petit tour de cette installation.

Tout d’abord il est bon de rappeler que je dispose d’un MacMini que voici:

![Mac mini powerpc](/files/gros_plan_mac_mini_powerpc.jpg)

De plus j’ai acheter mon OpenBSD, le projet à besoin de don ou d’achat. J’ai donc opté pour l’achat d’un jeu de CD et d’un t-shirt ;-)

J’ai donc insérer le CD 2 qui correspond au CD pour les machines à base de PowerPC, et démarré ma machine en restant le doigt appuyer sur la touche C (c’est un raccourci de l’OpenFirmeWare d’apple, je revient dessus plus bas) pour que la machine démarre sur le CD.

Puis je procède comme indiqué sur la pochette du CD. Cette pochette est très bien faites, et une fois déplié (j’essayerais de mettre une photo tiens) elle contient d’un coté un exemple complet d’installation par défaut, et de l’autres divers astuces ou petite commande de base à effectuer.

Une seul chose diffère. Au moment de créer mes DiskLabel (tranche) j’ai retailler la dimension maxi d’occupation pour OpenBSD sur mon disque.

Apparement même en demanant d’utiliser tout le disque dur (dans mon cas 80g) fdisk ne peut pas faire une partition unique aussi grande. Il faut donc par le biais de l’utilitaire de disklabel modifier la taille. En l’occurence il sagit de saisir la commande ‘b’ dans l’utilitaire, celle-ci déclenche une série de question sur la taille d’occupation, et permet donc de spécifier '\*' pour rectifier la taille max

Ceci effectué j’ai pu créer mes divers label, avec un /usr de 5g et un /home de 70g (plus le swap, le /, le /tmp et le /var comme recommendé dans la doc)

A part ce redimmensionnement, j’ai laissé toutes les options par défaut. J’utilise un réseau DHCP avec un roteur D-Link, donc la configuration réseau n’a posé aucun problème. Par contre, à la fin de l’installation, le programme propose d’activer sshd et de désactiver ntpd. Si j’ai bien compris, sshd est un démon SSH ([man page d’SSHd](http://www.openbsd.org/cgi-bin/man.cgi?query=sshd&sektion=8)), il sert à pouvoir se connecter à distance sur la machine. Personnelement je ne le fait pas car en général si je ne suis pas chez moi je l’éteind. J’ai donc répondu ‘No’. Par contre pour le démon ntpd ([doc OpenBSD sur OpenNTPD](http://www.openbsd.org/faq/fr/faq6.html#OpenNTPD)) qui permet de garder une horloge système synchronisée, même si ça ne met pas trop utile, je l’ai activé. De toute façon, comme la pluspart des démons, je pourrais aller modifier ce comportement plus tard.

Une fois le reste de l’installation terminé, j’ai effectué le dmsg comme demandé dans l’afterboot, et envoyé à l’équipe d’OpenBSD. Ca leur permet d’avoir des infos sur les architectures sur lesquel OpenBSD est installé. Dans mon cas je l’ai fait la première fois, mais pas hier: ma machine n’a pas changée entre temps ;-)

Viens ensuite le premier reboot. Et là, il faut démmarrer en utilisant 4 doigts ! Un pour la touche Alt, un pour la pomme (j’ai utilisé celle de gauche, à coté du alt :-p) un sur la lettre o et un sur la lettre f.

Cette séquence de démarrage peut paraitre barbare comparé au bios boot que l’on retrouve sur les PCs. Effectivement Apple sur ces machines utilise ce que l’on nome l’OpenFirmWare pour la gestion de démarrage de ces machines. Toujours est-il que c’est tout le contraire. Suite à cette séquence de touche, on arrive sur une sorte de console sur fond gris clair, très agréable (déjà là on sent bien la touche design apple ;-) ) sur laquelle, et malgré un clavier reconnu comme qwerty, on peut effectuer un tas de commande. Le gros problème c’est qu’il n’y a pas de commande d’aide. Je ne pourrais donc pas vous expliquer tout ce que l’on peut faire, mais juste ce que j’ai fait.

*Attention je crois que ce fameux OpenFirmWare a disparu avec les machines MacIntel*.

A partir de cette “console” j’ai donc pu démarré mon openBSD via `boot hd:,ofwboot /bsd`

La commande boot comme son nom l’indique c’est pour démarrer. HD c’est l’emplacement ou trouver le système à démarrer. On pourrais ici mettre cdrom pour démarrer à partir du CD ou tout autre emplacement reconnu par l’OpenFirmware (je ne sais pas si ça pourrais booter sur l’USB ou le réseau). Ensuite ofwboot est le programme de démarrage d’OpenBSD pour macppc (pour d’autres sytême ? ) qui vas lui utilisé le /bsd pour trouver le noyau à démarrer.

faire ces manipulation à chaque démarrage me gène un peu (surtout les 4 doigts à vrai dire). Je me suis donc renseigné et j’ai découvert une superbe commande: `setenv boot-device hd:,ofwboot /bsd`

En appuyant sur entré, la séquence de boot automatique à changer. Maintenant  Le firmWare ne cherchera pas mac OSX au démarrage, mais mon OpenBSD ;-)

Il faut maintenant que je configure mon Xorg, et mon environnement graphique. Ca devrais se passer ce soir ou demain.

