---
layout: post
title: Typo Thèmes
---


Ce blog est motorisé par [Typo](http://www.typosphere.org/), ça vous le saviez :) Je suis en train de me demander si je refait la peinture moi même (solution la plus probable, mais pas pour tout de suite) ou alors si j’utilise un des nombreux thèmes proposé sur [proofread.digital-achievement.com/catalog/](http://proofread.digital-achievement.com/catalog/)

Personnellement j’aime bien [Blindess](http://proofread.digital-achievement.com/catalog/themes/view/61) de [Bob Aman](http://sporkmonger.com/), trés épuré, simple.  Eventuellement ça pourrait me servir de base pour travailler sur une idée que j’ai en tête d’ailleurs :).

Dans un autre style, plus classique, j’aime bien le [BrownType](http://proofread.digital-achievement.com/catalog/themes/view/71) de [Todd Ericksen](http://ericksenfamily.com/) aussi.

J’hésite beaucoup à prendre l’un d’eux, ou attendre que je refasse quelque chose de plus personnel. Et si je refais du personnel, je me demande si je fais du fond sombre avec texte en clair ou l’inverse.

*De toute façon j’ai pas trop le temps là :p je me pose juste la question. Ah tiens j’ai remis en place un typouype.org et www.typouype.org :) Merci Sylvin!, il fallait effectivement que je fasse une petite modif chez [Gandi](http://gandi.net) ET dans ma config de [lighttpd](http://www.lighttpd.net/) :)*

