---
layout: post
title: radio_libre_attitude.entree_libre.saison2.start
---

  

Le grand come back, la grande rentrée ! une bonne émission de radio libre attitude. Tout les mardi soir via cette url: [http://libre-attitude.org/radio/RLA.m3u](http://libre-attitude.org/radio/RLA.m3u).



Venez écouter les interviews de [Fimagina](http://fimagina.jmtrivial.info/blog/index.php). Toujours armé de ça bonne humeur. Des morceaux sous license libre aussi, en général y'en a pour tout les gouts. Je ne sais pas si [rOotix](http://rootix.info/) est encore au commande (je pense que oui ;-) ). Mais [mon petit doigt me dit qu'il y a de la nouveauté, et surtout un nouveau](http://vsjmbessot.freecontrib.org/articles/2006/09/26/pour-ceux-qui-laurait-loup%C3%A9)



Pour en savoir plus, [Cameleon nous fait un leger historique et présente le programme de ce soir](http://blog.cameleon.mine.nu/index.php?2006/09/26/131-entree-libre-est-de-retour). Il y a aussi [le site principal de l'association libre-attitude](http://libre-attitude.org/wakka.php?wiki=PagePrincipale) et [le site dédié à la radio-(libre-attitude](https://libre-attitude.org/radio/)



Moi j'ajouterais le channel de la radio: #radio-libre-attidue sur irc.freenode.net. Un robot nous donne les infos sur les titres en cours, et bien souvent on peut intervenir, poser des questions.. Arf je viens de retrouver [le post de Fimagina qui explique comment s'y rendre](http://fimagina.jmtrivial.info/blog/index.php?2006/06/01/56-xchat) (pour ceux qui découvre les chats :) ).



A noter que libre-attitude est assez proche (mais pas lié) à [Jamendo](http://jamendo.com) diffuseur de musique sous [license creative commons](http://creativecommons.org/)



A ce soir  !


  