---
layout: post
title: Retour au clickodrome
---


Et oui en revenant sous debian, j’ai commencé par reprendre ce bon vieux gnome. J’aime beaucoup les rendus GTK. Je trouve gnome très agréable, et depuis la 2.14 j’avoue que tout vas plus vite ;-).

Mais voilà: gnome, tout comme kde, s’intalle avec un tas de petit utilitaires très pratique, mais que je n’ai pas choisi. J’aime les programmes qui ne font qu’une seul chose, mais bien. J’ai donc décidé de voir ou en était [xfce](http://xfce.org)

Environnement / gestionnaire de fenêtre utilisant également gtk, il ne fait que ce qu’il est sensé faire: gérer mon bureau et mes fenêtres. De plus dans les dernière version (4.3 pour debian etch) on bénéficie des changements programmé pour la 4.4 (qui viens de sortir en béta... vite vite vite dans les depots debian ;-) ).

Voici ce que l’on arrive à obtenir:

![Screenshot gestionnaire de bureau XFCE](/files/xfce_green.jpg)

Dans les environnement clickodrome, je dirais que je conseillerais gnome pour les débutants, et xfce pour les confirmés... Mais peut-être qu’avec la 4.4, xfce pourrais très bien aller au débutants également !

