---
layout: post
title: "OpenBSD: un systême complet (installation)"
---

Je vais essayer de retranscrire ici mes expériences avec OpenBSD. J’ai [fait une première tentative](/articles/2006/05/06/openbsd-premier-pas) qui a [échoué](/articles/2006/05/08/openbsd-debian), pour [mieux retenter une autre fois](/articles/2006/06/08/openbsd-installation-sur-mac-mini), puis [recraqué pour recommencer](/articles/2006/06/11/openbsd-xorg-config) puis encore [fait un bilan final](/articles/2006/07/14/openbsd-mon-bilan) pour finalement [faire le retour du retour du retour du retour](/articles/2006/09/05/openbsd-le-retour-du-retour-du-retour-du-retour). Je crois que j’aime trop ce système et surtout ça philosophie... Mais je ne vais pas exposer mes aller retour. Je vais plutôt partager ce que j’en apprend, et comment je m’y prend ;-) (d’ailleurs vous pourrez peut-être m’aider ?)

## Acquérir OpenBSD

Pour commencer il faut acquerir OpenBSD. Personnellement j’ai commandé les cd de la 3.9 (la version 4.0 devrais sortir en  novembre). Ca permet d’aider un peu le projet, et de bénéficier de jolis CD avec une belle pochette :)

[![Cover openbsd cd 3.9](/files/openbsd_39_cd.jpg)](/files/openbsd_39_cd.jpg)

[![Intérieur du cd 3.9 d'openbsd](/files/openbsd_39_interieur.jpg)](/files/openbsd_39_interieur.jpg)

Sérieusement, les CDs contiennent tous ce qu’il faut pour une installation de base pour la plupart des architectures disponibles, et les sources (qui permettent de ne faire qu’une synchronisation CVS si on souhaite utiliser les ports, mais on en parlera plus tard). De plus, la pochette contient toutes les instructions pour installer le système, des recommandations, des dessins sympa, bref autant quand pour découvrir linux j’avais acheter la boite RedHat et regretté, autant là, je suis très heureux de l’avoir acheté.

[aller voir sur le site pour plus d’info](http://www.openbsd.org)

Si vous préféré télécharger une image d’OpenBSD, il faut savoir qu’il n’existe pas d’image complète du système (pour pousser à l’achat des cds ? ). Cependant on peut trouver une image minimale permettant l’installation via le réseau d’un système complet: [par exemple dans ce repertoire on trouve une  un CD39.iso pour powerpc qui correspond au démarrage de l’installation](ftp://ftp.arcane-networks.fr/pub/OpenBSD/snapshots/macppc) et on peut trouver [ici toute les architectures pour lesquelles un cd d’installation minimale peut être trouvé](ftp://ftp.arcane-networks.fr/pub/OpenBSD/snapshots)

## L’installation

On vas pas faire dans la dentelle, pas de dual boot, on utilise tout le disque. Pour le dual boot ça dépends surement due l’environnement, étant sous powerpc je ne pense pas couvrir la plupart des bessoin (puis j’aime pas les dual boot :-p ).

D’ailleurs je ne vais pas rentrer dans un détail de questions/réponses, juste aborder les points que je pense spécifique à OpenBSD (peut-être à tout les <span class="caps">BSD</span> ? ). Dans un premier temps: la partition: on en a déjà entendu parler vaguement, les <span class="caps">BSD</span> n’utilise pas le système de partition tel qu’on le connais. Il utilise une partition certes, mais une seul. Après on peut toujours monter d’autres partition, mais le système doit être installé completement sur une seul. Par contre les <span class="caps">BSD</span> utilise des *slice* ou tranche pour compartimenter le système. Rien de bien effrayant. L’utilitaire [DISKLABEL](http://www.openbsd.org/cgi-bin/man.cgi?query=disklabel&apropos=0&sektion=0&manpath=OpenBSD+Current&arch=i386&format=html) devrais se lancer automatiquement quand ça seras nécessaire. Personnellement j’ai du utiliser la commande me permettant de redéfinir la taille de prise en charge du disque, pour qu’il prenne tout (peut-être du à mon système osX qui traine encore par là).

Voici ce que disklabel propose comme commande:

!(http://media.typouype.org/disklabel_commands.png)

En gros je me suit servi donc de b pour modifier la taille à prendre en compte (*set OpenBSD disk boundaries*)

d pour effacer les eventuelles tranche mal faites et de a pour en ajouter d’autres.

Il y a quelque informations importantes par rapport à ce disklabel. Tout d’abord, le disklabel c correspond à la tranche de base, c’est l’image de la partition sur laquelle on s’installe. Il ne faut pas le supprimer (par contre la commande b permet de l’agrandir si elle ne prend pas toute la partition :) ). Ensuite j’ai une particularité sous mac (uniquement ?) c’est une une tranche i. Ca correspond à mon <span class="caps">MBR</span>, un bout de disque qui vas servir pour le démarrage. Donc pas touche non plus (je crois que c’est spécifique powerpc). La tranche b est automatiquement placé en swap. C’est comme ça et pas autrement. Ensuite les conseils/bonne lecture propose d’utiliser:

* a : /
* d : /tmp
* e : /var
* g : /usr
* h : /home


(comment ça on a sauté une lettre ? ben oui c’est bizarre... enfin c’est pas très grave :) ).

Pour ce qui est des tailles, à part pour le swap conseillé avec un minimum de 32m, le reste est à la disposition de chacun. Petite précision, on peut saisir les tailles en précisant l’unité: 10k, 10m , 10g...). Voici ma table de disklabel (à titre d’exemple)

  !(http://media.typouype.org/disklabel_sample.png)

  Comme ce n’est pas une isntallation serveur, il n’y a pas d’élement exotique au niveau du réseau par exemple, je précise <span class="caps">DHCP</span>, le système me trouve tout ça tout seul, tranquillement. Les options par défaut sont bien souvent les bonne. On crée juste un compte root (en fait juste le mot de passe).

  Ah si, il faut choisir les paquets de base à installer. Effectivement les BSDs (?) on leur serveur X "embarqué". Pour openBSD les raisons sont la sécurité. Du coup le serveur X sur openBSD est passé au crible, revue et corrigé par l’équipe. L’inconvénient c’est que du coup les mises à jour sont moins fréquentes, le serveur X est un peu "vieux".

  Donc on choisi ces paquets, la liste n’est pas longue:


  Tout les paquets commençant par X sont les paquets graphique. Pour une installation "de bureau" forcement, on prend. Je ne pourrais pas vous expliquer l'utilité de chacun, mais pour un système comme celui que l'on veut, on prend tout. Je pense que pour un serveur ça peut être interessant de ce pencher sur la question (game39 par exemple ne doit pas vraiment servir :) ).

  h4. Post installation

  Et voilà, une fois fait, le système est fonctionnel. Un "EJECT (1)":http://www.openbsd.org/cgi-bin/man.cgi?query=eject&amp;apropos=0&amp;sektion=0&amp;manpath=OpenBSD+Current&amp;arch=i386&amp;format=html du cdrom s'impose, et un petit reboot ne fait pas de mal.

  Nous voilà avec un prompt qui ne pourras marcher qu'avec le user Root (le seul existant pour le moment). La première chose à faire une fois connecté c'est envoyer le résultat d'un "DMESG(8)":http://www.openbsd.org/cgi-bin/man.cgi?query=dmesg&amp;apropos=0&amp;sektion=0&amp;manpath=OpenBSD+Current&amp;arch=i386&amp;format=html à l'équipe OpenBSD:

  <notextile>
  Cela leur permet de recueillir des informations sur les systèmes installé.

  Voilà, j’aborderais la suite plus tard, ça fait déjà un billet bien long. J’allais oublier: le site officiel [OpenBSD](http://www.openbsd.org) sur lequel il y a beaucoup de docs, d’infos. Notamment [les pages man qui sont accessible](http://www.openbsd.org/cgi-bin/man.cgi) (voir aussi les liens sur les commande <span class="caps">DISKLABEL</span>, <span class="caps">EJECT</span>, <span class="caps">DMESG</span> dans le billet) et un très bon [guide d’installation](http://openbsd.mcom.fr/faq/fr/faq4.html). Oui oui, finalement je n’ai fait qu’un commentaire de texte :) la prochaine fois ça seras peut-être plus personnelle
  </notextile>
