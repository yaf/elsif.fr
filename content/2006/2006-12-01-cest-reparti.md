---
layout: post
title: C'est reparti !
---

Dans l’optique d’arrêter de me disperser, pour finalement ne rien voir aboutir, j’avais stopé toute activité pour mettre en place cette dédibox, accueillir [Jean-mi et son blog de la comte](http://www.lacomte.net), préparer ma migration, essayer de mettre en place une bonne architecture pour [Ror](http://www.rubyonrails.org/). Maintenant c’est fait, enfin presque.

Je dit presque parce qu’il reste quelque bricole à faire niveau administration/paramétrage de ce serveur:

*Modifier le lighttpd.conf pour prendre en compte le [typouype.org](http://typouype.org) de la même manière que le [www.typouype.org](http://www.typouype.org).*

Faire une bonne gestion des sous domaine. Pour [Jean-mi de la comté](http://www.lacomte.net) je sais pas, mais moi j’avais [photos.typouype.org](http://media.typouype.org) et [zone.typouype.org](http://zone.typouype.org). J’aimerais les récupérer, ou les modifier, mais pouvoir jouer avec des alias quoi :) c’est plus propre.

*Revoir la gestion des mails aussi, histoire que le système soit sous surveillance un peu plus poussée.*

Peut-être revoir l’architecture pour Ror aussi. J’ai actuellement opté pour un Lighttpd+mongrel, mais un récent articles/tutorial à éveillé ma curiosité [Au sujet de l’installation de Ror sur OpenBSD avec Apache+FastCGI dans un environnement CHROOTé \[en\]](http://bsd.phoenix.az.us/faq/openbsd/rails-chroot-fastcgi) (bon c’est pour openbsd 4.0, mais en épluchant un peu, peut-être que à passeras sous OpenBSD 3.9 :) ).

*Par rapport à au dessus: peut-être mettre à jour le serveur en OpenBSD 4.0 en recompilant ce qui vas bien (on peut très bien le faire apparemment...) Est-ce nécessaire ? Dans un environnement serveur je dirais oui. Mais je vais peut-être attendre un peu.*

Faire un nouveau thème. Celui là est proposé dans le Typo 4.0.3 sur lequel tourne ce blog maintenant.

Ca en fait encore pas mal des choses à faire :‘( . Je parlerais pas des divers changement de ma vie privée pour le moment, mais ça commence à prendre forme (j’en parlerais quand ça seras décidé, verrouillé). Je parlerais pas non plus de l’idée qui germe dans mon esprit de m’acheter une machine de type [Soekris](http://www.soekris.com/) plutôt qu’un deuxième mac mini :).

En tout cas, la migration fini, l’installation sur la dedibox + le changement de gestionnaire de nom de domaine (passé chez [Gandi](http://gandi.net)) me permet de voir des choses avancer. Ca fait du bien, et ça motive pour continuer comme ça.

*On est pas sorti de l’auberge quand même, mais là maintenant la lumière ne s’éteindras pas (enfin... on verras :p )*

ps: J’ai ENFIN activé les commentaires en textile :) on peut mettre un peu de gras et de saut de ligne maintenant :) désolé de pas l’avoir fait plus tôt.

Edit: Raa ! il faut que je m’habitue aussi à mettre des tags sur mes billets :)

