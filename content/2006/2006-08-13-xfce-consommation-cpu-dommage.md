---
layout: post
title: "Xfce: consommation CPU, dommage"
---


Dire que je m’était décidé à utiliser [Xfce](http://www.xfce.org/) tout le temps...

Xfce c’est le gestionnaire de fenêtre qui est souvent appelé le petit frère de [Gnome](http://gnome.org). Surement parce qu’il utilise lui aussi <span class="caps">GTK</span> comme librairie graphique. Mais il est plus léger que Gnome. En fait c’est surement parce qu’il intègre beaucoup moins de fonctionnalité divers. De plus il n’embarque pas avec lui tout une série d’application plus ou moins obligatoire (genre Evolution, Synaptic,...). Attention  je n’ai rien contre ces applications (j’utilise Synaptic comme gestionnaire de paquets), mais j’aime avoir le choix.

Donc j’utilise Xfce, mais ces dernire temps j’avais une consommation de <span class="caps">CPU</span> un peu trop forte...

Tout a commencé quand j’ai supprimer le repertoire Desktop de mon <span class="caps">HOME</span>. Ca ma supprimer les icones placer sur le bureau. Je pensais me pencher sur le problème plus tard. Mais maintenant à chaque fois que je veut glisser un dossier sur le bureau, le <span class="caps">CPU</span> s’emballe, et le pire c’est qu’il ne s’arrête pas :’(

[![Screenshot du problem avec Xfce sous debian](/files/screenshot_debian_xfce.jpg)](/files/screenshot_debian_xfce.jpg)

Je vais essayer de réparer le coup du Desktop effacer, peut-être ça reviendras à la normal.

**Edit:** Je viens de remettre en place le repertoire Desktop (je l’avais fait, mais en me trompant dans la casse, j’avais mis desktop au lieu de Desktop). Ca me permet de pouvoir remanipuler des éléments avec le bureau, mais cependant, j’ai toujours le problème de CPU...

