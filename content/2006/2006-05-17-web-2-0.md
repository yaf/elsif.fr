---
layout: post
title: Web 2.0
---

Qui aurais pensé à ça ? l'apparition de la methode XmlHTTPRequest, disponible uniquement sur IE ? Pas grand monde sur le coup.

Plusieurs personne toutefois on commencé ) en voir l'interêt et s'en servir, discretement. Je me souviens dans ma société de services à l'époque, nous débutions dans les interfaces Web. Marre de refaire tout le temps la même chose, du coup nous avions entrepris de developper un "framework" javascript. Au moment de décider d'un mode de communication avec le serveur, nous avions choisi d'utiliser cette methode dont personne n'osais parler à l'époque. Nous nous demendions si c'etais le bon choix. L'explosion des solutions à base de génération de page coté serveur nous faisais pensé que nous avions tord

Et finalement. Maintenant un site qui n'utilise pas la combo Xml Javascript n'est plus dedans, n'est pas "Web 2.0". La honte.

Le nombre de service basé sur cette technologie est impressionnant [à en croire cette liste fourni sur www.econsultant.com](http://www.econsultant.com/web2/index.html). Un groupe de travail c'est même formé autour de ce Buzz: [web20workgroup](http://www.web20workgroup.com/).

Et pourtant, comme à l'époque avec mes collègues, je n'arrive pas à me dire que c'est la meilleur solution. Le web n'a pas été créé sur ce modèle, mais plutôt pour passer d'un document à un autre. Alors faut-il être contre le progrès ?

Peut-être que ce qui me dérange c'est que souvent tout ceci est fait au détriment de l'accessibilité. A en croire [ces recommandations \[www.w3.org\]](http://www.w3.org/WAI/References/QuickTips/qt-fr.htm) ce n'est pas le tout AJAX qu'il faut faire.
De même qu'en lisant [ce livre blanc de l'accessibilite](http://www.braillenet.org/accessibilite/livreblanc/ressources.html) je me demande si [gmail](http://mail.google.com/mail/) , [traitement de texte web](http://www2.writely.com/info/WritelyOverflowWelcome.htm) et autres, sont utilisable via [lynx](http://lynx.browser.org/). A voir.

De mon coté je souhaite l'accessibilité, il faut d'ailleurs que je me penche très sérieusement sur mon site, qui est, pour le moment, propulsé par typo, pour vérifier sont accessibilité.

Grand utilisateur de javascript à une époque, j'ai des remorts à m'en servir maintenant. Même pour des application intranet qui pourtant sont souvent effectué en obligeant les gens à utiliser tel ou tel navigateur, tel version. Mais ceci n'empèche-t-il pas l'intégration dans l'entreprise de personnes ayant un handicap ? Pourquoi une personne mal-voyante ne pourrait-elle pas bénéficier des même outils que les autres ?

Bref, je sais pas si c'est le printemps, mais en ce moment je remonte tout ces débats du fond de ma mémoire, en espérant trouver cette fois une ébauche d'idée de solution. En attendant, je vais aller faire à contre coeur de l'ajax dans mon application intranet.

