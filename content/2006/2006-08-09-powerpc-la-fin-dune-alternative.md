---
layout: post
title: "PowerPC: la fin d'une alternative ?"
---

En lisant un journal de [neriki](http://neriki.free.fr/)  [au sujet des processeur PowerPC \[via DaLinuxFrenchPage\]](http://linuxfr.org/~neriki/22353.html), je me suis rendu compte que Apple ne proposerais bientôt plus de machine sous PowerPC...

Quand Apple avais annoncé sont passage à Intel, on a pensé assez vite que c'etais pour bénéficié de tarif plus bas et pouvoir affiché les même fréquences que les concurrents PC.

Moi qui viens utilise ce type de processeur/architecture depuis maintenant plus de 6 mois, je trouve quand même quelque avantage non négligeable à l'architecture PowerPC. Mon mac mini est silencieux, consomme peut d'énergie. Est-ce un avantage uniquement présent sur les mac mini ? Il me semble pourtant avoir approché quelque iBook et autre Machine à base de G5 qui me semblais également silencieuse et fraiche au touché :). Bref, je suis déçu. Je pensais pouvoir continuer à utiliser , acheter des machines à base de powerPC... J'ai peur que ça s'arrete...

Mais finalement, en cherchant un peu (merci à neriki et aux divers commentaires du journal) on arrive à trouver des machines à base de powerPC. C'est sur c'est pas Apple, mais finalement je ne suis pas attaché à la marque personnellement. Surtout pour sans manque de participation aux standards permettant l'interopérabilité ...

Voici donc que je me renseigne un peu sur les machines disposant de PowerPC en vente ici ou là.

* Dans un premier temps chez IBM bien sur: [et là je trouve deux machines](http://www-03.ibm.com/servers/intellistation/power/). Bon le système est à base d'AIX, mais je suis certains de trouver un NetBSD qui tourne la dessus, voir une Debian. Ce qui bloque le plus c'est le prix: $5,999 pour l'une et $8,099. J'espère qu'à ce prix la, l'écran est fourni :D. Bon je note, mais c'est hors budget dans un futur proche.


* Puis viens [q40](http://www.q40.de/index.html)  des Allemands. Basé sur de vieux PowerPC  (68LC060), leurs machines n'ont pas l'air de foudre de guerre... Mais c'est interessant, et à suivre.


* On retrouve ensuite [IYONIX](http://www.iyonix.com/index.html). Déjà une belle Box en photo, un petit pinguin qui traine: Ca me plait bien.
Je suis loin d'être un professionnel en ce qui concerne l'administration de système, et du coup j'ai de grosse lacune en ce qui concerne le matériel. Du coup quand je lit ça:

> Processor
> ARM compatible INTEL XScale 80321

Je me demande si c'est vraiment du PowerPC :). De toute façon ce qui est interessant c'est que ce n'est pas du X86, c'est sur :-p.
Apparement le système d'exploitation par défaut n'est pas un linux/unix:

> OPERATING SYSTEM
> RISC OS 5 held in 4MB Flash ROM (field upgradeable)

mais en cherchant un petit peu pret du pinguin:

> The IYONIX pc is able to run a full featured Linux system, as a powerful alternative to its native RISC OS. This version of Linux is based upon a port of ARM Linux kernel, and the flexible Debian distribution.

debian ? parfait :)

Coté tarifs, ça vas de £799 à £1299 (sans écrans ni option en tout genre). Bon ça vais quand même entre 1 185.81 euros et 1 927.87 euros, c'est pas donnée, mais c'est moins cher que chez IBM (c'est peut-être pas le même niveau de finition au niveau des machines non plus ...). Ce qui est sympa c'est qu'on peut acheter de bon écran et un tas de périphérique en même temps (au moins on est sur de la  compatibilité matériel). A suivre également.

* J'ai gardé le meilleur pour la fin (à mon avis): [Genesi](http://www.pegasosppc.com). Pas mal de lien et autres forums mon amené sur leur site. Ils ont un [partenariat avec Debian](http://www.debian.org/partners/index.fr.html) ce qui assure déjà un fonctionnement de debian sur leurs machines. En regardant de plus prêt la machine qui m'interesserais: [Open Desktop Workstation (PegasosPPC)](http://www.pegasosppc.com/odw.php) (j'adore le nom déjà :-p) j'ai été agréablement surpris de voir un G4 en guise de procésseur (possibilité de G3 également). Les spécifications sont interessante:

> Specifications are as follows:
>  * Pegasos II with Freescale 1.0GHz processor
>  * 512MB DDR RAM
>  * 80GB ATA100 Hard Disk
>  * Dual-Layer DVD±RW Drive
>  * ATI Radeon 9250 graphics - DVI, VGA and S-Video out
>  * Low Profile Small Footprint Case - Tower or Desktop Orientation (92x310x400mm)
>  * USB 2.0 (High Speed) expansion

Et j'avoue surprenante pour un fabriquant qui n'occupe pas le devant de la scene.

Au niveau système d'exploitation ça promet:

> Genesi has become a partner/sponsor of the Debian, Ubuntu, Gentoo, Fedora, openSUSE, Crux Linux as well as OpenSolaris/Blastware and NexentaOS communities. Genesi is also enrolled in the official board support and/or partner programs of Terra Soft Solutions, Montavista and QNX Software Systems which are also targeted at the platform.
>
> A variety of Operating Systems are available for the platform and in particular Genesi offers the following Developer Support Site. More information on Operating Systems available for the Open Desktop System can be found here.

En allant voir la liste des OS disponible
[Et ben y'a le choix effectivement !](http://www.pegasosppc.com/software.php).
[La vidéo de présentation de genesi est également très interessante](http://www.genesippc.com/presentation.php).

Le tout pour $799.00 (€ 650.00), sans écran, clavier souris, mais bon comme un macmini finalement. En plus ça n'a pas l'air de prendre beaucoup plus de place (92x310x400mm):
![image de ODW à partir du site de genesi](https://www.pegasosppc.com/images/products_opendesktop.png)

En gros, je crois que je vais pas tarder à m'acheter ce petit joujou... Enfin on vas faire les comptes post vacances avant, mais c'est un bon point de repli puisqu'Apple ne veux plus proposer de PowerPC :-).

