---
layout: post
title: self.job montpellier.job.new
---


Une page qui se tourne disent certain. Vendredi soir j’ai terminé un épisode de ma vie de développeur. Lundi j’en commence un nouveau.

J’ai beau avoir vécu plusieurs fois cette situation, ça fait toujours une drôle d’impression de quitter une aventure, une équipe. Car même si au niveau du boulot j’avais certain désacord avec les choix pris, j’appréciais beaucoup l’équipe. Mais c’est comme ça. La vie est faite de rencontre, certaines d’entre elles durent plus que d’autres, certaines s’en vont et reviennent.  Les rencontres sont toujours enrichissante.

Lundi donc, je fait mon arrivée au sein de l’équipe Montpelliéraine. Juste deux jours pour, je supose, une présentation, introduction, mise en jambe. Ensuite je passe le reste du mois de décembre au siège, à Paris.

Cette page qui se tourne à un gout spécial, je ne change pas juste de boulot, c’est un nouveau chapitre qui commence, que dis-je, un nouveau livre :)

*Tout ça pour dire que je serais surement absent jusqu’à mercredi :)*

