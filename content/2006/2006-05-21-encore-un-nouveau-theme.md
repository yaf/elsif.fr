---
layout: post
title: Encore un nouveau thême
---

Le thème discouype est trop coloré à mon gout, c'etais amusant à faire mais je pense que jour après jour c'est fatiguant pour les yeux.

J'ai donc réalisé ce nouveau thème, plus simple, plus épuré au niveau des couleurs.

Je suis incapable de me situé sur une echelle de mesure au niveau de mon adoration pour la nature. Mais je ne pourrais pas habité trop loin d'une forêt. La photo qui me sert de banière ici est une photos prise dans la forêt juste à coté de chez moi, une magnifique forêt ou j'adore me balader, et ce à toute les saisons.

Voilà, ce nouveau thème s'appel donc "simple and green". Je pense que je finirais quand même le discouype, histoire de proposer ensuite à chacun de modifier le thème à sa guise ;-). Je founirais également un tarball contenant tout ce qu'il faut.

J'en profite pour remettre les commentaires, après tout, même s'il n'y a pas grand monde, c'est tout de même agréable de pouvoir échanger sur un sujet.

