---
layout: post
title: RubyFrance cherche son logo
---


_Oui je suis super à la bourre, mais bon, mieux vaut tard que jamais :p Surtout que les délais ne sont pas dépassé._

L’association RubyFrance cherche son identité visuelle. Se voulant être le groupe des utilisateurs francophone du langage de programmation [Ruby](http://www.ruby-lang.org), utilisé, entre autres, par le framework qui monte qui monte [RubyOnRails](http://www.rubyonrails.org/), le logo doit contenir un lien fort avec le ruby de ruby ;). Mais les touches personnel sont justement rechercher pour démarquer un peu l’asso et qu’elle soit reconnaissable facilement, bref une marque de fabrique quoi, un logo :D

N’étant pas des graphistes trés doué (vous devriez voir les rubis que je dessine huhu), nous lançons un appel à propositions :) quelques une sont déjà arrivée, y’a des trucs vraiment sympa. Qui à dit que les graphistes n’était pas intéressé par l’univers libre ?

**Toute les informations sur le site de l’association [RubyFrance \[RubyFrance.org\]](http://rubyfrance.org/articles/2006/11/23/logo/)**

