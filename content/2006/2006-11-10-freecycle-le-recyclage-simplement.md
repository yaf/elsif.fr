---
layout: post
title: Freecycle, le recyclage simplement
---

  
Je en sais plus par quel moyen je suis tombé la dessus, mais c'est un des moyens qui permettrais d'avancer dans le bon sens à mon avis.

[Les groupes FreeCycle](http://fr.freecycle.org/) se proposent de mettre en relation des gens qui souhaite se débarrasser d'objet divers, et ceux qui pourrais en avoir besoin. Combien de fois on a mis (moi le premier) quelque chose à la poubelle, avec un pincement au coeur en se disant: "Mince ça pourrais encore servir quand même"....  Voici leur explication sur le concept:

>
> Le réseau mondial Freecycle est constitué d'une multitude de groupes à travers le globe. Il s'agit d'un mouvement basique de personnes qui offrent (et récupèrent) des objets gratuitement dans la ville où ils habitent (et alentour).
>
> Les groupes Freecycle mettent en relation des personnes qui souhaitent se débarrasser d'objets qui les encombrent avec des personnes qui en ont besoin. Notre but est de libérer les espaces naturels d'objets abandonnés bien qu'encore utiles. En utilisant ce que nous avons déjà sur cette planête, nous réduisons le consumérisme à outrance, la production de masse, et en réduisons l'impact nocif sur la planête. Un autre avantage à utiliser Freecycle est qu'il nous incite à nous défaire d'acquisitions compulsives dont nous n'avons plus usage et encourage chacun à adopter une attitude communautaire.
>

Ce n'est peut-être pas grand chose, mais ça permettrais de limiter un peu la consommation, et de s'entre aider un peu aussi, ça ne nous feras pas de mal au contraire, je crois que tout ça se perd un peu...

*Ca me fait un peu penser au [Bookmates](http://www.chezjoelle.net/bookmates/) de JoÃ«lle, mais bon, c'est plus global comme concept.*
  
