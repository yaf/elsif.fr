---
layout: post
title: Git ou Mercurial
---

  Cela fait un moment qu'un nouveau troll à pointé son nez. Après _vim versus emacs_, _gnome versus kde_ et tant d'autres, on a maintenant _git versus mercurial_.

Derrière ces deux noms se cache un outil de "gestion de versions":http://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_versions. Contrairement à "CVS":http://fr.wikipedia.org/wiki/Concurrent_versions_system ou "SubVersion":http://fr.wikipedia.org/wiki/Subversion_(logiciel) ces deux là (et quelques autres) sont dit "décentralisé":http://fr.wikipedia.org/wiki/Gestion_de_version_d%C3%A9centralis%C3%A9e. Ce mode permet de nouvelles possibilités dans la manière dont les équipes travaillent.

On trouve beaucoup de comparatif entre ces deux outils, et je ne suis pas convaincu par les uns ou les autres. Pour moi les seules différences que je vois aujourd'hui c'est:

* langage source : Git est écrit en C, Mercurial en Python
* Commande: Git utilise trois lettre @git@, Mercurial en utilise deux @hg@

Une des grandes forces de la communauté des utilisateurs de Git est d'avoir eu très rapidement accès à un outil d'hébergement : "github":https://github.com/ . De plus, l'équipe du framework "RubyOnRails":http://rubyonrails ayant adopté Git, la communauté Rails l'a également adopté. Bien sur, beaucoup d'autres projets utilisent Git, notamment le noyau Linux.

Mais Mercurial n'est pas en reste (contrairement à ce que l'on pourrait croire).

La communauté d'utilisateur de Mercurial a également un outil d'hébergement: "bitbucket":http://www.bitbucket.org/ ou encore "freeHg":http://freehg.org/, et pour ce qui est des projets phare ayant choisi mercurial on retrouve "mozilla":http://hg.mozilla.org/ , "NetBeans":http://hg.netbeans.org/ , "OpenJDK":http://openjdk.java.net/ , "OpenSolaris":http://opensolaris.org/, "Xen":http://xenbits.xensource.com/ , et "beaucoup d'autres":http://www.selenic.com/mercurial/wiki/index.cgi/ProjectsUsingMercurial

Pour le moment mon choix c'est porté sur Mercurial (allez savoir pourquoi). Cependant, je crois qu'avant de faire un choix définitif, il me faut apprendre à me servir des deux. Je me suis donc créé un compte sur GitHub, un sur BitBucket et un sur freeHg. GitHub et BitBucket propose tout deux une utilisation de type _premium_. Par exemple:

* "*GitHub*":http://github.com/plans propose un nombre *illimité de repository public* associé à *un nombre illimité de collaborateur* le tout avec 100MB d'espace disque. Ensuite c'est une location par mois avec une augmentation des repository privée associé à un nombre restreint de collaborateur et une augmentation de l'espace disque disponible.

* "*BitBucket*":http://www.bitbucket.org/plans/ propose lui *un repository privé* et un nombre *illimité de repository public* le tout devant tenir sur *150 MB*. Ensuite, ce sont des tarifs par MB et fonction du nombre de repository privée.

Ces deux là sont partis sur des offres payantes assez différentes. Chacune d'entre elle peut avoir sont intérêt selon les besoins.


* "*freeHg*":http://freehg.org/tos/ semble plus libre en apparence (je n'ai rien vu au sujet de ma carte bleu, à part un bouton _donate_). Par contre il impose l'utilisation de licence libre pour les projets hébergé, et décline toute responsabilité en cas de problème.


Pour être honnête, je viens de découvrir freeHg en écrivant ce billet... Je crois que tout ceci est un peu _frais_ pour moi, je vous en dirais plus quand j'aurais manipulé un peu.


  