---
layout: post
title: SQLite Backup
---


Evolution de la [bricabox](http://www.bricabox.info) oblige, certain scripts doivent aussi évoluer. C’est la vie du code, je ne vous apprend rien. C’est donc le moment de reprendre un peu la [tache Rake de backup](/2007/08/07/rake-sauvegarde-de-base-de-donnee.html) pour y ajouter quelque morceau.

La bricabox a maintenant un site pour elle, basé sur [Radiant CMS](http://radiantcms.org/), Jean-mi a construit un [portail de la comte](http://www.lacomte.net) également basé sur RadiantCMS (oui au passage, sont blog à changé d’adresse: [blog.lacomte.net](http://blog.lacomte.net)). Tout les deux n’ayant pas pour vocation d’être gavé de données, ni d’avoir beaucoup d’accès en écriture, nous avons tout les deux opté pour [SQLite](http://www.sqlite.org/). <del>Une petite base de donnée</del> un petit programme de gestion de base de donnée relationnelle écrit en C, très leger, très pratique dans ce genre de cas de figure (pas de configuration complexe, pas de serveur...).

Mais voilà, la tache de backup des bases de donnée avait été écrit uniquement pour les bases de l’époque: [MySQL](http://www-fr.mysql.com/) (je vous passe l’actu du jour sur le rachat de MySQLAB par Sun Microsystem ? :D). Donc il faut ajouter quelque ligne là dedans.

Un peu de recherche m’amène sur [une bonne introduction pour SQLite chez IBM](http://www-128.ibm.com/developerworks/opensource/library/os-sqlite/) (enfin, vu la facilité d’utilisation, est-ce nécessaire :D). Tout ça pour finalement ajouter un test et une petite ligne:

<pre>
  {% highlight bash %}
    sh “sqlite3 -batch #{db_config[‘database’]} .dump &gt; #{backup_file}”
  {% endhighlight %}
</pre>

Et voilà les taches de backup qui sont à nouveaux opérationnelles.

*Bon reste à voir où je place ces fichiers...*

*J’aime de plus en plus SQLite :) Attention à utiliser avec modération quand même*


