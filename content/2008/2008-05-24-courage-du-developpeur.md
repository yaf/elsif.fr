---
layout: post
title: Courage du developpeur
---

Le **courage** fait parti des clefs de l'[eXtreme Programming \[wikipedia\]](http://fr.wikipedia.org/wiki/Extreme_programming). Il est nécessaire lors du ***refactoring***. Bien trop souvent les développeurs n’osent pas modifier du code existant. Il existe plusieurs raisons à cela:

## C’est le code de...

AAah et bien si c’est le code du grand guru maison, qui oserais modifier sont code. Il penserait surement que l’on critique son code, qu’on ne le trouve pas assez bien. Et c’est peut-être le cas, ou alors tout simplement, ce code à besoin d’évoluer. Et plutôt que d’ajouter de nouvelle chose, il faut reprendre, modifier une partie du code existant. Cela évitera les redondances, les erreurs, et le code mort. Je garde mon apologie du refactoring pour un autre billet.

## Trop compliqué à lire

Justement ! C’est qu’il faut le reécrire ce code ! Quel horreur du code illisible. Source de bugs, peut-être que ce source fait beaucoup trop de chose par rapport au besoin. Simplifions le !

## Je n’ai pas le temps

Je crois qu’il faut de temps à autre avoir le courage de prendre le temps, de perdre du temps. Cela pourrais s’avérer bénéfique par la suite. Si j’ajoute plutôt que de modifier, qui me dit que je ne vais pas devoir y revenir une fois, deux fois, n fois pour corriger un bug, un disfonctionnement, un effet de bord ?


On parle du courage du codeur, le courage de modifier du code, mais je voudrais juste aborder ici un autre courage, celui-ci c’est pour les divers responsables et autres chefs de services: **le courage de revenir sur une décision quand elle s’avère être mauvaise**.

Trop de projets sont ecrasé contre un mur (ou alors y vont tout droit) parce qu'en haut personne n’ose, personne n’a le courage de tirer les leçons d’une série d’échecs, personnes n’ose revenir sur une méthodologie mauvaise.

Personnellement, je fais de l’informatique pour rendre service à des utilisateurs. Si je vois les utilisateurs heureux lors d’une livraisons, je le suis aussi. C’est un bon moyen de vérifier que nous sommes sur la bonne route. Mais justement, je m’égare de la route du courage dont je voulais parler :-)

N’ayons pas peur de modifier du code. Pour nous aider dans ce sens nous avons plusieurs outils à notre disposition:

## Outils de gestion de configuration (ou versioning)

Ces outils nous permette de revenir à une version précédente avec une facilité déconcertante. Alors bien sur il faut en choisir un qui corresponde bien à nos besoin, mais je crois qu’aujourd’hui nous avons l’embarras du choix !

## Test unitaire

Les tests ! En voilà un outil. Avec une bonne batterie de test, nous sommes sur de ne rien casser. Comment ne pas *oser* un refactoring avec ça ? On modifie, on relance les tests, ça passe ? bien ça marche alors :)

## Langage et architecture

Tout cela est bien beau, mais c’est vrai qu’avec un langage et/ou une architecture ou tout élément et imbriqué dans l’autres, une architecture ou tout est lié, une architecture ou l’on gère plusieurs fonctionnalité dans un même source, c’est beaucoup plus effrayant de modifier un morceau. C’est une des raisons qui me font adorer l’Objet et les architectures associé. Un objet à une responsabilité, et une seul (enfin, il devrait). Pas de code cherchant à tout faire, souvent mal. Au moins c’est simple.


**Courage et <acronym title="Keep It Smart Simple">KISS</acronym> <acronym title="Don't Repeat Yourself">DRY</acronym>**


