---
layout: post
title: OpenBSD Fondation - donation
---


Ca y est, le premier *communiqué de presse* de la [fondation OpenBSD](http://www.openbsdfoundation.org/) annonce les contributions reçus pour le financement du projet [OpenBSD](http://www.openbsd.org).

> From: The OpenBSD Foundation
> Contact: directors@openbsdfoundation.org
> Date: January 4, 2008
>
> THE OPENBSD FOUNDATION ANNOUNCES FIRST CORPORATE DONATIONS
>
> The OpenBSD Foundation is privileged to announce the receipt of its first corporate cash donations.
>
> The very first donation was received from Norwegian network engineers SystemNet AS.
>
> This was followed very shortly by several significant anonymous donations, as well as contributions from Hewlett Packard and Google.
>
> Many thanks to our corporate donors.
>
> All donations were received soon after the Foundation announced its existance, enabling the Foundation to start operations and perfect its donation handling.  These donations have enabled the Foundation to assume the costs of OpenBSD’s networking expenses, and begin to acquire several key pieces of hardware for development and infrastucture.
>

[full version](http://www.openbsdfoundation.org/press/pressrelease-1.txt)

J’espère que tout ça permettra au projet de ne plus avoir de soucis financier !

*tiens faut que je fasse un petit don aussi moi cette année ;-)*

*via [undeadly](http://undeadly.org/cgi?action=article&sid=20080104201743) évidemment*


