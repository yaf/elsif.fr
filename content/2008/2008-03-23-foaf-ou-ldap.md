---
layout: post
title: Foaf ou LDAP
---

  Je cherche depuis quelque temps à constituer mon carnet d'adresse dans un format _portable_,_ouvert_ comme il faut quoi.

La démarche normale serais de placer un serveur "LDAP":http://fr.wikipedia.org/wiki/LDAP sur la "bricabox":http://www.bricabox.info. Mais LDAP, bien qu'énormément utilisé ne me semble pas convenir à mes souhait, ou peut-être est-ce parce que je ne suit pas assez familiarisé avec l'engin. Ou peut-être me manque-t-il un bon client fait pour _administrer_ mon carnet. L'utiliser, ça c'est facile, tout les client mail peuvent se brancher sur un LDAP. Et pour ce qui est de l'authentification, c'est pareil, on trouve des tutoriaux pour chacun d'entre eux, ou presque.

Etant un petit fan d'XML, je suis un peu les discussions autour du WebSemantic également. Et j'avais noté l'existance de FOAF(Friend of a Friend) ("FOAF sur wikipedia":http://fr.wikipedia.org/wiki/FOAF). C'est un vocabulaire "RDF(Ressource Description Framework" ("RDF sur wikipedia":http://fr.wikipedia.org/wiki/Resource_Description_Framework) pour décrire des personnes et leurs relation. Interessant. Mais finalement cela me posait un problème: la mise à disposition de certaines informations personnel. Pour les adresse email il y a apparemment un mécanisme basé sur un hashage qui permet de masquer les adresse email. Mais rien n'empêcherais quelqu'un de faire un mauvais usage de simples noms.

Et a force de fouiller sur le sujet, je suis tombé sur une présentation en PDF de "Mr Sebastian Dietzold":http://aksw.org/SebastianDietzold?v=6rk dont je met à disposition une copie chez moi: "Accessing RDF Knowledge bases via LDAP clients":http://zone.typouype.org/access_rdf_know_db_ldap_client.pdf (Si ça pose un soucis je l'enleverrais). Et la je voie du LDAP ET du FOAF (en fait du RDF plutôt mais bon).

Du coup je suis paumé dans ma reflexion. Je ne sais pas trop quoi penser de tout ça. Je devrais surement poser un peu mes idées sur papier, et ensuite seulement chercher un outil qui covienne (voir en modifier un s'il le faut).

Et vous vous faites comment ? Un fichier csv tout moisi  et pas à jour ? Un carnet papier ?

*edit:*
Et voilà qu'en continuant mes recherches d'infos sur le sujet, je tombe sur un post-it du "forum d'alsacreations":http://forum.alsacreations.com/forum.php rédigé par "RaphaÃ«l Goetter":http://blog.goetter.fr/ : "Qu'est-ce que FOAF":http://forum.alsacreations.com/topic-3-16402-1-Quest-ce-que-FOAF-Friend-Of-A-Friend-.html On y apprend qu'il y a une mutinerie... Ou plutôt que quelque grand monsieur du web n'apprecie pas trop la compléxité de Foaf et on créé XFN...
Mais la "critique constructive de Karl Dubost":http://www.la-grange.net/2003/12/17.html a coupé cours mes envies d'investigation dans ce nouveau venu. Il fallait cependant que je vous tienne au courant :-p


  