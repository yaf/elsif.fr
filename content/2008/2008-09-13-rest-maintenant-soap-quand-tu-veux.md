---
layout: post
title: REST maintenant, SOAP quand tu veux
---

  
J’ai lu il y a quelques jours un billet de [Jean-François Helie: REST ou SOAP](http://jfhelie.blogspot.com/2008/09/rest-ou-soap.html) . J’aimerais ajouter ici mon petit piment, ma petite reflexion sur ces deux *mode de communication*.

Pour ce qui ne le savent pas encore, [REST](http://fr.wikipedia.org/wiki/REST) est une architecture basé sur ce qui fait le web: [URI](http://fr.wikipedia.org/wiki/Uniform_Resource_Identifier) , [HTTP](http://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol) et un concept d’object, ou plutôt de ressources bien pensé. C’est une architecture légère et relativement facile à mettre en place. Elle est de plus très ouverte et simple à utiliser.

Quant à [SOAP](http://fr.wikipedia.org/wiki/SOAP), c’est un protocole de communication très utilisé (le seul?) dans la construction d’architecture de type [SOA](http://fr.wikipedia.org/wiki/Service_Oriented_Architecture). Basé sur <span class="caps">XML</span>. Très verbeux, il est souvent opposé à <span class="caps">REST</span> (mêms si c’est plutôt la <span class="caps">SOA</span> qui s’oppose à <span class="caps">REST</span>).

Je rejoint Jean-François sur le fait que finalement, selon ce que nous voulons mettre en place, l’une ou l’autre des solutions s’adapte mieux. Mais j’ajouterais que pour moi, <span class="caps">SOAP</span> est interessant dans une architecture oÃ¹ les échanges sont asynchrone. Effectivement, <span class="caps">REST</span>, basé sur le protocole <span class="caps">HTTP</span>, donne une réponse immédiate. <span class="caps">SOAP</span> peut-être utilisé comme tel, mais finalement, <span class="caps">SOAP</span> utilisé sur le protocole <span class="caps">HTTP</span>, ça faut un peu double emploi: *Une enveloppe dans une enveloppe*. <span class="caps">SOAP</span> dans une utilisation asynchrone prend du sens. Utilisé sur un protocole spécifique (beurk ! je préfère les *formats ouverts* !) ou bien du type smtp/imap/pop cela devient légitime.

Donc pour moi, outre l’aspect gestion de sécurité (il me semble qu’avec une architecture <span class="caps">REST</span>, il existe des solutions pour la gestion de la sécurité), le choix entre l’utilisation de <span class="caps">REST</span> et l’utilisation de <span class="caps">SOAP</span>, c’est plutôt une question de *timing* :-)

  