---
layout: post
title: Quel type de développeur je suis ?
---

  La question du moment: "Quel développeur êtes vous ?":http://www.miximum.fr/culture/99-quel-developpeur-etes-vous.

Ce n'est pas facile de se définir. Je me retrouve assez bien dans la "categorie Laurent Jouanneau":http://ljouanneau.com/blog/post/2008/10/16/Quel-type-de-developpeur-suis-je :

* Le Ninja
* Le pragmatique
* Le libriste

Avec des nuances:

*Ninja 10%*, mais pas trop: ne suis pas à la recherche de l'outil ultime, je ne connais pas _tout_ les raccourci de "vim":http://www.vim.org/ , je ne pense pas être dangereux, je ne méprise que les cons, par contre c'est promis, dès que j'ai le temps, je m'essaie à Dvorak  !

*Libriste 20%* Deux grosse nuances. La première, c'est que je ne travail pas dans un environnement très libre. Disons que je parle peu de licence (un tord peut-être ?). Je suis un peu comme TF1 pour Coca: je vend mon temps de cerveaux disponible à mes clients. Je les aident comme je peut en leur conseillant en général d'utilisé un outil libre quand c'est mieux pour lui. Mais si ces contraintes ou autre lui fond préférer du sources fermé, tant pis, je continue à l'aider. A la maison c'est tout autre chose, on passe à beaucoup plus ! J'ai une petite barbe, j'aime l'entreaide, une petite tendance écolo, et je n'ai aucun logiciel propriétaire sur mon système @home (ni aucun blob d'ailleurs :p).

*Pragmatique 70%* Faire ce qu'il faut pour que cela marche, ni plus ni moins. Plus peut engendrer des problèmes bien souvent, moins ne suffit pas. Après, le coté rentrer à la maison c'est tout... De toute façon à la maison, je continue, du coup c'est moins pragmatique...

* *Os* : "OpenBSD":http://www.OpenBSD.org / "Windows Vista":http://www.microsoft.com/windows/windows-vista/default.aspx
* *Editeur* : "Vim":http://www.vim.org , "Eclipse":http://www.eclipse.org / "Vim":http://www.vim.org, "SciTE":http://www.scintilla.org/SciTE.html, "Eclipse":http://www.eclipse.org
* *Langage Favori* : "Ruby":http://ruby-lang.org / "Java":http://java.sun.com/, "Progress":http://www.progress.com/fr/index.ssp
* *VCS* : "Mercurial":http://www.selenic.com/mercurial/wiki/ / "Mercurial":http://www.selenic.com/mercurial/wiki/, "CVS":http://fr.wikipedia.org/wiki/Concurrent_versions_system
* *Navigateur* : "Firefox":http://www.mozilla-europe.org/fr/firefox/ / Firefox, "IE":http://www.microsoft.com/france/windows/products/winfamily/ie/default.mspx

*Edit: pour être honnête, je suis obligé d'ajouter les outils/environnements qu'il m'arrive d'utiliser en journée, parfois toute la journée :-/*

En regardant la liste, je me demande si tout colle à mon profile... :-D

Je reprend maintenant la question de base, *et vous quel développeur êtes vous ?*


_Merci à "Thibault":http://www.miximum.fr/ pour ce billet très interessant !_

*Edit* : Pour être tout à fait honête, je doit ajouter windows, progress et compagnie..

*Edit 2* : JRuby n'est effecitvement pas un langage à part entière, c'est une simple implémentation de Ruby sur la JVM.


  