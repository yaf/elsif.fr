---
layout: post
title: OpenBSD 4.4 precommande
---


[Après la beta](http://www.typouype.org/2008/07/17/openbsd-4-4-beta) voici qu’"OpenBSD 4.4":http://www.openbsd.org/44.html est “disponible en precommande”. Je vous passe les [détails de tout ce qui bouge](http://www.openbsd.org/plus44.html), y’a du monde, vous irez voir par vous même :-)

[![SourceWars](http://www.openbsd.org/images/SourceWars.jpg)](http://www.openbsd.org/44.html)

*Vous pourrez noter que le thème est basé sur StarWars cette année, c’est toujours mieux qu’Asterix*

J’avoue que je me tate, car en même temps que cette version au numéro si symbolique, l’"euroBSDConf 2008":http://2008.eurobsdcon.org/ à lieu en France, à Starsbourg, et je compte bien y aller. Du coup, est-ce que j’attend d’être sur place pour me l’acheter, ou bien je fait une pré commande ?

[OpenBSD 4.4 is available for preorder](http://undeadly.org/cgi?action=article&sid=20080904204021) via [undeadly, le journal officiel](http://undeadly.org).


