---
layout: post
title: Bricabox news
---

  
Et bien voilà, la [bricabox.info](http://www.bricabox.info) accueille un nouveau colocataire: le [parablog.fr](http://www.parablog.fr). Esperons que celui-ci ne nous fasse pas le coup du teaser de plus d’un an :-).

*Je ne m’aventurerais pas à vous expliquer de quoi il va s’agir, l’auteur en parlera sûrement plus tard*

**Bienvenue ! :)**

  