---
layout: post
title: WindowMaker come back
---

  On a cru pendant un moment que "WindowMaker":http://windowmaker.info/, le gestionnaire de fenêtre basé sur "GnuSTEP":http://www.gnustep.org/, était mort:

* Site quasi indisponible
* Dernier message de 2006
* Peut d'activité sur la liste
* Eternel version 0.92


Mais voilà, il y a quelque mois, c'est reparti. Reprise d'activité du site (doucement, mais au moins il est en ligne). La mailing list déborde d'annonce de patch et autres. Pour le moment ça sent la stabilisation de l'existant, mais l'équipe en place semble parti pour faire avancé le tout vers une version 1.0 ! :-)

Le dépot de version est passé sous "Mercurial":http://www.selenic.com/mercurial/wiki/ : "hg.windowmaker.info":http://hg.windowmaker.info/

A suivre donc !

!http://farm2.static.flickr.com/1205/1411878360_6bbbaf86b7.jpg?v=0!:http://www.flickr.com/photos/yafra/1411878360/in/set-72157594188207460/



  