---
layout: post
title: Gem les packages
---

Les utilisateurs de [Ruby](http://www.ruby-lang.org) nous connaissent bien l'outils de gestion  de paquet (ou librairies, c'est comme on veut) [RubyGems](http://www.rubygems.org/). Cet outil permet d'installer des paquets ruby enrichissant le _coeur_ de notre langage préféré.

Cependant la plus part des systèmes d'exploitations de la famille des "\*nix" (comprendre les divers distribution linux, les divers bsd et autre opensolaris) bénéficient déjà une gestionnaire de paquet permettant l'installer des applications.

Bien souvent certaines _gems_ (c'est ainsi que l'on désigne les paquet ruby disponible via RubyGems) sont porté dans le gestionnaire de paquet de système que nous utilisons. Alors pourquoi avoir deux gestionnaire de paquet pour ruby : celui du système et RubyGems ?

RubyGems à l'avantage d'être disponible sur toute les plateformes, et ne serait-ce que pour les utilisateurs de fenêtre ou de pomme, c'est indispensable pour une meilleur gestion de l'installation Ruby.

Mais je pense qu'il faut utiliser en priorité les paquets spécifique au système (pour [OpenBSD](http://www.openbsd.org) il y a aujourd'hui dans -current environ 75 paquets ruby disponible). En effet, ces paquets sont là pour s'intégrer au mieux avec le système. Et bien qu'ils s'installent de toute façon au même endroit qu'avec RubyGems, certain patch ou autres _flavor_ spécifique peuvent être mis en place pour le bien de l'installation et l'intégrité du système d'exploitation.

Alors RubyGems n'est pas inutile sur ces systèmes, loin de là, ne serait-ce que pour avoir la collection complète des applications ruby, mais j'utilise personnellement les paquets du système en priorité.

Et vous ?

_ps: RubyGems offre d'autre fonctionnalité interessante mais ce n'est pas le sujet ici :-)_

