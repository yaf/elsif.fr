---
title: Lundi 9 novembre 2021
---

> "Plus tu tailles, plus tu fais croire à l'arbre qu'il est en phase juvénile et plus tu retardes son développement d'adulte", explique Eric Charton. En agissant ainsi, le jardinier tailleur freine la maturité de son arbre, autrement dit ne respecte pas sa nature profonde.

> En un mot comme en cent, il suffit de respecter la croissance naturelle de l'arbre, sans lui jouer des tours, sans le "titiller" comme dit Eric. Faute de quoi, l'arbre risque de se sentir en phase de croissance permanente et va s'épuiser à se développer dans tous les sens. Ce qu'Eric résume par cette lapalissade : "Si l'arbre doit être grand, il doit être grand."

[Et si on arrêtait de tailler nos arbres](https://france3-regions.francetvinfo.fr/grand-est/alsace/jardinage-et-si-on-arretait-de-tailler-nos-arbres-2320069.html)

Le non agir.
Et en même temps, parfois, ça permet de provoquer certaines choses... Sans en faire trop justement. C'est un rappel.

> Voici en bonus, le replay d'une émission de Radio Canada avec pour invitée Jeanne Millet, chercheuse et spécialiste de l'architecture des arbres.
[L'architecture des arbres et leur stratégie de croissance, selon Jeanne Millet](https://ici.radio-canada.ca/ohdio/premiere/emissions/la-nature-selon-boucar/segments/entrevue/79826/tailler-arbres-jeanne-millet-hydro-quebec)

[Le développement des arbres révélé par le dessin](Le_developpement_des_arbres_revele_par_l.pdf)

---

Construire un vrai service client à la capitaine train
[L’obsession du service client chez Captain Train](https://djo.medium.com/obsession-service-client-captain-train-cb0b91467fd9)

voilà ce que j'aimerais bien pour RDV-Solidarités

---

[oiseaux.net](https://www.oiseaux.net/), un site pour identifier les oiseaux ?

Il y en a quelques uns dans le jardin. Et j'aimerais savoir quoi leur donner ou pas à manger, comment les aider à construire leurs maisons... Comment les aider ? Pour commencer, il faut les identifier.

Nous avons déjà une liste des plantes à faire, j'ajoute donc la liste des oiseaux. Une petite participation en comptant les ver de terre aussi ?

---

Une mine d'or en ressource autour du logiciel 

[What Is Software Design?](https://www.developerdotstar.com/mag/articles/reeves_design.html)
