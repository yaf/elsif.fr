---
title: Vendredi 19 mars 2021
---


[La dérivation - Éducation populaire & enjeux numériques](https://dérivation.fr/)

> Nous mobilisons les pratiques de l’éducation populaire autour des questions numériques.
>
> L’éducation populaire en tant que processus d’émancipation, de conscientisation, de développement du pouvoir d’agir et de transformation sociale, permet de redonner du sens politique à des années de militantisme axés sur les enjeux numériques.
>
> Nous souhaitons utiliser notre expérience en médiation numérique, intelligence collective, analyse politique, intervention publique pour aider les associations, les coopératives et les collectifs à améliorer leur fonctionnement dans un monde au numérique omniprésent.

---

> Technologie et contrôle du travail
>
> Pour des centaines de millions de travailleurs à travers le monde, le travail reste avant tout un effort physique épuisant, apparemment retiré du régime de haute technologie de l’automatisation et de la gestion numérique qui est venu intensifier le travail.
>
> Ce qui a le plus changé dans la nature du travail au cours des deux dernières décennies, c’est le degré, la pénétration et l’application des technologies numériques qui surveillent, quantifient, normalisent, modulent, suivent et dirigent le travail des individus et des groupes. Celles-ci transcendent les efforts du taylorisme pour quantifier, fragmenter, normaliser et ainsi contrôler le travail individuel et collectif, quel que soit le produit ou le service qu’il produit. La numérisation de nombreuses technologies liées au travail signifie que le travail peut être mesuré et décomposé en nanosecondes, par opposition aux minutes et secondes de Taylor. Cela signifie également que chaque aspect du travail est quantifié. La simplification via la quantification permet de quantifier la vitesse et les demandes de vitesse.

[La classe ouvrière mondiale dans la réorganisation du capitalisme](http://www.contretemps.eu/classe-ouvriere-mondiale-reorganisation-capitalisme/)

