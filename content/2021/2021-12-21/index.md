---
title: Mardi 21 décembre 2021
---

- [Tera.coop - "Un éco-système pour le XXIème Siècle"](https://www.tera.coop/)
- [La suite du monde - achetons les terres de notre émancipation](https://www.lasuitedumonde.com/)

---

Je me relance dans l'essai d'utilisation d'outil graphique plutôt que tout dans le terminal... 

[VSCodium](https://github.com/VSCodium/vscodium) pour coder, mais la partie [Git](http://git-scm.com/) ne me conviens pas vraiment. Sauf peut-être pour faire des commits, et encore. Il faudrait que je regarde s'il y a des plugins sympa pour faire les commit comme j'aime, en mode patch. Et pour la gestion des synchro et changement de branche, je ne trouve ça pas trop pratique. Aussi je m'essaie à lancer Gitg.

Peut-être que je pourrais me payer un [Rubymine](https://www.jetbrains.com/ruby/) ? Je vais attendre d'être passé en CAE.

