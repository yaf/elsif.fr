---
title: Mercredi 17 mars 2021
---

[Les tiny-houses basses technologies, l’avenir ?](https://leszuts.coop/)

---

Suite à des discussions sur le slack des EIG, je découvre qu'il y a plusieurs personnes intéressées et passionées par la pédago !

L'une d'elle est en train de monter une plateforme d'apprentissage (?) de la programmation en ligne [Electronic Tales](https://www.electronictales.io/)
> Dégommons le syndrome de l'imposteur ensemble !

J'aime bien cette approche :) La suite aussi.

> ☁️Pour les développeurs·euses juniors qui n'ont pas suivi un cursus d'ingénieur, ne démontent pas des ordinateurs tous les week-ends et n'ont pas commencé à coder à 5 ans
> 🌈Fabriquée avec amour par des devs féministes, queers, inclusifs·ves et autres personnes fucking bienveillant·e·s
> 🍰Plateforme open-source et 100 % gratuite
> 📆Sortie prévue pour l'hiver 2021

Je m'inscrit pour aider, pour voir.

