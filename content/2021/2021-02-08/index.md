---
title: Lundi 8 février 2021
---

C'est marrant comme j'ai l'impression que Damien ajoute un truc dans cette article 

[Maximiser l'efficacité des développeur·ses](https://damien.pobel.fr/post/maximiser-efficacite-developpeurs/)

> En résumé, dans un environnement de haute efficacité :
>
> - les tâches à réaliser sont sans ambiguïté, en d'autres termes elles ont été spécifiées et discutées pour avoir pour chacune, une vision claire du problème à résoudre, de la cible et des cas d'utilisation (use cases) et si en plus le périmètre a été réduit à son strict minimum, c'est parfait ;

Je ne suis pas sur d'avoir lu ça dans l'article original. Et c'est pas la première fois que je lis ou entends des personnes qui font du développement évoquer ça.

C'est sans doute oublier que la features doit être négociable.
C'est sans doute l'envie de pouvoir faire une tache (de tacherons ?) dans un coin, avec un casque sur les oreilles, sans chercher à parler de ce que l'on code avec personne ?

L'article original [Maximizing Developer Effectiveness](https://martinfowler.com/articles/developer-effectiveness.html)

---

Une CAE dont j'entends reparler encore... [Oxalys](https://oxalis-scop.fr/).

---

[Télécom Paris et Framasoft vous invitent à contribuer au futur MOOC « Contributing to Free-Libre and Open Source Software »](https://framablog.org/2021/02/04/telecom-paris-et-framasoft-vous-invitent-a-contribuer-au-futur-mooc-contributing-to-free-libre-and-open-source-software/)

> Enfin, avec ce MOOC Contributing to FLOSS, l’objectif est de permettre à des développeur·euses d’avoir une introduction théorique (de quoi parle-t-on ?) comme pratique (comment entrer en contact, comment contribuer ?) à l’univers de la contribution au libre.

Je note au passage que [Contribulle](https://contribulle.org/) est en ligne maintenant :)

---

> Ils sont même allés encore plus loin : plutôt que se concentrer sur leur maison individuelle, ils se sont regroupés pour construire ensemble des quartiers entiers. “Les gens qui travaillaient sur les maisons ne savaient pas si c'était la leur ou celle du voisin, une fois qu'elles étaient terminées, elles étaient tirées au sort”, explique Eric Tortereau, co-président de l'association des autoconstructeurs Castors Rhône-Alpes.

[Ils construisaient leurs maisons tous ensemble puis les tiraient au sort : l'histoire des Castors](https://www.18h39.fr/articles/ils-construisaient-leurs-maisons-tous-ensemble-puis-les-tiraient-au-sort-lhistoire-des-castors.html)

---

Note de lecture du livre « Produire ses graines bio » de Christian Boué.

Trois types de sélection de graine (exprimé dans une métaphore de course cycliste):
- sélection conservatrice. On pioche un peu partout.
- sélection massale positive. On prend l'échappée.
- sélection massale négative. Les pire sont éliminés.

Nous pouvons aussi voir ça sous deux autres axes :
- maintenir une variété en prenant tout les types d'individu (on pioche sur tout les types)
- améliorer une plante soit en gardant sélectionnant les meilleures graines, soit en supprimant les moins bonnes.


Nous pouvons aussi faire des hybrides.

> quiconque a voulu garder ses semences de courges sans s'inquiéter d'isoler la culture a vu, l'année suivante, plusieurs de ses potimarrons ou de ses courgettes prendre des formes et des couleurs inconnues. (...)
> Pourtant l'hybridation, dans le cas par exemple des cucurbitacées, peut être réalisée par le jardinier averti assez matinal pour se lever avant le soleil, quand abeilles et bourdons sont encore en pyjama. (...)
> La semence obenue est un hybride F1, homogène à la première génération mais instable par la suite. Vous pouvez en rester là et reproduire, année après année, votre recette en croisant toujours les deux mêmes parents pour en rester au F1, comme le font les semenciers commerciaux. Mais si vous souhaitez pousser plus avant l'expérience et stabiliser vote hybride, il faudra alors ssélectionne à la deuxième génération (F2) les pieds les plus fidèles au F1 et ainsi de suite, génération après génération, jusqu'à n'obtenir que des plantes homogènes.

Moi qui croyait que les hybride F1 c'était le mal. Finalement, pas forcement.

> Trois grands principes à respecter
> La sélection se fait quand le légumes est abouti
> - La sélection est une course de fond
> - Plus on garde de porte-graines, plus le choix, et les chances d'encourager l'adaptation ou de profiter d'une mutation naturelle, est grand.

> On sait que les semences stockées trop longtemps, même dans d'excellentes conditions, ne sont généralement pas bien vigoureuses : la meilleure méthode pour pérenniser une variété est de la cultiver année après année.

> Il existe donc pour les professionnels, des fiches descriptives (les fiches UPOV) qui ont pour rôle de situer une variété dans ses expressions par rapport à une variété témoin.

> Les lois de Mendel
> Il fallut qu'en 1865, un petit moine curieux, Gregor Mendel, se penche sur la recherche botanique pour qu'enfin le voile se lève. Après avoir croisé des pois aux grains jaunes avec d'autres aux grains verts puis, également, des pois aux grains lisses avec d'autres ridées, Mendel s'efforça de suivre leur descendance sur plusieurs générations. Il découvrit ainsi l'homogénéité de l'hybride de première génération (F1) puis, avec les générations suivantes (F2, F3, F4, ...), les recompositions surprenantes mais très précises des caractères visibles.


> Bien souvent il est difficile d'isoler les fleurs mères de tout pollen étranger ; la nature privilégie la fécondation croisée. C'est pour cela que l'on avance souvent le chiffre de huit à dix générations (F8 à F10) pour être bien certain d'avoir stabilisé une variété de plantes d'espèce allogame.

> Gagner et perdre
> Dans sa sagesse, la nature n'a pas permis qu'un légume présente toutes les qualités ; les superhéros n'existent que dans les films. J'appelle cela la «théorie des Shadoks » : tout érudit sait, de longue date, que le cerveau des Shadoks ne présentait que quatre cases ; lorsqu'on voulait insérer une cinquième donnée, la première était expulsée. Il semble en aller de même avec les plantes et tous les êtres vivants ; lorsqu'on sélectionne obstinément sur un caractère, on risque d'en perdre d'autres en chemin. C'est sans doute en renforçant certains critères recherchés par l'industrie et la grande distribution (fermeté du fruit, tenue à long terme....à que l'on a perdu la saveur de la tomate ou du melon ; c'est certainement en poussant le potentiel de rendement des plantes qu'on les rend plus sensibles à certaines maladies.

> deux extrêmes sont à bannir :
> - les excès d'azote
> - les carences en acide phosphorique et en potasse

> Tenez un journal sur un cahier et soyez zélé ; retrouvez la soif de perfection qui vous animait le jour de votre entrée en CP, lorsque vous exhibiez fièrement vos beaux cahiers.


Dans les outils, on évoque des tamis, des séchoirs solaires et autres claies.

À propos du stockage

> N'utilisez jamais de bois aggloméré ou de contreplaqué renfermant des formaldéhydes dans ses colles pour vos boites de stockages en bois ou vos étagères ; ces produits sont germicides. Évitez également les boîtes étanches ; sachez que même si la graine est en sommeil, elle respire encore. Elle a donc besoin de l'oxygène passant au travers des récipients ou des sacs, y compris les plastiques tressées.

- Protégé de l'humidité
- Protégé de la chaleur


---

La découverte musicale de la soirée [Wahala Wahala par K.O.G & the Zongo Brigade](https://zongobrigade.bandcamp.com/album/wahala-wahala)

