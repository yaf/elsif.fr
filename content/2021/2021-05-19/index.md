---
title: Mercredi 19 mai 2021
---

Pendant la réunion [Terre de Lien Bretagne](https://www.terredeliens.bzh/), j'ai encore entendu parlé de l'[association Défi](https://www.defis.info/) de Lanester.

Apparement, il font aussi de développement. Ce qui me rend un peu triste, c'est d'imaginer que le site de TDL BZH serait sur Wordpress... Pour 2 500 € je me demande bien à quoi ressemble la demande. Je vais essayer de regarder et de proposer autre chose. Peut-être avec l'aide de Claire ?

Par rebond, je me retrouve aussi avec la page de [Digiskol](https://digiskol.bzh/). Pas de programmation proposé :-/

---

Intéressante découverte en participant au selection de personnes candidates pour les [EIG](https://eig.etalab.gouv.fr/)

[Change the way you work # Doing TDD does not only improve your skill, it also changes the way you work](https://dmerej.info/blog/post/why-you-should-try-tdd/#change-the-way-you-work)

---

> Summary: Discoverability is cut almost in half by hiding a website’s main navigation. Also, task time is longer and perceived task difficulty increases.

[Hamburger Menus and Hidden Navigation Hurt UX Metrics](https://www.nngroup.com/articles/hamburger-menus/)

---

> The ultimate goal of Teaching Responsible Computing is to educate a new wave of students who bring holistic thinking to the design of technology products.

[Teaching Responsible Computing Playbook](https://foundation.mozilla.org/en/what-we-fund/awards/teaching-responsible-computing-playbook/)

---

> Les  compétences  informatiques  complexes  (programmation) sont concentrées sur quelques professions du numérique,  les  techniciens  et  les  ingénieurs  de  l’informatique  (graphique 5). Elles sont encore peu diffusées dans le reste des professions, ce qui peut relever d’une adoption encore modérée  des  technologies  numériques  dans  l’économie

[Cartographie des compétences par métiers](https://www.strategie.gouv.fr/sites/strategie.gouv.fr/files/atoms/files/fs-2021-na-101-cartographie-competences-metiers-mai.pdf)

---

À lire ? « Je veux me battre partout où il y a de la vie » Clara Zetkin

> Le principal tort de Clara Zetkin (1857-1933) – et la principale raison pour laquelle elle est si souvent oubliée aujourd’hui – est probablement d’avoir toujours été trop révolutionnaire pour les féministes et trop féministe pour les révolutionnaires. 

[Lutte des femmes et lutte des classes. Clara Zetkin vivante !](https://www.contretemps.eu/zetkin-communisme-feminisme-revolution-classes-travail-syndicalisme/)
