---
title: Dimanche 11 avril 2021
---

Emprunté à la médiathèque, et apprécié, un livre pour enfant qui permet d'aborder la question de ce qu'est un herbier :)

[L'herbier fou. À la poursuite de la mouche mange-couleurs !](https://roxannebee.com/parutions-cv) de [Roanne Bee](https://roxannebee.com/).

Amusant de voir qu'elle en proposé également [une exposition](https://roxannebee.com/copie-de-gravures). Je trouve que c'est chouette de proposer plusieurs format autour d'un univers.

---

[Le lierre terrestre de [presque] A à Z](https://www.youtube.com/watch?v=wplua04RK0g).

Tige carrée, stolon, sous-bois, fleur 5 pétale soudée en 2 lèvres, feuilles opposées, feuille rein ou cœur, découpe crénelé, cillé, sous la feuille des poils d'huile essentielle.


Récolte les parties aérienne fleuri. Il est possible de récolter les feuilles toutes l'année.
Récolte par temps sec, ensoleillé, plutôt en milieu de journée pour les parties aérienne, fleurie.

Possible confusion avec 
- l'alliaire officinale _Alliaria officinalis_ qui elle n'a pas de tige rampante, odeur d'ail au froissement, bord de la feuille irrégulièrement et sinué
- Ficaire fausse renoncule _Ficaria verna_ qui n'a pas de tige rampante, feuille brillante sans poil et sans créneaux régulier, pas d'odeur aromatique
- Les violettes _Viola sp_ pas de tiges rampantes, présence de stipule (petite feuille à la base des feuilles) , parfum non camphré/citronné


À prendre en décoction plutôt : pour 1l d'eau, 50g de plantes sèches ou 250g de fraîche. Bouillir 10 minutes, laisser infuser 10 minutes. Filtre.
Vertu cicatrisante, antiseptique, astringent.


Possible de faire un macéra huileux pour massage.
Vertu expectorante : calme la toux, évacue les sécrétions. Pour toux grasse, bronchite. La plante assèche, donc pas bon si c'est sec. Infusion dans ce cas.

Tonique du foie, production de bille. Presque toutes les lamiacées on des vertus digestive.

Plante pour traiter les acouphènes.

Durétique : nettoyage des reins.


Teinture mère plus concentrée.
Vertu (...) évacue le plomb et d'autres métaux.
À forte dose pourrait donner des diarrhées. Convulsion aussi.


Recette du sirop de lierre terrestre
- 25 cl d'eau
- 100g de lierre terrestre (frais)
- sucre/miel

Stériliser la bouteille, faire bouillir l'eau, mettre le lierre, coupé, laisser 30 minutes infuser, ajouter le même poids de sucre/miel et refaire chauffer doucement en remuant.


[Oumbi](https://www.oumbi.fr/)


