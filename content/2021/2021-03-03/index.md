---
title: Mercredi 3 mars 2021
---

> Une pédagogie féministe ne pourrait jamais être, apprendre aux femmes à être les « hommes », ou apprendre aux hommes à laisser les femmes être les « hommes ». Une pédagogie féministe c’est commencer dès maintenant la pratique d’un monde inouïe, où tout est à construire, car les fondements de la représentation, la politique et les rapports sociaux ont été tout chamboulés. Où l’on réécrit les rapports d’autorité et de domination qui règlent et génèrent le monde des « hommes » du haut en bas, du visible à l’invisible, du concret à l’abstrait… en commençant par la salle de classe : le rapport entre la professeure et les étudiantes, entre les étudiantes, des deux aux matières étudiées et inventées, les critères d’évaluation, la disposition spatiale et temporelle, l’organisation et la philosophie institutionnelle.

> Il y a un corpus mouvant, hybride, multimédia, et non-hiérarchisé. Rien n’échappe au statut du savoir culturel. Toute le monde y contribue.

[Vers une pédagogie féministe (brouillon de la baignoire)](http://leschattes.net/vers-pedagogie-feministe-brouillon-baignoire/)

---

À propos des badges de compétences

> Sortir les badges scouts hors de leur contexte, comme créer un badge « savoir faire du feu » accessible par tous et qui serait « reconnu » par les Scouts serait une complète hérésie

> Déconnecter la reconnaissance de la pratique et de sa communauté court le risque de réifier l’individu en une série d’attributs définissant la personne qui n’aurait plus qu’à se conformer à une norme. Et si la norme n’existe pas, alors la personne non plus.

> “l’acquisition de diverses compétences ne rend pas nécessairement un manager compétent”. Contrairement à l’hypothèse de la plupart des référentiels de compétences de management, il n’existe pas de relation linéaire, ni même causale, entre les compétences et les performances professionnelles

> Si reconnaître des compétences est un processus complexe qui demande une expertise et peut prendre plusieurs mois, voire années, que ce soit dans la construction de référentiels ou la validation des acquis de l’expérience (VAE), en revanche, reconnaître des pratiques et des praticiens est une activité que tout le monde pratique.  Tout d’abord en reconnaissant ses propres pratiques et celles des personnes et communautés avec qui nous sommes en contact. Pas besoin d’un référentiel de compétences pour reconnaître un bon boulanger qu’on soit client ou boulanger.

> Prenons le cas du dispositif [ACOUSTICE](https://acoustice.educagri.fr/mod/book/view.php?id=452&chapterid=979) développé dans l’enseignement agricole. C’est une communauté naissante d’enseignants et de formateurs, qui développe ses propres badges autour des usages du numérique dans la pédagogie, pour se reconnaître mutuellement et se faire reconnaître comme communauté par l’institution. Les badges décrivent des pratiques, qui sont portées par des praticiens. La reconnaissance par l’institution peut se matérialiser par l’endossement des badges proposés par le dispositif alors que la reconnaissance par les pairs peut se faire par l’endossement des badges obtenus par praticiens. Et si demain de nouveaux outils et nouvelles pratiques émergent, de nouveaux badges pourraient être créés par la communauté, en temps réel.


[Badges de compétence : la mauvaise solution à un vrai problème](https://reconnaitre.openrecognition.org/2020/03/04/badges-de-competence-la-mauvaise-solution-a-un-vrai-probleme/)


> How to earn your badge
>
> Choose three of these activities to do. You must have your leader or another adult with you for one of these activities.
>
> - Imagine an event, character or scene. Now draw it using a pencil, brush, pen or crayon.
> - Design and make a greetings card.
> - Make a poster advertising Scouting or a Cub Scout event.
> - Design and make a decorated book cover.
> - Draw or paint a picture of still life or a landscape.
> - Make a display of photographs on a subject that interests you.
> - Make a video on a subject that interests you.
> - Design and build a model.
> - Visit an art gallery.
> - Make a model out of clay.

Je ne trouve plus l'article d'où j'ai extrait cette liste : https://members.scouts.org.uk/supportresources/4298/artist-activity-badge/?cat=12,67,776&moduleID=10

Il y a un autre article sur le sujet des badges sur leur site [Scouts Hobbies Activity Badge](https://www.scouts.org.uk/scouts/activity-badges/hobbies/).

---

À lire ? [Puissance de la reconnaissance](https://reconnaitre.openrecognition.org/2021/01/14/puissance-de-la-reconnaissance/) de Claire Héber-Suffrin

---

À propos de la normalisation pédagogique

> En somme elle renvoie à un monde dichotomique sans nuance coupé en deux. C'est vrai ou c'est faux. D'un côté ceux qui sont dans la norme et de l'autre côté ceux qui y sont extérieur.  Les normes inspirent et construisent des références, des repères, des chartes, des guides censés orienter nos comportements.

> C'est une stratégie bien connue de domination que d'édicter des règles puis de les prétendre légitimes et enfin de créer un corps de douaniers pour les faire respecter et de rejeter ceux qui disposent de moins de moyens pour faire valoir leur singularité.

> Plusieurs arguments plaident pour l'évitement de référentiels en matière de formation
> Argument 1 : La qualité certifiée n'est pas la qualité 
> Argument 2 : Les modèles basés sur la compétence s'appuient sur des notions floues. On trouve des milliers de définitions. Qui décide de la référence ?
> Argument 3 : Les référentiels n'ont jamais évité les tricheurs
> Argument 4 : Les référentiels évitent l'essentiel de la vie
> Tout ce qui est dit dans les référentiels est la vision morale du métier, technique dicible, un concentré de gestes professionnels dénué de contexte et d'affects. Pourtant le travail et la formation sont vivants. La mise en place de barrières est une gêne à l'innovation. 
> Argument 5 : Pourquoi la formation professionnelle plutôt qu'un autre secteur d'activité ? 

> Pourquoi l'école 42 attire elle du monde ? Parce qu'elle permet d'accéder à un emploi valorisant, pas parce qu'elle délivre un parchemin estampillé.

[Ni Dieu, ni maître, ni certification ! Toujours la liberté pour apprendre](https://cursus.edu/articles/43593/ni-dieu-ni-maitre-ni-certification#.XkuORj3o5Ic)

---

Un petit air de Wu Tang [If It Bleeds It Can Be Killed](https://bigghostlimited.bandcamp.com/album/if-it-bleeds-it-can-be-killed)

