---
title: 23 avril 2021
---

Le moment où [le site
Litchess](https://lichess.org/blog/YF-ZORQAACAA89PI/why-lichess-will-always-be-free)
refait surface.

Un outil intéressant pour apprendre les échecs ? Un site intéressant pour Aydan
? J'aimerais bien essayer de jouer au go également. Je n'ai jamais pris le
soin d'essayer. Ça serait amusant de découvrir avec les enfants ?

---

> la Nø School propose un concept d’anti-école alternative durant deux semaines
> de rencontres, d’ateliers, de conférences et de temps de travail en studio,
> sur les bords de Loire à Nevers

> Cette nouvelle session sera différente de la précédente. En 2019 le temps
> était réparti ainsi : le matin nous proposions des ateliers, et l’après-midi
> était consacrée au temps studio, les applications pratiques des ateliers en
> quelques sortes. Certains workshops prenaient plus de temps que prévu car il
> y avait aussi des personnes qui n’avaient jamais eu accès à certaines
> technologies, comme l’Arduino par exemple, ou qui n’avaient jamais codé.
> Pour cette session 2021 nous prévoyons plutôt d’organiser des ateliers d’une
> journée qui permettront aux étudiants de se familiariser plus longuement avec
> toutes les technos que nous proposons, et le lendemain une journée de studio.

> De notre côté nous avons également vraiment envie de réaliser tout ça
> ensemble, avec de vraies personnes. On ne remplace pas ça. Cette année nous a
> justement montré à quel point nous étions saturés de sessions zoom et de
> webinaires en tout genre. Notre vie professionnelle se passe beaucoup en
> ligne et cela génère un ras le bol bien compréhensible.

[NØ SCHOOL NEVERS, Covid édition : «une envie de rencontres et d’échange dans la vie réelle»](https://www.makery.info/2021/04/06/no-school-nevers-covid-edition-une-envie-de-rencontres-et-dechange-dans-la-vie-reelle/)

---

> Le problème des résidences secondaires : Les zones touristiques sont les plus
> concernées. Il faut par exemple voir qu’une ville comme Biarritz à perdu 20 %
> de ces habitants passant 25 000 à 20 000 au cours des deux dernières
> décennies. On a donc dans ces zones un taux de résidences secondaires qui
> dépasse souvent 50% des habitations voir 80 à 90% dans certaines zones
> côtières de Bretagne.

> Résultat : Les inégalités sociales augmentent, les tensions aussi. On observe
> de plus en plus d’actions contre les agences immobilières, même si elles ne
> sont que le maillon visible du problème. Des actions légales aussi contre
> certaines ventes de terres agricoles quand c’est possible.

[Le phénomène de migration de Paris vers les régions](https://perso.iergo.fr/bazar/le-phenomene-de-migration-de-paris-vers-les-regions/)

---

[Taking steps toward greater
inclusivity](https://opensource.com/open-organization/17/11/privilege-walk-exercise)

---

> The dual approach of marrying a Single Page App with an API service has left
> many dev teams mired in endless JSON wrangling and state discrepancy bugs
> across two layers. This costs dev time, slows release cycles, and saps the
> bandwidth for innovation.

> And if you’ve split your team into “front-end” and “back-end” people, you
> might as well go ahead and double that cost (both time and money) for many
> non-trivial components where you need two different people to finish the
> implementation. Sure, the SPA mitigates some of the ever-growing spaghetti
> problem, but at what cost for a business racing to be relevant in the market
> or get something important out to the people who need it?

[The Future of Web Software Is HTML-over-WebSockets](https://alistapart.com/article/the-future-of-web-software-is-html-over-websockets/)

> Hotwire is an alternative approach to building modern web applications
> without using much JavaScript by sending HTML instead of JSON over the wire.
> This makes for fast first-load pages, keeps template rendering on the server,
> and allows for a simpler, more productive development experience in any
> programming language, without sacrificing any of the speed or responsiveness
> associated with a traditional single-page application. 

[Hotwire - HTML over the wire](https://hotwired.dev/)

[Loading and replacing HTML parts with
HTML](https://justmarkup.com/notes/2020-12-28-loading-and-replacing-html-parts-with-html/)

---

[Je veux devenir un hébergeur
alternatif](https://wiki.chatons.org/doku.php/devenirhebergeuralt)


