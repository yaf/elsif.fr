---
title: Vendredi 7 mai 2021
---

[The MGR Window System](https://hack.org/mc/mgr/)

> If you want to compare MGR to the X Window System, you might consider the MGR server as something like a combination of an X server + window manager + xterm, but with xterm's Tektronix 4014 graphics terminal emulation and its DEC VT100 emulation in the same window.
>
> A window in MGR doesn't emulate any existing hardware terminal. It has a new set of terminal codes combining text, vector graphics and basic bitmap graphics. See the termcap entry in the distribution.
>
> Compared to X11, MGR has a very small memory footprint and demands very little of the graphics hardware. MGR isn't very fast on graphics and doesn't provide any form of access to direct access to framebuffers or hardware acceleration as modern X servers do. It is, however, comfortably fast for every day use if you mainly use text windows and occassionally view an image.

---

" Reprendre la terre aux machines : manifeste pour une autonomie paysanne et alimentaire " est en librairie aujourd'hui !

Ce livre est le fruit du travail de plus d’un an du groupe d’écriture de notre coopérative composé de sociétaires. Un travail particulièrement enrichi par de copieuses contributions de clarification, d’approfondissement ou de relecture. Il vient éclairer plus d’une décennie d’explorations collectives en technologies paysannes, période jalonnée de nos cheminements techniques, politiques.

Il situe la question des technologies agricoles dans le champ agricole et alimentaire de la fameuse « ferme France »…

À lire ? [Reprendre la terre aux machines - Manifeste pour une autonomie paysanne et alimentaire](https://www.seuil.com/ouvrage/reprendre-la-terre-aux-machines-l-atelier-paysan/9782021478174?fbclid=IwAR2oldKrncajHm4ZJeCd44nZHSskOqXFZQToomLVYg11UdFUD6JpqVoYao0)
