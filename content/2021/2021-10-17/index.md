---
title: Dimanche 17 octobre 2021
---

> La question de l’inclusion numérique est une question de justice sociale et d’équité. C’est la question de l’accès aux droits, c’est le fait que, par exemple, dans une situation de dématérialisation nous ne sommes pas égaux et la question de l’éducation au numérique, pour moi en tout cas, c’est une question d’émancipation.

>  C’est apprendre à contourner les pièges, c’est développer son esprit critique pour être en capacité de faire des choix et ça marche sur tout, ça marche sur le matériel comme sur les services ; c’est aussi déconstruire ce qu’on voit mais aussi, par exemple, tout est ce qui l’influence publicitaire pour résister aux injonctions normatives. C’est aussi utiliser les technologies de manière active, en tout cas utile, et utiliser, évidemment, Internet potentiellement comme un levier d’expression ou de participation citoyenne, qui sont quand même des idées qui nous semblent aussi être importantes dans une société des médias et de la communication.

> L’autre question qui mérite d’être posée c’est, finalement, est-ce que les professionnels de l’action sociale ne doivent-ils pas développer des compétences de médiation numérique parce qu’ils sont au contact des populations qui sont déjà les plus en difficulté ? Donc c’est aussi un projet de transformation des compétences des professionnels dans des contextes d’économies, de tensions et même parfois de rôles à jouer qui sont un peu problématiques.

> Pour agir. Des programmes pour développer les compétences qui aillent évidemment, au-delà du Pix [8]. Même si Pix, franchement, c’est déjà une brique que je ne trouve pas inintéressante puisque, par exemple sur les data qui sont un des grands enjeux de la révolution numérique, si on peut parler de révolution, eh bien le pilier Pix qui travaille là-dessus est assez intéressant et permet vraiment de mesurer son niveau de compétences.

> Oui, les libristes sont des vieux mâles blancs de plus de 50 ans, absolument !

> Et peut-être, à ce moment-là, revenir effectivement sur cette notion de logiciel libre qui, je pense, poindra immanquablement lorsque j’interroge le vice-président de l’April.
>
> (Jean-Christophe Becquet :) Effectivement, je peux peut-être introduire sur cette citation de Richard Stallman [9] qui a formalisé le concept de logiciel libre en 1984 et qui disait : « La liberté logicielle n’est pas plus importante que les autres libertés. Simplement, la liberté informatique est essentielle parce que, aujourd’hui, l’informatique traverse tous les aspects de nos vies ». Aujourd’hui on utilise le numérique, on utilise les outils informatiques pour communiquer, pour apprendre, pour travailler, pour exercer sa vie citoyenne, pour consommer. Donc à chaque instant et dans tous les aspects de nos vies, que ce soit personnelle ou professionnelle, on est dépendant de ces outils et, du coup, il est extrêmement important, il est essentiel, comme l’a dit Dorie, de permettre à chacun l’accès à ces outils et leur maîtrise.

> ça s’oppose complètement à une forme du numérique qui existe aussi aujourd’hui, qui est un numérique complètement contraint, où un certain nombre de personnes se retrouvent face à un service – typiquement on parlait des services de l’État ou des collectivités : les gens doivent passer par le numérique pour accéder à ces services, on le leur a imposé, ils n’ont pas contribué à quoi que ce soit, on ne leur a pas demandé leur avis. Là on est dans un modèle complètement différent qui est un numérique subi.

> je pense que les fondamentaux pour utiliser un traitement de texte c’est de savoir ce qu’est un fichier, comment on code une information et, du coup, pourquoi le fichier qu’on a fait dans un logiciel de traitement de texte on peut, ou pas, l’ouvrir dans un autre logiciel de traitement de texte ; ce que veut dire envoyer un fichier par le réseau à un destinataire et pourquoi, parfois, on envoie un fichier et ça se passe mal au niveau de la réception par le destinataire ou alors, parfois, on veut ouvrir un fichier qu’on a fabriqué il y a quelques années en arrière et ça ne fonctionne pas parce que le logiciel ou la version ont changé et qu’on n’a pas pris les bonnes précautions ; ou alors encore, parfois un entrepôt de serveurs brûle, le fichier disparaît et on perd son travail parce qu’on n’a pas fait de sauvegarde. 

>  Moi, par exemple, je me suis longtemps battue pour que l’éducation aux médias numériques ne soit pas le sujet des professeurs d’informatique ou de mathématiques ou des référents numériques mais que ce soit un sujet qu’on puisse travailler dans toutes les disciplines puisque, du coup, ça concerne toutes les disciplines, l’histoire, les mathématiques, les lettres, même l’anglais. Bref ! Ça concerne toutes les disciplines.


[Formation et culture numérique. Un numérique inclusif, accessible et choisi](https://www.librealire.org/formation-et-culture-numerique-un-numerique-inclusif-accessible-et-choisi?var_mode=calcul)

Je suis bien content d'être adhérent à l'APRIL depuis aussi longtemps.

---


> L'informatique est en fait une SHS (Sciences Humaines et Sociales) et devrait être rangée ainsi dans les universités. C'est par cette idée provoquante (mais, je vous rassure tout de suite, que l'auteur estime irréaliste) que commence l'article de Randy Connolly « Why computing belongs within the social sciences ». Une thèse intéressante et bien exposée, qui s'appuie sur des éléments sérieux mais qui a aussi des sérieuses faiblesses.
> -- Formation et culture numérique. Un numérique inclusif, accessible et choisi

[L'informatique doit-elle être rangée dans les sciences humaines et sociales ?](https://www.bortzmeyer.org/computing-social-sciences.html)

---

Pour les grandes cultures, pour la céréales... J'oublie souvent l'option traction animal ! C'est pourtant tellement évidant !

C'est beau

[Pendant ce temps là - 1er année](https://www.youtube.com/watch?v=4GxEuYmUliQ)

_Si j'avais le courage... :)_

---

> La question qui reste en suspend ici, c'est celle du rôle du designer dans cette pratique. Peut-on faciliter cette appropriation? Doit-on penser les interfaces numériques comme une porte d'entrée vers une adaptation progressive par le lecteur de l'interface à ses propres envies/besoins?

[hacking graphique](https://carnet.nolwennmaudet.com/home/hacking-graphique)

---

Encore un appel du pieds pour un retour sur OpenBSD : [OpenBSD WebZine](https://webzine.puffy.cafe/). Au fait, je perds quoi à y retourner ? En dehors de Zoom, je ne crois pas que je perde grand chose ? :thinking:

---

Pour apprendre à programmer, apprendre à manipuler le terminal, lister les commandes de base, c'est une bonne première étape.
[Linux Tricks - Mémo : Commandes de base Linux](https://www.linuxtricks.fr/wiki/memo-commandes-de-base-linux)
