---
title: Mardi 9 mars 2021
---

Des idées intéressantes pour la construction d'un navigateur « low-tech »

> Une jauge pour limiter sa consommation de données

[Quel design pour un navigateur « low-tech » ?](https://graphism.fr/quel-design-pour-un-navigateur-low-tech)

---

> Comme le chantait si bien ce groupe venu du Nord, « The winner takes it all » : premier arrivé, premier servi ! En écologie, on appelle ce phénomène la compétition. C’est naturel, il n’y a pas lieu de s’en offusquer. Mais cela a une conséquence immédiate : c’est l’hécatombe.
> 
> Une des rares études menées en Europe sur l’efficacité de la « méthode de Miyawaki » fait état de 61 à 84 % de mortalité des arbres 12 ans après la plantation. Ce n’est pas un problème en soi, cela signifie seulement que toutes les jeunes pousses plantées dans une métropole ne donneront pas, à terme, des arbres. La nuance est de taille.

> Plusieurs entreprises au discours bien rodé proposent leurs services pour restaurer les écosystèmes forestiers de manière frugale et respectueuse des équilibres naturels, en promettant de faire mieux que les méthodes traditionnelles et de restaurer la biodiversité des forêts. Elles appuient leurs promesses à grand renfort de photos sorties de leur contexte et de citations d’Akira Miyawaki.

> La microforêt est un concept a priori séduisant qui, bien utilisé, peut effectivement contribuer à la (re)végétalisation de nos villes, au bien-être des citadins et peut-être même à leur reconnexion avec leur environnement naturel. Mais une microforêt n’est pas une forêt, c’est au mieux un oxymore, au pire un moyen de se donner bonne conscience sous couvert de science.

[Méthode Miyawaki : pourquoi les « microforêts » ne sont pas vraiment des forêts
](https://theconversation.com/methode-miyawaki-pourquoi-les-microforets-ne-sont-pas-vraiment-des-forets-155091)

Un texte critique de la méthode. Pour le moment, j'observe. La ville se cherche des stratégies pour reverdir. Celle-ci en vaut bien une autre. Peut-être que son nom est mal choisi si ce n'est pas vraiment une forêt.

---

Je suit le [MOOC de la Trame Verte et Bleue de TelaBotanica](https://mooc.tela-botanica.org/course/view.php?id=21). Je fais un croisement avec mon envie d'apprendre à faire de l'informatique géographique. Est-ce que toutes les cartes de la trame verte et bleue sont faite ?

Note en vrac du cours de ce soir.
Chercher des infos auprès des [DREAL](https://fr.wikipedia.org/wiki/Direction_r%C3%A9gionale_de_l%27Environnement,_de_l%27Am%C3%A9nagement_et_du_Logement), associations locale. Il y a beaucoup de carto à faire pour définir les corridors.

Calculer le _Chemin de moindre coût_.

- Pour la trame bleue, on regarde les obstacles.
- Selon les espèces, les problèmes ne sont pas les mêmes.
- Le hérisson est géné par le réseau de cloture. Et pour traverser les routes, il se met en boule s'il a peur. Donc pas très efficace pour traverser ou éviter les voitures. Le fait que tout le monde tue les limaces et autres, difficile de trouver de la nourriture pour lui.
- Pour les poissons, le prélèvement diminue parfois trop le niveau du court d'eau. Les turbines sont un problème aussi.
- Les crapauds marchent bien.
- Les éoliennes posent des problèmes pour les chauve-souris. L'éclairage urbain aussi.


Aller sur le terrain pour vérifier. Les cartes ne sont pas toujours correct, ni lisible.


