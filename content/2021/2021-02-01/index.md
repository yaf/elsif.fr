---
title: Lundi 1er Février 2021
---

Par où commencer son chemin d'apprentissage de la programmation ? Quels outils pour coder sans rien installer sur sa machine ?

_Note sur une discussion avec Thomas à propos de son usage de Glitch pour des cours_

[Glitch](https://glitch.com/) propose un environnement assez sympa pour aborder la programmation dite « web ». Celle où nous avons besoin de mettre en marche du HTML, du CSS, de JavaScript et parler avec un serveur.
Plusieurs choses sont intéressantes avec Glitch
- le coté coder à plusieurs ;
- la faciliter de re-utilisation (remix) ;
- possibilité d'utiliser un serveur python ou autre via npmstart ;
- le branchement à Git ;
- plugin VSCode

Il est peut-être intéressant de prendre un compte pro ? [Glitch/pricing](https://glitch.com/pricing)


C'est un peu un Codepen avec plusieurs éditeur et du git.

Je pense beaucoup à [Paysage](https://paysage.xyz/). Un outil pour coder à plusieurs dans un même environnement, avec des modèles qui facilite le démarrage... Un glitch.com pour [Processing](https://paysage.xyz/) en quelque sorte ?

Je vois l'axe apprendre la programmation par l'interface web qu'on connais VS apprendre la programmation par l'amusement et une boucle de feedback courte.

---

Note d'une présentation sur la transformation végétale.
Parce qu'à Brangoulo, il y a une envie d'être autonome. Il est donc question d'avoir un labo.

- Formation à l'hygiène HACCSP
- Par de certif obligatoire (contrairement à la transfo animal)
- Les trois types de microbes sont détruit par la pasteurisation (63 à 100 °C). Pour l'une, c'est la stérilisation (> 100 °C). Ne pas trainer entre 10 et 63 °C

Pasteurisation == 85 °c au « cœur » du produit.

Processus industriel vs processus artisanal.
Pour l'autoconsommation ou pour la production.

