---
title: Vendredi 1er janvier 2021
---

Note de lecture « Sorcières » de Mona Cholet

> Je crois que la magie est de l'art, et que l'art est littéralement de la magie. L'art, comme la magie, consiste à manipuler les symboles, les mots ou les images pour produire des changements dans la conscience. En fait, jeter un sort, c'est simplement dire, manipuler les mots, pour changer la conscience des gens, et c'est pourquoi je crois qu'un artiste ou un écrivain est ce qu'il y a de plus proche, dans le monde contemporain, d'un chaman.
> -- L'auteur de bande dessinée Alan Moore (V comme Vendetta):
> Aller débusquer, dans les strates d'images et de discours accumulés, ce que nous prenons pour des vérités immuables, mettre en évidence le caractères arbitraire et contingent des représentations qui nous emprisonnent à notre insu et leur en substituer d'autres, qui nous permettent d'exister pleinement et nous enveloppent d'approbation : voilà une forme de sorcellerie à laquelle je serais heureuse de m'exercer jusqu'à la fin de mes jours.

_Et la programmation qui me semble être une forme d'écriture, un moyen d'expession... Est-ce que c'est un moyen de faire de la sorcellerie ?_


> Qui est ce Diable dont le spectre, à partir du XIVe siècle, s'est mis à grandir aux yeux des hommes de pouvoir européen derrière chaque guérisseuse, chaque magicienne, chaque femme un peu trop audacieuse ou remuante, jusqu'à faire d'elles une menace mortelle pour la société ? Et si le Diable, c'était l'autonomie ?
> « Toute la question du pouvoir, c'est de séparer les hommes de ce qu'ils peuvent. Il n'y a pas de pouvoir si les gens sont autonomes. L'histoire de la sorcellerie, pour moi, c'est aussi l'histoire de l'autonomie.

> Les seuls cas où l'on accorde à un féminicide la place qu'il mérite, où 'on reconnaît sa gravité, c'est lorsque le meurtrier est noir ou arabe, mais il s'agit alors d'alimenter le racisme et non de défendre la cause des femmes.

> Aujourd'hui, l'infanticide, pratiqué dans des situations de panique et de détresse, suscite l'horreur de la société, qui voit en celle qui l'a commis un monstre, sans vouloir s'interroger sur les circonstances qui l'ont conduite à cette extrémité ou sans vouloir admettre qu'une femme puisse refuser à tout prix d'être mère.

> Le collectif (...) met en garde contre l'illusion selon laquelle, avec l'autorisation de la contraception et de l'avortement, il n'y aurait plus en France de grossesses non désirées menées à leur terme.

> Khadoula, une vieille campagnarde grecque, sage-femme et guérisseuse, fille de sorcière, est accablée par le sort des femmes dans sa société : non seulement les filles passent d'un esclavage à l'autre, du service de leurs parents à celui de leur mari et de leurs enfants, mais leur famille doit se ruiner pour payer leur dot.
> Roman d'Alexandre Papadiamantis « Les petites filles et la mort »

> Si j'avais été gentille et docile, aujourd'hui, je vivrais probablement en Floride avec six enfants, je serais mariée à un mécanicien et je me demanderais comment payer la prochaine facture. Ce n'est pas ce que je voulais.
> Sara, 46 ans

> Ne pas avoir d'enfant, c'est savoir qu'à votre mort vous ne laisserez pas derrière vous quelqu'un que vous aurez mis au monde, que vous aurez en partie façonné et à qui vous aurez légué une atmosphère familiale, le bagage énorme - parfois écrasant - d'histoires, de destins, de douleurs et de trésors accumulés par les générations précédentes et dont vous aviez vous-même hérité.

> L'injonction faite aux femmes de paraître éternellement jeunes lui (Sophie Fontanel) apparaît comme une manière subtile de les neutraliser : on les oblige à tricher, puis on prend prétexte de leurs tricheries pour dénoncer leur fausseté et mieux les disqualifier.

> Caliban symbolise les esclaves et les colonisés dont l'exploitation, comme celle des femmes, a permis l'accumulation primitive nécessaire à l'essor du capitalisme. Mais l'asservissement des femmes a aussi été parallèle à un autre, auquel il était peut-être encore plus étroitement lié : celui de la nature. C'est en particulier la thèse développée en 1980 par la philosophe écoféministe Carolyn Merchant dans _La Mort de la nature_, un ouvrage qui complète celui de Federici. Elle y retrace comment, à la Renaissance, l'intensification des activités humaines, qui exigeait d'énormes quantités de métal et de bois ainsi que de vastes superficies cultivables, et qui altérait la physionomie de la Terre à une échelle inédite, a impliqué un bouleversement identique dans les mentalités.

_À lire ? La mort de la nature par Carolyn Merchant ?_

> L'idée que les médecins puissent montrer leurs émotions semble les terroriser, eux, et terroriser certains patients, comme si la révélation de leur humanité et de leur vulnérabilité risquait de les priver de leurs compétences, de les réduire à l'impuissance - ce qui dit bien sur quoi reposent ces compétences dans notre esprit.



