---
title: Mercredi 12 mai 2021
---

Dans mes histoires avec GNU/Linux et les logiciels libres, j'ai eu une grande période exploratoire, où j'ai essayé plein de distribution, plein de gestionnaire de bureau, de fenêtre. Puis je me suis calmé, j'ai eu une grande période OpenBSD durant laquelle j'ai évolué avec WindowMaker, puis XFCE, un peu 3wm. Puis je suis reparti sur Debian, avec Gnome.

Là, depuis quelques mois, j'ai envie de revenir sur OpenBSD. Je n'y vais pas part crainte de perdre en « compatibilité » avec les autres personnes avec qui j'interagis. Je viens malgré tout d'installer XFCE et de m'installer dessus. Nous verrons bien. Ma première impression c'est d'avoir un peu plus « besoin » de configurer l'environnement pour qu'il corresponde à ce que je souhaite.

Est-ce que je vais perdre mon temps ?

---

Nouvel ouverture de structure de mon schéma mental de l'apprentissage de la programmation.

Il y a toujours les langages avec les fiches ;
Il y a toujours plusieurs couches : la matière, les gestes, mais je pense qu'au dessus se place l'algorithme. Les principes et concept s'appliquent à toutes les couches.

J'imagine des archipels, une mer pour les variables avec des iles par langage, pour les structures de données, puis plus large pour les organisations boucle et structures conditionnées, ...

Il manque des dessins;
Il me manque actuellement, dans mon approche, un cheminement entre l'algorithme et le code. Penser la structure, l'organiser, la coder, et inversement, lire du code, s'en faire une représentation, l'affiner, l'améliorer, intervenir dans le code.

C'est sans doute important d'aborder une dimension culturelle dans l'apprentissage de la programmation. Ça semble aller de soi, mais c'est pas le cas dans les fait. Ça me rappel ce que disait Julien à propos de s'appuyer sur les anciens pour faire ce que nous pouvons.

Reprendre tout ce qu'il c'est passé au Rookie Club comme un livre pour l'apprentissage de la programmation. Raconter mes explorations, les informations. Ma conclusion pourrait être ce que je viens de notre... Ou bien ce que j'arriverais à mettre en place sur de nouveau cursus de formation.

---

Dans un échange très intéressant sur l'architecture sur le slack des artisans du logiciel, un article (en autre) a attiré mon attention. Sans doute parce que ça fait un moment que Rails me gratte un peu vis à vis de ce sujet, et que j'aimerais essayer des choses. Cet article est une invitation à structurer autrement le code de RDV-Solidarités.

> Ruby on Rails a été pensé pour être facile à utiliser. Ce qui est très pratique pour un POC ou un petit projet. Mais cela manque clairement de séparation des responsabilités — comme on a pu le constater avec l’exemple ci-dessus —, ce qui est dommageable quand le projet commence à grossir, car en créant du couplage, le code devient alors plus difficile à maintenir, à tester et à faire évoluer.

[Rails n'est pas simple !](https://www.synbioz.com/blog/tech/rails-nest-pas-simple)

Cependant, je trouve que la proposition, bien qu'intéressante, déstructure pour restructurer à l'intérieur. Je trouve ça un peu dommage. Ça me fait beaucoup penser au bouquin PODR de Sandy Metz

Je me demande comment tendre vers ce type d'architecture. Est-ce qu'il faut isolé un élément du domaine au moment où on le découvre ?

---

[Bike map](https://www.bikemap.net/en/search/?bounds=-3.2237189679769642%2C47.83833075875148%2C-3.0707144538898774%2C47.939757401849874)

---

> What is SES?
> 
> SES (Secure ECMAScript):
> 
>     Is a JavaScript runtime library for safely running third-party code.
>     Addresses JavaScript’s lack of internal security.
>         This is particularly significant because JavaScript applications use and rely on third-party code (modules, packages, libraries, user-provided code for extensions and plug-ins, etc.).
>     Enforces best practices by removing hazardous features such as global mutable state and lack of encapsulation in sloppy mode.
>     Is a safe deterministic subset of "strict mode" JavaScript.
>     Does not include any IO objects that provide ambient authority
> 
>     (opens new window).
>     Removes non-determinism by modifying a few built-in objects.
>     Adds functionality to freeze and make immutable both built-in JavaScript objects and program created objects and make them immutable.
> 

https://agoric.com/documentation/guides/js-programming/ses/ses-guide.html#what-is-ses
