---
title: Mercredi 10 février 2021
---

[Github1S](https://github.com/conwnet/github1s)

> One second to read GitHub code with VS Code.

---

Très bon texte de référence (merci David B.)

[The Characteristics of White Supremacy Culture](https://www.showingupforracialjustice.org/white-supremacy-culture-characteristics.html)

> - Perfectionism
> - Sense of Urgency
> - Defensiveness
> - Quantity Over Quality
> - Worship of the Written Word
> - Only One Right Way
> - Paternalism
> - Either/Or Thinking
> - Power Hoarding
> - Fear of Open Conflict
> - Individualism
> - Progress is Bigger, More
> - Objectivity
> - Right to Comfort

Je me dis que ça vaudrait sans doute la peine de faire une traduction.

---

[#6 - Henri Verdier : La racine du problème, c’est le design des réseaux sociaux](https://f14e.fr/hackerspublics/ep6_henri_verdier/)

> Aujourd'hui je regrette d'appeler ça startup.

> Dans l'état, il y a un paradigme qui a la vie dur (que je vais couper en 2 morceaux):
> 1. la vision méprisante que le code c'est une fonction support
> 2. Nous vivons les derniers résidus de ce fameux New Public Management



