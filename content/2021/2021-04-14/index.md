---
title: Mercredi 14 avril 2021
---

Est-ce que les fleurs bleues dans la cour sont des _Veronica persia_ ? Pas sur. Peut-être [_Brunnera macrophylla_](https://fr.wikipedia.org/wiki/Brunnera_macrophylla) ? Non, fleurs trop petites. C'est sans doute une variété de [_Myosotis_](https://fr.wikipedia.org/wiki/Myosotis). Reste à vérifier et savoir laquelle.

J'ai besoin de m'entrainer à l'identification. Mais j'ai du mal à prendre le temps, et à avoir les bon « outils ». Dans les outils j'entends d'une part les éléments à regarder pour faire de la discrimination, et à la fois, le recueille dans lequel je peux identifier toutes plantes. La flore que j'ai ne concerne que les plantes sauvages. Comment savoir du coup si c'est sauvage ou pas ?

Il est peut-être temps que je mette mon œil d'informaticien dans ces outils d'identification pour mieux les comprendre.

Peut-être https://fr.wikipedia.org/wiki/Myosotis_sylvatica ? ou bien https://fr.wikipedia.org/wiki/Myosotis_scorpioides voir https://fr.wikipedia.org/wiki/Myosotis_alpestris

(Après être allé vérifier sur le terrain)

Bon, c'est sur que c'est une Myosotis. Vu l'altitude ici qui n'est pas beaucoup au dessus de 0, ce n'est pas une Alpestris. Et, bien qu'il y ait parfois beaucoup d'eau, surtout pendant l'hiver car le soleil ne touche pas la terre dans la cour, je ne pense pas que ce soit une Scorpioides malgré tout... Je penche donc pour une Sylbatica. Il serait intéressant que je sache comment les distinguer, quels éléments de clef d'identification m'aiderais à les distinguer.

---

[Bookwyrm](https://bookwyrm.social/), un outil pour partager des infos sur les livres que nous avons lu...
Trouver une instance pour y publier / partager les lectures et les notes prises sur certains livre ? Mais pourquoi faire ?

Et pourquoi pas [inventaire.io](https://inventaire.io/welcome) ? Peut-être aucun rapport ? Peut-être pas le même objectif.

---

[Atos gagne le marché interministériel du support des logiciels libres](https://www.lemondeinformatique.fr/actualites/lire-atos-gagne-le-marche-interministeriel-du-support-des-logiciels-libres-82578.html)

Je n'ai pas l'impression que ce soit une bonne chose. Et en même temps, c'est le signe que le logiciel libre va être plus utilisé dans le publique, mais combien d'argent Atos va-t-il se faire sur le dos des citoyen, et du logiciel libre ?

---

[Pour trouver une expertes](https://expertes.fr/) pour évoquer un sujet, une conférence, une intervention...


