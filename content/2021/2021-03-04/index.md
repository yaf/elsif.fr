---
title: Jeudi 4 mars 2021
---

> Dans les parkings des HLM, les nouveaux «  job verts  » consistent à faire pousser des fruits et légumes en environnement contrôlé, qui seront vendus à 10 euros le kilos. Une gentrification verte renforcée par certains discours paternalistes qui voient l’agriculture urbaine comme le moyen d’éduquer les citadins, leur faire prendre conscience de la valeur du travail agricole périurbain, dans l’idée à terme d’augmenter leur consentement à payer pour des produits alimentaires locaux et bio. En bref  : culpabilisation individuelle et déresponsabilisation politique.

> Enfin, la volonté politique derrière l’idée qu’il est possible grâce à une production hors-sol intensive de subvenir à nos besoins alimentaires en milieu urbain, est aussi de justifier l’extension illimitée de l’urbain et l’artificialisation des terres. Pourquoi s’en préoccuper si les parkings peuvent être cultivés et les terrasses retenir les eaux de pluies et accueillir la biodiversité  ? Alors que la France est le pays européen qui consomme le plus de sols agricoles, Sylvia Pérez-Vitoria met en garde et soutient que seule l’agriculture paysanne nourrira l’humanité  : l’agriculture urbaine reste un complément qui peut être bénéfique si elle conduit à «  la recomposition conjointe du rapport ville-campagne  ».

[Agriculture urbaine : de la « guerilla gardening » aux start-up capitalistes](https://www.unioncommunistelibertaire.org/?81-Agriculture-urbaine-de-la-guerilla-gardening-aux-start-up-capitalistes)

---

> Les exposés pour construire sa culture technique
>
> - 8 exposés (minimum) par apprenant·e dont 5 thématiques imposées pour se faire une culture technique
> - validation entre pairs lors du forum ouvert (jury de 2 apprenantes ayant déjà validé cet exposé)
> - fiches de validation à disposition du jury (liste de questions à poser et de termes à couvrir)
> - approfondissement de 3 thématiques au choix de l’apprenant·e selon son orientation

Une nouvelle activité chez Ada Tech School. C'est vrai que ça avait bien fonctionné chez Simplon quand nous faisions ça. Ça s'appelait les café code je crois. Là, ce qui est intéressant c'est l'aspect « validation » par les paires, l'échange que cela permettait entre les différentes personnes et les différents niveaux de connaissance.

> Une liste de  savoirs à connaître :
> - Comment fonctionne le Web
> - Classification et historique des langages de programmation
> - Fonctionnement basique d’un ordinateur
> - Modèle ISO et bases de réseau
> - Patrons de conception

> Une liste de  savoir-faire à maîtriser : :
> - Variables et flux d’exécution
> - Fonctions et structures de contrôle
> - Structures de données
> - Bases de developpement web
> - Programmation orientée objet
> - Programmation asynchrone
> - Tests et TDD
> - Expressions régulières et littéraux de gabarits

