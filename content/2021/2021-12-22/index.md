---
title: Mercredi 22 décembre 2021
---

> Il est donc important de bien distinguer et ne pas confondre taux de conformité à la norme et niveau réel d’accessibilité.

[Accessibilité : conformité vs. réalité](https://www.24joursdeweb.fr/2021/accessibilite-conformite-vs-realite/)

---

Une foncière pour de l'habitat !

[Antidote, la Foncière pour des lieux collectifs pérennes & solidaires](https://lafonciereantidote.org/qui-sommes-nous/notre-histoire/)

> Bâtir des alternatives aux modèles dominants – c'est-à-dire, par exemple, habiter des immeubles sans loyers, travailler dans des organisations sans patron·nes, fertiliser des terres sans héritier·ères, utiliser un atelier, une boutique un garage, une cuisine, un bar, une salle de spectacle ou tout autre espace dont on a fait disparaître cet élément d'ordinaire indispensable au décor : le propriétaire – bâtir cela donc, c'est d'abord évoluer aux marges du Droit. Et bien souvent, tenter de faire rentrer des ronds dans des triangles. 


---

[Votre Adresse Mail BZH - La messagerie électronique bretonne](https://www.korrimel.bzh/)

---

[La Clameur, podcast social club](https://laclameur.org/)

> Actuellement association loi 1901, la Clameur a pour objectif de devenir une maison de production de podcasts et un espace de création collective organisé sous la forme coopérative.


> Déclarée à Bordeaux, mais regroupant des membres de nombreuses régions françaises, La Clameur PSC souhaite rendre accessible la production sonore à l’ensemble des territoires, notamment ruraux et périurbains.
>
> Aujourd’hui, le monde du podcast est dominé par la concurrence et l’individualité, ce qui crée de la précarité et de l’isolement pour l’ensemble des acteur‧ices  de la chaîne de production sonore. Nous souhaitons mettre en place un modèle de production s’appuyant sur l’entraide, l’expérimentation et la coopération.
>
> C’est pourquoi, nous défendons une création sonore sans sponsoring ou contenu de marque. Un modèle où l’ensemble des parties prenantes, des technicien‧nes du son aux créateur‧ices sonores, seront payé‧es équitablement. La rémunération et l’organisation du travail sont au cœur de notre démarche : nous pensons que ce n’est qu’ensemble et sans hiérarchie que nous pourrons faire émerger de nouvelles voix.

---

[Terre de liens - UI Style Guide](http://tdlui.semaphore-communication.fr/)

---

[La ferme des Zulus](https://la-ferme-des-zulus.wixsite.com/lafermedeszulus)

Le site de la ferme des Zulus de Marie à refaire avec un vrai nom de domaine ? C'est peut-être déjà un coup de main facile à faire...

Que faisait le club informatique à Lanvénégen ? Et pourquoi je veux le savoir ? Sans doute pour monter un truc qui fera peut-être pareil et donc éviter de tomber dans les pièges qu'ils et elles ont eut ? Qui contacter ?

---

[Simplifier sa vie de développeur avec les raccourcis clavier](https://blog.ippon.fr/2021/12/17/simplifier-sa-vie-de-developpeur-avec-les-raccourcis-clavier/)

Ça peut sembler évidant, mais pourtant, beaucoup de personne qui souhaite approfondir leur usager de l'informatique gagnerais à s'approprier ces raccourci. Tant de personne ne les connaisse pas et on le sentiment d'être « lente » derrière leur ordinateur.

