---
title: 29 avril 2021
---

Il n'est pas impossible que je me retrouve à utiliser
[Azure](https://azure.microsoft.com/fr-fr/) prochainement... Ça pourrait être
intéressant.

- [Deploying your Rails + PostgreSQL app on Microsoft Azure](https://medium.com/paris-rb/deploying-your-rails-postgresql-app-on-microsoft-azure-180f8a9fab47)
- [Create a Ruby on Rails App in App Service](https://docs.microsoft.com/en-us/azure/app-service/quickstart-ruby)
- [Ruby on Rails sample for Azure App Service](https://docs.microsoft.com/en-us/samples/azure-samples/rubyrails-tasks/ruby-on-rails-sample-for-azure-app-service/)

---

[Hack Commit Push - A family of Hackergarten
events](https://hack-commit-pu.sh/)

> hack-commit-push is a series of events inviting attendees to start
> contributing to Free/Open Source projects with the help of their maintainers.
Get together, support free/open source projects by fixing issues and adding new
features and most of all learn and have fun.
>
> The event is open to everyone, regardless of their experience.
>
> Every contribution counts!

---

Rejoindre une CAE ?

- [Oxalis](https://www.oxalis-scop.fr/)
- Local ? [Filéo Groupe](https://www.fileogroupe.coop/groupement-dentrepreneurs/)
- [Coopaname](https://www.coopaname.coop/)

---

[Electronic Tales](https://platform.electronictales.io/)

