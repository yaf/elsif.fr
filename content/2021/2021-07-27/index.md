---
title: Mardi 27 juillet 2021
---

> regarde tout ce que l’on peut faire aujourd’hui que l’on ne pouvait pas faire avant! Vous vous méprenez et c’est ce qui me rebute le plus de ces (pas si) petits appareils: ils limitent le possible, ils ne l’élargissent pas. Ils étriquent le réel.

> La créativité et la singularité demandent des efforts, elles ne sont pas intuitives. Elles demandent même parfois de renoncer à être facilement avec les autres: ma mère ne m’écrit plus puisque je ne suis pas sur WhatsApp, le courriel est so 2010. Mais n’est-ce pas notre créativité, notre singularité et les efforts que nous produisons pour les entretenir qui ont la capacité de nous rendre libres?

[Ce qui pourrait être autrement: contre les téléphones](http://blog.sens-public.org/marcellovitalirosati/cequipourrait/telephones.html)

---

> J’ai envie de pouvoir faire, penser, écrire des choses inutiles, qui ne servent absolument à rien. Des choses gratuites, dont les conséquences ne sont pas importantes ou, de toute manière, ne constituent pas une cause finale. J’ai envie de faire aussi des choses nuisibles, qui nuisent à l’impératif de productivité, qui cassent cet espèce de flux insensé. J’ai envie de perdre du temps, de développer un outil informatique qui ne sert à rien, qui ne fonctionne pas, qui ne fait pas gagner du temps, qui n’est même pas beau - car la beauté aussi est assujettie à l’utilité.

[Ce qui pourrait être autrement: éloge de l'inutilité](http://blog.sens-public.org/marcellovitalirosati/cequipourrait/inutilite.html) 

---


Partez à la découverte
des rivières et rias !

    contact@exploriders.fr
    06 03 64 14 98


[Exploriders](https://www.exploriders.fr/)
