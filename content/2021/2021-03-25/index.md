---
title: Jeudi 25 mars 2021
---

Un début de liste de femmes que je trouve top dans le monde de la programmation, de l'informatique...

- https://twitter.com/shadjiat
- https://twitter.com/AudreyBsnw
- https://www.linkedin.com/in/raphaelle-roffo/
- https://www.linkedin.com/in/sandra-chakroun
- https://www.linkedin.com/in/clairecasubolo/
- https://www.linkedin.com/in/segolene-alquier/
- https://twitter.com/Seeeeeeeeeeeg
- https://twitter.com/SegoleneAlquier
- https://www.linkedin.com/in/amiralakhal/
- https://www.linkedin.com/in/karesti/
- https://www.linkedin.com/in/ambalaurelie/
- https://www.linkedin.com/in/mathilde-renversade-developpeur-web-backend/
- https://www.linkedin.com/in/st%C3%A9phanie-baltus/
- https://www.linkedin.com/in/sgamerman/
- https://www.linkedin.com/in/lailatrmouh/
- https://www.linkedin.com/in/mathildelemee/
- https://twitter.com/MathildeLemee
- https://www.linkedin.com/in/ludwineprobst/
- https://www.linkedin.com/in/thimy-kieu-40041687/
- https://www.linkedin.com/in/czuliani/
- https://www.linkedin.com/in/estellecomment/
- https://twitter.com/celine_m_s
- https://twitter.com/Ynote_hk
- https://twitter.com/endlat
- https://twitter.com/materrier
- https://twitter.com/TiffanySouterre
- https://twitter.com/cfalguiere
- https://twitter.com/Ly_Jia
- https://twitter.com/LoozBonita
- https://twitter.com/semia_l
- https://twitter.com/ShirleyAlmCh
- https://twitter.com/renversou
- https://twitter.com/nivdul
- https://twitter.com/laurenfraz
- https://twitter.com/gayahel
- https://twitter.com/Loreen_
- https://twitter.com/C_Briard
- https://twitter.com/lenafaure
- https://twitter.com/womenonrails

---

Aujourd'hui, je reparle des [Core Protocols](https://thecoreprotocols.org/), [CNV](https://fr.wikipedia.org/wiki/Communication_non_violente) et autres outils dans le cadre d'une formation. Oui, c'est pas facile de constitué un groupe. Surtout quand la plupart si ce n'est tout, les membres se mettent en dehors de leur zone de confort pour apprendre un nouveau métier, bien souvent en mode dernière chance...

Donc oui, il y a besoin d'accompagner ce groupe, de le coacher, en posture basse, en l'amenant à apprendre par lui même. [Freinet](https://fr.wikipedia.org/wiki/C%C3%A9lestin_Freinet) avait une posture basse la plus part du temps je pense :thinking:.


---

Très chouette retour [J’enseigne, je code et je partage – EP 01 : Rémi Angot](https://blog.faire-ecole.org/2021/03/23/jenseigne-je-code-et-je-partage-ep-01-remi-angot/)

Ça me donne envie de devenir prof pour faire des trucs pareils... Mais peut-être que je pourrais contribuer sans être prof ?

---

> A quick quiz
>
> What’s the goal of doing code reviews? Is it:
>
> 1. To catch defects before they reach production
> 2. To share knowledge about helpful patterns and best practices
> 3. To discuss alternative approaches and viewpoints
> 4. To allow developers of all levels of experience to learn
> 5. To link to useful documentation and other resources
> 6. To distribute knowledge across the team
> 7. To ask questions and check understanding
> 8. To improve readability and maintainability of the code
> 9. To identify documentation needs
> 10. To ensure that work meets quality standards
> 11. To record the rationale for certain changes
> 12. To coach and mentor junior developers
> 13. To promote sharing ownership of the codebase
> 14. To notify other affected teams of changes that are being made
> 15. All of the above


> In fact, we sometimes wonder if catching defects is one of the weakest arguments for code reviews

> Communication is at the heart of code review
> As mentioned earlier, often the real benefits of code reviews are communication and understanding. GitHub is a powerful collaboration tool but not the only one at our disposal. Jumping on a Zoom or Teams call to talk it through can be friendlier and more efficient than back-and-forth debates on PRs.

[Looks Good To Me: Making code reviews better for remote-first teams](https://medium.com/bbc-design-engineering/looks-good-to-me-making-code-reviews-better-for-remote-first-teams-95bd92ee4e27)

