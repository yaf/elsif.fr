---
title: Mercredi 13 octobre 2021
---

[Un projet incroyable sur 3000 hectares (pour commencer)](https://www.youtube.com/watch?v=3SuLOmgW5-8)

[Koridori les pas sage de la vie](https://www.koridori.org/)

> Un travail à l'échelle d'une commune, un travail pour plus de bio diversité, plus de respect des uns et des autres, animaux, végétaux, humains, ...

C'est [Ver de terre prod](https://www.verdeterreprod.fr/) qui va aider à documenter l'expérimentation. C'est à suivre.

Où j’apprend que les trognes à hauteur sont appelé des prairies aériennes... Merci Dominique Mansion, artiste et auteur, défenseur de la trognes.

_Tiens, pas de page Wikipedia pour Monsieur [Dominique Mansion](https://www.babelio.com/auteur/Dominique-Mansion/78502). À faire ?_

À lire ? [« Identifier les traces d'animaux » de Dominique Mansion](https://www.babelio.com/livres/Mansion-Identifier-les-traces-danimaux/412803)
À lire ? [« Les trognes : L'arbre paysan aux mille usages » de Dominique Mansion](https://www.babelio.com/livres/Mansion-Les-trognes--Larbre-paysan-aux-mille-usages/283658)
À lire ? [Flore Forestière Française - volume 1 plaines et collines](https://livre.fnac.com/a13074416/Jean-Christophe-Rameau-Flore-forestiere-francaise) voir aussi la référence aux trois tomes sur tela botanica [La Flore forestière française](https://www.tela-botanica.org/2011/05/article4425/)
À lire ? [Petit traité du jardin punk](https://www.babelio.com/livres/Lenoir-Petit-traite-du-jardin-punk/1061676)

---

C'est quoi les problèmes des paysans ?

Prix de vente ? J'entends dans une vidéo un paysan qui souhaite vendre une carotte à 3 €, voir 2,60 si quelqu'un viens la chercher sur place, alors que c'est vendu 0,80 € en bio industriel. Mais le prix du bio indus ne reflète pas les cout caché (pollution, social & co)

Il y a le transport et la vente, distribution...

Ça serait intéressant d'explorer.

[Le grand manger](https://www.legrandmanger.bzh/), cantine... Mais si on faisait plutôt un atelier de transfo partager ? Un point de rencontre.

Le maraichage c'est cool, mais ça ne suffit pas. Il faut aussi beaucoup de céréales. Il faut aussi pas mal de transfo. Tout le monde n'a pas le temps de le faire...

