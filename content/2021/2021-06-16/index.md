---
title: Mercredi 16 juin 2021
---

Quels ingrédients utiliser pour un savon fait maison ?

1/ Un corps gras : huiles ou beurres

En général, la part d’huile ou de beurre représente 40% à 60% de la masse totale de gras. Beurre de karité, huile de coco; huile de ricin, huile de pépins de raisin, huile de macadamia, beurre de cacao …

2/ Une base forte : de la soude ou du potasse

On trouve en magasin de bricolage sous forme de

- « lessive de soude » , elle est alors déjà diluée mais il faut s’assurer de la présence du terme hydroxyde de sodium accompagné du pourcentage de soude contenue.
- Soude solide qui doit être diluée avant utilisation. Comme liquide de dissolution on utilise le plus souvent de l’eau déminéralisée mais d’autres liquides peuvent convenir (lait, infusion …)

L’hydroxyde de sodium est extrêmement caustique ! Portez des gants, lunettes et une blouse de protection du début jusqu’à la fin du processus de fabrication.

3/ D’autres agents

- Colorants (végétaux ou argiles mais pas alimentaires)
- Huiles essentielles pour leurs propriétés aromatiques car la saponification altère leurs propriétés
- Les actifs pour donner davantage de propriétés au savon. Par exemple : du marc de café pour un savon gommant, de l’argile pour un savon purifiant …

La saponification késako ? Calcul des quantités

La saponification c'est la réaction chimique qui transforme permet la synthèse du savon ( en transformant un ester en ions carboxylate et un alcool). En résumé, c'est l'étape indispensable pour créer son savon maison. Et celle-ci mérite un peu de maîtrise et quelques connaissances.

Avant de se lancer dans la confection de savon, il faut ainsi savoir que l'indice de saponification correspond à la masse d’hydroxyde de sodium (soude) nécessaire pour saponifier complètement 1g de corps gras. L'indice de saponnification peut ainsi varier pour chaque ingrédient/mélange/composition.


    Exemple. l’indice de saponification du beurre de karité et de 0,133. Cela signifie que pour saponifier 1 g de beurre de karite il faut 0,133 g de soude.

Pour connaitre la quantité de soude nécessaire, il faire le calcul suivant : Indice de saponification de l’huile ou du beurre X quantité de cet huile ou beurre en gramme = quantité de soude nécessaire en gramme.



    Exemple : si on veut utiliser 500 g de beurre de karité dans un savon, on calcule 0,133 (indice de saponification du beurre de karité) × 500 g (corps gras) = 66,5 g de soude pure.

MAIS on ne saponnifie jamais un savon dans sa totalité. Il est nécessaire de toujours en calculer un surgras pour réduire le risque de causticité. En général, ce surgras est de l’ordre de 5 % et 9 %. Si on souhaite surgraisser un savon à 7 % (donc saponifier seulement 93 % du corps gras), on calcule donc : quantité initiale de soudure en g X pourcentage de saponification (93%)= nouvelle quantité de soude en gramme.

    Pour notre exemple : 66,5 x 93 % = 61,5 g de soude pure nécessaires.

Indices de saponification (avec de la soude) de quelques huiles

Beurre de karité : 0,133

Beurre de cacao : 0,137

Huile de coco : 0,183

Huile de ricin : 0,128

Huile d’amande douce : 0,139

Huile d’Argan : 0,136

Huile d’olive : 0,135

Recette : le savon hydratant fait-maison à l’aloe vera

(Pour un savon surgras 20 %)

Ingrédients :

    227,6 g de lessive de soude (69,42 g de soude pure à diluer + 158,18 g d’eau)
    400 g d’huile de coco (80 %)
    100 g de macérat d’aloe vera (20 %)
    10 g de lait de coco
    10 g de poudre d’aloe vera

La "recette" :

     Peser les ingrédients
     Faire fondre l’huile de coco au bain-marie. Hors du feu ajouter Le macérat d’aloe vera.
     Ajouter progressivement la poudre d’aloe vera et le lait de coco. Bien mixer.
     Quand les huiles et la soude sont à température équivalente, verser la soude dans les huiles. Au fouet ou au mixeur plongeant, mélanger jusqu’à obtenir une pâte de la consistance souhaitée.
     Verser dans les moule, couvrir et laisser reposer 24 heures avant de démouler et de découper le savon.

Pour aller plus loin. « Savon au naturel, méthode, conseils et recettes » de Charlie Marandet aux éditions Alternatives. 13,50 €

[Comment fabriquer ses propres savons](https://www.magazine-avantages.fr/comment-fabriquer-ses-propres-savons,192617.asp)

----

À lire ? « Bureau des sabotages » de Frank Herbert
