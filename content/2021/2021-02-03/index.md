---
title: Mercredi 3 février 2021
---

Un [kit lowtech de conservation de fruits et légumes](https://wiki.lowtechlab.org/wiki/Module_de_conservation?). J'apprécie particulièrement la documentation du pourquoi et comment la conservation se fait.

---

Whaou ! Voilà une orientation qui me conviens bien, et des pistes pour publier aussi les contenus avec le protocole gemini !

[Maintenir un site statique avec un Makefile](https://ybad.name/log/2021-01-27.html).

Merci David pour la découverte.

> Le script "atom.sh" est un peu plus complexe, et profite de la façon dont les fichiers sont nommés.  Il est spécifique à OpenBSD car la commande "stat" permet d'obtenir la date de dernière modification d'un fichier, et le format n'est pas le même sur une distro GNU 

Un utilisateur d'OpenBSD en plus. Voilà qui me donne envie d'y retourner !

J'aime beaucoup le concept de « chatons de Schrödinger » sur [3hg.fr](https://www.3hg.fr/index.html)

> Pendant la période de confinement de 2020, j'en ai profité pour m'y remettre et réécrire certains projets en C.  J'ai donc de nouveau lu du code, notamment celui de suckless.org mais aussi celui d'OpenBSD (en partie).  Cette expérience a été une aventure exaltante qui me fait dire aujourd'hui : j'adore le C.

[Pourquoi j'adore le langage C?](https://ybad.name/Logiciel-libre/Code/C/index.html)

C'est beau.

---

À lire ? [Petit traité du jardin punk](https://boutique.terrevivante.org/librairie/livres/4140/champs-d-action/458-petit-traite-du-jardin-punk.htm)

Découvert après la lecture du [portrait d'Éric Lenoir, le jardinier punk](https://asso.wwoof.fr/2021/01/21/portrait-dhote-eric-lenoir-le-jardinier-punk/)

---

Note d'écoute de la vidéo / réunion du 16 décembre 2020 création / projet de la SCIC Usage Communs

Beaucoup d'EIG, betagouv, de desginer surtout. Urscop & co.

OpenFisca, MesAides

Raphael aidant connect, aides territoire, ...

Objet

Difficulté à interagir avec l'administration / utilité publique. Création d'un espace de coopération.

S'inscrire dans le temps.

Hugo, frustré de réfugié.info qui s'arrête plus ou moins. Difficile de pérenniser. Ne pas être seulement dans une posture de prestataire.


Reproduire un schema de betagouv sur la simplification d'interaction avec l'état - freelance - petit groupe.

> Un betagouv extérieur à l'état


SCIC est plus légitime qu'une asso (?).

> Scalabilité forte.

Même chose que le groupement dev, mais coté design.

Évocation de « Vraiment Vraiment ». Beaucoup de design, mais manque de dev.

Pas de référence au groupement, mais uniquement à Codeur en libertés :-/

Format SCIC, faire en sorte qu'il y ait plusieurs intérêt, plusieurs acteurs qui interagissent pour faire en sorte de faire sortir un communs, un intérêt général.

Possibilité d'intégrer des collectivités.
- producteurs
- bénéficiaires
- autres

---

> Yotter allows you to follow and gather all the content from your favorite Twitter and YouTube accounts in a beautiful feed so you can stay up to date without compromising your privacy at all. Yotter is written with Python and Flask and uses Semantic-UI as its CSS framework.

[Yotter](https://github.com/ytorg/Yotter)


---
> « Être le luthier de vos écrits », est un résumé de nos valeurs : faire un bel objet artisanal ; un objet utile en fonction de vos besoins et de votre santé ; pour donner un aspect artistique pour inspirer votre travail ; une poignée de main entre artisan et client.

[ToucheLibre](http://touchelibre.fr/)

Est-ce que c'est plus facile d'avoir un équipement libre et facilement réparable avec une tour plutôt qu'avec un portable ?

---

> Où que vous soyez, cette carte vous permettra de trouver du poisson frais en direct des pêcheurs. Cliquez simplement sur les liens correspondants aux lieux qui vous intéressent. Si vous souhaitez vous investir avec nous, cliquez sur “je souhaite soutenir la pêche locale” !

[Cartographie des circuits courts de la filière pêche](https://associationpleinemer.com/cartographie-des-circuits-courts-dans-la-peche/)

