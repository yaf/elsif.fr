---
title: Samedi 20 mars 2021
---

> La référence aux communs évoque avant tout le mouvement des enclosures, qui se déroula en Angleterre à partir du XIIe siècle, mais surtout entre les XVIe et XVIIIe siècles. Au cours de ce processus historique de longue haleine, les terres « ouvertes », cultivées selon des méthodes traditionnelles et souvent gérées de façon collective, furent graduellement transformées en propriétés privées. Cela marqua la fin des droits coutumiers d’usage sur les terrains communaux, dont de nombreux paysans dépendaient pour leur subsistance, et occasionna un appauvrissement considérable de la population rurale. 


> Même si les deux mouvements du free software et des communs ont des origines distinctes et des spécificités fortes qui fondent de vraies différences (les dualités : bien rivaux/non rivaux, conservation/enrichissement de la ressource, accès universel/accès limité), un rapprochement substantiel entre les deux mondes va s’effectuer. Tout ici vient d’un commun rejet de l’exclusivisme au cœur des doctrines de la propriété depuis longtemps hégémoniques, dont la remise en cause tant du côté des communs que du logiciel libre va opérer comme catalyseur d’un rapprochement.

> Richard Stallman décrit ainsi le raisonnement à l’origine de la GPL comme « une forme de ju-jitsu intellectuel, destiné à retourner le système légal mis en place par ceux-là mêmes qui souhaitaient retenir pour eux seuls les biens logiciels » (Stallman, Williams et Masutti, 2010, p. 173).

[Le logiciel libre et les communs - Deux formes de résistance et d’alternative à l’exclusivisme propriétaire](https://www.cairn.info/revue-internationale-de-droit-economique-2015-3-page-265.htm?contenu=article)

---

_Mail envoyé le 20 mars 2021_

> Bonjour Claude,
>
> Nous nous sommes croisé dans les Pyrénées, lorsque Stéphane avait organisé un petit rassemblement aux Angles. Je travaille avec lui à [Scopyleft](http://scopyleft.fr/). Peut-être te souviens-tu de moi. Ou pas, et ce n'est pas très grave
>
> Je viens de lire ton article [Expériences avec Scrum en télétravail](https://www.aubryconseil.com/post/2021/scrum6-teletravail/). Je n'ai pas lu ton livre. Je ne connais donc pas le chapitrage utilisé. Mais je crois que le télétravail à sa place dans le chapitre qui parle (s'il y a un chapitre dédié à ça) de l'équipe et son organisation, de la co-localisation de l'équipe.
>
> Le télétravail me semble posé effectivement plusieurs questions :
> - faut-il reproduire l'effet co-localisation ? J'entends par là arriver aux même heures, passer la journée ensemble ?
>
> J'ai eu une expérience comme ça où nous faisions du mobProgramming. C'était épuisant à distance, plus qu'en présentiel. Mais ça fonctionnait pas mal. Nous avions décidé de faire des sessions de 2 à 3 heures max, et je dirais 6 maximum par semaine. Nous avions donc aménagé des espaces pendant lesquels nous travaillions dans notre coin sur un sujet ou un autre.
> 
> Une autre expérience plus récente me montre que c'est aussi possible d'être asynchrone. Alors pas complètement bien sur. Nous avons des temps de rencontre, une rétro, un point produit (priorisation ?) et une démo. Puis nous faisons parfois un peu de binomage sur un point particulier. Ça me fait beaucoup penser au travail sur du logiciel libre. C'est plaisant par certains aspects, mais est-ce que nous sommes vraiment une équipe ?
> 
> Sur cette dernière expérience (qui est en cours), il me semble beaucoup plus dure de partager une culture commune du code, du produit.
> 
> 
> - Quel matériel pour télétravailler en équipe ?
> 
> Un élément pénible pendant mon expérience de mobProgramming, était le port du casque durant la session. Je n'avais pas de bon micro, et je n'étais pas dans un environnement qui me permettait d'utiliser les enceintes de l'ordinateur. J'en ai parlé avec mon collègue David. Lui à une installation intéressante. Dans un bureau dédié, il peut utiliser les enceintes pour écouter les personnes qui lui parler, et il a acheté un bon micro, posé sa table. Et le voilà dégagé des oreilles et de la bouche.
> 
> Il y a aussi les fondus de la webcam. C'est vrai que c'est bien de se voir. Mais ça pose des questions sur la vie privée. Et puis est-ce bien nécessaire ? Nous nous faisons un petit coucou de temps en temps, mais rien de plus. Le son est plus important ici. De toute façon le langage corporel passe très mal la webcam. Enfin, c'est ce que je constate.
> 
> Voilà, c'est un peu décousu sans doute, et je ne suis pas sûr que ce soit ce genre de message que tu voulais recevoir.
> 
> Merci de faire ce que tu fais
> 
> -- Yannick
> 

Mail envoyé suite à la lecture de l'article de Claude sur l'[Expériences avec Scrum en télétravail](https://www.aubryconseil.com/post/2021/scrum6-teletravail/)

---

Je découvre que Christophe a utilise [Middlelan](https://middlemanapp.com/) comme générateur de site static pour [Merci Edgar](https://www.merciedgar.com/). Je suis surpris, dans l'écosystème Ruby, c'est souvent Jekyll qui est choisi. J'attends son retour pour en savoir plus.

Je me retrouve, une fois de plus, à vouloir lire des [Décision d'architecture](https://adr.github.io/). Nous avions mis ça en place sur DossierSCO, j'avais trouvé ça très intéressant, et comblant un manque. J'aimerais le mettre en place sur RDV-Solidarités, mais mes coéquipiers n'était pas chaud pour le moment. Je vais peut-être profiter d'un changement de binôme pour remettre le sujet sur la table.

Et sur Merci Edgar alors ?

---

À lire ? Brutalisme de Achille Mbembe

Découvert via l'[annonce d'un atelier d'arpentage de Brutalisme](https://dérivation.fr/evenement/arpentage-brutalisme/)
