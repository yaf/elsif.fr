---
title: Vendredi 10 septembre 2021
---

À la recherche d'un meunier, voir paysan-meunier...

- [Atelier des Pains](https://www.mangeons-local.bzh/producteurs/atelier-des-pains/)
- [La Ferme du Lannic](https://www.mangeons-local.bzh/producteurs/la-ferme-du-lannic/) (plus prêt)
- [Les paysans solidaires](https://www.lespaysanssolidaires.fr)

Sans doute possible d'essayer [le moulin de Loudeac](La Ferme du Lannic). Ça fait un peu loin par contre :-/

Et sinon, encore des possibilités pour utiliser mon temps de manière enrichissante ?
- [Le Cric, Accompagnemen des projets d'éducation à l'entreprenariat en collectif en Bretagne](http://lecric.coop/)
- [Kejal Favoriser l'émancipation individuelle par la formation-accompagnement de projet](https://www.kejal.fr/)

