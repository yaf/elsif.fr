---
title: Jeudi 20 mai 2021
---

> L’idée de “services” est en opposition avec celle du service public : réduire le service public au service rendu, c’est réduire ses ambitions de manière drastique. On n’est pas juste là pour délivrer une prestation. C’est d’ailleurs là une des différences entre public et privé : on ne propose pas un produit, on essaie de résoudre des problèmes. C’est beaucoup plus dur et c’est vachement plus ambitieux. Ça nécessite de se confronter à une énorme complexité, pour essayer de remonter le fil des problèmes.

[Collectif Nos services publics : “le point de départ, c’est le décalage entre l’orientation des services publics et les besoins des gens”](https://autrementautrement.com/2021/05/19/collectif-nos-services-publics-le-point-de-depart-cest-le-decalage-entre-lorientation-des-services-publics-et-les-besoins-des-gens/)

---

> Abeilles :
> Ce qui est vrai partout et conformément à l'article L 211-7 du Code Rural :« Ne sont assujetties à aucune prescription de distance les ruches isolées des propriétés voisines ou des chemins publics par  un  mur,  une  palissade  en  planches  jointes,  une haie  vive  ou  sèche  sans  discontinuité.  Ces  clôtures doivent avoir une hauteur de 2 mètres au-dessus du sol et s'étendre sur au moins 2 mètres de chaque côté de la (ou des) ruche(s). »
> Y'a donc moyen que ça passe chez moi.

Rappel fourni par S. Abelard sur le slack des Artisans du logiciel

