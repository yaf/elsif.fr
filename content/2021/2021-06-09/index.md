---
title: Mercredi 9 juin 2021
---

> Bonjour Yannick,
> 
> On voulait vous remercier de votre réactivité sur l'ensemble du projet. Le client a été exigent et vous avez su répondre parfaitement.
> 
> Le projet touche à sa fin et nous ne rencontrons plus de soucis majeur grâce à vous.
> 
> Merci et au plaisir de retravailler avec vous,

Ça fait plaisir ce genre de message. C'est pas souvent, et c'est chouette. Merci à elleux !

