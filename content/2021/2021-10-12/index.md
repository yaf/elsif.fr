---
title: Mardi 12 octobre 2021
---

Je découvre [Paulette Nardal](https://fr.wikipedia.org/wiki/Paulette_Nardal). Il y a Aimé Césaire, mais avant, il y avait Paulette :)



- [La donnée en histoires](http://rudidatacitoyen.fr/) Intéressante approche.
- [Des modèles pour la construction d'avion en papier !](https://www.foldnfly.com/#/1-1-1-1-1-1-1-1-2)
- [Découvrir la vallée des Saints](https://www.carhaixpohertourisme.bzh/decouvrir/la-vallee-des-saints/)
- [CIAP 22 : la coopérative des paysans créatifs !](https://www.cae22.coop/ciap-la-cooperative-des-paysans-creatifs.html)
- [Réseau Rural Bretagne](https://www.reseaurural.fr/region/bretagne)

---

Les associations et autres structures lié au numérique dans le COB.

Rencontrer les petits débrouillard.
[LES PETITS DÉBROUILLARDS GRAND OUEST](http://www.lespetitsdebrouillards.org/?rub=ou&region=sgo)

Odile, équipe de Rostrenen, est la/une des formatrices de la formation longue en informatique
[Petit deb de Rostronen](https://www.lespetitsdebrouillardsgrandouest.org/qui-sommes-nous/petits-debrouillards-grand-ouest/equipe-rostrenen/)

[Ti Numerik Un espace de travail partagé et des salles de réunion à Rostrenen en Centre Bretagne](https://ti-numerik.bzh/)

[Parcours Tremplin numérique. Des places à pourvoir à Rostrenen](https://www.gref-bretagne.com/Actualites/Revue-de-presse/Parcours-Tremplin-numerique.-Des-places-a-pourvoir-a-Rostrenen)

