--- title: Mercredi 21 avril 2021 ---

Quelques lectures à propos de composant graphique.

[A Complete Guide To Accessible Front-End
Components](https://www.smashingmagazine.com/2021/03/complete-guide-accessible-front-end-components/)
[ ARIA Authoring Practices Guide (APG)
](https://www.w3.org/TR/wai-aria-practices/)

---

Deux adresses de minoterie pour acheter de la farine pas loin de la maison

- [Minoterie Dréan - Artisan meunier depuis
  1790](https://www.moulinderestaudran.fr/)
- [Moulin de Sebrevet](http://www.moulindesebrevet.com/)

---

> Il fut un temps où presque toutes les familles rurales britanniques qui
> élevaient des abeilles suivaient une étrange tradition.  Chaque fois qu'il y
> avait un décès dans la famille, quelqu'un devait se rendre aux ruches et
> annoncer aux abeilles la terrible perte qui avait frappé.
>
> Ne pas le faire entraînait souvent d'autres pertes :  les abeilles quittaient
> la ruche, ne produisaient pas assez de miel ou même mouraient.
>
> Traditionnellement, les abeilles étaient tenues au courant non seulement des
> décès, mais aussi de toutes les affaires familiales importantes: naissances,
> mariages et longues absences dues à des voyages. Si les abeilles n'étaient
> pas informées, on pensait que toutes sortes de calamités allaient se
> produire.
>
> Cette coutume particulière est connue comme "l'annonce aux abeilles".
>
> Cette pratique trouve peut-être son origine dans la mythologie celtique,
> selon laquelle les abeilles étaient le lien entre notre monde et le monde des
> esprits. Ainsi, si vous souhaitiez transmettre un message à un défunt, il
> vous suffisait de le dire aux abeilles et elles le transmettaient. La façon
> typique de le faire était que le chef de famille, ou la "bonne épouse de la
> maison", se rendait aux ruches, frappait doucement pour attirer l'attention
> des abeilles, puis murmurait doucement, tristement, la nouvelle solennelle.
>
> Au fil des siècles, selon les régions, des particularités se sont
> développées. Ainsi, dans les Midlands de l'est, les épouses des défunts
> chantaient tranquillement devant la ruche : "Le maître est mort, mais ne
> partez pas ; Votre maîtresse sera une bonne maîtresse pour vous."
>
> En Allemagne, un couplet similaire disait : "Petite abeille, notre maître est
> mort, ne me laisse pas dans ma détresse". C'est un fait que les abeilles
> nous aident à survivre.
>
> 70 des 100 principales espèces cultivées qui nourrissent 90 % de la
> population humaine dépendent des abeilles pour leur pollinisation. Sans
> elles, ces plantes cesseraient d'exister et avec elles tous les animaux qui
> se nourrissent de ces plantes. Cela pourrait avoir un effet en cascade qui se
> répercuterait de manière catastrophique sur la chaîne alimentaire.  La
> tradition de "l'annonce aux abeilles" souligne ce lien profond que les
> humains partagent avec l'insecte.
>
> Cristof Courcy

Recueilli sur [un poste facebook](https://www.facebook.com/groups/188955998580548/permalink/957466585062815/)

Voir aussi [Le folklore des abeilles](http://perso.numericable.fr/cf40/articles/4849/4849623A.htm)

---

> - Envoyer balader Google et son FLoC !
> - Arrêter d’utiliser Chrome et conseiller aux autres d’en faire autant, si ça
>   leur est possible.
> - Protégez-vous et montrez aux autres comment en faire autant
> - Découvrir et utiliser des solutions alternatives.
> - Passez le mot !
> - Choisissez un autre business model

[Développeurs, développeuses, nettoyez le Web !](https://framablog.org/2021/04/20/developpeurs-developpeuses-nettoyez-le-web/)

