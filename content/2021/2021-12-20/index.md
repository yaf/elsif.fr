---
title: Lundi 20 décembre 2021
---

Condamné à faire un logiciel à structure tech médiocre pour suivre le rythme des demandes d'utilisateurs ? Sans soin dans le code ?

J'ai l'impression qu'en ce moment nous nettoyons l'application pour aller dans le bon sens sur [RDV-Soldarites](https://www.rdv-solidarites.fr), mais que ça remue trop pour nos utilisateurs. Et je ne suis pas confortable avec ça.

Il me faut sans doute relire [le demi-cercle](https://blog.octo.com/le-demi-cercle-episode-1/)
