---
title: Mardi 15 juin 2021
---

> - Comment est né idée que l’accession au royaume de l’abstraction impliquait que l'on sacrifia toutes connivence avec la matière ? Platon ?
>
> - Oui, il y a l'idée que le monde des idées l'emporte sur le monde de la matière.

> - Vous dites que vous aviez besoin de faire pour comprendre. Apprendre ça ne suffit pas pour comprendre ?
>
> - Dans mon apprentissage de la vie c'était de, en premier lieu, démonter tout ce qu'il y avait à la maison pour aller au cœur du fonctionnement.

On parle encore de Spinoza... Toujours très envie d'explorer ce philosophe. Et en même temps, j'ai peur de m'y attaquer, de ne pas comprendre.

> Tous savoir qui n'est pas transmis et un savoir volé 

[La réparation dans tous ses états](https://www.franceculture.fr/emissions/la-conversation-scientifique/la-reparation-dans-tous-ses-etats)
