---
title: Samedi 3 avril 2021
---

Pollution lumineuse..

> nous sommes des animaux diurne, mais nous avons fait le choix de vivre la nuit et donc de produit notre propre lumière
>
> -- « MOOC TVB Séquence 6 - Sujet 1 : La Trame noire : un réseau écologique pour la vie la nuit »

_Entendu lors du [mooc sur la trame verte et bleue](http://www.trameverteetbleue.fr/mooc-tvb)_
