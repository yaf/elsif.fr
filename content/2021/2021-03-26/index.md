---
title: Vendredi 26 mars 2021
---

Quelque part sur un slack, `ToF_` :

> En fait je pense qu'il n'existe pas de rupture entre la compétence "coder" et la compétence "écrire". Mais on travaille généralement dans un contexte où la fonctionnalité livrée maintenant (ou bientôt) prend quasiment tout le focus, et la facilité de changer la fonctionnalité plus tard est quasi absente des préoccupations.

Tellement intéressant... Est-ce que nous avons une approche différente lorsque l'on écrit un roman ou un texte à publier et un écrit sur un wiki ?
