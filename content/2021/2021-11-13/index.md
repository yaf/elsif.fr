---
title: Samedi 13 novembre
---

Enfin commandé deux pneus sur [XXCycle](https://www.xxcycle.fr/). Des pneus marathon plus comme conseillé par Tristram et tant d'autres sur Betagouv [Schwalbe marathon plus](https://www.schwalbe.com/fr/tour-reader/marathon-plus)

J'espère que j'arriverais à les monter sur mon vélo.

Ça me fait penser que je n'ai pas vraiment d'atelier de réparation de vélo ici. Le mieux sera peut-être dans le garage. En même temps, ça serait super d'avoir un abri devant la maison pour y laisser les vélos et les outils ! J'y penserais si on achète quelque chose un jour.


---

À force d'observer ce jardin, je commence à entrevoir ce que j'aimerais faire sans trop le modifier. Nous sommes en location, je n'ose pas tout chambouler. Dans le contrat de baille est écrit clairement que nous devons entretenir le gazon...

J'ai malgré tout quelques plan en tête. Une demande pour le potager (avec deux options) et un possible poulailler. Il y a aussi des arbres et arbustes à bouturer. J'envisage aussi de planter des noisetiers. Issu de certains rejet que j'ai repéré, et je me demande si je ne vais pas aussi en acheter dans le commerce, pour voir.

Il y a des rejets et pousse à déplacer, supprimer.

J'ai repéré deux zone où je pourrais mettre des fruitiers. Je vais sans doute aller jeter un coup d'œil aux pépinières du coin.
- [Jardin Simorin](https://www.facebook.com/jardineriesimorin56/) (fleuriste, pas sur qu'il y ait des arbres)
- [pépinière Ker-Anna](http://www.pepinieres-keranna.fr/index.html)
- il y a aussi un [point vert](https://www.magasin-point-vert.fr/magasin/faouet) ...

Je crois que Ker Anna va avoir ma préférence, mais un petit coup d'œil aux autres magasins du coin sera sans doute enrichissant.

Plus loin
- [Vivaces Bretagne](http://vivaces-bretagne.fr/)
- [Roman Cotten](https://www.facebook.com/PepinieresRomanCotten)
- [DeportdeCarhaix](https://pepinieresdeportdecarhaix.fr/)
- [Kefandol](http://www.pepinieresdekerfandol.com/)
- [Du Belon](http://www.pepinieresdubelon.fr/)
- [Talascorn](https://www.pepinieres-talascorn.fr/)
- [Des fruits des fleurs](http://desfruitsdesfleurs.fr/)
- [Brot Élodie](https://www.pepinieres-brot-elodie.fr/)
- [Judicarre](https://www.pepinieresdejudicarre.com/)

---

Il y a une formation sur le chemin de Carhaix... 
J'apprécie le concept de micro-promo

> Des micro promotions
>
> 1 formateur pour 8 stagiaires.
> Tutorat au contact de professionnels dans une entreprise polyvalente. Recherche de stage coachée individuellement

Je suis moins fan du travail « gratuit ». Je trouve ça délicat. Je comprend l'intérêt, ceci étant, ça me semble une pente un peu savonneuse.

> Une ouverture professionnelle
>
> Conception et réalisation de projets commandés par diverses entreprises (artisants, TPE, PME...). Nos stagiaires bénéficient de 6 mois d’expérience professionnelle

> Coût de la formation : 6400 euros
>
> Restauration et hébergement à prix étudiant

J'imagine (j'espère) que Pôle Emploi prend en charge une parti des frais de formation. Je n'arrive pas à m'imaginer demander ça... C'est sans dout e pour cette raison que je n'ai jamais monté ce type de formation :)

[Callac Soft College](https://www.callac-soft-college.fr/)


---

[GR491, Le guide de Référence de Conception Responsable de Services Numériques](https://gr491.isit-europe.org/)

---

À lire ? Féminisme pour les 99 %

« La transformation de l’Afrique en une sorte de garenne commerciale pour la chasse aux peaux noires, voilà un autre procédé idyllique de l’ère capitaliste à son aurore. »

« Le droit au développement des peuples suppose l’exercice du droit inaliénable à la pleine souveraineté sur toutes leurs richesses et leurs ressources naturelles »

« Les biens communs vont de la propriété collective des terres jusqu’aux services publics qui constituent des conquêtes sociales »

« Dans l’espace des biens communs, les relations marchandes sont soit exclues, soit réduites au minimum. L’activité de reproduction sociale est venue également au centre des préoccupations sur les biens communs grâce à l’action des mouvements féministes. »

« La dette publique a été et est utilisée par le système capitaliste pour s’attaquer aux biens communs »

[La destruction et l’accaparement des biens communs](https://www.contretemps.eu/destruction-privatisation-biens-communs-capitalisme/)

