---
title: Vendredi 31 décembre 2021
---

> Tout code est, ou deviendra assez tôt, du code legacy. Ce qui signifie que tout logiciel en production subit un désalignement entre les objectifs, le contexte et les méthodes initialement définis en vue de produire la solution qui est en place aujourd’hui. 


> Lorsque vous cherchez à réduire le coût des tests, vous devez réfléchir au coût de ne pas écrire assez de tests.

> C’est pour cela que l’on écrit des tests : non pas parce que nous ne savons pas coder, mais parce que nous sommes des êtres humains ordinaires et non des « Ninjas » ou des « cowboys », et que nous comprenons les risques, même confusément. Dans ce contexte l’écriture de test autovérifiants constitue une stratégie de prévention des défauts.

[ Défense et illustration des tests isolés – #1 ](https://blog.octo.com/defense-et-illustration-des-tests-isoles-1/)
