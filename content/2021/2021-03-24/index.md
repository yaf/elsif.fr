---
title: Mercredi 24 mars 2021
---

Une super démarche à Lorient

[Lorient. Un groupement « d’employeurs des transitions » pour mutualiser les salariés](https://www.ouest-france.fr/bretagne/lorient-56100/lorient-un-groupement-d-employeurs-des-transitions-pour-mutualiser-les-salaries-e30a077c-88c3-11eb-b072-a1ccecc16b0f)

Un groupement d'employeurs. La legislation a du bouger, j'avais en tête les GIE. Alors soit c'est autre chose, soit ça a changé de nom.

> On compte deux associations, deux SAS, et une société publique locale : Aloen, Book Hémisphères, OnCIMè, Optim’ism, et la SPL-BER. Leur point commun : « participer aux transitions énergétiques, écologiques, alimentaires, et avec une volonté de coopération »

> Parfois c’est dur d’employer une personne à temps complet quand on démarre d’assez bas. Avec ce système, quand l’un des employeurs a besoin de trois compétences, il n’est pas obligé d’employer trois personnes à temps plein

Je trouve une référence au GIE : [Le groupement d’intérêt économique (GIE), une structure pour la coopération économique](https://www.economie.gouv.fr/entreprises/groupement-interet-economique-gie). Pour voir quelque chose qui parle des deux status, je tombe sur un site non gouvernemental (confiance ou pas ?) [Réseaux d’entreprises : quelle forme juridique choisir ? Groupement d’Intérêt Economique (GIE)](http://www.placedesreseaux.com/Dossiers/creer-reseau/formes-juridiques2.htm)

---

> Que concevoir des logiciels puissent être un plaisir, c’est un fait. Mais concevoir des logiciels, particulièrement si l’on souhaite qu’ils participent à une transformation sociale et donc qu’ils soient utilisables par le plus grand nombre, nécessite beaucoup de travail et surtout un large éventail de compétences. Cela nécessite un important travail d’équipe, dont des tâches souvent peu visibles, comme l’organisation de réunions, l’animation du groupe, la gestion de conflits, le secrétariat… ou peu valorisées, telles que la recherche d’utilisabilité, la communication, ou encore la traduction. Ce qu’Ashley Williams, actuellement à la tête de la Rust Foundation, pointe comme une liberté implicite mais fondamentale du Libre : la liberté de coopérer… dont le coût a trop souvent été glissé sous le tapis.

> De nombreuses personnes qui promeuvent le Libre travaillent dans l’informatique, ce qui va souvent avec une position économique privilégiée.

> Il (Martin Owens) a cherché à résoudre ce problème (celui des entreprises qui finance et décide des fonctionnalités à développer dans le libre, plutôt que de prendre en compte TOUTES les personnes utilisant le dit logiciel) en sortant du modèle qui consiste à rendre des services, mais en voulant plutôt mettre en place une « création dirigée par les utilisateur·ices ». Son idée ainsi est de sortir de l’opposition traditionnelle entre business et bénévolat, et de trouver un compromis plus équitable à la fois pour les utilisateur⋅ices et les développeur⋅euses.
> 
> Il alerte néanmoins sur les risques qu’il est nécessaire de prendre en compte avec un tel modèle : négliger les utilisateur·ices qui n’auraient pas les capacités de contribuer financièrement, se retrouver en compétition entre contributeur·ices, vouloir garder tout le contrôle sur un projet pour garantir ses revenus.

[La dérivation - Du pouvoir économique sur le Libre](https://dérivation.fr/du-pouvoir-economique-sur-le-libre/)

---

[Github/yaf](https://github.com/yaf/), [Gitlab/yaf](https://gitlab.com/yaf), [Framagit/yaf](https://framagit.org/yaf), [git.scopyleft.fr](https://git.scopyleft.fr/)... Où poser mes projets ? Où poser mes bouts de code ?

Github permet sans doute une meilleure découvrabilité. Et permettra aux personnes qui arrive dans ce milieu de découvrir plus facilement ce que j'ai écrit. Est-ce que c'est une bonne raison pour mettre mes projets sur github ? Utiliser github au commencement est sans doute un aspect « incontournable » (aujourd'hui au moins) dans le monde du développement. Et je reste persuadé que le plus important est d'apprendre à utiliser Git. Peut-être faut-il éviter d'utiliser le client Github du coup.

Mais moi ? Quel intérêt aurais-je à ça ? Attirer des contributions ? Je ne suis pas sur de savoir gérer cela, en tout cas pas sur les bouts de projets expérimentaux que je mène.

L'utilisation d'un repo pour Scopyleft aurait du sens si nous y partagions notre code commun. Or, nous sommes sur Gitlab, et potentiellement, ça sera peut-être mieux de retourner sur Github, pour faciliter l'accès à nos informations à des personnes qui ne programme pas forcement. Est-ce bien raisonnable de maintenir cette instance de Gitea ?

Github, Gitlab, Framagit ? Et pourquoi pas l'instance de Gitea de l'April ? Quelle différence ça ferait avec Framagit ?

Je trouve que ce n'est pas si simple dans le cadre d'une forge. C'est sans doute lié au fait que nos systèmes de gestion de version d'aujourd'hui sont décentralisé. Alors à quoi sert ce système central ? C'est sans doute pourquoi il est difficile de trouver un argument, un critère qui permette d'en choisir un plutôt qu'un autre. Pour de vrai, nous n'en avons pas besoin...

Le modèle de partage, de travail serait sans doute à revoir. Il y aurait à documenter comment partager du code à partir de sa machine, au travers d'une box. Comment ouvrir les bon ports ? Comment organiser ses repos pour les partagers facilement depuis sa machine (Git web ou bien un autre outil ?).

Là dessus, j'avais trouvé le `hg serve` vraiment très simple.

Peut-être que publier le readme de chacun de mes projets sur mon site, avec un lien pour m'envoyer une demande par mail pourrait faire le job.

Est-ce qu'un code sous licence libre doit être disponible sur une plateforme centralisé ?

Une autre option c'est de maintenir une copie sur chacune des instances qu'on apprécie : Github ET Gitlab ET Framagit ET git.scopyleft.fr ET forge.chapril.org ...

Bien sur, il y a la gestion des issues, souvent déporté dans l'outil qui héberge le repo aussi.

Comment rendre accessible, simplement, à des personnes non dev, ce genre de chose ? Faut-il former tout le monde à Git ?

Beaucoup de questions, peut de réponse.
