---
title: Dimanche 6 juin 2021
---

[Ethical Alternatives & Resources](https://ethical.net/resources)

---

[Appliquer une pommade à partir d'huile de cire et de jaune](https://cg.torontogesneriadsociety.org/389-apply-ointment-from-wax-oil-and-yolk.html)

---

[Adullact](https://adullact.org/)

Adhérer à l'ADULLACT ? Me mettre en relation avec une collectivité histoire de me faire recruter là bas pour travailler sur du logiciel ?

Je découvre [Mégalis Bretagne](https://www.megalis.bretagne.bzh/).

> Le Syndicat mixte Mégalis Bretagne a été créé en 1999 et a pour compétences :
>
> - D’assurer, en lieu et place de ses membres, la construction et l’exploitation du réseau public régional en fibre optique dans le cadre du projet « Bretagne Très Haut Débit » ;
> - De favoriser le développement de l’administration électronique en proposant une offre de services numériques mutualisés et de fournitures annexes associées.


Rejoindre une entreprise du [réseau libre entreprise](https://www.libre-entreprise.org/) ? Beaucoup semble orienté vers ce type de travaux. Comment les rejoindre ? En dehors d'[Azaé](https://azae.net/), je ne connais aucune personne de ces entreprises ? Pourquoi je ne suis pas dans ces réseaux ? Je suis sans doute plus dans celui de l'agilité que dans celui du libre...

J'avais déjà remarqué [Coopérative Cliss XXI](https://www.cliss21.com/site/), et je découvre [IggDrasil Informatique libre en géomatique et archéologie](https://www.iggdrasil.net/) sur Paimpont ! Peut-être [LDD - Les Développements Durables Service en logiciel libre](https://www.ldd.fr/) ? [Nereide - intégrateur ERP Libre Apache OFBiz](https://www.nereide.fr/) semble intéressante aussi.

Et pourquoi [EnRoute](https://enroute.mobi/) n'est pas adhérente de ce réseau tiens ? Je devrais peut-être aussi appeler Alban...
