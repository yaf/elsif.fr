---
title: Vendredi 1er octobre 2021
---

[ Des FORMATIONS PROFESSIONNELLES conçues pour un monde en mouvement ](https://formations.coop/)

---

Réunion des parents d'élèves à l'école élémentaire. Je découvre l'[Usep](https://usep.org/). C'est plutôt chouette cette école. Je me fait mon petit trou dans cette « campagne ». Je me sent bien. Il reste des tas de choses à explorer et faire, des « solutions » à trouver. Mais ça me plait.

Je crois que le dynamisme associatif et coopératif Breton aide beaucoup. Je n'aurais sans doute pas autant apprécié en allant à Miribel, dans la campagne de Grenoble.


