---
title: Vendredi 9 avril 2021
---

[Atmos vert](https://www.atmosvert.fr/)

Faire une petite pépinière ? Il y a plus de demande que d'offre, c'est sans doute une bonne idée. Il semble qu'un pépinière de 200 m2 soit suffisante.


[vidéo](https://www.youtube.com/watch?v=_eVTTamNhZM)


---

[David réagit](https://eldritch.cafe/@davidbruant/106036371844448932) à [un message de David](https://larlet.fr/david/2021/03/09/).

> > C’est une des choses qui me titille avec les entretiens utilisateur·ices, les approches LEAN, etc. On obtient des produits plus pertinents, plus rapidement mais qu’en est-il de la partie fun ?
>
>
> J'ai cet article en tête depuis des semaines
> Running lean, c'est l'outil pertinent pour résoudre un problème de manière optimale en temps/argent
>
> Et l'existence n'est pas réduite à une séquence de résolutions de problèmes (même s'il y en a pleins à résoudre)

J'ai l'impression que c'est surtout que la résolutions de problème peu être fun si on s'autorise à :
- supprimer le problème à la racine quand c'est possible. Ça entraine souvent plus de liberté et de fun.
- résoudre de manière simple, la plus simple possible, en général, sans mettre du numérique dedans. J'ai l'impression que ça permet de recréer du lien social, et donc des rencontres agréables, du fun quoi ?

Mais j'ai peut-être pas la même définition du fun que tout le monde :thinking:

---

À lire ? [Évaluer pour apprendre](https://librairie.cahiers-pedagogiques.com/fr/revue/841-l-evaluation-pour-apprendre.html)
