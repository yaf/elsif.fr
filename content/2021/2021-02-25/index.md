---
title: Jeudi 25 février 2021
---

Réponse de Tristram à propos d'une de mes questions sur ce qu'est la Géomatique et par où commencer pour apprendre.

> quand y’a indiqué « géomatique », probablement  ils s’attendent à une connaissance des outils « tradi ».
> Je dirais qu’un début pourrait être de prendre qGIS, des données en opendata et essayer de faire une carte
> Essaye de prendre des données au format shapefile (.shp) pour commencer, c’est le format « legacy ».

Je pense que ça va générer tout plein de questions plus concrètes :wink:

Plein de données là aussi [naturalearthdata.com](https://www.naturalearthdata.com/) qui pourraient t’inspirer (par exemple reproduire une carte scolaire)
