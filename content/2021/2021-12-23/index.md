---
title: Jeudi 23 décembre 2021
---


Note de lecture de [L'avenir de l'eau - Petit précis de mondialisation II](https://www.livredepoche.com/livre/lavenir-de-leau-9782253129936) de Érik Orsenna

> - Je suis communiste
>
> J'avais oublié que Calcutta était depuis longtemps une municipalité rouge. Amitav ne décolérait pas. Le maire venait de décider une révolution. Désormais l'eau serait payante. Seuls les habitantes des taudis en seraient exemptés.
>
> L'eau, c'est la vie. Comment peut-on faire payer la vie ?

> Réchauffement global ou crises régionales
>
> Première erreur, généralement commise : l'eau n'est pas le pétrole. L'eau est une ressource _renouvelable_. Pour comprendre l'eau, il ne faut pas comprendre _gisement_, mais _cycle_. Le risque n'est donc pas que s'épuisent des gisements, mais que se dérèglent des cycles. La déforestation, par exemple, dérègle u cycle et réduit les pluies.
>
> Deuxième erreur, elle aussi généralement commise : le réchauffement global ne va pas diminuer la quantité d'eau disponible, mais l'_accroitre_. L'intensification de l'effet de serre va augmenter le rayonnement solaire à la surface du globe. En conséquence, l'évaporation aura tendance à s'amplifier. Plus d'humidité dans l'air se traduit par davantage de précipitations.
>
> Troisième et quatrième erreurs : croire à la régularité, croire à l'égalité. Par sens moral tout autant que par paresse intellectuelle, on voudrait penser que ce surcroît de précipitations se répartira régulièrement tout au long de l'année, équitablement sur tout la planète.
>
> Des mécanismes complexes, qui impliquent le jeu des courants d'air dans l'atmosphère, font qu'il n'en sera rien. La violence et l'injustice triompheront. Des canicules alterneront avec des déluges. Les régions déjà bien arrosées seront inondées. Les zones arides recevront moins encore.
>
> Un point de vue global ne raconte rien d'utile. Pour servir à quelque chose, toute analyse doit se référer à des réalités _locales_. D'un bout à l'autre de la planète, les saisons, par exemple, ne se ressemblent pas.

> - Quand tu dois faire face à l'urgence, tu oublies le long terme. En plus, beaucoup de ces paysans louent une terre pour une courte période, parfois une seule année. Alors le respect de la ressource... À propos, tu sais pourquoi le gouvernement subventionne le butane ?
> Une fois de plus, je donnai ma langue au chat.
> - Mais, tout en préservant la forêt, on assèche la nappe...
> - C'est la vie !


> Notre atmosphère, ont calculé les scientifiques, contient 12 900 kilomètres cubes d'eau douce : 98 % d'eau à l'état de vapeur et 2 % d'eau condensée (nuages). Cette masse impressionnante est comparable à celle des ressources en eau, liquide et renouvelable, des terres habitées : 12 500 kilomètres cubes.
>
> La conclusion s'impose : si l'on parvenait à extraire l'eau et l'air qui nous entoure, plus personne sur terre ne souffrirait de la soif.

> Pourquoi les Sénégalais manquent-ils de riz ?
>
> Le riz a déferlé sur l'Afrique noire par décision coloniale. Dans les années 1920, la République française voulait mettre de l'ordre dans son empire. La Grande Guerre avait saigné ses finances. Il lui fallait accroître la rentabilité de ses terres lointaines. D'où l'idée de les spécialiser, d'assigner à chacune un rôle précis.
> C'est ainsi que revint au Sénégal la tâche de produire de l'arachide, beaucoup d'arachide. Au détriment des cultures vivrières, mil ou sorgho. Les Sénégalais voulaient se nourrir ? Pas de problème : on leur livrerait des brisures de riz venues d'une autre possession lointaine, l'Indochine. On ne leur avait pas dit, bien sûr, que là-bas elles étaient réservées aux animaux. (...)
> Ces brisures bouleversèrent les habitudes alimentaires locales. On s'accoutuma aux brisures. Mieux, on les aima. Ce produit lointain pouvait passer pour du luxe.

> Plaignons ces malheureux. Un jour ils ont cru médicalement fondé le slogan publicitaire des compagnies qui produisent les fameuses petites bouteilles (d'eau minéral, en plastique) : « Buvez au minimum un litre et demi d'eau par jour. » Et voilà, le mal était fait.
>
> Urgemment, j'ai consulté l'un de nos meilleurs urologues, le professeur Guy Vallancien. Plus angoissé par cette affaire que par l'état de ma prostate, je lui ai demandé si je devais rejoindre au plus vite la secte des petites bouteilles.
>
> - Billevesées ! M'a-t-il répondu dans un grand éclat de rire. Malignité du marketing : Aucun excès d'eau ne rendra les peaux plus douces, le teint plus lumineux, les cuisses plus fermes, le vendre plus plat, les infections urinaires moins fréquentes... La seule règle qui vaille est simple : il faut boire quand on a soif.

_Après quelques paragraphe où il est évoqué que le compteur d'eau est le meilleur ami de l'homme. Qu'il nous rappel que l'eau est rare, qu'elle arrive bien chez nous, que nous y avons accès en ouvrant un robinet, c'est un témoin impartial des pratiques du logement qu'il déserre._
> Le compteur n'est pas responsable de la tarification. Il se contente d'afficher un volume, dont le prix sera déterminé par des autorités bien plus hautes que lui. Que ces autorités décident d'établir la gratuité ou d'accabler le consommateur, le pauvre compteur n'y peut rien.

> L'un des grands rêves du libéralisme pur et dur était de _dépolitiser_ aussi l'eau, de considérer l'eau comme une marchandise semblable aux autres, qu'on peut produire et distribuer en ne se souciant que d'efficacité. Quelle que soit l'issue du contentieux argentin (histoire entre Suez et l'état argentin en 2006), la preuve est faite que ce rêve est imbécile et ne sera jamais réalisé. L'eau, de par sa double nature, essentielle à la vie et fortement symbolique, est _toujours_ politique. Quelles que soient les options choisies par les gouvernants, il gardent toujours sur l'eau la haute main.

> Le mouvement des porteurs d'eau, où se rejoignent ONG et particulier, soutient des dizaines de projets qui doivent respecter sa charte : 
>
> 1. L'eau n'est pas une marchandise, l'eau est un bien commun non seulement pour l'Humanité, mais aussi pour le Vivant.
>
> 2. Afin de garantir la ressource pour les générations futures, nous avons le devoir  de restituer l'eau à la nature dans sa pureté d'origine.
>
> 3. L'accès à l'eau est un droit humain fondamental qui ne peut être garanti que par une gestion publique, démocratique et transparente, inscrite dans la loi.

> Pour lui (Bernard Barraqué), l'eau n'est pas un bien gratuite (puisque sa distribution implique toujours un coût). Ce n'est pas non plus un bien public (pourquoi appartiendrait-il à l'état ?). C'est un _bien commun_. Pour gérer ce système si particulier, trois systèmes ont été employés au fil des temps : 
>
> 1. le despotisme oriental : les paysans fournissent de la nourriture, l'État s'occupe de l'infrastructure hydraulique (Égypte, Babylone, Amérique centrale, Chine).
> 2. Le droit romain : les eaux courantes sont un bien commun, sauf si elles sont navigables. Auquel cas c'est l'État qui s'en occupe. Les eaux du sous-sol restent privées. Ce système a été appliqué par les pays latins dont, jusqu'à une date récente, la France.
> 3. la tradition germanique : aucune eau n'est appropriable. C'est cette dernière conception qui, la ressource devenant de plus en plus rare, s'impose un peu partout.

Et encore des ver de terre, utilisé dans un système de lombristation d'épuration. Qu'elles insectes formidable !

Un court chapitre à propos de la Bretagne, dans lequel je trouve des éléments me motivant à rester installer à la campagne, à reprendre une maison existante plutôt que de construire...
> Outre les côtes, les néo-Bretons choisissent les villes importantes. Lesquelles se sont bâties jadis sur les terres les plus fertiles. Si bien qu'on bétonne et couvre de macadam en priorité les meilleurs champs.

Je suis content de m'engager avec Terre de liens.

> Huit convictions
>
> 1. _Au commencement de toute humanité est l'eau_. Au commencement de toute dignité, de toute santé, de toute éducation, de tout développement. Dans l'ordre des priorités, rien de précède l'accès à l'eau. (...)
> 2. L'eau vient de la nature. _Préserver le milieu naturel est donc la meilleure manière de garantir la ressource_. (...)
> 3. Toute l'eau est liée à des _lieux_. Car l'eau est inégalement réparti sur notre planète. (...)
> 4. Étant donnée sa double importance, réelle et symbolique, l'eau, source de vie, relève toujours d'une _responsabilité politique_. (...)
> 5. _Deux préférences sont fort dommageable_. (...) de préférer le visible à l'invisible, les solutions qui se voient à des stratégies d'_économies et de recyclage_ (...). De préférer l'eau à la merde. En d'autre termes, de dédaigner l'assainissement.
> 6. _À l'illusion de la gratuité, préférons l'obligation de solidarités_. (...)
> 7. _Changeons la logique de tarification_. Dans la logique actuelle, plus les opérateurs, publics ou privés, distribuent d'eau, plus ils gagnent de l'argent. (...)
> 8. (...) Où allons-nous développer l'agriculture capable de nourrir 9 milliards d'être humains ? (...)

