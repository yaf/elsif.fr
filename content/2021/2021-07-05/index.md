---
title: Lundi 5 juillet
---

> There are a few central principles I use to guide my maintainership work:
>
> - Everyone is a volunteer and should be treated as such.
> - One patch is worth a thousand bug reports.
> - Empower people to do what they enjoy and are good at.

[ How I maintain FOSS projects ](https://drewdevault.com/2018/06/01/How-I-maintain-FOSS-projects.html)

