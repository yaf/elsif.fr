---
title: Samedi 20 novembre 2021
---

J'aimerais bien rencontrer [Yohann Boniface](https://yohanboniface.me). Beaucoup de signaux m'indique que ça pourrait être assez sympathique. L'aspect multi activité me plait bien, et j'apprend qu'il boulange (ou a boulangé) dans une ferme TDL https://terredeliens.org/toussacq, https://www.leschampsdespossibles.fr/la-ferme-collective-de-toussacq/ lié à la SCIC le champs des possibles \o/ Ça cumul :)

---


1 000 € d'aide pour un cargo électrique... [Mes Aides Vélo pour le Faouët](https://mesaidesvelo.fr/ville/le-faouet)

Il serait peut-être temps que je m'y intéresse ? Est-ce que ces aides fonctionne sur des achats d'occasion ?

---

À lire ? [Piaget, « On fabrique, on vend, on se paie. Lip, 1973 »](https://www.unioncommunistelibertaire.org/?Lire-Piaget-On-fabrique-on-vend-on-se-paie-Lip-1973)

_Attention, c'est pas le pédagogue psychologue Jean Piaget, mais Charles Piaget un syndicaliste autogestionnaire https://fr.wikipedia.org/wiki/Charles_Piaget_

---

> Actuellement on est donc dans l’obligation de déchiffrer les données pour faire fonctionner n’importe quel algorithme ou programme informatique. A notre sens, il est dangereux de déchiffrer des données de santé sur une plateforme américaine (Microsoft ainsi que les autres GAFA) ou chinoise (BATX).

Très bonne intervention (encore) du collectif InterHop. Avec des précisions techniques pertinentes.

[Les données de santé sont-elles un bien commun ?](https://interhop.org/2021/11/03/les-donnees-de-sante-sont-elles-un-bien-commun)

---

[manuskript](https://www.theologeek.ch/manuskript/) 

Open-source tool for writers

