---
title: Dimanche 14 février 2021
---

Note de lecture du livre « Assainir et fertiliser » de Bernard Fontvieille.

C'est à propos de l'assainissement. L'auteur décrit dans ce livre une alternative aux toilettes sèches et toilettes « standard » reliées au tout à l'égout. L'idée est d'utiliser de l'humus et des vers pour désagréger rapidement, sans odeur, et en relativement peu de temps les urines et déjections humaines. Un bonne partir de l'étude et des expérimentations ont lieu à Madagascar et autour de Montpellier.

Il y a tout une introduction intéressante évoquant le contexte de la réflexion sur cette alternative. L'auteur y évoque, avec beaucoup de citations à l'appuie, les enjeux politiques, l'accès à l'eau, l'industrialisation du monde de l'assainissement.

Il est également rappelé beaucoup d'information sur les vers de terre.

> L'odeur apparaît quand on plonge les excréments dans l'eau. S'ils ne sont pas mouillés ou à peine mouillés, ils n'ont pas beaucoup d'odeur, n'attirent pas les mouches et si on leur enlève l'eau qu'ils contiennent, ils ne sentent plus du tout.

> Le traitement des eaux usées contre les micropoluants est à améliorer. 50 % des produits potentiellement nocifs restent présents à la sortie des stations dépuration.
> _citation issu du journal Le Monde de janvier 2010, s'appuyant sur une étude du Cemagref, de Suez-environnement et de l'université de Bordeaux._


L'auteur prone donc un retour _à la nature_. Une base de biomimétisme, en essayant d'adapter le processus aux modes de vie humain.


_À propos de la sélection de vers efficace pour désagréger la matière fécale_

> Formation d'un tas végétal
> 1. Récolter la masse végétale dans l'environnement immédiat (déchets de taille de haies, herbe du désherbage sans graines, ...)
> 2. Choisir un endroit à l'ombre, près d'un point d'eau pour établir le tas
> 3. Un broyeur ou un sécateur permettent de couper la végétation afin de faciliter la fermentation
> 4. Tremper la végétation dans l'eau
> 5. on obtient un tas de broyat humide
>
> Sélection des vers _Eisenia foetida_
> 1. Trouver des déjections animales et les faire tremper dans l'eau. Araser le tas de broyat végétal
> 2. Déposer une couche de déjections animales mouillée. Recommencer l'opération avec plusieurs couches séparées par le broyat végétal
> 3. Veiller à une bonne oxygénation du tas (introduire des branches dans la masse végétale)
> 4. Le tas s'échauffe, les végétaux noircissent, c'est l'humidification
> 5. Le tas se compacte. Quand la température baisse, la faune épigée envahit le tas. Prélever les lombrics sélectionnés.
>
> On obtient le même résultat avec du fumier de bovins ou d'autres animaux domestiques.

Ces points sont illustrés, dans le livre, avec de beaux dessins. La récupération des vers épigées me rappel l'atelier d'observation des vers de terre auquel j'ai participé. Pour récupérer les vers, utiliser un récipient avec des bords assez haut, en mettant de l'eau au fond. Les vers tolère assez bien l'eau (tant que ça ne dur pas trop longtemps). L'eau les gênera pour sortir.

> Quand on install ce cube (contenant humus, bactéries, lombrics et qui recevra déjections et urines), on doit tenir compte du parcours possible de l'urine dans l'humus, il doit être suffisamment long pour que les bactéries aient le temps de se mettre au travail. On doit aussi tenir compte du nombre de lombrics et du volume des déjections attendu. Pour un assainissement optimal, il est nécessaire de densifier les lombrics au fur et à mesure (environ deux milles par personne) et de tenir compte du volume d'humus potentiel (cent trente litres par personne et par an).

> Ce cube reçoit les déjections poussées par l'eau d'une chasse. Par un autre tuyau on envoie de l'humus pour recouvrir les déjections dès leur arrivée. Le rejet de ce cube où se trouvent de l'humus et des lombrics sélectionnés passe dans un tuyau où circulent toutes les autres eaux qui sortent de la maison.
> Ces liquides sont répartis dans un bassin où sont installées des plantes phyto-épuratices (salicaires, _Phragmite australis_, typha, _Iris pseudo-acorus_) dans de l'argile de Bédarieux, des galets et des sables de rivière. L'étanchéité du bassin est assurée par une bâche imperméable.

> En matière collective, on peut installer des système lombrics par quartier puisque à la différence des stations actuelles, ils ne génèrent  aucune odeur. Dans ce cas, les coûts sont très différents des coûts actuels. Pour les stations d'épuration collectives : 9 000 à 11 000 euros par habitation, contre 150 à 200 euros pour les lombrics.

> Le point essentiel, la certitude intangible, est que l'optimum de l'assainissement ne pourrait avoir lieu qu'avec des eaux vannes transportant des matières fécales, des urines et du papier.
>
> Si cette condition n'est pas remplie, ce qui est le cas presque partout depuis le début du XXe siècle, la séparation des réseaux devient une opération nécessaire.

> Ce développement se situe surtout à l'opposé du programme des Nation unies pour le développement (PNUD). Ce dernier reconnaît que la pertinence du tout-à-l'égout, c'est-à-dire du mélange des effluents domestique, est à réévaluer. Les excréments humains ne peuvent durablement être gérés comme des déchets, évacués par de l'eau.

> Ces analyses (sur le trio N azote, P phosphore sous forme de phosphate, K potasse sous forme de potassium), auxquelles s'ajoute l'attestation du docteur Basset, introduisent une évidence : les humains grâce aux lombrics et aux bactéries sont à l'origine d'une usine d'engrais sain.


> Souvenons-nous qu'en France, 80 000 tonnes de boues sont rejetées chaque jour et qu'elles restent impropres à toute véritable utilisation.
> Claude et Lydia Bourguignon disent la même chose :
> Voilà un siècle et demi, avec l'invention du tout-à-l'égout, l'humanité a rompu ce cycle. Les ingénieurs qui fabriquent les égouts de nos villes ne sont pas des agronomes et ignorent tout de ces cycles du vivant. Il est urgent qu'ils mettent en place une division ds égouts en trois circuits : un pour l'eau de pluie qui pourrait regagner les rivières, un pour les eaux toxiques des usines, dont il faut isoler les déchets de l'environnement, et un pour les déchets organiques des ménages qui doivent retourner, après compostage, sur nos sols agricoles.

Il y a des descriptions d'expérimentations, d'erreur et d'apprentissage.


---

Parfois, j'ai envie de me mettre à faire du C.

- [Tout ce que j'aurais aimé qu'on me dise la première fois que j'ai appris le C](https://ybad.name/log/2021-01-20.html)
- [Coding Style de suckless.org](http://suckless.org/coding_style/)
- [Coding Style OpenBSD](http://man.openbsd.org/style)
- [RosettaCode](http://rosettacode.org/wiki/Rosetta_Code)

C'est souvent couplé à l'envie de revenir sur OpenBSD. J'associe tout ça à une envie plus méta : revenir à des choses simple. Est-ce que c'est l'âge ou bien une forme de lassitude de toutes ces couches qui s'ajoute dans notre univers logiciel ? Est-ce que mon « mentor » (aujourd'hui à la retraite) ne m'avait pas répondu des choses dans le genre quand je me suis aventuré sur Java, Ruby et autre à l'époque ?

J'ajoute [C for Python Programmers](https://realpython.com/c-for-python-programmers/)

---

> What is awk useful for?
>
> Awk is useful for data file manipulation. Already, having used it for a few days only, I wish I had invested time in learning it earlier.

Si Awk est un langage de programmation, c'est quoi ça « fiche » ?
- paradigme : procédurale
- typage : dynamique
- execution : interprété
- documentation : man page et [The GNU Awk User’s Guide](https://www.gnu.org/software/gawk/manual/gawk.html)
- gestion de librairie : est-ce qu'il y en a ?
- historique & communauté....

> Well, what is an awk program? We know it is best used for simple data reformatting or manipulation. The way it does this is by performing different actions on different patterns within a data file. The basic syntax of an awk program depends on these pattern and actions.
> We can give as many pattern { action } pairs as we want. Each pair will be executed independently of the others. 

[awk: `BEGIN { ...`](https://jemma.dev/blog/awk-part-1)

---

Note de lecture du livre « DGR Deep Green Resistance, un mouvement pour sauver la planète » Tome 1 de Derrick Jensen, Lierre Keith et Aric McBay.

> Si la gauche souhaite vraiment organiser une véritable resistance contre le pouvoir qui brise les cœurs et les os, détruit les rivières et les espèces, elle devra etendre, et envin comprendre, cette phrase courageuse de la poétesse Adrienne Rich « **Sans tendresse, nous sommes en enfer** ».

> Nous devenons plus stupides, plus cruels et plus déprimés de minute en minute. Oliver James appelle _affluenza_ les valeurs de l'industrie des médias, les comparant ainsi à un virus qui se répand dans les sociétés. Il souligne que l'augmentation de l'anxiété, de la dépression et des addictions est directement proportionnelle aux inégalités qui caractérisent un pays. Les valeurs nécessaires à l'institutionnalisation de l'inégalité sont destructrices du bonheur et des communautés humaines. L'injustice nécessite de réduire les autres êtres humains - mais aussi soi-même - à des « marchandises manipulables ».
> « En considérant l'autre comme un objet pouvant être manipulé pour servir nos intérêts, que ce soit pour le travail ou le jeu, nous détruisons toute possibilité d'établir des relations humaines saines [...]. Cela nous laisse un sentiment de solitude, un manque de contact émotionnel, et nous rend vulnérables à la dépression. »


> Plus la classe dominante parvient à rendre son idéologie attractive, plus sa domination est assurée. Pour cela, rien de mieux que les technologie induisant la passivité, l'addiction, l'isolement, avec, en premier lieu, la télévision, puis internet.

> Le rôle du parent est de socialiser l'enfant. Jusqu'à récemment, les parents et les enfants s'inscrivaient dans un système social plus large, dont les valeurs élémentaires étaient les même que celles du foyer. Désormais, on conseille aux parents de «protéger » leurs enfants de la société, au sens large, ce qui est impossible. La société, nous y vivons tous, à moins d'être prêts à déménager en Antarctique. Même si vous parvenez à préserver l'environnement immédiat de vos enfants des pires excès du consumérisme et des éléments misogynes, ils ne peuvent pas éternellement rester enfermés chez vous. **Si notre société est toxique au point que nous ne pouvons pas lui confier nos enfants, alors nous devons chaner de société**.

> Cependant, les drogues et l'alcool ont terriblement nui aux cultures militantes, aux communautés opprimées et aux cultures de résistance. Je vois des gens que j'aime être détruits par l'addiction, au cours d'une mort lente que je suis incapable d'empêcher. (...) Il était pour moi, dès l'âge de 14 ans, que j'avais besoin de deux armes pour le combat : un esprit qui peut penser, et le cœur d'une guerrière. Or les drogues auraient anéanti l'un et engourdi l'autre, si bien que je n'ai jamais regretté la décision de m'en tenir éloignée.

Autant je retrouve dans mon expérience des choses évoqué ici, autant je ne suis pas super à l'aise avec le paragraphe qui suit, donc voici la phrase de fin :

> Les toxicomanes n'ont pas leur place aux premiers rangs de la résistance, parce qu'il feront toujours passer leur addiction d'abord. Toujours.

> « Nos attaques contre [choisissez : l'impérialisme, la civilisation industrielle, le patriarcat] ne donneront rien à moins que nous changions la culture de destruction et de consommation infinies. La vraie question est en fait de savoir comment changer de culture. »
> Il s'agit encore ici d'un tour de passe-passe libéral qui élude la nature du pouvoir, lequel est à la fois sadique et systématique. Si les Noirs dans le Sud ségrégationniste aient décidé que la meilleure stratégie de libération consistait à changer « la culture » de ségrégation ou « les cœurs et les esprits des Blancs », ils seraient toujours assis au fond du bus.

> Il est possible que le Premier ministre de Monsanto ou le Prince héritier du porno aient un jour une épiphanie spirituelle, mais est-ce probable ? Une chance sur un milliard ne constitue pas une base solide sur laquelle édifier une stratégie politique.

> Les êtres humains doivent obtenir leur subsistance en tant que membres de communautés biotiques intactes, pas en tant que leurs destructeurs.

> **L'enfer est pavé de combustible fossiles.** Aucune source d'energie ne peut permettre la perpétuation de la société industrielle. Les donquichottistes doivent affronter la vérité.

> Lester Brown explique tout aussi clairement que la diminution du nombre des naissances dépend principalement de l'amélioration du statut de la femme et de l'élimination de la pauvreté : si l'objectif est d'éliminer la faim et l'analphabétisme, nous n'avons d'autre choix que de diminuer notre population. Parmi les étapes élémentaires pour y parvenir, il mentionne l'allègement de la dette, ainsi que l'accès universel et gratuit aux soins de santé, à l'éducation primaire, particulièrement pour les filles, et au planning familial.

> Sur le plan social, l'initiative la plus importante consista à améliorer le statut des femmes. L'alphabétisation des femmes, qui atteignait 25 % en 1970, dépassa les 70 %  en 2000. À l'heure actuelle, 90 % des filles sont scolarisées.
> En l'espace de 7 ans, le taux de natalité de l'Iran fut divisé par deux, passant de 7 enfants par femme à moins de 3. C'est donc réalisable.

> « Nous avons obtenu davantage en fracassant des vitrines que ce que nous avons obtenu en les laissant nous fracasser »
> -- Christabel Pankhusrt, suffragette

> Comme Derrick l'explique succinctement dans _EndGame_, « faire tomber la civilisation signifie priver les riches de leur capacité à voler les pauvres, et les puissants de leur capacité à détruire la planète ». Cela signifie qu'il faut intégralement détruire les infrastructures politiques, sociales, physiques et technologiques qui non seulement permettent aux riches de voler et aux puissants de détruire, mais qui les récompensent d'agir ainsi.

![Taxonomie de l'action DGR](taxonomie-action_actes-omission-actes-de-comission-directs-ou-dgr-indirects.jpg)

> Le lobbying par la persuasion est une impasse, non seulement dans l'optique de démanteler la civilisation, mais également en ce qui concerne tous les objectifs radicaux. Il part du principe que les puissants sont essentiellement moraux et peuvent être convaincus de changer leur comportement. Mais parlons franchement : s'ils voulaient faire les choses bien, nous n'en serions pas là aujourd'hui. Ou, pour le dire autrement, leur sens moral - s'il existe - est si profondément déformé qu'ils sont presque tous imperméables à la persuasion.

> Cela dit, une action peut à la fois être efficace et attirer l'attention. Le théoricien anarchiste et révolutionnaire russe Mikhaïl Bakounine affirmait que « nous devons diffuser nos principes, non pas avec des mots, mais avec des actions. Il s'agit de la plus populaire, de la plus puissante et de la plus irrésistible forme de propagande ». L'intention derrière une telle action n'est pas de commettre un acte symbolique pour attirer l'attention, mais de mener une action vraiment significative qui servira d'exemple pour d'autres.


Je ne suis pas encore très convaincu. Je vois beaucoup de violence dans ce livre, dans la proposition de cette association. Et je pense que c'est nécessaire, mais j'ai parfois l'impression que c'est au détriment de certaines personnes, et pas forcement les puissants, ce qui me rends inconfortable.

