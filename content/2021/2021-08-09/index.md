---
title: Lundi 9 août 2021
---

Les amies d'Azaé rappel sur mastodon que tous les supports des formations qu'ils et elles donnent sont en accès libre dans [leur gitlab](https://gitlab.com/azae).

Dalibo, entreprise spécialisé en PostgreSQL propose également ses [supports de formation librement accessible](https://public.dalibo.com/exports/formation/manuels/formations/).

Publier les supports de formation librement, c'est vraiment chouette.

En même temps, je me demande ce qu'apporte un support vis-à-vis du contenu vivant de la formation ?

C'est un peu comme la multitude de formation « en ligne » de vidéo... Youtube, Udemy et autre OpenClassRoom. Que proposent-ils vraiment de nouveau ? Un cours à distance ? Un vidéo à regarder ? Peu d'interaction ?

J'aime bien le principe du mentorat qu'Open Class Room à lancé, ça permet de mettre un peu de relation humaine dans la formation.

Nous avons été formaté (ou déformé ?) par l'école, alors oui, il y a pas mal d'adulte qui pense que pour apprendre, il faut être attentif, écouter ce qui est dit et faire des exercices. Et pourtant, chacune et chacun d'entre nous à en fait besoin de temps différents, de déclic sur des points différents pour pouvoir avancer. Comment prendre en compte toutes ces différences dans ce genre de format très unilatéral ? Ce n'est pas possible.

Avec les formats unilatéraux proposé par ces plateformes en ligne, il ne reste plus qu'à chaque personne de gérer elle même les différences, c'est à elle de faire le travail, en plus de celui d'apprendre. Ça s'accumule souvent avec beaucoup d'autres contraintes, surtout pour des personnes en reconversion.

Au tout début, je me suis demandé s'il fallait faire vivre toutes les épreuves que j'avais vécu aux personnes apprenantes pour qu'elles sachent faire la même chose que moi aujourd'hui. L'autre option, c'était de les amener à faire directement les bon gestes, à porter leur attention au bon endroit, sans faire passer par les épreuves qui justifie une façon de faire.

J'ai à l'époque choisi la deuxième option. Bien que ça soit parfois plus complexe pour aborder certains éléments, ça permet malgré tout de réduire la violence de certaines expériences. La violence ne me semble pas être un levier intéressant dans l'apprentissage. Et pourtant, beaucoup l'utilise...

