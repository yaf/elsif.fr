---
title: 30 avril 2021
---

[Gemini Quickstart!](https://geminiquickst.art/)

Pour s'y mettre, je crois que j'ai besoin de prendre des habitudes avec un
navigateur. Je ne sais pas si ça s'appelle comme ça dans cette univers ?

Et là, je me retrouve confronté à mon choix pour Debian stable, pas de
navigateur Gemini dans les dépôts. Et je découvre pourquoi certaines et
certains on créer les Appimages, pour court-circuiter justement la « lenteur »
d'entrée des applications dans les dépôts stable.

Faut-il que je change de distribution ? Simplement passer à Debian Testing ? Et
si c'était le moment de retenter une BSD ?  Sur [FreeBSD il y a des clients
Gemini](https://www.freebsd.org/cgi/ports.cgi?query=gemini&stype=all) [LaGrange
est sur OpenBSD](https://openports.se/net/lagrange), [Castor
également](https://openports.se/www/castor).

Le hic de BSD ça va être les outils de visioconférence ?

Pas de telegram, pas de zoom sur OpenBSD.

Keybase est disponible sur openbsd.

Zoom et telegram sur freebsd par contre

Une autre étape est la publication. Il y a apparemment pas mal de serveurs qui
propose d'héberger gratuitement du contenue pour ce protocole. Et ça ne semble
pas si difficile d'avoir un serveur quelque part... Je crois que mon ami Julien
utilise une RaspberryPI pour faire tourner son serveur Gemini.

---

- Je suis bien avec le grand écran quand même, toute la journée sur le petit,
  c'est ok, mais tellement plus confortable sur le grand ;
- Je suis bien en bureau debout ;
- J'ai pas assez de recul sur le bureau debout pour utiliser le grand écran...

J'ai besoin de changer mon installation de bureau debout.

Je pourrais aussi changer mon installation ordinateur. Pourquoi utiliser un
portable alors que je bouge pas de mon bureau la plus part du temps ?

Il serait temps que j'ai rencontrer l'association Defi de Lanester pour acheter
un ordinateur fixe d'occasion pour les enfants. Ça me permettrait de voir un
peu ce que ça signifie de revenir à ce genre de machine.

---

[Veille design accessible et inclusif
#8](https://blog.hello-bokeh.fr/2021/04/26/veille-design-accessible-et-inclusif-8/)

---

Une personne ne souhaitant pas apprendre Git parce qu'il connait déjà
SubVersion et n'a pas envie d'apprendre Git à également évoqué sont plaisir
d'avoir utiliser [CodingTeam](http://codingteam.org/). Apparemment un bien bel
outil de forge logiciel. Je ne crois pas avoir déjà utilisé, c'est peut-être à
regarder ?

---

[Le guide d’éco-conception de services
numériques](https://eco-conception.designersethiques.org/guide/) des [Designers
Éthiques](https://designersethiques.org/) association de recherche-action
autour de la conception responsable et durable.

> L’unité fonctionnelle correspond à la fonction principale que remplit le
> service et se traduit souvent par un acte métier. Par exemple : “Acheter un
> place de concert”, “Regarder une vidéo en ligne”, “Rechercher un numéro de
> téléphone”…

> Servir les images selon la taille d’écran
>
> Le concept de “Responsive images” permet, surtout lorsque les images ne sont
> pas vectorielles, de charger une taille d’image selon la taille de l’écran de
> l’utilisateur. Cela nécessite de la puissance de calcul pour le serveur qui
> doit redimensionner dynamiquement l’image selon le terminal d’affichage.
>
> Pour éviter cette sollicitation inutile, il est recommandé de correctement
> compresser ses images et d’en fournir plusieurs, par exemple pour ordinateur,
> tablette et smartphone, à différentes résolutions.  Cela se fait avec la
> balise html <picture> qui est largement supportée par les navigateurs mais la
> bonne nouvelle c’est que cela fonctionne même sans le support grâce à une
> “fallback” avec la balise <img />. 

> Carte interactive
>
> Lorsqu’il s’agit d’une carte interactive pour localiser des lieux, considérez
> l’utilisation qui en sera faite, et remplacez, par exemple, par un annuaire.


> Eviter la complétion automatique
>
> Les mécanismes d’autocomplétion ou de suggestions qui visent à compléter
> automatiquement ou suggérer des options "intelligentes" nécessitent beaucoup
> de requêtes vers le serveur.

> Tester le service sur différentes connexions
>
> Cette étape consiste à vérifier que le service fonctionne bien sur des
> connexions “dégradées”, par exemple 2G ou 3G.
>
> Utiliser des outils comme
> [lowband.com](http://www.loband.org/loband/simulator.jsp) ou les outils de
> développement Firefox pour simuler une connexion internet bas-débit et
> vérifier que le site reste accessible.
>
> Exemple des outils de développement Firefox
>
> Dans l’onglet “Network” ou “Réseau” en français, vous pouvez sélectionner le
> type de connexion de l’internaute et identifier les temps de chargement
> associés.


> Dé-numériser, re-matérialiser
>
> Parfois, la meilleure solution pour répondre à un besoin utilisateur tout en
> réduisant ses impacts environnementaux peut être non-numérique !
>
> Cela demande une réflexion de fond, pas mal de créativité et surtout
> d’évaluer les avantages et inconvénients d’une solution “analogique” comparée
> à une solution numérique.

---

> Quand Empreinte Digitale devient une SCOP
>
> Créée à Paris fin 1993, Empreinte Digitale, alors appelée V-Technologies, a
> planté ses racines en Anjou dès l’année suivante. Depuis nos débuts, nous
> travaillons et nous évoluons avec le numérique ; nous avons commencé avec le
> Minitel, les CD-ROM et les DVD-ROM, puis, très vite, sont venus les sites et
> applications Web “sur mesure”, hébergés sur notre centre serveur.
>
> Le 24 janvier 2020, Empreinte Digitale est devenue une SCOP (Société
> Coopérative et Participative) avec 50 collaborateurs, dont 32 salariés
> associés. À l’origine du projet, deux ans de travail avec l’ensemble de
> l’équipe pour réussir la transmission de l’entreprise par ses fondateurs,
> Véronique Lefèvre-Toussaint et Jean-Luc Renaud.

_Qui a dit qu'il n'y avait pas beaucoup de SCOP en informatique ?_


[Empreinte Digitale](https://empreintedigitale.fr/)

---

[La théorie du Donut : une nouvelle économie est possible](https://www.oxfamfrance.org/actualite/la-theorie-du-donut-une-nouvelle-economie-est-possible/)

---

Monter une structure ? À force d'en voir, d'en essayer, d'y participer, je
crois que j'affine ce que j'aimerais.

A la question : est-ce que nous pouvons faire évoluer collectivement une
structure vers un autre objet ? J'ai bien envie de dire oui en théorie. Dans la
pratique je ne l'ai pas trop vue. À chaque fois, ça ne bouge pas. C'est sans
doute pour ça aussi que la création d'une nouvelle structure me démange. Je me
sent fatigué de vivre des micro déception.

Mon contexte actuel ne m'invite pas à me lancer dans ce genre d'aventure. Je
pourrais dire un truc comme « c'est pas le moment ». Et pourtant ça me trotte
dans la tête.

L'écrire pour me vider la tête ; l'écrire pour le partager ; le partager pour
peut-être avoir de l'aide ; le partager pour peut-être voir des personnes s'en
inspirer.

Une Scop pourquoi pas, une SCIC m'intéresserais plus, histoire d'embarquer tout
le monde dans les discussions, dans les décisions. Un modèle commercial plus
proche de l'agence me semblerais pas mal. Mais peut-être le l'ESN serait plus
simple.

La distinction que je fait entre les deux ? L'agence, c'est Captive Studio :
l'équipe est présente dans ces propres locaux. Les clients demande un produit,
un site.  L'ESN c'est /ut7, Scopyleft et consort. Nous sommes ensemble, mais en
mission chez d'autres, pour d'autres. Les clients demande du temps de cerveau
disponible.

Un petit avantage que je vois aussi avec la version agence, c'est qu'il est
plus facile d'envisager de construire un produit, un service que lorsque nous
sommes dans un modèle commercial d'ESN.

C'est bien ce qui me gratte d'ailleurs, l'aspect commercial. Comment trouver
des missions ? Comment trouver du boulot ? Quel plait pour moi, alors pour
plusieurs personnes ?

Parce que l'axe qui m'intéresse le plus serait de faire une structure qui à
vocation à accueillir les personnes qui arrive dans le milieu, celles qui ont
besoin de se construire de l'expérience pour être un peu plus à l'aise.

C'est la suite de ma théorie.

Puisque la plupart des offres d'emploi dans l'informatique demande un bac +5 et
2 à 3 ans d'expérience (je dirais bien 80% des offres si ce n'est plus ?) ;
Puisqu'en vrai, il suffit d'avoir un seul des deux critères pour passer bien
souvent ;

Du coup, les personnes qui peuvent faire une école d'ingénieur, la fac, ou
autre, permettant de cumulé 5 ans au compteur peuvent s'en sortir sans aide.
Mais les personnes en reconversion, les personnes qui tente un bootcamp, une
formation courte. Celle-ci, c'est moins simple. Alors j'ai envie de fabriquer
un endroit où ça serait bien pour elles, pour faire ces deux à trois ans
d'expérience.

Il faudrait que ça soit un deal clair.

Que le client sache qu'il y a une option très clair d'embauche si lui et la
personne sont d'accord.

Après tout, c'est le schéma des ESN depuis toujours. Sauf qu'on a l'impression
qu'elles ne cherchent pas à travailler correctement cette aspect des choses.
J'ai l'impression que c'est plutôt une sorte de conséquence subit.

En plus, je pense que l'ESN ou l'agence, c'est un bon moyen d'explorer
plusieurs projets, différents contexte, pour se construire une riche expérience
en peut de temps.

À réfléchir encore et encore.

---

Découverte de coopérative :

- [Opteos - Coopérative d'entrepreneur-es](https://www.opteos.fr/)
- [Artéfacts - coopérative d'activités & d'emploi culturelle](https://www.artefacts.coop/)

Et un logiciel 

[Opaga](https://opaga.fr/)

> OPAGA est un logiciel libre développé depuis novembre 2017 par Dimitri Robert et qui a pour objectif de gérer tous ces aspects administratifs.
