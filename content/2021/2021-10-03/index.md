---
title: Dimanche 3 octobre 2021
---

> HTML tips you won't see in most tutorials.

https://twitter.com/denicmarko/status/1443890483453956097 Un fil Twitter à propos de tips HTML assez intéressant.

> 1. The `loading=lazy` attribute
>
> You can use the `loading=lazy` attribute to defer the loading of the image until the user scrolls to them.

> 2. Use the `<datalist>` element to create native HTML autocomplete.

La page à propos de [datalist sur MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist) semble montrer que c'est disponible sur tout les navigateurs. Comment ces balises se comporte sur un vieu navigateur ? Je pense à l'éco-conception...

> 5. Use `<input type="reset">` to create clear button for your forms.

>11. HTML Accordion
>
> You can use the `details` element to create a native HTML accordion.
> <details> <summary> Click me to see more details </summary> <p> Details shown after click on the `summary` element </p> </details>

Cette balise n'est pas suporté par le vieux internet explorer d'après la page [details sur MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/details)

> 12. `mark` tag
>
> You can use the `<mark>` tag to highlight text.

Ça me permettra d'arrêter d'utiliser `<em>` ? Mais comment l'utiliser dans Markdown ? En l'incrustant comme balise dans le texte sans doute.

---

[Le premier numéro d'un nouveau webzine à propos d'OpenBSD](https://webzine.puffy.cafe/issue-1.html) !

J'ai bien envie de tenter OpenBSD :)

