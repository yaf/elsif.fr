---
title: Samedi 10 avril 2021
---

Je crois que j'avais déjà noté quelque part ce site https://librecours.net/?query=&filters=%5B%7B%22terms%22%3A%7B%22starred%22%3A%5B%22true%22%5D%7D%7D%5D

J'ai l'impression d'avoir des choses à faire dans l'univers de la transmission de savoir, mais pourquoi je ne fait rien depuis que je suis en Bretagne ? À moins que ce soit depuis que je suis chez Scopyleft ?

Non, il y a eu Ada Tech School, puis les DesCodeuses avec qui je fait encore des choses. Mais c'est moins direct. Peut-être depuis que je suis en Bretagne effectivement, et depuis le covid.

---

Sans lien, ou peut-être pas justement je découvre [Community Canvas - A framework to help you build meaningful communities](https://community-canvas.org/). C'est peut-être le moment de relancer un rookie club à distance ? Une communauté d'apprentissage, en essayant consciemment des outils de communauté.

---

> Le terme déployer est utilisé dans le domaine du développement informatique pour signifier qu’un programme est mis en application. Il peut s’agir autant d’un site web que d’un logiciel. Les lignes de code s’activent selon des règles établies et dictées. 

> Plusieurs questions en lien avec l’édition numérique tout autant que les pratiques littéraires peuvent être posées. Les scripts utilisés pour déployer les livres sont-ils les nouveaux gestes éditoriaux ? Viennent-ils remplacer les mains qui relient les pages, les muscles qui serrent la presse typographique, les doigts sur le clavier qui activent les fonctions d’un logiciel, les algorithmes secrets des logiciels propriétaires ?

[Déployer le livre](https://deployer.quaternum.net/)

---

> The tone of code reviews can greatly influence morale within teams. Reviews with a harsh tone contribute to a feeling of a hostile environment with their micro-aggressions. Opinionated language can turn people defensive, sparking heated discussions. At the same time, professional and positive tone can contribute to a more inclusive environment. People in these environments are open to constructive feedback and code reviews trigger healthy and lively discussions. 

> Better code reviews are also empathetic. They know that the person writing the code spent a lot of time and effort on this change. These code reviews are kind and unassuming. They applaud nice solutions and are all-round positive.

[Good Code Reviews, Better Code Reviews](https://blog.pragmaticengineer.com/good-code-reviews-better-code-reviews/)


---

[Mutation Testing](https://blog.ippon.fr/2020/05/20/le-mutation-testing-ou-comment-tester-ses-tests/)

> Le principe est très simple. Il s’agit de rendre le code “malade” à l’aide de mutations et d’observer la capacité de nos tests à diagnostiquer l’anomalie introduite.
>
> Les mutations appliquées au code peuvent être de différentes formes comme :
>
> - la modification de la valeur d’une constante,
> - le remplacement d’opérateurs,
> - la suppression d’instructions,
> - et encore bien d’autres !


