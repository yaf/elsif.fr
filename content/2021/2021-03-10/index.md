---
title: Mercredi 10 mars 2021
---

Notes à propos de la [Séquence 5 - Sujet 2 : Milieux humides entre terre et eau du MOOC de la Trame Verte et Bleue](https://vimeo.com/464351007/f513927886)

Ne pas utiliser de tourbe, ni de terreau contenant de la tourbe, c'est ne pas participer à la descruction des millieux humides.

---

[Jean-Michel Blanquer annonce la création de l'agrégation d'informatique à la rentrée 2021-2022](https://www.developpez.com/actu/313300/Jean-Michel-Blanquer-annonce-la-creation-de-l-agregation-d-informatique-a-la-rentree-2021-2022-quel-impact-sur-la-formation-des-futurs-developpeurs-et-professionnels-de-l-IT/)

> Dans le système scolaire, c'est le CAPES et l'agrégation qui sont les consécrations d'une discipline et de son intégration dans le système
> -- Jean-Michel Blanquer

> Une question qu'on pourrait se poser est de savoir quel sera leur profil. Orienté théorie ou pratique ? Le salaire sera-t-il aussi à la hauteur pour éviter une fuite vers le privé ?

Et si je devenais prof au lycée ?

Je suis un peu ennuyé, voir effrayé par l'intégration dans le système... Peut-être que de toute façon, le système sera effrayé par moi et que je n'arriverais jamais à avoir un CAPES informatique.

---

[Idée balade : Le chemin de Cadoudal, itinéraire entre mer et forêt](https://blog.morbihan.com/idee-balade-le-chemin-de-cadoudal-itineraire-entre-mer-et-foret/)

---

> La filière PPAM bio est une filière en plein développement qui éprouve un grand besoin de structuration. Afin de mieux accompagner les producteurs qui souhaiteraient se lancer, la FNAB publie un recueil d’expérience « Produire des PPAM bio », fruit d’un travail entrepris en juillet 2017 par le groupe interrégional PPAM bio de la FNAB.
> 
> Ce recueil rend compte de la diversité des modes de production des plantes aromatiques et médicinales dans les principaux bassins de productions français. Autonomie, investissements, organisation, gestion des cultures, transformation, commercialisation : le fonctionnement de 10 fermes est décrit. 10 productrices et producteurs passionnés, avec leur histoire, leurs motivations, leurs réflexions et les solutions parfois originales imaginées en réponse aux problèmes rencontrés.

[Produire des PPAM bio – un recueil d’expériences](https://www.produire-bio.fr/articles-pratiques/produire-des-ppam-bio-un-recueil-dexperiences)


