---
title: Vendredi 24 décembre 2021
---

J'ai installé [Rubymine](https://www.jetbrains.com/ruby/), en version d'essai jusqu'au 21 janvier. [VSCodium](https://github.com/VSCodium/vscodium) (comme [VSCode](https://code.visualstudio.com/)) sont fait en HTML/CSS/Js et parfois, ça rame. J'ai eu un ou deux cas ces derniers jours. C'était pénible.

C'est sans doute pour ça que pour éditer du code il est préférable d'avoir un outil écrit dans le langage du système.

J'apprécie [Vim](https://www.vim.org/) depuis bien longtemps maintenant, sans doute parce qu'il est simple, disponible presque partout, et que le terminal me semble un outil efficace.

Il est vrai que renoncer à une forme de frugalité dans le terminal comme dans Vim me fera sans doute du bien. Où trouver des infos sur les trucs et astuces pour Vim ?

[Vim-plug](https://github.com/junegunn/vim-plug)


[12 Vim Tips](https://dalibornasevic.com/posts/43-12-vim-tips)

- 1. Repeat the last Ex command
- 2. Sort properties in CSS
- 3. Select yanked and pasted text
- 4. Paste text while in insert mode
- 5. Delete in insert mode
- 6. Run normal mode commands across a range
- 7. Repeat last change on multiple lines
- 8. Replace in multiple files
- 9. Search and replace in multiple files
- 10. Edit already recorded macro
- 11. Execute macro in multiple files
- 12. Vi mode on command line


---

où je découvre que sur debian / gnome, l'outil graphique d'installation de logiciel est en fait snap...


[Debian Facile - Le système de paquets snap](https://debian-facile.org/doc:systeme:snap)

> Un snap:
>
> - est un système de fichier [SquashFS](https://fr.wikipedia.org/wiki/SquashFS) contenant le code de l'application et un fichier spécifique de métadonnées snap.yaml. Il est en lecture seule et, une fois installé, dispose d'une zone accessible en écriture.
> - est autonome. Il inclut la plupart des librairies et des outils dont il a besoin et peut être mis à jour ou restauré sans affecter le reste du système.
> - est séparé du reste du système d'exploitation et des autres applications grâce à des mécanismes de sécurité, mais il peut échanger du contenu et fonctionner avec d'autres snaps suivant des règles précises contrôlées par l'utilisateur et les paramétrages généraux du système d'exploitation.

Ça fait bien trop longtemps que je ne me suis pas intéressé à tout ça !

Est-ce que l'historique APT va disparaitre au profit de Snap ?

Je m'en moque un peu si ne fini par retourner sur [OpenBSD](openbsd.org) :D Il y a [NixOS](https://nixos.org/) qui était vraiment chouette aussi dans l'approche...

Pour me simplifier la vie, je crois que rester sous [Debian](https://www.debian.org/), c'est très bien.

---

> Documenter
>
> Une interprétation de la documentation en qualité d’objet à concevoir pourrait postuler d’inclure et de structurer celle-ci autour de quatre fonctions différentes : tutoriels, guides pratiques, explications et références techniques.
>
> - La forme tutorielle, qui est axée sur l’apprentissage, permet au nouvel arrivant de commencer, telle une leçon. Elle est similaire à l’acte dapprendre à planter des légumes ou d’apprendre à faire la cuisine à un individu.
 > - La forme de guide pratique, qui est axée sur les buts, montre comment résoudre un problème spécifique, tout comme une série d’étapes. Elle est semblable à l’acte de cultiver des légumes ou à une recette dans un livre de cuisine.
> - La forme d’explication, qui est axée sur la compréhension, explique, fournit des renseignements généraux et le contexte. Elle est comparable à un article sur l’histoire sociale de la tomate ou l’histoire sociale culinaire.
> - La forme d’un guide de référence, qui est axé sur l’information, décrit la conception réalisée. Elle vise l’exactitude, donc la vérification et l’ajout de sources, et recherche à être complète. Elle est semblable à un article d’encyclopédie de référence.


[ De l’hypothèse de la documentation comme technique de résistance et du wiki comme objet de ces résistances ](http://sens-public.org/articles/1375/)

