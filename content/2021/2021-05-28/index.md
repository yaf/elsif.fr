---
title: Vendredi 28 mai 2021
---

Découvert et utilisation de [NGrok](https://ngrok.com/).

Un outil pour faciliter le fait d'exposer des fichiers locaux sur internet.

À utilise en combinaison avec un serveur web. NGrok ne fait que la passerelle.
Vu le style, je pense que c'est codé en Go. Par contre ce n'est pas opensource. Comment ça fonctionne ? Serait-il possible d'en faire un clone libre ?

---

Dans les discussions hier avec Paquita, j’ai découvert le concept de la VAE inversé.

L’idée vient d’un certain [Dominique Rivoire](https://www.linkedin.com/in/dominique-rivoire-4226bb161/?originalSubdomain=fr ?).

L’objectif est d'embaucher des personnes pour les former « sur le tas » et, au bout d'un ou deux ans (peut-être plus dans certains cas ?), les accompagner vers une validation des acquis d’expérience.

Ça me fait beaucoup penser à ce que j’ai vécu à l’époque de mes débuts, sans la partie VAE pour « valider »... Mais est-ce bien nécessaire ? J’ai l’impression que ça remet sur la table le fait que les entreprises ont leur rôle à jouer dans la formation des personnes. Tout ne peux pas reposer sur des écoles hors sols.

> Retour vers le futur : la VAE est un processus qui permet d’acquérir un diplôme - le véritable diplôme, pas une copie - en faisant valoir son expérience, sans retour en formation et sans examen. On ne lui connait pas, à ce jour, d’équivalent aussi puissant.

> La VAE hybride est une alternative qui consiste à proposer une VAE assortie d’un complément de formation qui permet d’acquérir les compétences nécessaires à son évolution professionnelle. Au terme du parcours, un jury neutre et souverain validera ou non le diplôme et donc la montée en compétences du salarié.

> Elle propose un parcours de formation d’entreprise qui se dédouane d’une approche scolaire de sanction par l’examen et qui n’est donc ni déconnectée du terrain, ni anxiogène.


[La VAE dans tous ses états](http://www.magrh.reconquete-rh.org/index.php/articles/formation/453-la-vae-dans-tous-ses-etats=)

Je me demande si cette histoire de VAE n’est pas un outil de plus pour faire valoir les diplômes. C’est basé sur l'expérience, et tant mieux, mais pourquoi faire un bout de papier avec ? Il y avait les carnets ouvrier/artisan à une époque (il me faut retrouver la référence). Un peu à double tranchant, mais ça relatais ce que les personnes savait faire, une sorte de book à l'ancienne, un journal de bord que les personnes pouvait partager pour « montrer » ce qu’elles savent faire. Ça me semble tellement plus efficace qu’un diplôme !

