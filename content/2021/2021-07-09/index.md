---
title: Vendredi 9 juillet 2021
---

Belle rencontre avec le directeur de la future école des enfants au Faouët. Cette expérience de vie dans une ville de moins de 3000 habitants s'annonce très enrichissante !

---

J'essaie d'ouvrir VSCodium pour programmer. Du coup, à quoi me sert mon terminal ? Il y en a un déjà accessible.

Là, je l'ai ouvert pour accéder à la commande `journal` que je me suis créée. Mais peut-être que, comme pour le journal d'équipe que je fais, je pourrais utiliser Apostrophe ou un autre outil d'édition de markdown.

Ça pourrait sans doute aller de pair avec une autre forme de journalisation, sur le temps long, mais sur des sujets particuliers. Plutôt qu'en désordre comme aujourd'hui. Enfin, en ordonnée dans le temps, mais non lié entre les sujets.

Peut-être que l'intérêt de ne publier que ce qui viens d'être modifié / créer (je pense à l'outil de publication que j'ai bricolé) est justement aussi de permettre une « mise à jour » et republication d'un article sur un sujet donnée.

C'est à essayer sans doute.

---

> Un forum ouvert n’existe que parce que les personnes qui le composent donnent leur temps, leur énergie etleur passion. Merci à vous toustes qui avez passé toute ou partie de ces 48 heures en notre compagnie. Vousavez été fabuleux·ses.

> Au-delà de toutes les réussites du mouvement du Libre, nous avons le sentiment qu’à se focalisersur les libertés, nous en avons oublié de penser l’émancipation et la justice

> Est-il vraiment possible d’encoder des normes sociales dans des objets techniques?

> des CLODO à “On est la Tech” : https://sniadecki.wordpress.com/2018/10/04/rmu-clodo/ - onestla.tech

> Hacker space féministe à Rennes : le hackerspace au bocal à Rennes. En autogestion en mixité non choisie.Volonté de déconstruire des objets du quotidien, du capitalisme sans tomber dans la do-ocratie

> Problème d’avoir un mouvement peu défini comme le mouvement hacker : une fois que la commuanuté estcréée, il est difficile de faire émerger des valeurs à l’intérieur de la commuanuté parce que ca va diviser.

> Pourquoi la technique est dépolitisée? 

> Découvrir les mouvements hors « occident » autour du libre
> — “noms” du libre hors “occident” : [Audrey Tang](https://fr.wikipedia.org/wiki/Audrey_Tang)
> — Femmes informaticiennes féministes d’Amérique du sud
> — Clarissa Borges : designeuse UX pour Gnome
> — Communautés en Indonésie GImpskape qui se structurent autour de logiciels
> — Communautés autour de hackerspace/fablab en Afrique
> — [FOSS Asia](https://fossasia.org/) -> oula les photos avec des hommes blancs en premier plan c’est creepy.
> — Communistes en Inde utilisent des logicels libres
> — [accessnow](https://www.accessnow.org/)

> Le libre n’est-il pas une manière néo-colonialiste de pousser des valeurs libérales américaines?

> [« Infrastructures féministes et réseaux communautaires » : notes de lecture](https://hackriculture.fr/infrastructures-feministes-et-reseaux-communautaires-notes.html)


> Comment en finir avec les boys club (ensembles de mecs cis imperméables à la venue de diversité) dans le libre?
> —rendu possible par l’accumulation des privilèges des mecs cis (absence de nécessité de remise en question de légitimité; + de temps libre; privilège économique; etc.)
> — Question de l’identification de quand on est dans un boys club
> — Demande une énorme énergie pour faire évoluer un boys club
> — À la base une question de sécurité : vigilance par rapport à une position différente
> —un boys club est repérable car il peut “rigoler de” qqn extérieur ou remettre gratuitement en question ses compétences par exemple : l’idée est “d’écraser des gens pour exister” & c’est ce qui faut questionner
> — -> former les gens à ne pas former de boys club

> La licence a permis d’évacuer les questions politiques sur les usages et les finalités

> Il faut parfois accepter de réduire sa puissance d’agir pour éviter de déséquilibrer les rapports de pouvoir.

[Compte-rendu du forum ouvert « Faut-il en finir avec le Libre ? »](https://dérivation.fr/wp-content/uploads/2021/04/compte-rendu-forum-ouvert-faut-il-en-finir-avec-le-libre-2-4-avril-2021-v1.0.pdf)
