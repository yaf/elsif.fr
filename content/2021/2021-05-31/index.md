---
title: Lundi 31 mai 2021
---

Note de lecture « Le guide du potager bio en Bretagne ».

Voici un livre que j'ai beaucoup apprécié. Je pense l'acquérir.

> En terres pauvres et acides, Philippe recommande le blé noir (sarrasin) et le lotier corniculé. Si, de plus, la terre est lourde et humides, il conseille de semer des féveroles, du seigle (en terres modérément acides) ou de l'avoine (en terre très acides).
>
> Le blé noir est également un très bon « nettoyeur » du sol : on en sèmera plusieurs années consécutives pour retrouver une terre saine (par exemple, si l'on doit cultiver une terre précédemment traitée avec des produites phytosanitaires).

Pourquoi produire des graines

> - une graine locale
> - une graine économique
> - une graine de sécurité
> - une gaine sociale
> - une graine politique et militante
> - une graine de plaisir

> À propos de lactofermentation, l'ouvrage « conserves naturelles des 4 saisons » est riche d'enseignements


Ce que j'apprécie dans ce livre c'est que c'est une série de témoignage. Ça donne moins l'impression d'un livre qui dirait « voilà ce qu'il faut faire ». C'est plutôt « voilà ce qu'une telle à fait, et ce qu'un tel a essayé et raté ». Ça permet plus facilement de se projeter.

_Et si je faisait un recueille de parcours en informatique pour joué le même genre de rôle ?_


