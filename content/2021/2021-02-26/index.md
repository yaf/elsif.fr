---
title: Vendredi 26 février 2021
---


> Rappelons qu'un logiciel est dit libre si la personne qui l'utilise bénéficie de plusieurs libertés qui lui permettent d'accéder au code source du logiciel, de le modifier pour l'améliorer et d'en redistribuer des copies. C'est l'analogue d'un théorème en mathématiques où il est heureusement possible d'accéder à la démonstration, l'améliorer et s'en inspirer pour trouver d'autres résultats que l'on repartagera à nouveau dans les mêmes conditions. Imaginons un instant voir le théorème de Pythagore comme un logiciel propriétaire. Sa démonstration serait cachée, il serait même interdit d'essayer d'en retrouver une, son utilisation serait limitée aux personnes titulaires d'une licence... 

[Logiciels libres et enseignement de l'informatique : une coopération forte entre l'April et l'EPI](https://www.april.org/logiciels-libres-et-enseignement-de-l-informatique-une-cooperation-forte-entre-l-april-et-l-epi)

Est-ce que je devrais adhérer à l’[Association Enseignement Public & Informatique](http://www.epi.asso.fr/) ?

---

[Learn to Code in Spanish, Chinese, and 30 Other Languages – freeCodeCamp's Translation Effort](https://www.freecodecamp.org/news/world-language-translation-effort/)

Et dire que j'avais proposé à Souad de travailler sur la traduction du cursus FreeCodeCamp comme support de formation pour les DesCodeuses. C'est peut-être quelque chose à relancer ?

---

Découvert chez les DesCodeuses : [Kahoot!](https://en.wikipedia.org/wiki/Kahoot!)

> Kahoot! is a game-based learning platform,[3] used as educational technology in schools and other educational institutions. Its learning games, "kahoots", are user-generated multiple-choice quizzes that can be accessed via a web browser or the Kahoot app. 

Et pendant ce temps là, chez Ada Tech School

- [Jigsaw](https://en.wikipedia.org/wiki/Jigsaw_(teaching_technique))
- [Jigsaw presentation](https://fr.slideshare.net/ocallahanm/jigsaw-presentation-57788591)
- [THE JIGSAW CLASSROOM](https://www.jigsaw.org/)
- [Jigsaw classroom (wikipedia)](https://fr.wikipedia.org/wiki/Jigsaw_classroom)

---

> Parmi les pistes d’ouverture du numérique aux femmes, Anne-Marie Kermarrec pointe le mentorat, un procédé qui a fait ses preuves : dans le milieu de la recherche ou de l’entrepreneuriat, on a souvent des 'mentors', des personnes qui nous conseillent, nous aident ou nous servent de modèles, tout au long de la carrière.

> Les métiers de l'informatique au carrefour de plusieurs dicipline.

[Numérique : compter avec les femmes](https://www.rtbf.be/lapremiere/emissions/detail_tendances-premiere/accueil/article_numerique-compter-avec-les-femmes?id=10703974&programId=11090)

À lire ? [Numérique, compter avec les femmes](https://www.odilejacob.fr/catalogue/sciences-humaines/questions-de-societe/numerique-compter-avec-les-femmes_9782738154446.php) de Anne-Marie Kermarrec.

---

Finalement inscrit pour le MOOC [Femmes et territoires ruraux en Europe](https://www.fun-mooc.fr/courses/course-v1:grenoblealpes+92016+session01/info)
