---
title: Samedi 13 mars 2021
---

Ce matin, taille au [verger citoyen d'Hennebont (Pomme d'amis)](http://www.verger-citoyen.fr/). Toujours très chouette, un peu bruyant (juste à coté de la 4 voies). J'ai oublié mon cahier, j'ai pas pu noter les tailles et quels arbres j'avais fait.

Le stagiaire d'Optim'ism qui va travailler sur l'éco-construction du bâtiment agricole sur le lieu fait aussi de la géomatique ! J'ai découvert ça en parlant de la liste des arbres plantés, des variétés avec Lionel, l'animateur-arboriculteur qui nous accompagne. Il a parlé linux ! J'ai vraiment envie de rencontrer les personnes de [Optim'ism](https://www.optim-ism.fr/) dans un contexte plus large pour leur proposer mon aide.

J'ai très envie de me mettre en relation avec Hugo, et voir quel logiciel libre il va utiliser, et pourquoi pas aider à corriger, améliorer le logiciel en question.

Lequel ?
- [Open Jardin](https://openjardin.eu/)
- [Garden planner](http://www.smallblueprinter.com/garden/)

---

[lambda function in python](https://www.nbshare.io/notebook/47746562/Python-Lambda/)

