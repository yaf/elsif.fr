---
title: Jeudi 14 janvier 2021
---

> R1 à 18 h 31
> Για σας !
> Erwan (R1, air-one) ʟᴇ Gᴀʟʟ, intrus du pôle [DSN](https://design.numerique.gouv.fr/equipe/), chargé de mission :fauteuil_roulant: ([RGAA](https://numerique.gouv.fr/publications/rgaa-accessibilite/), toussa),
> Après avoir été, quel paradoxe, :ampoule: sysadmin en PME industrielle, :ampoule::ampoule: prof en lycée (dessindus + option info), à Estienne etc. puis :ampoule::ampoule: dév LAMPX full stack et :ampoule::ampoule: expert métamorphe (accessibilité, [juridique](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679), [indexation documentaire](https://www.boutique.afnor.org/norme/nf-z76-041-1/norme-de-description-pour-l-education-en-france-partie-1-description-des-ressources-pedagogiques-nodefr-1/article/923855/fa191804)…) au MENJS. :ampoule:= un [lustre](https://fr.wikipedia.org/wiki/Lustre_(Rome_antique))
> J’:cœur: la [tech mise au service de tou·te·s](https://www.education.gouv.fr/projet-focus-41588)… jusque dans ses recoins les plus alchimiques (IA, code prouvé, solveur SAT…) et l’orthotypographie. :prière: la [3e loi de Clarke](https://www.education.gouv.fr/projet-focus-41588)…
> 6.727 n’est pas si loin, il y a du café (pas Lᴇɢᴀʟ :) et de la lecture dans le couloir : passez !


Présentation qui envoie... Beaucoup d'étalage ?

---

> L’agriculture capitaliste et l’agroécologie ne font pas bon ménage, rappelle l’auteur de cette tribune. Quand la première mise sur le profit et le tout-technique, la seconde parie sur l’observation et l’intelligence du milieu pour fructifier, et repose sur des exploitations familiales durables et diversifiées.

[Agroécologie et capitalisme ne sont pas conciliables](https://reporterre.net/Agroecologie-et-capitalisme-ne-sont-pas-conciliables)

J'aime bien dire que le capitalisme n'est pas une solution, mais je trouve que cette article dur avec les agriculteurs parfois coincé dans ce modèle contre leur grès. Je comprends que c'est plus facile de désigner une personne que de parler d'un concept économique. Et peut-être que je trouverais intéressant de mettre en avant les chemins que pourrait prendre ces personnes pour avancer dans le bon sens, pour se sortir de ce contexte capitaliste.
