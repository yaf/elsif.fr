---
title: Mardi 19 janvier 2021
---

[Une liste de cours libre d'accès](https://librecours.net/?query=&filters=%5B%7B%22terms%22%3A%7B%22starred%22%3A%5B%22true%22%5D%7D%7D%5D)

Le format « cours classique », diffusion de savoir me gène un peu. Et en même temps, c'est peut-être nécessaire d'avoir des capsules de cette manière.

Et si je participais ?

---

> Plus que deux petites semaines avant le lancement du MOOC Trame verte et bleue le 25 janvier prochain ! Pour patienter, découvrez en avant-première quelques extraits des vidéos et certains des intervenants que vous croiserez dans le MOOC.

[Un avant-goût du MOOC Trame verte et bleue](https://www.tela-botanica.org/2021/01/un-avant-gout-du-mooc-trame-verte-et-bleue/)

Je me suis inscrit à ce MOOC, histoire d'en savoir un peu plus sur ces trame. J'y vois un point très important de l'aménagement du territoire, du vivre ensemble, tous ensemble (avec tous les animaux / végétaux) !

---

[Construisons ensemble des projets plus conviviaux et plus respectueux de nos libertés](http://contribulle.org/). Un bien bel outil pour accompagner la contribution.

---


> Direct community support for artists
>
> Ampled is a Patreon-like platform for musicians, owned by its artists and workers.
>
> Ampled allows artists to be directly supported by their community without intermediaries or gatekeepers (and is collectively owned by its artists and workers).

[Ampled](https://www.ampled.com/)

Une inspiration pour Merci edgar ?

---

> Une fois assimilée, la compostabilité des projets permet d’envisager très en amont nos actions et de développer des automatismes fertiles. C’est une manière de se dire « ce que je crée va mourir, je vais mourir, alors comment faire pour que cette énergie que je pose là puisse être utilisée par tout le monde ? ». Se poser cette question, c’est préparer la mort des projets. Pas dans le sens d’un délire paranoïaque et morbide, mais en prenant conscience que notre seule contribution au monde, c’est ce que nous aurons réussi à sincèrement partager.
>
> En écho, le risque d’une mort non-préparée, c’est de s’obliger à continuer à agir, à vivre des expériences, à rassembler des choses, à agréger, sans jamais prendre le temps de penser la transmission de cet héritage. On crée alors une chimère, un projet que l’on va sans cesse persister à nourrir sans jamais en remettre en question le sens et sans accepter qu’il ne s’arrête. 

> De la compostabilité, il faut donc retenir trois choses :
>
> - elle se pense et se prépare très en amont,
> - elle permet de mettre fin aux projets lorsque nécessaire tout en leur assurant un nouveau départ,
> - elle nécessite le partage sincère de l’ensemble des ressources produites.
>
> Rendre son projet compostable, c’est fertiliser les idées en les partageant et tendre vers des organisations plus vivantes, vivaces et vivables.

[La compostabilité : pour un écosystème de projets vivaces](https://vecam.org/La-compostabilite-pour-un-ecosysteme-de-projets-vivaces)

---

Extrait d'un [échange sur Mastodon avec Raphaël à propos de design inclusif](https://mamot.fr/@iergo/105572377188607298).

> Raphaël @iergo@mamot.fr
> Tiens, il y a un peu plus de 3 ans j'avais écris ça sur le "web inclusif" et les conférences : [Web inclusif, conférence et diversité](https://perso.iergo.fr/bazar/web-inclusif-conference-et-diversite/)
> Je constate que c'est toujours d'actualité, certaines conférences n'ont toujours pas fait les efforts nécessaires ( ex : [Blendwebmix, je t’aimais bien…](https://blocnotes.iergo.fr/breve/blendwebmix-je-taimais-bien/) )
> Je me rends compte que je suis toujours aussi gêné par le terme "Inclusif" dans le sens "Design inclusif" où tu découpes en sous-sous-catégories pour concevoir pour chacune au lieu de concevoir pour "toutes les personnes".
>
> Cette culture "inclusive" me parait très lié à la culture des USA où les discriminations et la ségrégation sont encore courantes suivant des diverses critères (ethnie, sexualité, classe sociale,… ) associé à la notion de communautés. Donc, si on peut éviter d'importer ce type de racisme dans notre culture lié au travail, ça m'irait bien. Et je rejoins @mcpaccard sur l'aspect « Supprimer le design inclusif », mais elle l'explique mieux que moi.
>
> @yaf Je pensais avoir écrit un truc sur ce sujet, mais je ne le retrouve pas. En 2 mots design inclusif, tu découpes les catégories de handicaps pour trouver "des solutions" pour chacune. Design pour toutes les personnes, tu conçois une solution qui fonctionne pour tous : Que tu sois en fauteuil roulant, avec une poussette, vieux, une jambe dans le plâtre, tu as besoin d'une rampe d'accès. @mcpaccard

> @yaf Un exemple type de design inclusif : http://www.luciole-vision.com/
> C'est une typo construite "pour les malvoyants" alors que la question de la lisibilité est une question de base pour faire des documents numérique ou non. Et les typos "lisibles" existent déjà comme le Georgia qui est utilisé d'ailleurs sur le site de la typo luciole.  

---

> Currently, Rectify consists of the following concepts:
>
> - [Form Objects](https://github.com/andypike/rectify#form-objects)
> - [Commands](https://github.com/andypike/rectify#commands)
> - [Presenters](https://github.com/andypike/rectify#presenters)
> - [Query Objects](https://github.com/andypike/rectify#query-objects)
>
> You can use these separately or together to improve the structure of your Rails applications.

Une gem pour aider à l'organisation des projets Ruby On Rails ? [Rectify](https://github.com/andypike/rectify)

