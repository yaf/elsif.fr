---
title: Vendredi 15 janvier 2021
---

> Le racisme structurel et institutionnel a des effets cumulatifs et imprègne le tissu de notre société. Il se manifeste dans nos préférences, nos attitudes, notre psychisme et nos relations économiques, ainsi que nos relations sociales et politiques. Il est insidieux.

> Ainsi, les coopératives ne peuvent pas être automatiquement antiracistes. Nous devons délibérément promouvoir et pratiquer l’équité raciale – et désapprendre délibérément les attitudes et les stéréotypes racistes.

> Comment reconnaissons-nous la manière dont nos coopératives perpétuent le racisme institutionnel et se livrent à des microagressions racistes et à l’exclusion ? Comment nous le vérifions-nous en tant qu’individus et organisations ? Sommes-nous prêts à faire tout le travail ? Nous ne pouvons pas simplement dire que nous ne sommes pas racistes et que, bien sûr, nous ne faisons pas de discrimination. Nous devons vraiment examiner nos propres hypothèses et pratiques – et examiner comment nos coopératives fonctionnent réellement, ainsi que les conséquences non intentionnelles. Comment vois-je le racisme dans le mouvement coopératif américain ? Il y a six façons dont je veux discuter ici.
>
> 1. Absence de préoccupation
> 2. Le privilège blanc
> (...) Je vois une grande partie de ce genre de privilège blanc dans le mouvement coopératif. Nous devons être beaucoup plus conscients de ces attitudes et de ce privilège. Son caractère invisible et sa normalité sont toxiques et difficiles à traiter. (...)
> 3. Effacement de l’historique
> 4. Classe et gentrification
> 5. Les coopératives comme agents colonisateurs
> 6. Manque de coopération

[Les coopératives face à l’équité raciale dans : six défis clés et comment les relever](https://autogestion.asso.fr/les-cooperatives-face-a-lequite-raciale-dans-six-defis-cles-et-comment-les-relever/)

---

[Advancing in the Bash Shell](https://samrowe.com/wordpress/advancing-in-the-bash-shell/)

et

[Bash Functions](https://linuxize.com/post/bash-functions/)

---

[Méthode Miyawaki : la méthode pour recréer des forêts natives !](https://permafforest.fr/methode-miyawaki/)

> Découvrez la méthode Miyawaki, cette fabuleuse technique de plantation qui permet de créer des écosystèmes de forêts centenaires en quelques dizaines d’années !

---

> La racine de bardane est riche en inuline, un sucre assimilable par les diabétiques. Elle est de ce fait utilisée dans le traitement du diabète. Elle renferme aussi des substances antibiotiques et bactéricides.

> Si vous vous faites piquer par une guêpe ou même une vipère, la bardane peut constituer un remède de premier secours par son action antivenimeuse. Faire un broyat de tiges et de feuilles et l’appliquer sur la piqûre.

> Récolter la racine de bardane
>
> Pour déterrer la racine de bardane, mieux vaut se munir d’une bonne bêche. La racine peut s’enfoncer jusqu’à 1 mètre dans le sol. Le meilleur moment pour récolter les racines est l’hiver après la première année de la plante, quand elle concentre son énergie dans la terre. Pour utiliser la racine en infusion par la suite, on peut alors bien la laver, la couper en rondelles et la faire sécher. Par contre, le séchage réduit son efficacité, donc si on le peut, il vaut mieux l’employer fraîche.

> Même en plein hiver on voit encore les “squelettes” des bardanes se dresser dans le ciel.

[La grande bardane – excellent légume](https://plantes-sauvages-comestibles.com/la-grande-bardane-excellent-legume)


Je n'en ait pas encore repéré. Je vais pas aller me balader avec ma fourche-bêche pour le moment, mais la recherche de Bardane pourrait être un objectif amusant de promenade !

---

[Les Ergogames !](https://www.ergogames.fr/)

> Les ergonomes de Sogilis vous proposent des petits jeux démontrant des principes d'ergonomie.
>
> Chaque jeu est composé de 2 versions : l'une respecte le principe tandis que l'autre le transgresse.
Après avoir joué les 2 versions, le principe vous sera expliqué.

---

> Nous avons publié et nous maintenons à jour des guides pour aider les personnes victimes de violences à se protéger d’utilisations abusives de la technologie.

[Association de lutte contre l'utilisation de la technologie dans les violences faites aux femmes - Nos guides](https://echap.eu.org/ressources/)

