---
title: Samedi 20 février 2021
---


Ce matin, taille de pommier au verge citoyen. C'est vraiment une activité agréable. J'ai découvert ; la taille pour le plein vent. Les arbres, hautes tiges, sont effectivement balayé par le vent. Je suis surpris de ne pas réduire les branches. On les laissent prendre de la place, grossir et grandir.

En fin de journée, dans notre cour, j'ai planté des fèves, jeté des graines de coquelicot (est-ce que ça poussera ?).

Je n'ai pas encore de cahier pour noter. Un investissement à faire.

