---
title: Samedi 3 juillet
---

Il est 9h, je suis chez Fred & Hélène à Conflans-Saint-Honorine. La famille au loin, moi en balade. Ça fait du bien d'avoir vu du monde, ça fait du bien d'avoir parler longuement avec des ami-e-s. Je n'ai pas pu voir toutes les personnes que j'avais envie de voir.

Ce séjour, qui touche à sa fin, me confirme aussi que je suis content d'être parti.

Il y a également une sorte de déception. Le fait de ne pas être sur Paris pour pouvoir donner des formations aux nombreuses personnes qui sont là.

---

[Très jolie site sur les plantes](https://howmanyplants.com/plant-guides)

---

[Thinkerview - Pass sanitaire, géopolitique de la Data, copie privée ? Benjamin Bayart et Marc Rees](https://invidious.fdn.fr/watch?v=EOWeewlc2CE)

Sans forcement écouter les 2 heures... Mais écouter Benjamin Bayart qui raconte les problèmes qu'amène l'informatique dans nos vie quotidienne au début, ça vaut la peine !

Ou l'on aborde des questions éthiques indispensable à faire apparaitre dans les formations !

---

Merci David de m'avoir faire redécouvrir [Carl Rogers](https://fr.wikipedia.org/wiki/Carl_Rogers)

> Dans l'approche rogérienne, le psychothérapeute ou le psychopraticien se doit d'être un exemple de congruence ou d'authenticité pour son client afin de signifier au client qu'il est, lui aussi, une personne et non pas un expert ou un conseiller. Il y a congruence lorsqu'il y a correspondance exacte entre l'expérience, la prise de conscience et l'expression de soi, lorsqu'il existe en fait une cohérence dans l'expression de la personne, entre ce qu'elle ressent, ce qu'elle pense et comment elle agit. 


> Le travail de Carl Rogers s'est étendu à la pédagogie et à la résolution des conflits internationaux. Dans le droit fil des pédagogies libertaires (voir Tolstoï), en France, il fut une source d'inspiration pour les courants de pédagogie non directive (Daniel Hameline) et dans la pratique du soutien psychosocial aux victimes de catastrophes. 

> « La vie est, au mieux, un processus fluide et changeant en lequel rien n'est fixé. »
> — Extrait de On Becoming a Person - Le développement de la personne, Carl Rogers, 1961, Houghton Mifflin Company, Boston)

Créateur de la [méthode centrée sur la personne](https://fr.wikipedia.org/wiki/Approche_centr%C3%A9e_sur_la_personne)

L'article wikipédia anglais propos également les 19 propositions de Carl

> Nineteen propositions
>
> His theory (as of 1951) was based on 19 propositions:
>
> 1. All individuals (organisms) exist in a continually changing world of experience (phenomenal field) of which they are the center.
> 2. The organism reacts to the field as it is experienced and perceived. This perceptual field is "reality" for the individual.
> 3. The organism reacts as an organized whole to this phenomenal field.
> 4. A portion of the total perceptual field gradually becomes differentiated as the self.
> 5. As a result of interaction with the environment, and particularly as a result of evaluative interaction with others, the structure of the self is formed—an organized, fluid but consistent conceptual pattern of perceptions of characteristics and relationships of the "I" or the "me", together with values attached to these concepts.
> 6. The organism has one basic tendency and striving—to actualize, maintain and enhance the experiencing organism.
> 7. The best vantage point for understanding behavior is from the internal frame of reference of the individual.
> 8. Behavior is basically the goal-directed attempt of the organism to satisfy its needs as experienced, in the field as perceived.
> 9. Emotion accompanies, and in general facilitates, such goal directed behavior, the kind of emotion being related to the perceived significance of the behavior for the maintenance and enhancement of the organism.
> 10. The values attached to experiences, and the values that are a part of the self-structure, in some instances, are values experienced directly by the organism, and in some instances are values introjected or taken over from others, but perceived in distorted fashion, as if they had been experienced directly.
> 11. As experiences occur in the life of the individual, they are either, a) symbolized, perceived and organized into some relation to the self, b) ignored because there is no perceived relationship to the self structure, c) denied symbolization or given distorted symbolization because the experience is inconsistent with the structure of the self.
> 12. Most of the ways of behaving that are adopted by the organism are those that are consistent with the concept of self.
> 13. In some instances, behavior may be brought about by organic experiences and needs which have not been symbolized. Such behavior may be inconsistent with the structure of the self but in such instances the behavior is not "owned" by the individual.
> 14. Psychological adjustment exists when the concept of the self is such that all the sensory and visceral experiences of the organism are, or may be, assimilated on a symbolic level into a consistent relationship with the concept of self.
> 15. Psychological maladjustment exists when the organism denies awareness of significant sensory and visceral experiences, which consequently are not symbolized and organized into the gestalt of the self structure. When this situation exists, there is a basic or potential psychological tension.
> 16. Any experience which is inconsistent with the organization of the structure of the self may be perceived as a threat, and the more of these perceptions there are, the more rigidly the self structure is organized to maintain itself.
> 17. Under certain conditions, involving primarily complete absence of threat to the self structure, experiences which are inconsistent with it may be perceived and examined, and the structure of self revised to assimilate and include such experiences.
> 18. When the individual perceives and accepts into one consistent and integrated system all his sensory and visceral experiences, then he is necessarily more understanding of others and is more accepting of others as separate individuals.
> 19. As the individual perceives and accepts into his self structure more of his organic experiences, he finds that he is replacing his present value system—based extensively on introjections which have been distortedly symbolized—with a continuing organismic valuing process.
>
> In relation to No. 17, Rogers is known for practicing "unconditional positive regard", which is defined as accepting a person "without negative judgment of .... [a person's] basic worth".
>
> [Carl Roger - Nineteen propositions](https://en.wikipedia.org/wiki/Carl_Rogers#Nineteen_propositions)


