---
title: Samedi 29 mai 2021
---

À lire ? [L'insoutenable subordination des salariées ?](https://www.cairn.info/l-insoutenable-subordination-des-salaries--978274926849.htm)

Très envie de le commander. C'est un livre qui reprend un thème chère à mon entourage ces dernieres années :)

---

J'ai animé un cours de soutien / ratrapage sur JavaScript. Ça faisait longtemps que je n'avais pas été dans la posture du sachant. C'était intéressant. J'aime vraiment déclencher des éléments de compréhension chez les autres. Ça me fait plaisir de les avoir aider.

[Révision JavaScritp](https://github.com/descodeuses/revision-javascript)

Pour le format, c'était un peu masterclass. C'est dommage, mais je n'ai pas préparé autre chose, et, à l'improviste, c'est ce que j'ai trouvé de mieux.

J'ai ouvert un fichier, et codé quelque chose pour parler des boucles et des variables. C'était, je crois, pas si mal au vu des retours. La question qu'il me reste : est-ce qu'elles ont vraiment intégré des connaissances ? Ont-elles vraiment construit un savoir ?

Autant, si j'avais le cours principale, je pourrais construire un parcours, un cursus. Prendre le temps de fabriquer une atmosphère ; autant, en cours de soutien, je ne sais pas trop à quoi m'attendre. C'est donc délicat de préparer quelque chose.

Ce cours viens au moment où Violaine me donne l'opportunité de me pencher à nouveau sur l'apprentissage de la programmation, et les théories que j'ai commencé à bâtir autour de ce sujet. Il y a aussi eu les émissions de Gerard Berry. Est-ce que tout ça sont des preuves que j'ai envie de reprendre le sujet ?

---

> Cette conception des compétences montre de sérieuses limites car elle ne donne pas à voir ce qui pourrait faire obstacle ou support, ni à la mobilisation, ni au développement des compétences. Le rapport que l’on entretient aux compétences a donc une incidence déterminante sur la façon d’en penser le développement. Pour ma part, les compétences sont le résultat d’une responsabilité partagée entre individus et organisation, qu’elle soit de travail et de formation.

> Et d’interroger plus spécifiquement le « pouvoir d’agir » d’une personne, plus que son « savoir agir », c’est à dire ce qu’elle peut faire compte tenues des contraintes du milieu dans lequel elle opère et de sa perception des événements, au regard de ce qu’elle est ou fait, voudrait être ou faire. Cette posture introduit l’idée de « disposition organisationnelle » au même titre qu’il existe des « dispositions personnelles ».

> Adopter le cadre des capabilités comme grille de lecture des situations de développement conduit à se poser différentes questions : de quelles ressources disposent les individus pour agir ou pour se former ? Quelles sont les opportunités dont ils disposent pour les utiliser ? A quoi conduit leur utilisation? Quels sont les buts qu’ils poursuivent ? Comment construisent-ils leurs choix ? Quels moyens se donnent-ils de les atteindre ? etc. Répondre à ces questions permet de renseigner de manière itérative la pertinence des ressources proposées, les contraintes qui pèsent sur les individus lorsqu’ils agissent (facteurs de conversion, facteurs de choix) et les chemins qu’ils adoptent au regard des moyens à leur disposition (accomplissements).

[« Tout problème de compétence ne relève pas systématiquement d’un problème de formation »](https://www.fonction-publique.gouv.fr/tout-probleme-de-competence-ne-releve-pas-systematiquement-dun-probleme-de-formation)

Et si j'imaginais un parcours pour apprendre le code à partir de son environnement direct ? La maison le soir, après le boulot ? Au boulot ? Sur le temps de pause du midi ? Dans quelle contexte sont les personnes que j'ai envie d'aider ? Si j'ai envie d'aider un peu tout le monde, faut-il que je multiplie les parcours ?

---

[NocoDB - The Open Source Airtable Alternative](https://github.com/nocodb/nocodb)

