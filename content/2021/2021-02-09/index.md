---
title: Mardi 9 février 2021
---

> Marie-Jo Kopp Castinel : Marie-Jo. Je suis responsable du centre de formation OpenGo, spécialisé en migration vers le logiciel libre et vers LibreOffice en particulier, depuis une quinzaine d’années ; OpenGo fait également d’autres formations. Engagée dans Open office puis LibreOffice, dans l’association La Mouette, dans l’association PLOSS-RA, dont nous reparlerons, et formatrice de métier.

> Jean-Michel Boulet : Jean-Michel Boulet. Je suis formateur avant tout, formateur en enquêtes statistiques, traitement, analyse et représentation de données, c’est le plus clair de mon temps. Par ailleurs, comme Marie-Jo, je suis responsable de formation et gérant de l’organisme de formation 2i2L depuis 2006. On est super spécialisés sur des niches métiers, on fait du métier et c’est pour ça que, généralement, on communique en disant qu’on assure de la formation en mode ingénierie sur des niveaux qu’on apprécie de transmettre et uniquement sur des logiciels libres, uniquement sur des logiciels libres. C’est la licence du logiciel qui nous fait rentrer la formation dans le catalogue.

> Marie-Jo Kopp Castinel : On constate, et Jean-Michel complétera avec d’autres aspects, que les gens ont un niveau informatique, donc le poste utilisateur, on ne va pas parler d’outils métiers mais d’outils de bureautique et autres, assez mauvais, on va dire, au 21e siècle, particulièrement mauvais, parce qu’on ne forme pas les gens, parce qu’il y a des sociétés qui font des logiciels avec des gros boutons qui disent que c’est facile, alors que ce n’est pas vrai. Il faut un mode d’emploi. C’est comme si vous dites à quelqu’un « voilà les clefs de la voiture, tu vas conduire ». Non, il y a un mode d’emploi pour un traitement de texte, pour un tableur, pour tout outil. C’est donc essentiel de former les gens, on en demande de plus en plus aux gens, ils sont de plus en plus sous pression, donc il faut qu’ils arrivent à gagner du temps, à ne pas perdre de temps sur des outils qu’ils ne savent pas utiliser. Et là je laisse compléter Jean-Michel sur d’autres aspects.

> Marie-Jo Kopp Castinel : Non seulement je le partage – je fais je fais beaucoup de formation de formateurs, je ne fais pas que de la bureautique – et c’est ce qu’on appelle l’incompétence inconsciente. La première phase de l’apprentissage, il faut bien le comprendre, c’est l’incompétence inconsciente. J’arrive en formation, déjà il faut que j’y aille et vous savez que c’est toujours une question de référentiel, la plupart pensent « je suis bon à côté de mon collègue ! ». C’est un référentiel. J’avais des pompiers que je salue, qui sont super, qui me disent « on n’est pas bons ». Je dis « si, vous êtes très bons à côté d’autres gens ! ». Les gens se sentent soit très mauvais soit très bons en fonction de leurs collègues, comme disait Coluche. Bref !


> Jean-Michel Boulet : Oui. Je vais reprendre un petit texte pour expliquer, mais c’est tout simple. En fait, Qualiopi c’est la future certification obligatoire pour les organismes de formation à partir du 1er janvier 2022. C’est une certification pour organismes de formation. Ça été pondu par la loi du 5 septembre 2018 et il faut le mettre en place au 1er janvier 2022.


[Émission Libre à vous ! diffusée mardi 12 janvier 2021 sur radio Cause Commune](https://www.april.org/libre-a-vous-radio-cause-commune-transcription-de-l-emission-du-12-janvier-2021)

- [OpenGo](https://opengo.fr/)
- [2i2l](https://www.2i2l.fr/)
- [CNLL](https://cnll.fr/)
- [Aliance Libre](http://www.alliance-libre.org/)

---

Des alternatives à [AirTable](https://airtable.com/) (à explorer)
- [SeaTable](https://seatable.io/fr/)
- [BaseRow](https://baserow.io/)

---

> If you don’t upgrade, you are left out in the cold.

> But could we imagine a computer built like a typewriter? A computer that could stay with you for your lifetime and get passed to your children?

> E-ink is currently harder to use with mouses and pointing devices. But we may build the computer without any pointing device. Geeks and programmers know the benefit of keyboard oriented workflows. They are efficient but hard to learn.

> Can we create a text-oriented user interface with a gradual learning curve? For a device that should last fifty years, it makes sense. By essence, such device should reveal itself, unlocking its powers gradually. Careful design will not be about « targeting a given customer segment » but « making it useful to humans who took the time to learn it ».

Ça me fait me reposer la question du système de bureau que j'utilise ; Gnome depuis bien longtemps. Et pourtant, j'ai utilisé xmonad et 3w pendant un temps. C'est peut-être le moment de s'y remettre ? Cwm était pas mal aussi (un projet OpenBSD)

> This offline first design would also have a profound impact on the hardware. It means that, by default, the networking block could be wired. All you need is a simple RJ-45 plug.


> The Offline First paradigm leads to a new era of connectivity: physical peer to peer. Instead of connecting to a central server, you could connect two random computers with a simple cable.


> It goes without saying that, in order to built a computer that could be used for the next 50 years, every software should be open source.

> The ForeverComputer that I described here would never gain real traction if released today. It would be incompatible with too much of the content we consume every day.

> I want an open source, sustainable, decentralised, offline-first and durable computer.
>
> I want a computer built to last 50 years and sit on my desk next to my typewriter.
>
> I want a ForeverComputer.

Du très bon Ploum. J'adore l'idée, j'adhère.

[The computer built to last 50 years](https://ploum.net/the-computer-built-to-last-50-years/)

