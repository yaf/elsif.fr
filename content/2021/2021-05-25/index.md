---
title: Mardi 25 mai 2021
---

Nous avons encore eu une réunion avec [Ada Tech School](https://adatechschool.fr/). L'idée est de structurer le contenu de la formation pour permettre aux personnes encadrantes de pouvoir le faire sans vraiment _connaître_ les sujets.

Est-ce faisable ? Pas sur, mais c'est intéressant à explorer.

Disons que quitte à se retrouver dans ce gene de situation, j'imagine que les personnes apprenantes pourraient être en autonomie sur les apprentissages, avec des temps de debrief, présentation, aide en commun, ou avec une personne du métier.

Je note quelque point qui me semble important à aborder dans le cadre de l'apprentissage de la programmation.
- [Algorithmique](https://fr.wikipedia.org/wiki/Algorithmique)
- [Parallélisme (informatique)](https://fr.wikipedia.org/wiki/Parall%C3%A9lisme_(informatique))
- [Programmation concurrente](https://fr.wikipedia.org/wiki/Programmation_concurrente)
- [Paradigme (programmation)](https://fr.wikipedia.org/wiki/Paradigme_(programmation))
- [Théorie de la complexité (informatique théorique)](https://fr.wikipedia.org/wiki/Th%C3%A9orie_de_la_complexit%C3%A9_(informatique_th%C3%A9orique))
- [Allocation de ressources](https://fr.wikipedia.org/wiki/Allocation_de_ressources)

Comment aborder ces sujets sans les expliquer ? Comment faire découvrir ce qu'il y a dedans, sans prononcer leur nom ?

Des articles intéressant à lire dans le cadre d'une formation
- [Quelle est la différence entre web et internet ?](https://www.miximum.fr/blog/quelle-est-la-difference-entre-web-et-internet/)
- [ La programmation asynchrone avec python ](https://python.doctor/page-programmation-asynchrone-python-thread-threading)
- [Comment écrire du code Asynchrone en Python](https://leblogducodeur.fr/code-asynchrone-python/)

----

Je m'essaie à journaliser dans un autre outils que [Vim](https://www.vim.org/). [Apostrophe](https://opensourcemusings.com/writing-in-markdown-with-apostrophe) est assez intéressant je dois dire. Les bordures disparaissent au moment de commencer à saisir, aucune distraction. Un environnement simple

C'est plutôt agréable à utiliser.

Est-ce le début de la fin de mon utilisation de Vim ? Je ne pense pas.

Est-ce que ce logiciel est disponible sous [OpenBSD](https://www.openbsd.org/), ou portable là bas ? [FreeBSD](https://www.freebsd.org/) Peut-être ?

---

Installation de [Element](https://element.io/get-started). J'en avait entendu parlé lors d'échange sur le slack des [EIG](https://eig.etalab.gouv.fr/), nous parlions des alternatives à Slack.

Le collectif [InterHop](interhop.org/) utilise cet outil pour discuter. J'ai beaucoup apprécié ces personnes lors de la rencontre de samedi dernier à [la Fabrique du Loch](https://www.lafabriqueduloch.org/). C'est une belle occasion de l'essayer.
