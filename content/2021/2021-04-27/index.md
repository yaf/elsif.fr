---
title: 27 avril 2021
---

> Si on ne reste que rarement junior plus de deux ou trois ans. L’évolution
> qui suit sera d’abord une question d’impact et pas d’expérience ou de
> niveau technique.

> Je préfère quelqu’un qui améliore de 5 % l’impact de tous ceux autour de lui
> que quelqu’un qui améliore de moitié sa propre contribution individuelle.

[Grandir en tant que développeur](https://n.survol.fr/n/grandir-en-tant-que-developpeur)

---

Découverte d'une belle coopérative autour de l'informatique.

> Cliss XXI est une coopérative de service informatique en logiciel libre
> (création de logiciel, hébergement web et mail, etc.). Un logiciel libre est
> un logiciel dont le code est librement accessible, modifiable et
> redistribuable ; contrairement au logiciel propriétaire, il n’est pas
> verrouillé à des fins marchandes (Linux, Firefox, VLC, etc.). Cliss XXI
> s’inscrit pleinement dans les valeurs du Logiciel Libre : partage de la
> connaissance, volonté de réappropriation de l’environnement technologique,
> respect de la vie privée, etc.

> Cliss XXI est implantée dans le bassin minier du Pas-de-Calais, et au sein de
> la Métropole européenne de Lille (MEL).

[Cliss XX1](https://www.cliss21.com/site/)

---

Création d'un compte [Bénévalibre pour
Descodeuses](https://benevalibre.chapril.org/association/)

Est-ce que nous allons nous en servir ? Valoriser le bénévolat, dans ce genre
d'association, permettrait sans doute de rendre plus visible l'impact qu'elle
a.

---

[Git from the inside out - This essay explains how Git works.](https://maryrosecook.com/blog/post/git-from-the-inside-out)

[Gitle](http://gitlet.maryrosecook.com/docs/gitlet.html)

> I wrote Gitlet to show how Git works under the covers. I wrote it to be readable. I commented the code heavily.

---

Recette de Kristen

- Feuilles de plantain lancéolé cueillies dans le jardin.
- Blanchiement des feuilles dans l’eau bouillante.
- Feuilles de lampsane. Blanchir également.
- Feuilles braisées dans de l’huile d’olive avec ail et oignon et assaisonnées.
  (Sel et poivre du sichuan).
- Appareil d’oeufs, lait, tofu soyeux et ou crème fraîche au choix versé sur
  les plantes dans une pâte brisée. Parsemer de gruyere ou comté...(j’ai
rajouté quelques champignons émincés, qui s’agrémentent bien avec le plantain)
- Cuisson à chaleur tournante a 175 degrés environ 40 mn. Décorer avec quelques
  fleurs du moment. ( ici violettes, myosotis et pissenlit du jardin).
Déguster! 😋

Alors je découvre que le Myosotis est comestible. J'en ai plein la cours !

C'est marrant comme ce genre de recette pourrait être vue comme :

> Whaaou ! c'est génial les plantes, je ne mange plus que des plantes sauvages.

Mais c'est oublié toute la farine nécessaire à la pate, l'huile d'olive, les
oignons, le sel, le poivre, le gruyere ou le comté, la creme fraiche, le
tofu...

Après, c'est vraiment chouette comme recette, je vais essayer. Mais en étant
lucide, c'est mieux.

---

[Calendso](https://github.com/calendso/calendso)

Ce que pourrait être RDV-Solidarités ? Ou ce que nous pourrions essayer de
devenir ? Une source d'inspiration ?

---

[Villages Vivants - Foncière rurale et solidaire](https://villagesvivants.com/)
