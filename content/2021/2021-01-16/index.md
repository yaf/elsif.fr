---
title: Samedi 16 janvier 2021
---

> La notion de gare d’aiguillage ou gare centrale
>
> Le terme de [gare centrale](https://source.animacoop.net/?LaGareCentralePourOrganiserEtRendreVis), repris des formations [Animacoop](https://animacoop.net/) [2] désigne un « espace de partage d’information qui rend visible tous les éléments utiles aux membres d’un collectif pour y agir en collaboration ».

[Riposte Créative Bretagne - Gare d'aiguillage](https://ripostecreativebretagne.xyz/?GareAiguillage)

[Riposte Créative Bretagne : expérimenter une « gare centrale » qui relie et mutualise](http://www.bretagne-creative.net/article21378.html)

---

["Pour lutter contre la marchandisation du monde, il faut revenir à un loisir fécond et désintéressé : l'otium" ](https://www.franceculture.fr/emissions/affaire-en-cours/affaires-en-cours-du-jeudi-07-janvier-2021)

> Que faire de notre temps bouleversé par la pandémie ? Selon l’historien, sociologue et chercheur à l’EPHE Jean-Miguel Pire, il faut commencer par réhabiliter la notion d’otium, ce loisir studieux et fécond, temps désintéressé et consacré à la quête de sens et de beauté.

À lire ? [Otium. Art, éducation, démocratie](https://www.actes-sud.fr/catalogue/sciences-humaines-et-sociales-sciences/otium)

> Dans son ouvrage Otium. Art, éducation, démocratie paru en février 2020 chez Actes Sud, l'historien, sociologue et chercheur à l'Ecole Pratique des Hautes Etudes Jean-Miguel Pire se penche sur une notion aussi riche qu'oubliée : l'otium. Au micro de Marie Sorbier, il définit ce concept difficilement traduisible et explore sa résonnance avec les temps répétés de confinement.
>
> L'otium, explique l'historien, est une forme de loisir inventée en Grèce antique, la skholè, qui a été popularisée sous l'Empire romain sous le nom d'otium. Si ce terme latin n'a pas d'équivalent direct en français, il décrit selon Jean-Miguel Pire un loisir fécond, studieux, au temps que l'on consacre à s'améliorer soi-même, à progresser pour accéder à une cohérence et à une compréhension du monde plus grandes. 

