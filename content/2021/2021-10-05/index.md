---
title: Mardi 5 octobre 2021
---

> Donc la notion de mérite c’est une notion qui est une sorte de dénomination extérieure que l’on va projeter sur les individus pour considérer au fond qu’ils sont responsables de leur destin, ce qui est tout-à-fait commode d’un point de vue politique puisqu’on pourra ainsi rendre chacun responsable de son sort.

> Ce système de méritocratie est tout-à-fait avantageux pour des politiques conservatrices qui veulent justifier l’ordre établi puisque plutôt que de remettre en cause cet ordre, plutôt que de prendre des mesures sociales, économiques, politiques, culturelles qui amèneraient à plus de justice plus d’égalité

[Les transclasses ou l'illusion du mérite par Chantal Jaquet](https://www.franceculture.fr/sociologie/les-transclasses-ou-lillusion-du-merite-par-chantal-jaquet)
