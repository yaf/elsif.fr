---
title: Dimanche 3 janvier 2021
---

Tiens, le chaton de l'april, Chapril, propose une forge [forge.chapril.org](https://forge.chapril.org/)

---

À lire ? [Hannah Arendt par Sylvie Courtine-Denamy](http://livres.onpk.net/?9782714432193)
Sur [les conseils de Perrick](http://www.onpk.net/index.php/2020/12/29/801-huit-bouquins-lus-la-vingt-huitime-vague)

---

[#tuto | publier un livre en impression à la demande](http://www.tierslivre.net/spip/spip.php?article4878)

---

> Gemini a été conçu pour qu’un codeur moyen puisse développer un serveur et un client Gemini en moins d’une journée (à comparer avec votre client web qui représente des années d’efforts par des milliers de développeurs).

[Gemini, le protocole du slow web](https://ploum.net/gemini-le-protocole-du-slow-web/)

> La question ici est de savoir si certains seraient intéressés pour se lancer dans l’hébergement de capsules Gemini (en gros, l’équivalent d’un site Web mais pour Gemini). Personnellement, je suis intéressé par l’idée et je sais qu’un autre CHATONS souhaiterait essayer, donc je me suis dit qu’on pouvait en parler ici pour proposer l’idée à d’autres membres.

[Idée de service à proposer : des capsules Gemini](https://forum.chatons.org/t/idee-de-service-a-proposer-des-capsules-gemini/1866)

