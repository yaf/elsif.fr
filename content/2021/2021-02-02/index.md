---
title: Mardi 2 février 2021
---

Quelques notes de lecture du livre « Les jardins d'ombre » de Laura Piravano

Il existe plusieurs type d'ombres :
- Ombre froide : sombres et humides, où même la mousse pousse difficilement. Utilisation de minéraux
- Ombre intense : Très faible lumière, sombre et obscure. Sous les vieux et grands arbres, dans une cours intérieure, à proximité de bâtiment cachant le soleil une grande partie de la journée. Nombreuses plantes peuvent pousser dans ces environnements.
- Ombre lumineuse : ombre moyenne, sous les arbres caducs aux branches hautes. Exposition direct au soleil de 2 ou 3 heures. Sous-bois.
- Mi-ombre, ombre partielle : Ombre qui ne dure que quelques heures dans la journée. Le fait de recevoir une masse de plein soleil en alternance avec une masse d'ombre stimule une abondante floraison et une croissance compacte.
- Ombre légère ou ouverte : Ombre ne durant que très peu dans une journée, max un tier. La plupart des plantes de plein soleil s'adapteront bien à ce type d'ombre.

_À propos du Camelia Sasanqua_
> Au centre de l'arbre dix mille fleurs
> lancent des feux
> Se reflétant sur la neige
> Ils font rougie le ciel
> -- Poésie Chinoise

> on recueillait autrefois ses graines pour la cuisine japonaise, qu'elles enrichissaient d'un délicat parfum.

---

Voilà peut-être le moment d'apprendre à utiliser un nouveau truc avec Vim.

> Place Bookmarks Within Large Files
> Organized folders with bite-sized source files are always appreciated, but opportunity cost often leaves behind 800-line scripts to maintain long-term.

> Type the m key, followed by the letter you want to map your bookmark to, and you can easily jump back to that spot in your file moving forward.
>
> Example: ma will create your bookmark, and 'a / `a will jump to it.

> You can also type :marks, followed by the Enter key, to view all of your active bookmarks and the letters you mapped them to (as shown below).

[Write code faster in Vim](https://jacobcomer.medium.com/write-code-faster-in-vim-c564ff9b9f6c)

---

> Different types of complexity? That’s complex.
>
> Any time you’re solving a problem, not just software problems, there are two types of complexity:
>
> - Essential complexity – This is the complexity that is wrapped up in the problem. You can’t solve the problem without tackling this complexity. This is also sometimes referred to as inherent complexity.
> - Accidental complexity – This is the complexity that comes along with the approach and tools that you use to solve a problem. This complexity isn’t part of the actual problem you’re solving, this is the complexity that you bring in with your solution. This is sometimes referred to as incidental complexity.

[Why does it take so long to build software?](https://www.simplethread.com/why-does-it-take-so-long-to-build-software/)

---

À lire ? [Crack Capitalism](https://www.babelio.com/livres/Holloway-Crack-Capitalism/400450) par John Holloway

---

Note de lecture du livre « Le charme discret de l'intestin » par Giulia Enders

> Quand il s'agit de digestion, le paisible lecteur de romans-fleuves est plus efficace que le PDG hyperactif.

> Il ne faut pas beaucoup de temps aux enzymes pour venir à bout des glucides qui entrent dans la composition d'une tranche de pain de mie blanc. Quand il s'agit de pain complet, en revanche, la digestion est plus lente. Le pain complet renferme des glucides particulièrement complexes qui doivent être décomposés progressivement. **À ce titre, il doit plutôt être considéré comme un dépôt de glucide bien utile que comme une « orgie de sucre ».

> Dans les supermarchés américains, le sucre entre dans la composition d'environ 80% des produits proposés.

> certaines plantes contiennent tous les acides aminés importants en quantités suffisantes : c'est le cas par exemple du soja, du quinoa, de l'amarante, de la spirulien, du blé noir (sarrasin) et ds gaines de chia.

> Non décelée, une malabsorption du fructose qui dure depuis pusieurs années peut donc tout à fait être la cause d'humeurs dépressives.

> une alimentation trop riche en fructose pourrait-elle par exemple nuire à notre bonne humeur ? À partir de 50 grammes de fructose par jour (soit 5 poires, 8 bananes ou environ 6 pommes), les transporteurs naturels sont saturés chez plus de la moitié des gens. Au-delà de cete dose, il pourrait y avoir une incidence néfaste sur la santé.
> Aux États-Unis, la consommation moyenne de fructose est actuellement de 80 grammes par jour. La ration quotidienne de nos parents, qui sucraient leur thé aec une petite cuillérée de miel, consommaient peu de produit tout prêts et mangeaient des fruits en quantité raisonnable, n'était que de 16 à 24 grammes.

_Tout les plats préparé, les gateaux, et autre préparation industriel utilise de la synthèse de fructose pour remplacer le sucre, avec les effet que ça peut avoir..._

> Si vous avez les nerfs solides, c'est peut-être parce que vous disposez d'un gros stock de bactéries productrices de vitamine B. Votre voisin, lui, supportera mieux le bout de pain moisi avalé par inadvertance ou prendra plus rapidement du poids à cause de bactéries « patapouffantes » trop empressées à le nourrir.

> Depuis peu la science considère l'être humain comme un véritable écosystème. Mais les recherches sur le microbiote sont encore de jeunes écolières avec une dent de devant en moins.
> À l'époque où on ne connaissait pas encore bien les bactéries, on les classait dans le règne végétal - d'où le nom de « flore intestinale ». Le terme de « flore » n'est donc pas tout à fait correct, mais il nous permet de bien visualiser ce dont il est question. Comme les végétaux, les bactéries peuvent être classées selon leur lieu d'habitation, leur nourriture et leur degé de toxicité.

> Nous sommes loin de savoir tout ce dont est capable le petit peuple intestinal de l'être humain adulte. Pour l'abeille, nous sommes déjà mieux renseignés. Au fil du temps, les abeilles qui se sont imposées sont celles qui étaient dotées des bactéries intestinales les plus variées. Et si elles ont pu évoluer à partir d'ancêtres guêpes carnivores, c'est uniquement parce qu'elles ont accueilli de nouveaux microbes intestinaux capables de produire de l'énergie à partir du pollen. Voilà comment l'abeille est devenue végétarienne. Quand la nourriture vient à manquer, de bonnes bactéries veillent : en cas d'urgence, une abeille est aussi capable de digérer des nectars inconnus provenant de régions lointaines. Les intestins monotâches, eux, seraient nettement désavantagés. En temps de crise, on voit en outre rapidement qui dispose d'une armée de microbe efficaces : les abeilles dont la flore intestinale est bien pourvue reistent mieux que d'autres à certains fléaux parasitaires. Quand il s'agit de survie, les bactéries intestinales jouent un rôle de tout premier plan.
> Malheureusement, nous ne pouvons pas nous contenter de transposer ces résultats à l'être humain. Les êtres humains sont des vertébrés et ils utilisent Facebook.

_ces histoire de diversité et d'accueil me plaise beaucoup !_

> Chaque culture recèle quelques plats traditionnels « cuisinés » par des microbes pleins d'attention. Citons en France la crème fraiche, en Allemagne la choucroute, les concombres marinés ou le pain au levain, en Suisse le fromage à trous, en Italie le saucisson ou les olives, en Turquie l'ayran, ... - autan de spécialité qui n'existeraient pas sans les bactéries.
> La cuisine asiatique est très friande de ces spécialités : la sauce de soja, la soupe miso ou le kimchi coréen en sont quelques exemples. En inde, pensons au lassi, en Afrique au foufou - et la liste pourrait être rallongée à l'infini. Ces plats dont la préparation fait intervenir des bactéries sont dit « fermentés ». La fermentation produit souvent des acides, qui donnent par exemple au yaourt ou aux légumes fermentés un goût plus acide que l'aliment de départ. L'acidité et les bonnes bactéries protègent la nourriture des mauvaises bactéries. C'est la méthde la plus ancienne et la plus saine de conerver des aliments.

> Contairement à ce qui se passe aujourd'hui dans l'industrie agro-alimentaire, la production de yaourt était autrefois placée sous la responsabilité d'une vaste équipe composée de différentes bactéries - et pas seulement sous celle de quelques espèces sélectionnées.
> La diversité des bactéries dans les aliments fermentés a beaucoup diminué. L'industrialisation a entraîné la normalisation des processus, désormais fondés sur quelques bacéries de laboratoire sélectionnées individuellement.

> Pourtant, il n'est pas difficile de se faire du bien en faisant du bien à ses gentils microbes. Nous avons tous ou presque un « plat prébiotique préféré » que nous pourrions manger plus souvent. Ma grand-mère, par exemple, a toujours de la salade de pomme de terre dans son réfrigirateur, mon père fait la meilleure salade d'endives à la mandarine de la planète, et ma sœur se damnerait pour des asperges ou des salsifis en béchamel.
> Ce sont là quelques-uns des plats que les bifidobactéries et les lactobacilles commanderaient sans hésiter. Nous savons aujourd'huiq qu'ils ont un faible pour les liliacées, la famille des astéracées (ou composées) ou encore pour l'amidon resistant. Parmi les liliacées, il y a le poireayu et l'asperge, mais aussi l'ail et l'oignon. Les composées, ce sont par exemple l'endive et le salsifis, ou encore l etopinambour et l'artichaut. L'amidon resistant, quant à lui, se forme quand on laisse refroidir des pommes de terre et du riz après cuisson.

_Ce livre est fabuleux. Je me suis restreint dans les citations. Il y en auraient trop et de trop longue._
