---
title: Vendredi 5 février 2021
---

[Bookhemispheres](https://www.bookhemispheres.org/), une SCIC, entreprise d'insertion. C'est le type de structure que je trouve vraiment belle sur le concept. Eux font de la récolte et revente de livre. Apparemment, c'est possible de passer les voir. Peut-être que le covid change un peu la donne ? Je pourrais les contacter dans une premier temps, histoire de les rencontrer.

> « Ensemble, redonnons vie aux livres, et créons des emplois ! »

> Société Coopérative d’Intérêt Collectif (SCIC) et Entreprise d’Insertion (EI), Book Hémisphères collecte, trie, et vend des livres d’occasion de tous genres et de tous types. Elle est à ce titre un acteur de l’économie sociale et solidaire.
>
> Le 15 Février 2020, Book Hémisphères a fêté ses dix ans d’activité.


D'autres coopératives informatique ou CAE dans le coin ?

- [CAE 29](https://www.cae29.coop/)
- [Les octets libres](https://lesoctetslibres.com/)
- [Filéo Groupe](https://www.fileogroupe.coop/)

https://www.oxalis-scop.fr/ ?

---

[Cartovrac](https://cartovrac.fr)
