---
title: Dimanche 28 mars 2021
---

Appréciation de ce titre [Nutshell Pt. 2](https://www.youtube.com/watch?v=6kItxNOFJSs&list=RD6kItxNOFJSs&start_radio=1) et en cherchant un peu, je découvre que c'est un titre posthume. [Phife Dawg](https://fr.wikipedia.org/wiki/Phife_Dawg) est un membre de Tribe Call Quest, décédé il y a un an... Un groupe que j'ai beaucoup écouté, apprécié... Sans oublier que Redman fondateur de EPMD, et Busta Rythme qui fait pas mal chose intéressante :)

---

> Qu’est-ce qu’un brave space ? Espace d’encouragement à faire entendre des voix différentes.
>
> Le brave space (ou « espace d’encouragement »), comme le safe space, respecte le ressenti des personnes les premières concernées : leurs expériences vécues. Mais, il essaie d’aller plus loin. Il essaie de développer la cohérence intellectuelle par le dialogue et la cohérence pratique par le courage moral. 

[Pourquoi des « brave spaces » ?](https://iresmo.jimdofree.com/2021/03/21/pourquoi-des-brave-spaces/)

---

Note de lecture du livre « Enjeux éthiques du métier d'enseignant » par Anne-Marie Bazzo et Cyril Desouches.

C'est un livre basé sur une séries d'ateliers de réflexions sur ces enjeux éthiques.

> Se pencher sur les enjeux et problématiques éthiques de la fonction enseignante implique de se poser une question préalable et essentielle : quelles sont les finalités de l'éducation ? Le débat fait ressortir deux sortes de finalités : individuelles et sociétales, dans le sens où l'enseignement doit se penser comme une activité de développement à la fois individuel et collectif. Dès lors surgit une nouvelle interrogation : quelles réflexions l'enseignant doit-il mettre en œuvre ?

> Participant 2 : Cette notion de former un être libre, c'est faire peser sur l'enseignant et ,sur le système éducatif un enjeu de société extrêmement fort et qu'il ne peut pas être le seul à porter. En ce sens, il y a certainement un cadre à poser quant à ce que peut apporter l'Éducation nationale et quelle pourrait être sa contribution à cette formation d'un être libre.
>
> Participant 5 : Si l'ambition de l'Éducation nationale est de former des personnes libre, c'est-à-dire aptes à penser par elles-mêmes, se pose alors la question des connaissances qu'elles doivent maîtriser, des habiletés dont elles doivent être dotées et de leurs valeurs. Cela veut dire que l'enseignant doit également être capable d'incarner ces valeurs. Si l'on juge que l'on veut former des individus utiles pour la société et pour l'économie, la question qui se pose est alors : pour quelle société ?

> Participant 1 : Il y a une autre façon de voir les choses. La « société idéale n'existera jamais », ne serait-ce pas celle qui se donne comme tâche de former des personnes pour les intégrer dans une société théorique ? Cela présuppose qu'il y a une société idéale.... Ce qui n'est guère évident. Comment donner pour tâche à l'Éducation nationale de former des conformistes ? Il est vrai qu'on ne peut non plus lui donner pour tâche de former des anarchistes ! Alors que dire ? Former des gens qui pensent par eux-mêmes et qui savent penser aussi l'évolution de la société à venir.

_ah bon ? Pourquoi pas ne pas former des anarchistes ? Au sens qui refuse l'autorité, qui refuse la soumission._

> Participant 2 : Notre débat est le suivant : poursuivons-nous un objectif utilitariste triste, c'est-à-dire former uniquement un citoyen qui s'intègre dans une société qui est déjà là, avec ses codes, etc. ? Ou bien a-t-on également l'ambition de former des citoyens qui sauront, lorsque ce sera utile, être dans une dynamique de changement ? Pour moi, il y a là deux concepts qui s'opposent.

> Plus complexe que l'acquisition de savoirs, la compétence implique savoir, savoir-faire, savoir-être, et doit pouvoir être transferable d'un environnement à un autre. Maîtriser une compétence, c'est donc être capable, en un autre lieu et dans des contextes variés, de mobiliser ses acquis cognitifs et comportementaux pour atteindre un objectif donnée et relever ainsi de nouveaux défis.

Il y a ensuite un chapitre sur le triangle pédagogique « maître-élève-savoir ».

_J'ai toujours un soucis avec ce terme de maître._

Rappel du livre :

> Cette notion de triangle pédagogique a été originellement introduite par Jean Houssaye au cours de sa thèse en sciences de l'éducation présentée en 1986. Terme actualisé dans le livre « Le triangle pédagogique. Théorie et pratiques de l'éducation scolaire » de Peter Lang

> Penser la relation, en termes éthiques, c'est prendre conscience du style de relation que l'on entretient lorsque l'on a posé entre soi et l'autre quelque chose, en l'occurrence, pour le maître, le savoir comme instrument médiateur de sa relation à l'élève.

> **Le maître médiatise les rapports de l'élève et du savoir**
> - identification de l'élève au savoir (la discipline existe)
> - séparation entre l'élève et le savoir
>
> Pour le maître, éviter les dysfonctionnements passe par quatre nature de reconnaissance :
> - la reconnaissance des savoirs de l'élève
> - la reconnaissance du partage d'un savoir marginal
> - la reconnaissance des investissements consentis par l'élève
> - la reconnaissance qu'un échec est moins intéressant qu'une réussite.

Je ne suis pas super convaincu par tout ces points. Peut-être que je les comprends mal. Ça mériterais d'en parler. Autant que visualise bien le triangle en question. Mais j'ai l'impression qu'il y a d'autre manière de l'aborder.

Les dysfonctionnements possible quand le maître médiatise les rapports de l'élève et du savoir

> Ou bien le savoir « dévore » l'élève (sentiment de culpabilité de ne pas savoir, peur de l'échec) ;
> Ou bien l'élève « dévore » le savoir (quel rapport entre ce que j'apprends à l'école et la vrai vie, celle de dehors) ;

> **L'élève médiatise les rapports du maître et du savoir**

C'est l'idée que le savoir doit être « matérialisé » en dehors du maître, sinon, comment je pourrais me l'approprier ? L'élève doit donc se retrouver à séparer les deux.

Les dysfonctionnements possible

> Ou bien le savoir « dévore » le maître (le maître pense que le monde se réduit à sa discipline) ;
> Ou bien le maître « dévore » le savoir (le maître sait. L'élève ne peut pas lui apprendre quelque chose sur son sujet) ;

> **Le savoir médiatise les rapports du maître et de l'élève**
> le savoir permet l'identification du maître et de l'élève.
> Le savoir permet la séparation du maître et de l'élève.
> (...)
> le savoir les ayant séparés l'un et l'autre, ils peuvent entrer en relation sans danger d'envahissement réciproque sur tout autre terrain.

Si cette médiation n'opère pas

> Ou bien le maître « dévore » l'élève (ces maîtres qui entrent dans la toute-puissance et se font les producteurs de toute loi au moins dans leur discipline, voire dans l'univers) :
> Ou bien l'élève « dévore » le maître (un maître séduits par ses élèves, de même qu'il peut être terrorisé par eux) ;

> Le cours n'est pas une conférence ; il ne se résume pas à la seule délivrance d'un savoir, aussi érudit soit-il.
>
> La valeur ajoutée de l´enseignement consiste à placer l´apprenant au centre d´un processus d´acquisition de compétences, ce qui implique entre les élèves et lui une forme de contrat ; ces derniers doivent, avant de comprendre, connaître les objectifs, les moyens et les effets de l´action éducative.
>
> Le consentement est « éclairé » parce qu´il a été clairement exprimé : il est « éclairé » parce que l´on s´est compris.

> Participant 3 : C'est intéressant parce que nous avons là un rapprochement entre l´acte pédagogique et l´acte de soin. La notion de _care_. « Prendre soin » de celui à qui on s´adresse. C´est un petit aparté, mais c´est intéressant de voir les relations conceptuelles qui s´établissent entre la notion de soin d´une personne et celle d´'enseignement. Comme quoi l´enseignant doit prendre soin des élèves qui lui sont confiés, finalement.

Il est évoqué ensuite le fait d'avoir conscience de son histoire, et d'essayer de prendre en considération ces biais et l'histoire de chaque élève. Je repense beaucoup à Paolo Freire en lisant cela. Où Paolo évoque le fait d'aller se promener dans le quartier où habite les élèves avec qui il travail.


> La tentation de l'ironie, à ne pas confondre avec l'humour
>
> Respecter les élèves, les mettre en valeur, les encourager, ne pas les humilier ou les dévaloriser, voire les déconsidérer en cas d'erreur ou de progression lente, sont des enjeux clés.

> Passer du savoir à la compétence : illustration sur une discipline, l´histoire
>
> Participant 1 :  Prenons un exemple concret. Plaçons-nous en cours d´histoire dans un chapitre du programme portant sur la Révolution Française. Comment traitez-vous cette question suivant la logique qui est la vôtre : transmission de savoir ou bien acquisition de compétences ?
>
> Si l´on se place dans une logique de transmission de savoir, les choses sont claires : on va défiler une chronologie, des personnages ; on va prendre connaissance de textes, de documents qui apportent aux élèves, suivant leur niveau, une vision globale aussi précise que possible de la Révolution française. Mais que signifierait, dans ce contexte précis de la Révolution française, passer d´une transmission de savoir à une acquisition de compétences ?
>
> **Comment verriez-vous la chose ?**
>
> Participant 2 : Ce serait différent ! Mon premier mouvement serait de sensibiliser les élèves sur ce que veut dire « interroger une situation ». Que signifie comprendre une période, un situation ? Je m´attacherais à leur faire sentir qu´une situation peut s´aborder sous des aspects multiples. On peut s´intéresser aux conditions économiques qui ont pu faire germer la Révolution ; on peut s´intéresser au rôle des philosophes des Lumières qui ont préparé les esprits à l´avènement d´un nouvel ordre.


> La relation du maître avec la société
>
> (...)
> Face à une société constituée de communautés de plus en plus différenciées, au développement fort de l'individualisme, aux besoins variées du monde du travail et des logiques de l'emploi, on attend de l'institution éducative une personnalisation de plus en plus forte du service.

Une citation d´Hannah Arendt ouvre la conclusion :

> L´éducation est le point où se décide si nous aimons assez le monde pour en assumer la responsabilité, et de plus, le sauver de cette ruine qui serait inévitable sans ce renouvellement et sans cette arrivée de jeunes et de nouveaux venus. C´'est également avec l´éducation que nous décidons si nous aimons assez nos enfants pour ne pas les rejeter de notre monde, ni les abandonner à eux-mêmes, ni leur enlever leur chance d´entreprendre quelque chose de neuf, quelque chose que nous n´avions pas prévu, mais les préparer d´avance à la tâche de renouveler le monde commun.
> -- Hannah Arendt


Il livre intéressant par sa forme, et par la question de fond. Particulièrement au moment où je re-réfléchi à devenir enseignant. Le CAPES informatique ouvre bientôt, est-ce que je m'y lance ? Ce livre me donne envie d'essayer.

Je crois qu'il est grand temps que je complète mes lectures avec « Une société sans école » de Ivan Illich.

---

Nicolas B. de Codeur en liberté rejoint l'équipe RDV-Solidarités. Ça me fait plaisir de travailler avec lui en particulier, et avec une personne de codeur en liberté en général. Deux faits notable il me semble sur ces premiers jours :
- proposition de faire intervenir sur RDV une personne qui va faire un stage chez eux (de l'école 42). Depuis le temps que j'aimerais qu'on ouvre un peu l'équipe :)
- l'évocation des apostrophes courbe... Je vais donc m´entrainer à utiliser `´` au lieu de `'` comme le le fait tout le temps...

Voir aussi l'article sur l'[apostrophe sur Wikipédia](https://fr.wikipedia.org/wiki/Apostrophe_%28typographie%29)

---

Avec la médiathèque pas loin de la maison, et l'envie de moins acheter de livre, combiné au plaisir des enfants à trainer là-bas, et au fait que je n'arrive pas à lire des essais ou roman aussi vite qu'eux lisent des BD. Je me suis remis à lire des BD aussi. Et j'ai fait de belle découverte que je vais partager ici.

- [Wollodrïn](https://www.editions-delcourt.fr/special/wollodrin/). 10 tomes fonctionnant par deux, durant lesquelles nous suivons divers personnages. Pas un héros devant les autres... Des personnages singulier, un beau dessin.
- [Solo](https://www.editions-delcourt.fr/comics/series/serie-solo). Je crois ne pas avoir lu le tome 5. Mais j'apprécie le dessin, l'histoire post-apocalyptique. Il y a un aspect intéressant à faire grandire certains animaux pour les confronter aux humains !
- [Blacksad](https://www.dargaud.com/bd/blacksad/blacksad). Enquête de police, avec des animaux à la place des humains. J'y vois un travail intéressant pour exprimer des personnages plus fortement encore. Belle atmosphère polar noir avec un chat détective.


