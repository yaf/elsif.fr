---
title: Jeudi 3 juin 2021
---

_À crozon..._

Découverte du [café associatif les voyageurs](https://www.facebook.com/LesVoyageurs.LeCafe).

J'ai fait beaucoup trop de bruit en parlant fort avec mon casque sur les oreilles. Pas respectueux des personnes présentent. J'ai à m'entrainer encore sur le fait de travailler à distance, dans des lieux ouvert, avec d'autres personnes présentent.

Tier lieux encore évoqué hier en réunion, et là... Repair café, fablab, espace de vie, de partage... Monter un lieu, participer à un lieu.

---

[Cron.help Crontab syntax for us humans](https://cron.help/)

---

[Lowpital](https://lowpital.care/)

> Innovation en santé
> Conseil et formations pour accompagner les acteurs de santé dans leurs projets d'innovation.

---

À lire ? [L'Horloge du long maintenat. L'ordinateur le plus lent du monde](https://www.amazon.fr/LHorloge-long-maintenat-Lordinateur-monde/dp/2907681923)
