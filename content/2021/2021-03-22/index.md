---
title: Lundi 22 mars 2021
---

[Adhérer à l'association « Départ imminent pour l'hotel de la gare »](https://www.helloasso.com/associations/depart-imminent-pour-l-hotel-de-la-gare/adhesions/adhesion-2021-depart-imminent-pour-l-hotel-de-la-gare). C'est [une association proposant un chouette projet sur Hennebont, dans l'ancien hotel de la gare](https://lhoteldelagare56700.wordpress.com/). Une partie accueil d'urgence, un espace de coworking, un café avec restauration, ... Très chouette projet !


Pour élargir mon action envers la nature, le monde paysan, la société, je me dit que je pourrais [adhérer à l'association des amis de la confédération paysanne](https://www.helloasso.com/associations/les-amis-de-la-confederation-paysanne/adhesions/adherer-aux-amis-de-la-confederation-paysanne-1?banner=True)...

---

[Liste de lien vers des sites traitant du numérique à l'école](https://informatique-ecole.weblib.re/spip.php?article47&lang=fr)


