---
title: Samedi 23 octobre 2021
---

[Les titis, coopérative de transport](https://www.les-titis.fr/)

3 Kg de noix. C'est beau les noyers. Pas super fan du fruit, ça reste un truc bon :)

[Les noyers (Juglans L.)](https://fr.wikipedia.org/wiki/Noyer)

Quelle variété ? Sans doute le Noyer Commun [_Juglans regia_](https://fr.wikipedia.org/wiki/Noyer_commun)

> C'est le seul représentant en France de la famille des Juglandacées. Il est parfois appelé calottier, écalonnier, gojeutier ou noyer royal1.

Hmmm, les feuilles du [noyer noir](https://fr.wikipedia.org/wiki/Noyer_noir) On dirait un frene !

Comment tenter ce genre d'arbre ? Ça pousse en Roumanie et Hongris, ça devrait pouvoir le faire en Bretagne !

