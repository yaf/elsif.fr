---
title: Dimanche 5 décembre 2021
---

Le problème avec [Fish](https://fishshell.com/https://fishshell.com/) c'est qu'il ne semble pas vouloir lire les scripts [bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell))... ou autre shell standard. C'est génant, pas pratique au quotidien...

Est-ce qu'il y a un mode en fish pour être compatible ? Faut-il basculer sur un autre shell ?

Pour [pyenv](https://github.com/pyenv/pyenv), au lieu de bricoler (comme je suis en train de faire), il semble qu'il y ait moyen de préciser l'environnement d'exécution à PyEnv.

[Install pyenv on Ubuntu 18.04 + fish shell](https://gist.github.com/entropiae/326611addf6662d1d8fbf5792ab9a770)

C'est peut-être mon vim qu'il faut que je pimp surtout. Après tout, je m'en sort pas si mal avec bash.

[Grosse discussion sur la comptabilité POSIX, sur fish, sur bash](https://github.com/fish-shell/fish-shell/issues/522)

---

Dernière récolte de Kiwi : 16,25 kg

