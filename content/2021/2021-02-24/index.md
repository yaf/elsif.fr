---
title: Mercredi 24 février 2021
---

À lire ? BD [Imbattable](https://ybad.name/log/2021-02-10.html)

> L'idée est pourtant simple : le personnage principal, Imbattable, a le super pouvoir de la BD.  Il peut, en quelques sortes, voyager entre les cases.  L'auteur s'amuse avec talent à jouer avec les codes de la BD.  Comme toute bonne histoire, les règles simples du pouvoir sont l'occasion de tester ses limites.

À lire ? [DWELLING PORTABLY 2009-2015](https://www.la-petroleuse.com/livres-ecologie/4747-livre-dwelling-portably-2009-2015.html)
À lire ? [J'ECONOMISE L'EAU A LA MAISON](https://www.la-petroleuse.com/livres-ecologie/3414-j-economise-l-eau-a-la-maison.html)
À lire ? [J'AMENAGE MA MARE NATURELLE](https://www.la-petroleuse.com/livres-ecologie/3410-j-amenage-ma-mare-naturelle.html)
À lire ? [LA DÉCROISSANCE POUR TOUS](https://www.la-petroleuse.com/livres-ecologie/1096-la-decroissance-pour-tous.html)

---

[Learn Go with Tests](https://quii.gitbook.io/learn-go-with-tests/)

---

Ça fait plusieurs fois que je me retrouve confronté au Système d'Information Géoraphqiue (SIG).

La première fois, c'était pour répondre à une offre du [CBN de Brest (Conservatoire Botanique Nationale de Brest)](http://www.cbnbrest.fr/) qui cherchait une personne qui connaissait la Géomatique. J'avais tenté ma chance en me disant que je pourrait apprendre. Ça n'a pas fonctionné.

Ensuite, j'ai découvert [Makina Corpus](https://makina-corpus.com/) et discuté avec une personne de chez eux, pour un autre sujet.

Nous avons aussi, chez [Scopyleft](http://scopyleft.fr/), évoqué le fait de répondre à un appel d'offre pour travailler sur [GeoTrek](https://geotrek.fr/) (finalement, c'est Makina Corpus qui a répondu et gagné ?)

Je vois beaucoup d'usage super intéressant d'OpenStreetMap. J'ai envie de m'en servir, j'ai envie d'y participer.

Et plus récemment, nous avons pris le temps de nous rencontrer avec un EIG #2 et une équipe d'EIG #4 du Morbihan. C'était super intéressant, et ça parlait de carto maritime...

Il y a aussi la prise de conscience qu'une parti des personnes de Codeurs en libertés ont des connaissances sur le sujet.

J'ai l'impression que prendre conscience d'un nouvelle univers qui était à coté de moi tout le temps. Et dans ma liste de l'informatique qui vaut la peine, est-ce que l'analyse de carte est importante ou pas ? J'ai vaguement l'impression que oui. Et ça mériterais que j'en sache un peu plus pour savoir.

Comment entrer dans cette univers ? Où sont les portes ? Faut-il que je me forme de manière classique ? Est-ce qu'il y a des raccourcis ?

- https://ideo.bretagne.bzh/formations/cycle-complet-de-formation-sig
- https://www.formation.gref-bretagne.com/formation/cycle-complet-de-formation-sig-et-teledetection-op/1704121F
- https://www.cnam-bretagne.fr/formation/batiment-et-genie-civil/bim-infrastructures-et-sig
- https://www.formationsig-greta.fr/
- https://www.formationsig.com/
- https://formationsig.com/formation-a-distance
- https://naturagis.fr/cartographie-sig/ressources-en-ligne-gratuites-pour-se-former-a-distance-sig-geomatique-cartographie-donnees/
- https://formation.cnam.fr/rechercher-par-discipline/initiation-au-sig-mapinfo--208909.kjsp
- https://formations.univ-larochelle.fr/sig-fondamentaux

_J'ai toujours adoré les cartes. Dans les jeux de rôles, j'ai toujours aimé faire le cartographe... Est-ce que je suis passé à coté d'une vocation ?_

