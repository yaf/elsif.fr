---
title: Mardi 16 mars 2021
---

Dans le dossier spécial Paris 1871 (la Commune)
[Aspects éducatifs : L’alliance des mains et des cerveaux](https://www.unioncommunistelibertaire.org/?Aspects-educatifs-L-alliance-des-mains-et-des-cerveaux)

> On sait que la Commune, dix ans avant la IIIe République, a ­fondé l’école publique, laïque et gratuite. On connaît moins le programme pédagogique qu’elle se promettait d’y insuffler, qui abolissait la hiérarchie entre travail intellectuel et travail manuel.

> Trois éléments font, en particulier, l’originalité du projet ouvrier en matière d’éducation : l’éducation intégrale  ; ­l’école-atelier  ; la méthode syndicale.

> Pour Paul Robin (1837-1912), pédagogue et ami de Bakounine, qui rédigea un rapport sur l’enseignement en 1870 pour la section parisienne de l’Association internationale des travailleurs (AIT), il faut proposer une même éducation aux filles et aux garçons, sans hiérarchiser les formes de savoirs, sans exclure de l’école le travail manuel et la formation professionnelle.

> L’éducation intégrale, c’est aussi la diffusion par l’école d’éléments jusque-là réservés aux riches : par exemple, la culture du corps et la pratique des exercices physiques, ou l’entrée de l’art et de l’artiste à l’école, comme le propose le manifeste de la Fédération des artistes, animée par le peintre révolutionnaire Gustave Courbet.

> Il faut prendre le contre-pied de la société bourgeoise, qui ne concède aux prolétaires qu’une éducation fragmentaire pour mieux les enchaîner à un travail divisé. Il faut, comme l’écrit un journal communard, que « l’éducation soit professionnelle et intégrale [...], qu’un manieur d’outil puisse écrire un livre, l’écrire avec passion, avec talent, sans pour cela se croire obligé d’abandonner l’étau ou l’établi »

> Tout comme l’ouvrier ne peut pas apprendre sans faire, l’enfant ne peut apprendre qu’en agissant et en travaillant.

> La Commune souhaite même permettre aux travailleurs de participer à l’enseignement. Le modèle classique de l’enseignant défini avant tout par son instruction « scolaire » doit cohabiter avec une intervention directe des ouvriers, des artistes, des gymnastes, etc.

_on se croirait au rookie club :)_

---

> The Concern is a tool provided by the ActiveSupport lib for including modules in classes, creating mixins.

> Finally, it’s hard to say what problem concerns solve. Every problem that concerns solve can be solved with composition or aggregation. Better than that, composition/aggregation solve the same problem but explicitly.

> The PEP-20, Zen of Python reads: Explicit is better than implicit. Implicit means you need previous knowledge. Explicitness is self-explanatory, so it makes our brain work less. No one wants to lose time looking for the place where a method has been defined.

[About Rails concerns](https://medium.com/@carlescliment/about-rails-concerns-a6b2f1776d7d)


