
DUCK="duck"
ELEPHANT="elephant"
SNAKE="snake"
FILE="test_file"

File.open(FILE, "w") do |file|
  file.puts SNAKE
  file.flush
  sleep 5
  file.puts DUCK
end
