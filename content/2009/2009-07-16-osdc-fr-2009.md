---
layout: post
title: OSDC.fr 2009
---


Python, Perl, Ruby, et tout les langages "opensource" ont le vent en poupe. Mais on voit malheureusement trop souvent des trolls sortir du bois pour taper sur l’un avec l’autre et inversement...

L’Open Source Developper Conference France, c’est 2 jours de conférences pour échanger autour de ces langages, apprendre les uns des autres, découvrir les forces de chacuns et apprendre à mieux se connaitre. [OSDCon](http://www.osdcon.org) est une initiative mondiale, chacun y va de ça petite conf. Et bien, et pour la première fois, les associations [AFPY](http://www.afpy.org/), [Les mongueurs de Perl](http://www.mongueurs.net/) et [RubyFrance](http://rubyfrance.org) s’associent pour organiser le 2 et 3 octobre l’[OSDC france 2009](http://act.osdc.fr/osdc2009fr/index.html). Ces conférences auront lieux au Carrefour Numérique de la Cité des Sciences, à Paris (porte de la Villette).

L’entrée est libre et gratuite, alors j’espère que vous pourrez y venir !


