---
layout: post
title: Typo 5.4 Willy Ronis
---


Et bien voilà, comme un cadeau de noël, l’équipe du blogware [Typo](typosphere.org) viens de sortir la version 5.4 surnomé “Willy Ronis”. Pas mal de changement codé interface admin, je vous laisse lire l’annonce sur le blog de type [Release of Typo 5.4 Willy Ronis \[en\]](http://blog.typosphere.org/release-of-typo-5-4-willy-ronis.html).

