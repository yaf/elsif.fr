---
layout: post
title: JRuby 1.2.0
---

  

C'est pas tout frais, mais [Jruby](http://jruby.codehaus.org/) vient de sortir en [version 1.2](http://docs.codehaus.org/display/JRUBY/2009/03/16/JRuby+1.2.0+Released).



Au menu:



* Un bon support des fonctionnalité de [Ruby 1.9](http://www.ruby-lang.org/en/news/2009/01/30/ruby-1-9-1-released/)

* Le compilateur marche (cela permet de *compiler* des classes Ruby en byteCode Java)
* Amélioration des performances (*un classique*) sur le parseur, et le *runtime*

* Un début de support pour [Android](http://code.google.com/intl/fr/android/) (le projet est nomé [Ruboto](http://blog.headius.com/2009/02/ruboto-is-your-friend.html))
* Un gros paquet de bugs sont fixés (un peu trop long pour être listé ici :-))



Ce projet avance vite et bien. Je ne sais pas à quel niveau de compatibilité sont arrivé les autres VM aujourd'hui,je pense à [Rubinius](http://rubini.us/) et [IronRuby](http://www.ironruby.net/) surtout (vous en connaissez d'autres ?), mais certainement aussi bien. Je fais parti de ces gens qui pense que c'est une bonne chose, et vous ?



  