---
layout: post
title: PouypeOnTwit
---

Certains l'on déjà remarqué, je me suis laissé tenter par cette outil de [microblogging](http://fr.wikipedia.org/wiki/Microblog) qu'est [twitter](http://twitter.com).

Ce qui est amusant c'est que sans vraiment savoir quel interêt je trouve à l'outil, je l'utilise régulièrement pour voir ce qu'il se passe, ou plutôt ce que ce dit. Ca à un coté très spontané.

J'ai également commencé à creuser la question du protocole ouvert [OpenMicroBloggin](http://openmicroblogging.org/). Interessant, à creuser, ou pas.

Il y a par contre un billet qui m'a bien aidé à comprendre certaines associations de lettres dans twitter: [Brent Ozar: Twitter FAQ](http://www.brentozar.com/archive/2008/08/twitter-101/).

En résumé:

* `RT` pour ReTwitt : En gros, c'est quand on relaie un *twitt* pour en faire profiter les gens qui serait passé à coté (car ne suivant pas l'utiliasteur à l'origine du premier twitt)

* `OH` pour OverHead : C'est citer quelque chose d'entendu avec ces oreilles (j'ai pas encore vu quelqu'un s'en servir...)

* `@` : Ce signe accompagné (et sans espace) d'un nom d'utilisateur, c'est pour faire uen sorte de réponse ou impliquer l'utilisateur en question.

* `#` : Ce signe accompagné (et sans espace) d'un mot est une sorte de tag. Il existe quelque service annexe qui utilise ces tag pour faire quelque recherche, statistique et autres *mashup* comme on dit en 2.0

Twitter: [http://twitter.com/pouype](http://twitter.com/pouype)

*Edit suite au commentaire de LordPhoenix:* Identi.ca pour ceux qui le préfère libre :) [http://identi.ca/yaf](http://identi.ca/yaf)

ps: Si je ne me trompe pas Twitter est écrit en [RubyOnRails](http://rubyonrails.org) \o/ !

