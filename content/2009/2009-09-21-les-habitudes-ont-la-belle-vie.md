---
layout: post
title: Les habitudes ont la belle vie
---


Dur dur de faire changer les habitudes des développeurs (et des gens en général d’ailleurs).

Passer d’une technologie à une autre n’est pas facile, le faire au boulot apporte encore plus de pression: attente de résultat de la part de la hiérarchie, attente de résultat de la part des clients (même interne).

Passer d’une façon de travailler à une autre n’est pas facile non plus.  Quand l’équipe se connait depuis quelques années ça complique un peu la tâche. Surtout quand certain membre de cette équipe ont passé plus de 20 ans à faire les choses d’une certaines manière.

Faire ces changements quand un membre de l’équipe est ouvertement réticent, défendant ça technologie bec et ongle, c’est un vrai casse tête pour l’équipe, pour le consultant et pour la hiérarchie.

Du coup après 3 semaines d’analyse et d’écoute (très important ça l’écoute), des comportements de chacun, du groupe, et de la hierarchie, on a plein d’idée géniale en tête pour amélioré la qualité du travail de chacun tout en rendant ce dernier bien plus agréable... Du moins c’est ce que l’on pense jusqu’au moment ou, un par un, chaque idée, chaque pratique est démonté par un nouvel élément. Je vous rassure, on garde quand même le principal, mais c’est un peu frustrant.

Bref. L’accompagnement au changement est une science délicate nécessitant plusieurs corde à son arc. Je pense qu’il m’en manque quelques une. Esperons que l’expérience m’aidera à les acquerir :-).

Ceci dit, c’est très interessant, même quand on n’arrive pas à faire passer tout ce que l’on aimerais ! Et puis j’ai pas dit mon dernier mot.


