---
layout: post
title: Redirection sur Elsif.fr
---


C’est les vacances, cela me permet enfin de prendre le temps de basculer le typouypoblog sur un nouveau domaine, plus court à ércire : [elsif.fr](http://elsif.fr).

Normalement, tout devrait être transparent, mais pensé à changer vos flux:

* flux RSS article: [http://elsif.fr/articles.rss](http://elsif.fr/articles.rss)
* flux RSS commentaire: [http://elsif.fr/comments.rss](http://elsif.fr/comments.rss)

Je vais également ne plus limiter la date pour les commentaires, on verra bien. Si trop de spam ressort de cette opération, je reviendrais sur l’ancienne configuration.

J’espère également reprendre un peu le temps d’écrire ici, mais aucune promesse ;-)


