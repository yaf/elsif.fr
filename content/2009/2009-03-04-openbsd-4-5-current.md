---
layout: post
title: OpenBSD 4.5-current
---

Le gestionnaire de version d'[OpenBSD](http://openbsd.org) vient de voir un tag 4.5 arrivé. Comme d'habitude c'est [Théo](http://fr.wikipedia.org/wiki/Th%C3%A9o_de_Raadt) qui le pose tranquillement:

<pre>
  {% highlight bash %}
    CVSROOT:  /cvs
    Module name:  src
    Changes by: deraadt@cvs.openbsd.org 2009/02/28 19:21:07

    Modified files:
    sys/conf       : newvers.sh

    Log message:
    move to 4.5-current
  {% endhighlight %}
</pre>

Et hop, voilà le travail. Comme d'habitude, cela annonce le gèle des ports prochain jusqu'à la sortie le 1er mai de cette nouvelle version. Les possibilités de pré-commande ne vont pas tarder à apparaitre également.

Je suis impatient de voir quel thème va être associé à cetet version. J'ai adoré le précédent basé sur [starwars](http://fr.wikipedia.org/wiki/Starwars):

[![OpenBSD 4.4 cover](http://www.openbsd.org/images/SourceWars.jpg)](http://openbsd.org/44.html)

*via le journal officiel d'OpenBSD: [undeadly.org](http://undeadly.org/cgi?action=article&sid=20090304031205)*



