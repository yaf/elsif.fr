
Des feuilles : mûrier, framboisier, noisetier, noyer, myrtille, chèvrefeuille, ortie, houblon, menthe, verveine, cerfeuil, persil, mélisse, bourrache, sauge…
Des branchettes de plantes aromatiques : thym, romarin, origan…
Des fleurs, sommités fleuries et boutons floraux : coquelicot, rose, violette, lavande, chèvrefeuille, pâquerette, tilleul, camomille…
Des graines, comme celles du fenouil.

Tisanes dépuratives : nettoyage de certains organes
Appareil digestif

Certaines tisanes permettent le nettoyage de l'appareil digestif.

Le fenouil est connu pour ses propriétés antioxydantes et anti-inflammatoires. Il facilite le transit intestinal et est particulièrement recommandé pour dégonfler les ballonnements.

Le pissenlit, en particulier les racines, a une action détoxifiante sur le foie, la vésicule biliaire, les intestins, alors que les feuilles stimulent davantage le fonctionnement de l’appareil urinaire.

L’ortie favorise les fonctions digestives, et stimule le fonctionnement des organes tels le foie et le pancréas. L’artichaut est excellent pour le transit intestinal.

Appareil urinaire

La tisane de queues de cerise exerce un effet drainant sur les fonctions rénales, ainsi que la menthe poivrée, le persil, la chicorée, le thym, les feuilles de pissenlit. La vigne rouge, qui possède en plus des propriétés anti-œdèmes, décongestionnantes et vasoprotectrices.  


Pour la peau

On retrouve la vigne rouge, la bourrache, la sauge, le romarin, la fumeterre.



Quelles tisanes détox choisir ?

On recommande différents mélanges :

Pour le foie : le bouleau, le romarin, la fumeterre, l'artichaut, le chardon-Marie, la gentiane.
Pour les poumons : la viorne, le plantain, le bouillon-blanc.
Pour les reins : l'aubier de tilleul, le bouleau, la reine-des-prés, l'orthosiphon, les queues de cerise.
Pour les glandes surrénales qui produisent les hormones contre la fatigue : les racines d'angélique, les fleurs d'hibiscus, les feuilles de cassis.
Pour la peau : le pissenlit, la pensée sauvage, la bardane.




Bon à savoir : le pissenlit, Taraxacum dens leonis, amincit, et aide à lutter contre un terrain allergique et un excès de tension. Il aide le transit, draine le foie et le rein, apaise les douleurs articulaires.


Tisane détox pour nettoyer les reins, la peau et le foie : décoction d'aubier de tilleul

Versez deux bâtons d'aubier de tilleul dans un litre d'eau ;

Tisane détox d'exception : le fenouil

Le fenouil, Fructus foeniculi, est un merveilleux remède universel :

il est antispasmodique ;
il est carminatif (il apaise toutes les affections gastriques et intestinales) ;
il enlève les éléments putrides de l'intestin, l'acidité gastrique et les aigreurs générées par des erreurs d'alimentation ;
il lutte contre les odeurs corporelles et une mauvaise haleine ;
il donne une belle humeur.



Tisane au millepertuis : propriétés et bienfaits

En raison de la proximité de ses composés avec les médicaments de type antidépresseurs, le millepertuis et les infusions de millepertuis représentent une alternative naturelle en cas :

de déprimes ou de dépressions suite à un choc émotionnel violent ;
d’états de stress ou d’anxiétés répétés ;
de troubles du sommeil sous formes de problèmes d'endormissement, de réveils intempestifs ou d'insomnies répétées.

En effet, en agissant sur la production de la sérotonine, l’hormone de la bonne humeur, ainsi que les neurotransmetteurs du cerveau, le millepertuis permet de calmer certains états ou humeurs.




Tisane de passiflore : propriétés et bienfaits sur la santé

Les principes actifs de la passiflore se situent dans les parties dites aériennes de sa fleur. En effet, une fois séchées, les fleurs de passiflore contiennent une grande quantité de flavonoïdes et d’alcaloïdes, deux substances réputées pour leurs propriétés relaxantes et calmantes.

Ces dernières peuvent alors être consommées sous forme de tisane afin d’apaiser certains troubles comme :

l’anxiété et les crises d’angoisse ;
des insomnies légères à répétées ;
des palpitations cardiaques modérées ;
un sommeil ponctué de réveils intempestifs ;
l’hyperactivité ;
les problèmes de digestion liés à une période de stress ;
des contractions d'origine nerveuse ;
des spasmes musculaires.

Enfin, des études ont montré qu'une cure contenant des extraits des fruits produits par la passiflore pouvait également améliorer les troubles respiratoires dus à l'asthme.

À noter : dans le cas d’une utilisation de la passiflore pour retrouver un équilibre de vie et un meilleur sommeil, cette alternative naturelle ne présente aucun effets secondaires contrairement aux somnifères, aux antidépresseurs et aux anti-anxiolytiques.



Il est recommandé de boire 3 à 4 tasses par jour de tisane de passiflore à tout moment de la journée afin d’en ressentir tous les bienfaits. En revanche, la consommation de passiflore est déconseillée chez la femme enceinte et les enfants âgés de moins de 12 ans.



Le basilic est une «  herbe de Provence » comme la sauge, le thym, le romarin… Il est employé comme aromate.

Les feuilles de basilic utilisées en tisane favorisent une bonne digestion grâce à leur propriété antispasmodique. Faisons le point ensemble à ce sujet.

Découverte botanique du basilic

Le basilic, Ocimum basilicum, fait partie de la famille des labiacées (Lamiacae). On l'appelle aussi Pistou. C'est l'herbe de sainte Anne, patronne des maraîchers. En Grèce, c'est une Herbe royale : Basilikon et en Inde, où il est né, c'est une herbe sacrée. 

La tisane est utilisée pour l'indigestion, l'inappétence, les flatulences et les ballonnements grâce à ses propriétés carminatives et pour les pharyngites grâce à ses propriétés astringentes (qui diminuent les sécrétions et aident à la cicatrisation).



Bon à savoir : le basilic utilisé sous forme de pommade cicatrisante soigne les plaies et les blessures. Il peut alors être associé à d'autres plantes comme la menthe.



Le basilic peut être cultivé en pot ou dans un jardin. C'est un peu avant la floraison que son efficacité est la plus grande. Si vous n'avez pas de basilic frais :




Tisane de pissenlit : propriétés médicinales

Le pissenlit possède des principes actifs remarquables.
Taraxine

La taraxine est un de ces actifs essentiels.

Son goût est amer, mais il exerce une puissante action détoxifiante sur l'organisme, et stimule particulièrement les organes à fonction dépurative, comme le foie et les reins.

Il active également la sécrétion biliaire pour faciliter la digestion des graisses.

Il purifie le sang, stimule l'activité rénale et possède des propriétés diurétiques.





Flavonoïdes

Le pissenlit est également très riche en flavonoïdes, ces composantes responsables de la couleur des fruits et légumes, ainsi qu'en acides phénoliques :

Les flavonoïdes, tout comme les acides phénoliques, recèlent de nombreux antioxydants, tels la vitamine E et la vitamine C, ainsi que certains enzymes.

Ces composantes jouent un rôle essentiel dans la protection de l'organisme. Elles bloquent l'influence des radicaux libres, responsables de nombreuses maladies, ainsi que du processus du vieillissement.

Bon à savoir : le pissenlit contribue aussi à la guérison des maladies de peau, comme les dermatoses, les cors ou les verrues.



Préparation d'une tisane de pissenlit

Les parties du pissenlit utilisées pour les tisanes sont les racines et les feuilles. Les deux parties de la plante traitent des phénomènes différents.

Les racines sont plus adaptées pour traiter les problèmes digestifs, de foie, de vésicule biliaire.

Les tisanes préparées à l'aide de feuilles agissent davantage sur le fonctionnement des reins et de l'appareil urinaire.

Bon à savoir : les racines particulièrement charnues du pissenlit poussent à une profondeur de 50 cm dans le sol, ce qui lui octroie une grande résistance aux climats très froids. On trouve en effet des pissenlits en Europe, mais aussi en Russie et au Canada.



Décoction : utilisez 20 g de racines séchées pour un litre d'eau. Laissez macérer 2 heures, puis portez à ébullition.
Infusion : utilisez 50 g de feuilles pour 1 litre d'eau chaude. Laissez infuser 10 minutes.



Tisane de lavande : propriétés médicinales
Antispasmodique

La lavande est un puissant antispasmodique : elle calme les spasmes musculaires, ces contractions involontaires des muscles qui peuvent être parfois très douloureuses. Elle est ainsi fortement recommandée dans les cas de coliques hépatiques, ou de douleurs utérines, mais aussi en cas de migraines.
Sédatif

C’est aussi un puissant sédatif : elle agit de façon bénéfique sur les contractions musculaires telles que les entorses ou les courbatures.

Ses vertus sédatives agissent également sur le système nerveux : elle permet ainsi de lutter contre le stress, et permet la relaxation. Une bonne tisane de lavande permet de se détendre et de retrouver une paix intérieure. Est-ce grâce à son parfum particulièrement agréable qui induit un effet calmant, ou est-ce grâce à ses composantes médicinales ? Difficile de le savoir. En tout cas, la lavande diminue notablement le niveau d’anxiété d’une personne, et contribue à diminuer l’insomnie.




Diurétique

La lavande possède enfin un effet diurétique : elle permet l’élimination de toxines, et stimule l’activité des reins.
Préparation d'une tisane de lavande
En infusion

Les parties utilisées pour les infusions de lavande sont les fleurs, riches en huile essentielle. Il suffit de faire infuser une cuillère à café de fleurs séchées dans une tasse d’eau chaude pendant 10 minutes.

L'infusion de lavande peut aussi être préparée à l’avance en grande quantité et être consommée froide. Faites alors infuser 50 g de fleurs séchées pour un litre d’eau chaude. Elle se conserve très bien au réfrigérateur.

Vous pouvez en boire 4 tasses par jour, dont une le soir avant le coucher, pour améliorer la qualité de votre sommeil.



En décoction

Faites bouillir 50 g de fleurs de lavande dans un litre d’eau. Cette décoction possède de multiples vertus : elle soulage les entorses, les foulures et les courbatures. Frictionnez-en doucement le muscle douloureux plusieurs fois par jour.

La décoction de lavande est aussi excellente pour la peau : elle estompe les rougeurs diffuses, hydrate et soulage les peaux sèches et sensibles :

Le matin, après avoir lavé votre visage, faites une pulvérisation d’eau de lavande.
Le soir, après le démaquillage, rincez votre visage à l’aide de cette même eau.

Conservez votre décoction dans un endroit frais, et à l’abri de la lumière.

Bon à savoir : l'action apaisante et antiseptique de la lavande calme aussi les piqûres de moustiques et les démangeaisons.





Bienfaits de la tisane d'ortie

Les vertus sont différentes selon que l'on utilise les feuilles ou les racines :

Les feuilles sont dépuratives générales, diurétiques et reminéralisantes. Elles sont riches en calcium. On les utilise contre les dermatoses rebelles (eczéma, psoriasis et dartres), en rinçage des voies urinaires et en prévention et traitement des calculs rénaux. On les utilise aussi contre les douleurs rhumatismales et l'arthrite.
Les racines sont utilisées en traitement des affections bénignes de la prostate. Elles augmentent le débit et le volume urinaire et tonifient le sphincter de la vessie.



Tisane d'ortie : comment reconnaitre cette plante ?

Bon à savoir : l'ortie, ou Urtica dioica, est une plante de la famille des Urticacées.

Il est important de bien reconnaitre la plante pour l'utiliser en tisane :

C'est une plante herbacée, qui peut mesurer de 60 cm à 1,50 m.
Les feuilles sont opposées et dentées. Elles sont reliées à la tige par une petite tige, le pétiole, et portent des poils urticants.
La tige est quadrangulaire, velue et très fragile.
Les fleurs sont de couleur verdâtre.



Tisane d'ortie : où et quand ramasser l'ortie ?
Récolte avant floraison

Il existe des règles de récolte afin de préserver une teneur en principes actifs optimale et d'assurer leur conservation :

la récolte se fait par temps chaud et sec pour une bonne conservation ;
elle se fait le matin de préférence, ou le soir avant la fraîcheur ;
les feuilles se récoltent au moment de leur plein développement, mais avant la formation des boutons floraux, car elles seraient moins riches en principes actifs.



Toutefois, on pense assez peu à faire une infusion de gingembre (Zingiber officinalis), une plante dont on connaît pourtant les nombreuses propriétés et qu’il est aujourd’hui facile de trouver dans n’importe quel supermarché (en revanche vous n’en trouverez du bio qu’en magasin diététique).

Toutes les infos dans notre article.


Propriétés des infusions de gingembre

Le gingembre possède la particularité de calmer et d’apaiser (il fait baisser le taux de cortisol) tout en maintenant l’état d’éveil. Toutefois, sous forme d’infusion il peut aussi favoriser le sommeil.

Néanmoins, là où il excelle, c’est dans son action digestive. En effet, il :

facilite le transit intestinal ;
améliore l’absorption des éléments nutritifs ;
est antispasmodique ;
soulage les diarrhées ;
lutte contre les nausées ;
élimine les gaz intestinaux ;
calme en cas d’indigestion.

De plus, le gingembre est intéressant pour :

prévenir les rhumes, les sinusites et les maux de gorge en raison de son action anti-inflammatoire, antiseptique et antivirale ;
soulager les migraines et le mal des transports ;
réchauffer lorsqu’on a les extrémités froides, notamment en stimulant la circulation sanguine (ce qui réduit également le risque de maladies cardiovasculaires) ;
favoriser la transpiration ;
réguler la glycémie ;
soulager les douleurs menstruelles avec ses propriétés emménagogues (il stimule la circulation sanguine au niveau de l’utérus et dans la région pelvienne) ;
aider à perdre du poids en régulant l’appétit ;
accélérer les périodes de rémission (diminuer le temps de convalescence) en raison de son action stimulante auprès du système immunitaire ;
ses vertus aphrodisiaques, notamment en cas de troubles d’érection (il participe également à l’amélioration de la fertilité masculine en augmentant la concentration du sperme) ;
son action vermifuge, etc.


