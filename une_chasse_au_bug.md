# Journal d'une chasse au bug

Il était une fois, une personne qui travaillait sur création du logiciel
DossierSCO. Ce jour là, comme presque tout les jours de la vie d'une
développeuse...


> J'arrive bien à reproduire le problème signalé en production, mais pas en
> local, sur ma machine. Je vais donc aller regarder là bas directement pour
> pouvoir faire l'analyse.


DossierSCO est, au moment d'écrire cette histoire, une application qui n'est vraiment utilisé que de mai à juillet, voir septembre au plus tard. C'est une application saissonière. La production reste entre les deux périodes. Au moment de cette histoire, nous faisons quelques tests, en compagnie d'établissements expérimentateur. Les incidents et manipulations évoqué ci-dessous n'ont donc eu aucun impact, et les données exposées étaient factice.

## Mauvais endroit, mauvaise personne

**Pourquoi, après l'affichage de la page d'erreur (500), en cliquant sur le
bouton « retour à l'accueil » je me retrouve en page avec une élève déjà
connecté sur l'interface famillle ?**

> Je n'arrive pas à reproduire localement, pourquoi ?

Pour l'erreur, je viens de comprendre :
- en production,
- en tant qu'admin de l'établissement de test, je vais visualiser les pièces
  jointes d'un élève,
- j'ai une erreur avec le message générique m'invitant à contacter l'équipe,
- j'appuie sur le bouton bleu qui m'annonce un « retour à l'accueil »,
- je me retrouve sur la page d'accueil d'inscription d'une autre élève, d'un
  autre établissement....

Sur ce dernier point : juste avant d'afficher la page d'accueil, on vérifie si
une ou un élève est connecté, et si oui, on redirige sur l'accueil de cette
famillle.

Donc en cherchant l'identifiant qui est dans la session, on trouve `nil` (la
valeur vide en Ruby) comme valeur. Mais voilà, une élève a `nil` comme valeur
d'identifiant dans notre base de donnée.

Cette découverte n'explique pas encore pourquoi cette élève a un identifiant
`nil` ni pourquoi il y a un soucis avec les pièces jointes de l'autre, mais ça
explique pourquoi ce saut de navigation un peu surprenant depuis la page erreur
jusqu'à la page d'accueil d'une autre famille sur un autre établissement.

## Identifiant NIL

**Pourquoi l'élève à un identifiant `nil` en production**

Il y a bien qu'une élève qui a un identifiant avec la valeur `nil`. Elle est associée à deux
dossiers sur deux établissements, un dans le sud et un dans l'est.

Elle est de l'est pour de vrai, d'après son adresse. Comment est-elle aussi sur le sud ? Un test
de travers ?

Je ne vois pas le fichier de l'établissement en question dans nos répertoire, c'est en partie une bonne nouvelle, ça signifie que l'établissement en question a fait toute la procédure d'initiation de sa campagne d'inscription sans avoir besoin de nous ! Par contre, cela va me géner pour l'analyse du problème.

**Du coup, je ne peux pas voir s'il y a un soucis à la source (dans le fichier,
est-ce que cette élève a un identifiant ?** _peut-être un coup de fil à passer
à l'établissement ?_

_Faut-il ajouter une règle : pas d'élève avec un identifiant nil ?_


## Pièces jointes en echec

**Pourquoi les pièces jointes du premier élève n'apparaissent pas en préview
?**

Tiens, l'élève posant un soucis sur les pièces jointes n'a pas de MEF Destination (Module Élémentaire de Formation de destination). Est-ce que c'est parce que cet élève n'a pas de MEF destination que ça ne fonctionne pas ? Je ne pense pas.

Est-ce que la pièce jointe utilisée pour le test est particulière ? J'ai essayé sur un autre
élève, localement, avec un PDF à moi, et ça fonctionne. Je vais essayer sur
l'élève sur la base de prod.

Je confirme que ça viens de la pièce jointe que nous avons utilisée. Qu'est-ce qui ne
fonctionne pas bien dans ce PDF. Est-ce un problème au moment de l'upload
(soucis réseau local ?) ou bien le format du fichier ?

Est-ce que le fichier est quelques part sur amazon ? Est-ce que je peux le
récupérer pour voir ?  Je viens de regarder avec
[S3cmd](https://github.com/s3tools/s3cmd) et la config S3 présente sur le
keybase, mais je ne vois pas de trace du fichier. Étonnant !

