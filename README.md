# lespiedsdanslecode.org

Mon site web, sous ~~jekyll~~ ~~Hugo~~ ~~rien du tout~~ ~~[Panash](https://framagit.org/yaf/panash)~~, un bricolage avec pandoc.

## Fonctionnement

Pour construire le site
`make build`

Pour déployer le site sur AlwaysData
`make deploy`

Pour lancer le site localement
`make run`

Attention, le `make run` utilise [http-server](https://github.com/http-party/http-server#readme)
