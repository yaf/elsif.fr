

Livre de Frédéric Lenoir

« La puissance de la joie »

Sitina aime bien Frédéric Lenoir, elle ajoute pas mal de ses ouvrages à la
maison. Je n'avais pas encore lu. J'ai pris celui-ci parce que je n'ai jamais
lu sur le sujet de la joie, je trouvais ça intéresant.

Il fait souvent référence à Bergson, je crois avoir entendu Pegguy en parler
également, je me dis que c'est peut-être quelqu'un que je pourrais lire ?


Une citation de Bergson :

> Le plaisir, écrit-il, n'est qu'un artifice imaginé par la nature pour obtenir
> de l'être vivant la conservation de la vie; il n'indique pas la direction où
> la vie est lancée. Mais la joie annonce toujours que la vie a réussi, qu'elle
> a gzagné du terrain, qu'elle a remporté une victoire : toute grand joie a un
> accent triomphal.


Sur l'attention et la joie

> L'attention nous éduque à la présence. Mais la présence va au-delà du simple
> fait d'être attentif. Elle est une attention qui engage tout notre être : nos
> sens, mais aussi notre coeur et notre esprit. On est attentif quand on
> regarder bien, quand on écoute bien, quand on goûte bien. La présence n'est
> pas seulement sensorielle. Elle n'est pas une forme de réceptivité ordinaire.
> Elle consiste à accueillir, avec générosité, le réel, le monde, autrui, parce
> qu'on sait qu'ils peuvent nous enrichir intérieurement, peut-être même nous
> procurer de la joie, mais aussi parce qu'on peut leur donner quelque chose en
> retour ; un apprentissage, une joie.
> 
> Ce qui fait la valeur d'une vie n'est pas la quantité de choses que nous y
> avons accomplies, mais la qualité de présence qu'on aura placée dans chacune
> de nos actions.

À propos de la quantité versus la qualité. La joie se trouve plutôt dans les
aspects qualitatif (évidemment).

> Pour que la joie puisse fleurir, ne restons pas dans cette constnte dimension
> utilitaire qui nous interdit l'ouverture et la disponibilité. La joie surient
> bien souvent quand on n'attend rien, quand on a rien à gagner.

Il évoque le consentement aux évènements de la vie, aux évènements quotidien.
C'est pas très clair pour moi, j'entends parlé de consentement dans d'autre
contexte.


> La liberté, c'est l'autonomie. Chaque progès sur la voie de la libération
> conduit à la joie.

Spinoza évoque 

> On ne peut bien s'accorder aux autres que si on s'est déjà accordé avec
> soi-même. Tous les conflits, quels qu'ils soient, proviennent des passions.
> Un être humain qui est parvenu à surmonter ses passions, à les transformer en
> joies actives, ne peut plus nuire à autrui. Il a vaincu en lui l'égoïsme, la
> jalousie, l'envie, le besoin de dominer, la peur de perdre, le manque
> d'estime de soi ou un trop grande estime de soi, bref, tout ce qui crée les
> conflits entre les individus et les guerres entre les peuples. **La recherche
> éthique individuelle de l'« utile propre » mène donc nécessairement à la
> réalisation du bien commun.**

Il parle aussi d'un certain Khalil Gibran et de « son Prophète » (référence
d'un livre ?) qui dirait : 

> Sur votre chemin commun, créez des espaces et laissez-y danser les vents du
> firmament.  Aimez-vous l'un l'autre, mais ne faites pas de l'amour une
> alliance qui vous enchaîne l'un l'autre.  Que l'amour soit plutôt une mer qui
> se laisse bercer entre vos âmes, de rivages en rivages.  Emplissez chacun la
> coupe de l'autre, mais ne buvez pas à une seule et même coupe.  Partagez
> votre pain, mais du même morceau ne mangez point.  Chantez et dansez ensemble
> dans la joie, mais que chacun de vous soit seul.  Comme chacune des cordes du
> luth est seule alors qu'elles frémissent toutes sur la même mélodie.  Offrez
> l'un l'autre votre coeur, mais sans en devenir le possesseur.  Car seule la
> main de la Vie peut contenir vos coeurs.  Et dresse-vous côte à côte, mais
> pas trop près : Car les piliers qui soutiennent le temple se dressent
> séparés, Et le chêne ne s'élève pas dans l'ombre du cyprès 

Plus loin, toujours sur la métaphore musicale

> S'accorder au monde, c'est entrer en résonance avec nos proches, la cité, la
> nature, le cosmos. C'est refuser de détruire la planète et de la piller,
> c'est entretenir des relations respectueuses avec tous les êtres sensibles.
> C'est, fondamentaliement, mener une vie éthiquement juste, mais, plus encore,
> c'est vibrer dans la joie de se sentir en harmonies avec ce qui nous entoure.


À propos du consumérisme :

> Le système se gripperait si nous cherchions notre joie ailleurs que dans les
> rayons des magasins.




