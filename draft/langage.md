# Langage

Quelques notes autour des lanages, comme contrainte pour aborder certains sujets, ou juste pour l'apprentissage.

> C and C++ taught me about computer memory. Lisp and Scheme taught me how to build up complexity from the simplest parts. HyperTalk and Lingo gave me my first successful experiences with UI programming. Objective-C showed me how to make great frameworks.

[source](https://twitter.com/kocienda/status/1100078264209436678)
