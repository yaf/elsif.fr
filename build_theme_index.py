import os

keywords = "keywords.md"

with open(keywords) as f:
    lines = f.readlines()
    for line in lines:
        content = [y for y in line.strip().split(':') ]
        keyword = content[0]
        directory = "content/" + content[0]
        filename = content[1].replace("content", "")
        htmlfilename = filename.replace(".md", ".html")
        article = content[2]

        if os.path.exists(directory + ".md"):
            os.remove(directory + ".md")
        if os.path.exists(directory + ".html"):
            os.remove(directory + ".html")

        fileindex=directory + ".md"

        with open(fileindex, 'a') as keywordFile:
            if keywordFile.tell() == 0:
                keywordFile.write('---\ntitle: ' + keyword.capitalize() + "\n---\n")
                print("tel 0 ?")
            print(keyword +  " article : " + article + " html : " + htmlfilename)
            keywordFile.write("- [" + article  + "](" + htmlfilename + ")\n")

        fileindexhtml=directory + ".html"
        os.system('pandoc --template=lib/template.html -c lib/pandoc.css ' + fileindex + ' -o ' + fileindexhtml)

