---
layout: post
title: OpenBSD Installation de Grub
---

  
Cela peut parraître un peu ringare à l’heure de la virtualisation tout azimut, mais pour avoir la possiblité d’effectuer quelque test sous windows, j’ai du installer un double démarrage sur ma machine pro. J’ai donc opté pour [Grub](http://www.gnu.org/software/grub/).


En 4.3 (et peut-être même avant) on dispose de grub dans les packages i386. Passons l’explication de l’installation sur les partitions.


Voilà qui devrait vous donner l’accès à quelque binaire Grub. L’étape d’après est l’écriture d’un bout de grub sur le premier secteur du disque:


*Remplacer donc le /dev/sd0c par votre disque*


Il faut ensuite créer un fichier de configuration pour Grub. J’ai opté pour l’écriture en partant de zéro, mais un exemple est disponible sous */usr/local/share/doc/grub/README.OpenBSD*


Ce fichier de configuration (étrangement) ce place dans */grub/menu.lst*, j’aurais préféré dans */etc/grub/menu.lst* voir un */etc/grub.conf* mais passons.


Voici donc un exemple, le mien, d’un *menu.lst* permettant de démarrer sous OpenBSD par défaut (au bout de 10 secondes) et sinon d’opté pour un démarrage sous Windows:


Bonne installation ;-)


*ps: j’ai une excuse bidon, mais l’environnement de dev propriétaire dont une partie de la boite est spécialiste ne marche pas sous OpenBSD, ni même GNU/Linux, c’est null, mais c’est comme ça :-(*

  