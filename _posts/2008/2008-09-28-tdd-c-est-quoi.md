---
layout: post
title: TDD C'est quoi ? (En ruby bien sur !)
---

  
*Voici un petit billet d’initiation au Développement piloté par les Test (dit <span class="caps">TDD</span> pour Test Driven Development) avec Ruby. Initialiement publié sur le site de l’association "RubyFrance*":http://rubyfrance.org

Imaginons que nous ayons besoin d’un petit objet nous permettant d’afficher un nom. En bon développeur, nous allons d’abord écrire notre test.


Executons le test:

<notextile>
Mince une erreur. Vous allez me dire, c’était couru d’avance, on a encore rien codé. Bien. Allons-y alors. D’abord nous allons ajouter le fichier contenant l’objet que nous allons créer.
</notextile>

Ensuite créons ce fichier:

<notextile>
Cela suffira largement pour empecher l’erreur précedente. C’est un point important dans l’univers <span class="caps">TDD</span>. Il ne faut rien faire de plus que ce que les tests nous demande. Cela rejoint également un autre concept: <span class="caps">YAGNI</span> (You Ain’t Gonna Need It).

Executons encore ce test:
</notextile>

Hmm, encore une erreur, mais cette fois ce sont les paramètres de notre objet qui pose problème. Bien, corrigeons notre objet.

<notextile>
Executons encore ce test:
</notextile>

Encore une erreur. Mais cette fois c'est la method nom qui est manquante pour MonObjet. Ajoutons la:

<notextile>
Executons le test (oui, en tdd, on passe notre temps à tester ! :-)):
</notextile>

Voilà qui deviens interessant. Cette fois, ce n'est pas une erreur, c'est un echec du test. La méthode "nom" ne renvoi pas la bonne valeur.
La situation d'echec dans le test unitaire est aussi appelé "la barre rouge". Et quand il y a une barre rouge, le principe est de la faire redevenir verte le plus rapidement possible (en ajoutant très peu de code voir en enlevant du code).

Modifions donc rapidement notre code pour répondre au besoin du test:

<notextile>
Doucement, doucement, je vous vois venir, oui j’ai mis une valeur en dur, executons le test (c’est barre rouge), nous en parlons juste après.
</notextile>

Voilà, le test passe. Nous pouvons maintenant parler. J'ai mis une valeur en dur dans la méthode "nom", cela vous dérange ? Et bien pas moi. Je répond ici au besoin exprimé dans le test. Mais je n'ai pas dit que nous allions nous arreter là ! Ajoutons un test pour bien préciser notre besoin.

<notextile>
Executons le test unitaire maintenant enrichi d’un test.
</notextile>

Forcement, avec une valeur en dur, cela ne vas pas. Faisons passer la barre au vert avant de discuter:

<notextile>
Executons le test:
</notextile>

Parfait ! Barre verte !

Bien, maintenant, on peut laisser étaler nos connaissance en ruby pour effectuer un petit refactoring:

<notextile>
Executons le test a nouveau pour être sur que ce refactoring n’a pas changé la donne:

</notextile>
Un des interêt de faire un développement piloté par les tests c’est de tendre une sorte de filet de sécurité permettant de donnée plus de courage, ou au moins de tranquillité pour effectuer le refactoring. Mais il existe bien d’autre avantage à ce mode de développement. Notamment celui de ne pas faire plus que nécessaire.

Les tests ainsi écrit, modifié, mis à jour permette de disposer à tout moment d’une documentation sur l’execution du programme.

  