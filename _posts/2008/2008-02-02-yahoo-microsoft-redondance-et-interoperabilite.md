---
layout: post
title: Yahoo! - Microsoft redondance et interopérabilité
---

Tout le monde en parle en ce moment. Je ne souhaite pas commenter le montant de la transaction, c’est bien trop irréel pour moi, ni même le choix politique de Microsoft pour contrer Google... Non moi ce qui m’interesse ce sont l’avenir des applications que l’on trouve chez l’un et l’autre de ces acteurs majeurs de la scène *informatique*.

Pour ce qui est de [Flickr!](http://flickr.com/) et [Del.icio.us](http://del.icio.us/) je pense que ça devrait aller. Micrisoft n’ayant pas vraiment d’équivalent, il devrait survivre. Mais que deviendrons les moteurs de recherche, les toolbars, les messageries instantanées, les webmails et autres services ?

D’un point de vue optimiste, on pourrais imaginer qu’une intéropérabilité entre tout ces systèmes soit mise en place, et qu’en même temps, ces produits **s’ouvre** .

D’un point de vue pessimiste, on pourrais imaginer que les meilleurs services de l’un ou de l’autres disparaissent et que seul le pire persiste. Hypotèse peut crédible...

Un point de vue intermédiaire pourrais être de voir  un remplacement des services d’une des deux firmes les moins performant, les moins utilisé par leur équivalent de l’autre firme.

Enfin bref, je suis globalement d’accord avec [Olivier Ezratty: c’est un mariage risqué](http://www.oezratty.net/wordpress/2008/un-mariage-risqu/) A surveiller donc, *pour le pire et pour le meilleur*.

*J’aime bien Flickr! et Del.icio.us, mais il se pourrais que je coupe ces services selon leur avenir....*


