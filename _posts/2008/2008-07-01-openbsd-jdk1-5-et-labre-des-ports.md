---
layout: post
title: OpenBSD, JDK1.5 et l'abre des ports
---

  
Bien que le projet [OpenJDK](http://openjdk.java.net/) porte doucement ces fruits afin de permettre la mise en place de l’environnement Java sous licence libre. Et bien qu’"OpenBSD":http://www.openbsd.org propose pour la 4.4 et en 4.3-current [un paquet pour la jdk 1.7](http://www.typouype.org/articles/2008/06/10/openbsd-et-openjdk). On a des fois besoin d’acceder à une plus ancienne version du <span class="caps">JDK</span>, j’ai nomé la 1.5 (assez courante dans les applications pas toute neuve ;-)).

C’est toujours possible dans OpenBSD, il suffit de passer par les [ports](http://openports.se/devel/jdk/1.5). Il faut également, pour des problèmes de licence, télécharger un tas de path supplémentaire après avoir accepter la dite licence.

Mais surtout, surtout ! ce qu’il ne faut pas oublier, c’est **de mettre à jour son arbre des ports** !!! Ca évite de ne pas comprendre pourquoi ça ne veut pas compiler, et pourquoi la version requise d’iconv est la 4.0 alors que la 5.0 à été trouvé sur la machine *grrrbbllll*

Alors pour mettre à jour l’arbre des ports, rien de plus facile:



Le -r est le tag [Cvs](http://fr.wikipedia.org/wiki/CVS) qui correspond à la version 4.3, quand on suit *-current*, il faut l’enlever.

Une liste des serveurs *anoncvs* est disponible sur le site officiel à l’adresse [http://www.openbsd.org/anoncvs.html#CVSROOT](http://www.openbsd.org/anoncvs.html#CVSROOT)

Et je peut vous dire qu’avec l’arbre des ports à jour, ça marche vachement plus meilleur la compilation de la <span class="caps">JDK</span> sur Open :)

**PS: A noter que la jdk 1.3 et 1.4 seront supprimer prochainement. C’est dans le [rapport hebdo sur l’état des ports dans l’arbre chez undeadly.org](http://undeadly.org/cgi?action=article&sid=20080701033934)**

  