---
layout: post
title: Typo Upgrade
---

  Ce blog tourne maintenant avec la version 5.1.2 du moteur de blog "Typo":http://typosphere.org/projects/show/typo (également appeler "typoesphere":http://typosphere.org/projects/show/typo). Comme d'habitude, du bon boulot.

*Bravo à l'équipe !*
*Merci Cartier Bresson !*

L'annonce officiel: "Typo 5.1 Cartier Bresson is out":http://blog.typosphere.org/2008/07/21/typo-5-1-cartier-bresson-is-out

Et pour compléter ce billet, je tiens à vous signaler/rappeler que l'équipe de Typo à mis en place "Redmine":http://www.redmine.org/ un gestionnaire de projet écrit en "RubyOnRails":http://rubyonrails.org et que vous pouvez trouver tout plein de thème dans le "jardin de typo":http://typogarden.org.

_D'autres nouveauté devrait arriver d'ici le 15 aout dans les parrages..._


  