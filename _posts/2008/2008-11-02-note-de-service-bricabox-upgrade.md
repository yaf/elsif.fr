---
layout: post
title: "Note de service: Bricabox upgrade"
---

La machine qui héberge ce site (entre autres) viens de passer en version 4.4 d'[OpenBSD](http://openbsd.org). Si vous lisez ce message c'est que tout c'est bien passé.

