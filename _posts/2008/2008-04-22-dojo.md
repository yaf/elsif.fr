---
layout: post
title: Dojo
---

  Lundi soir, comme apparemment presque tout les lundi soir, c'est rendez-vous au *dojo*. Non pas celui des arts martiaux, mais celui du développement. Pour rester *agiles*, l'association "XP-France":http://xp-france.org organise des rencontres au dojo.

Dans une salle gentillement fourni par "EpiConcept":http://www.epiconcept.com, des praticiens agiles se retrouvent pour un Kata voir un Randori.

J'ai passé une super soirée. J'ai découvert "Haskell":http://www.haskell.org/ un langage fonctionnel pur (c'est a préciser apparemment ;-)). J'ai vu des tests, et encore des tests et c'est beau. Vivement le prochain !

Si vous voulez en savoir plus: Voir "le projet Dojo":http://xp-france.net/cgi-bin/wiki.pl?LeProjetDuDojo sur le wiki de l'asso.

Moi je vais tenter d'y aller tout les lundi :-D


  