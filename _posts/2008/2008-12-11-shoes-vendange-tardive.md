---
layout: post
title: "Shoes : vendange tardive"
---

  La communauté s'active beaucoup sur "Shoes":http://shoooes.net (le microframework graphique en "Ruby":http://ruby-lang.org)  depuis un bon moment, et la nouvelle mouture nommé "Raisins":http://shoooes.net/about/raisins/ en montre une partie du résultat. Très impressionnant. De quoi faire de bien belle application:

* Un manuel intégré (Alt+? ou pomme+? pour les clavier mac).
* Un outil de création de paquet (là comme ça on se demande bien à quoi cela peut servir).
* Un loader de gem au cas ou votre application nécessite quelques gems pour son fonctionnement (bien surle chargement ne s'effectuera qu'une seul fois au démarrage ;-))
* Une sort de console pour la gestion des messages d'erreurs.
* Une méthode de téléchargement asynchrone (dans le style de XMLHttpRequest)
* La visualisation d'image distante
* L'utilisation de font externe
* De nouveaux effets pour les images, une gestion par block
* Et plein de petits truc en plus...

Avec l'outil de création de paquet, l'installation des gems et le téléchargement asynchrone, on voit déjà l'outil façon javawebstart en plus petit et plus jolie ;-) Ou alors une sorte d'application dans l'esprit de Rebol.


!http://zone.typouype.org/shoes-splash.png!:http://shoooes.net/about/raisins/

Merci encore "_why":http://whytheluckystiff.net/ , et merci à toute la communauté qui oeuvre pour shoes, vous faite un travail exellent ! :)




