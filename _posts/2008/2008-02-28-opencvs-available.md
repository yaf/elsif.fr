---
layout: post
title: OpenCVS Available
---


Depuis le temps que j’en entends parlé de celui là ! :)

> for now it is not yet 100% ready as a replacement for gnu cvs, but it is time to let people start using it and breaking it.

*From deraadt@*

Et bien voilà, OpenCVS est dispo. Alors je sais, CVS c’est *hasbeen*, et j’avoue être en train d’étudier quelque <acronym title="Distribued Software Configuration Management">DSCM</acronym> et plus particulièrement [Mercurial](http://www.selenic.com/mercurial/wiki/). Mais ça fait plaisir de voir des projets *Open* aboutir, enfin commencer, car comme le dit Théo, nous autres utilisateurs on va pouvoir commencer à l’utiliser et à le casser :)

Alors je ne sais pas encore ce qui sera définitivement adopté sur la Bricabox, mais je vais quoiqu’il arrive faire des petits essais avec [OpenCVS](http://www.opencvs.org) !

News officiel sur undealy.org: [OpenCVS is connected to build](http://undeadly.org/cgi?action=article&sid=20080228093414)


