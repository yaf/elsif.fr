---
layout: post
title: OpenBSD 4.3 - Chrooted SSH
---

  
Ca y est, on est à peine mi février que le tag 4.3-beta d’"OpenBSD":http://www.openbsd.org vient d’être posé sur la ligne.

Ca promet !

Et comme si ça ne suffisait pas, voilà qu’un petit *addon* apparait dans [OpenSSH](http://www.openssh.org): on peut chrooter sshd très très facilement.

*Je vais devenir gardien de prison moi :D*

Bien sur, on retrouve plus de détail sur ces news chez [undeadly.org](http://undeadly.org)

* [OpenBSD turns 4.3-beta](http://undeadly.org/cgi?action=article&sid=20080220201259)
	* [Chroot in OpenSSH](http://undeadly.org/cgi?action=article&sid=20080220110039)
