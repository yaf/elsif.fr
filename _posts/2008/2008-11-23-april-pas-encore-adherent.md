---
layout: post
title: April, pas encore adhérent ?
---

  Vous ne connaissez pas encore l'"April":http://www.april.org ?

<blockquote>
Pionnière du logiciel libre en France, l'April, constituée de 3069 adhérents (2854 individus, 215 entreprises, associations et organisations), est depuis 1996 un acteur majeur de la démocratisation et de la diffusion du logiciel libre et des standards ouverts auprès du grand public, des professionnels et des institutions dans l'espace francophone.
</blockquote>

Les objectifs de l'April sont simple:

<blockquote> L'April a pour objectifs de :
    * Promouvoir le logiciel libre dans toutes les sphères de la société ;
    * Sensibiliser le plus grand nombre aux enjeux des standards ouverts et de l'interopérabilité ;
    * Obtenir des décisions politiques, juridiques et réglementaires favorables au développement du logiciel libre et aux biens communs informationnels ;
    * Favoriser le partage du savoir et des connaissances.
</blockquote>

"Pour en savoir plus sur l'April":http://www.april.org/fr/association/

Plus le temps avance et plus la mission de l'April prend de l'importance. Pour pouvoir la mener à bien, l'association a besoin de nouveaux adhérents. Vous pouvez Adhérer toute l'année bien sur, mais vous pouvez aussi vous faire un beau cadeau de noÃ«l en soutenant l'April, le logiciel libre et donc vous !

h4.  "Adhérer à l'April":http://www.april.org/adherer?referent=Yannick+FRAN%C3%87OIS+%28yfrancois%29


!http://www.april.org/files/images/banniere_campagne-adhesion-objectif-5000-adherents.png!:http://www.april.org/adherer?referent=Yannick+FRAN%C3%87OIS+%28yfrancois%29


  