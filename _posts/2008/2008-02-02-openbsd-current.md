---
layout: post
title: Openbsd-current
---

  
C’est un peu barbare comme titre, mais vous allez comprendre.

[OpenBSD](http://www.openbsd.org) permet plusieurs utilisation, plusieurs *Flavor*:

* 
**-release**: c’est la version qui sort tout les 6 mois. Soit sur les CDs officiel, soit depuis la 4.2 par un cd42.iso disponible sur le ftp prêt de chez vous. (*nota: remplacer le numéro de version par celui qui vous concerne :)*)


* 
**-stable**: on parle de version stable quand on utilise une *saveur* release à laquelle on applique les patchs de sécurité que l’on retrouve sur la page errata de chaque version: [openbsd.org/errata42.html](http://www.openbsd.org/errata42.html). C’est tout.


* 
**-current**: C’est, comme sont nom l’indique, la version courante. Sachant que l’équipe OpenBSD ne commit rien sans une grosse phase de relecture/analyse, on ne peut pas parler de version instable comme pour certaine distribution linux (je pense par exemple au *saveur* stable, testing et instable de Debian). Current et **la** saveur la plus sûre, la plus avancé. On m’à même conseillé de l’utiliser en production.


Alors moi qui trainais en **-stable** depuis mes débuts, j’ai donc décidé de passer en current, sur mon *desktop* dans un premier temps. Il faut un peu de temps pour ce familiarisé avec les mises à jours plus fréquente de cette *saveur*. Mais comme beaucoup de personnes utilise -current, dans le lot, chacun a fait sont petit script qui va bien pour suivre facilement les fréquentes mise çà jour.

Plutôt que de refaire un nième script, on m’a conseillé d’utiliser Openbsd-binary-upgrade. Et j’avoue que je l’ai adopté facilement.

[Openbsd-binary-upgrade](http://www.xs4all.nl/~hanb/software/OpenBSD-binary-upgrade/) est le script mis à disposition par @ham qui permet moyennant une petite configuration (je vous met la mienne a titre d’exemple sur ma zone: [zone.typouype.org/openbsd-bin-upgrade.rc](http://zone.typouype.org/OpenBSD-binary-upgrade.rc)) de tenir sont système à jour. Un *hook* permet d’ajouter quelques commandes à executé en fin de mise à jour, très pratique pour mettre à jour les paquets déjà installé.

Me voilà en OpenBSD-Current donc. J’en suis toujours ravi, de jour en jour toujours plus :).

  