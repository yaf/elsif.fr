---
layout: post
title: déçu
---

  
Au vu du tarif, je ne vais finalement pas me rendre à

 l’"EuroBSDCon 2008":http://2008.eurobsdcon.org/ à Strasbourg. Tant pis.

 Je suis vraiment surpris
[par la grille de tarif](http://eurobsdcon2008.eventbrite.com/) :

 255 € ça fait un peu chère.

Peut-être que je me consolerais en allant au
[JDLL](http://www.jdll.org/) (c’est le même week-end, à Lyon).

  