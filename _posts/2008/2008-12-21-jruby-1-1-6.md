---
layout: post
title: JRuby 1.1.6
---

  J'ai raté ça, mais le 17 décembre, "Thomas E Enebo annonçait la sorti de JRuby 1.1.6":http://docs.codehaus.org/display/JRUBY/2008/12/17/JRuby+1.1.6+Released.

Cette version corrige pas mal de bug, notemment sur l'objet IO (qui ces dernier mois a été l'objet de pas mal de remonté de bug). Cette version prépare également le support de la version 1.9 de Ruby: le parseur est complet, la pluspart des objets du _core_, des _standard lib_ sont supporté.

Le projet est toujours aussi actif. C'est bon pour Ruby et c'est bon pour les développeurs. J'espère que les entreprises frileuses qui ont déjà adopté Java accepterons plus facilement l'utilisation de Ruby par ce biais.



  