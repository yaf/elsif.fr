---
layout: post
title: OpenBSD Current -> 4.3
---


Ah ben oui, un truc que j’ai oublié c’est que quand [on suit OpenBSD-current](/2008/02/02/openbsd-current.html) et que le [tag de la 4.3-beta](/2008/02/21/openbsd-4-3-chrooted-ssh.html) vient d’être posé, et bien on suit 4.3 maintenant :D

Du coup, petite modif du `/etc/OpenOpenBSD-binary-upgrade.rc` pour y préciser que l’on suit maintenant la version 4.3:

<pre>
  {% highlight bash %}
    BSDVERSION='43'
  {% endhighlight %}
</pre>

On vérifie aussi niveau mirroir que l’on pointe bien vers soit uniquement les snapshots soit directement dans les paquet 4.3 (enfin là tout de suite vaut mieux suivre snapshot uniquement, il n’y a pas encore de paquet compilé 4.3 officiel :))


<pre>
  {% highlight bash %}
    # rsync mirrors
    # -------------
    # rsync is even cooler. It will merge the changes in binary packages!
    FETCH='rsync -P -L --recursive --times'
    #
    # The mirror needs a trailing slash for the listing function to work.
    MIRROR='rsync://rsync.mirrorservice.org/pub/OpenBSD/snapshots/i386/'
  {% endhighlight %}
</pre>

*Dans mon cas j’ai choisi le mirroir rsync anglais, mais on peut prendre celui qui correspond le mieux bien. Je vous laisse voir la liste sur* [la liste officiel des mirroirs Rsync d’OpenBSD](http://www.openbsd.org/ftp.html#rsync)

Bon, maintenant le `/etc/OpenOpenBSD-binary-upgrade.rc` est nickel. On n’oublie pas non plus de modifier le `PKG_PATH`  dans notre `~/.profile` histoire de pouvoir continué à utilise les commandes `pkg_*`:

<pre>
  {% highlight bash %}
  export PKG_PATH=ftp://ftp.arcane-networks.fr/pub/OpenBSD/snapshots/packages/`machine -a`/
  {% endhighlight %}
</pre>


*Pour les paquets, y’a beaucoup plus de serveur. Dont 3 en france, un ftp c’est plus facile à mettre en place qu’un serveur rsync ? Bref, pour choisir, voici* [la liste des serveurs ftp officiel d’OpenBSD](http://www.openbsd.org/ftp.html#ftp)

Et voilà, un petit `sudo  OpenBSD-binary-upgrade` et c’est parti !


