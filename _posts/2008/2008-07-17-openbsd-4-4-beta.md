---
layout: post
title: OpenBSD 4.4 beta
---

  Quel honte, j'ai laissé passer l'information sans vous tenir au courant !

MÃ¶sieur "Théo":http://fr.wikipedia.org/wiki/Theo_de_Raadt a posé le tag 4.4-beta dans l'arbre CVS d'"OpenBSD":http://www.openbsd.org ! C'est tôt, mais apparemment le "Hackathon":http://en.wikipedia.org/wiki/Hackathon de cet année à permis beaucoup de bonne avancé.

Ce numéro de release sonne une grande étape. C'est le dernier numéro de la version initial des système "BSD":http://fr.wikipedia.org/wiki/Berkeley_Software_Distribution. L'équipe d'OpenBSD s'investi énormément pour en faire une grande version !

_"OpenBSD turns 4.4-beta":http://undeadly.org/cgi?action=article&amp;sid=20080702151935 sur "undealy.org":http://undealy.org évidemment._


  