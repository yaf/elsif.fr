---
layout: post
title: OpenBSD - USB Mount
---


Voici deux petit script shell pour les utilisateurs d’OpenBSD  et autres... Enfin uniquement ceux qui utilise ce genre de système sur leur(s) desktop(s).

Le montage/démontage (pas avec un tournevis hein, je dis ça pour Terckan surtout ;-)), en fait, c’est le fait de *rendre disponible un périphérique externe sur son OS*. Et de nos jour y’en a un paquet: Clef usb, baladeur numérique, apareil photo numérique, lecteur de carte, disque dur externe, imprimante, scanner...

[OpenBSD](http://www.openbsd.org) et d’autres OS utilisent le démons [hotplugd](http://www.openbsd.org/cgi-bin/man.cgi?query=hotplugd&sektion=8&arch=i386&apropos=0&manpath=OpenBSD+Current) pour *surveiller* l’activité de branchement du matériel à connectique <span class="caps">USB</span>. Sous OpenBSD (utilisateurs d’autres \*nix utilisant hotplug: attention, il y a peut-être des petites adaptations à faire) on modifie le */etc/rc.conf.local* pour y ajouter l’activation du démon hotplug:

<pre>
hotplugd_flags=""
</pre>

Voilà, avec ça, le démon nous remontera des infos sur la connexion de notre matériel. Dans le dmesg sous OpenBSD ça donne ça:
<pre>
umass0 at uhub1 port 1 configuration 1 interface 0 "USB 2.0 Flash Disk" rev 2.00/1.00 addr 2
umass0: using SCSI over Bulk-Only
scsibus2 at umass0: 2 targets
sd1 at scsibus2 targ 1 lun 0: &lt;USB 2.0, Flash Disk, 5.00&gt; SCSI2 0/direct removable
sd1: 4052MB, 516 cyl, 255 head, 63 sec, 512 bytes/sec, 8300032 sec total
</pre>

Et dans le log d’activité des démons (*/var/log/daemon*):
<pre>
Feb  1 20:27:39 libellule hotplugd[18399]: sd1 attached, class 2
Feb  1 20:27:39 libellule hotplugd[18399]: scsibus2 attached, class 0
Feb  1 20:27:39 libellule hotplugd[18399]: umass0 attached, class 0
</pre>

*hotplug* va donc appeler un script lors du branchement d’apareil, et un lors du débranchement: */etc/hotplug/attach*  pour le branchement et */etc/hotplug/detach* pour le débranchement. Ces deux scripts ne sont en général pas fourni, à chacun de ce les concocter.

On en trouve plusieurs sur le net, moi je suis parti d’un trouvé sur [undeadly](http://undeadly.org/) que j’ai un peu modifier. Vous les trouverez dans ma zone: [zone.typouype.org/attach](http://zone.typouype.org/attach) et [zone.typouype.org/detach](http://zone.typouype.org/detach).

Je risque très certainement de les retoucher encore (aahh le refactoring, quel plaisir ! :)), je tacherais de vous le signaler (si vous décidé de les utiliser, ça pourrais servir. D’ailleurs n’hésitez pas à me signaler les améliorations diverses que vous pourriez apporter dessus hein ! :)

En gros, ces scripts *monte* la ressources branché dans */mnt* puis crée un lien dans le */home* de l’utilisateur actuellement connecté au système (faudrais voir avec plusieurs personne loggué ce que ça donne :-/) dans un repertoire */home/${user}/mount*.

J’aimerais bien changer certaines chose rapidement comme les droits des fichiers ainsi disponible, les points de montages, et bien d’autres chose que je n’ai pas en tête.

Amusez vous bien :)

*edit: Correction des blockquote textile. Merci Zifro de m’avoir signalé les “bq.” qui trainait.*


