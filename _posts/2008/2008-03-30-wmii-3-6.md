---
layout: post
title: Wmii 3.6
---

  Cette après-midi, je me suis lancer dans la compile de "Wmii 3.6":http://www.suckless.org/wiki/wmii histoire de bénéficier des quelques nouveautées.

Je ne suis pas déçu :) Et dans le plus voyant, il y a maintenant la possibilité de faire quelque ajustement de fenêtre. Le mode _Float_ aussi gagne en facilité d'utilisation. Du coup, j'ai même du remettre un vrai fond d'écran (pas sur que je le vois souvent, mais bon).

Un gestionnaire de fenêtre à au moins essayer :)

"!http://farm3.static.flickr.com/2010/2375421000_dbc90684b5.jpg?v=0!":http://www.flickr.com/photos/yafra/2375421000/

"!http://farm3.static.flickr.com/2348/2375421002_9305eea5a5.jpg?v=0!":http://www.flickr.com/photos/yafra/2375421002/

_j'ai encore pas mal de truc à découvrir sur wmii. Il faut notamment que je regarde de plus prêt le binding ruby_


  