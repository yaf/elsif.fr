---
layout: post
title: Got new Shoes with Ruby
---


[Why](http://whytheluckystiff.net/) est une personne bien connu dans la communauté [Ruby](http://www.ruby-lang.org) pour ces participations dont la quantité et la qualité n’ont d’égal que l’originalité. Quand on voit contenu du livre qui l’a fait connaÃ®tre le [Poignant Guide](http://poignantguide.net/ruby/) (*dont une traduction en français est en cours, mais très délicate vu le nombre de formulation humoristique que contient ce livre*) on comprend que ma phrase est emprunte d’un grand respect envers ce monsieur.

C’est donc tout naturelement, pour un fan de Ruby comme moi, que je m’interesse régulièrement à ces trouvailles. Une des dernières a retenu mon attention: [Shoes](http://code.whytheluckystiff.net/shoes/)

Shoes est un *kit graphique* multiplateforme. Codé en Ruby (enfin pas que, y’a du C en dessous, comme dh’abitude), c’est un bonheur pour la réalisation de petits interfaces graphiques (je pense que pour le moment personne ne c’est lancé dans un *eclipse like* en shoes, avis aux amateurs :p).

Pour approfondir ce *kit* et surtout montrer un peu de reconnaissance au bonhomme, je me suis procuré son livre: [Nobody knows shoes](http://www.lulu.com/content/1365064) dont voici un extrait photographique:

![Ruby Shoes, la lib graphic tranquille](/files/ruby_shoes_book.jpg)

*Maintenant il faut que j’installe Shoes sur OpenBSD :)*

Comme vous pouvez le voir, cela m’a également permis de tester les services de [lulu.com](http://www.lulu.com) et j’avoue qu’ils n’ont rien à envier à Amazon. Bien sur, le catalogue n’est pas du tout le même, et heureusement. Mais je trouve que lulu, tout comme [in libro veritas](http://www.inlibroveritas.net/), apporte quelque chose de nouveau dans le domaine de la vente en ligne. Ici nous pouvons vendre ce que nous écrivons sans avoir à signer avec une maison d’édition: la publication ouverte à tous. Faire un site, un blog ou autre, pour proposer ces écrits, c’est bien. Mais pouquoi ne pas proposer le téléchargement d’un fichier <span class="caps">PDF</span> ou l’achat d’un livre (pour ce qui aime avoir une version *papier* ?). Pour cela, je trouve que ces deux services en ligne (il en existe d’autres surement, quelqu’un a des noms ?) sont trés utile.

**Bravo Why, Bravo lulu (en attendant de tester in libro veritas) !**


