---
layout: post
title: Apero Ruby
---

  
L’association [RubyFrance](http://www.rubyfrance.org) organise pour la 4ième fois une rencontre autour d’un verre pour parler de [Ruby](http://www.ruby-lang.org), [RubyOnRails](http://www.rubyonrails.org) et de tout ce qui peut toucher de prêt ou de loin à ce langage que nous aimons.

Rendez-vous donc à partir de 20h à [la Cantine](http://lacantine.org/). Une présentation de ce qui nous attend avec la version 1.9 de Ruby devrais être faites, et peut-être quelque *lightning talks* sur d’autres sujet (en rapport avec Ruby quand même).

A mardi donc ! ;-)

  