---
layout: post
title: Méthodes agiles
---

  Je viens de finir le livre de "Véronique Messager Rota":http://www.eyrolles.com/Accueil/Auteur/81735/veronique-messager-rota.php?xd=b199d9f0f9a187dade3afbe1c307e31e "Gestion de projet - Vers les méthodes agiles":http://www.amazon.fr/Gestion-projet-Vers-m%C3%A9thodes-agiles/dp/2212121652/ref=sr_1_1?ie=UTF8&amp;s=books&amp;qid=1207386961&amp;sr=8-1 .

Un très bon complément à mes précedentes lectures sur le sujet, ainsi qu'au divers informations lu de-ci de-là sur le web.

J'aimerais maintenant une chose, c'est pouvoir participer à un projet agile :-) En tant que développeur dans un premier temps, et pourquoi pas plus tard, avec plus d'éxpérience dans le domaine, en tant que coach agile.

Messieurs les coachs et autres chefs de projets, j'étudirais très sérieusement toute proposition de rejoindre une équipe agiles !


"!http://zone.typouype.org/gest_proj-methodes-agiles.jpg!":http://www.amazon.fr/Gestion-projet-Vers-m%C3%A9thodes-agiles/dp/2212121652/ref=sr_1_1?ie=UTF8&amp;s=books&amp;qid=1207386961&amp;sr=8-1

_Et je vous conseil la lecture de ce bouquin_


  