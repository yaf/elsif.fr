---
layout: post
title: Solution Linux 2008
---


Et oui, j’y suis passé, juste le dernier jour. J’ai aidé Jean-François (Underflow) a tenir le stand de l’association [RubyFrance](http://www.rubyfrance.org). Très sympa cette manifestation. Beaucoup de monde, pas mal de question sur Ruby et/ou Rails, j’espère que nous avons répondu à la pluspart.

Mon regret: ne pas avoir pris mon Laptop... A ne pas oublier pour la prochaine fois :)

Sinon, nous étions bien entouré, a gauche: php avec un teneur de stand qui est venu prendre des renseignement sur ruby pour s’y mettre :D, a droite afpy: le nez dans le code toute la journée dur dur de coder en python, en face [OpenBSD](http://www.openbsd.org) alors là, j’étais ravi. Disons que [Wim de kd85](http://kd85.com/) est venu de Belgique avec tout plein de truc dans ces cartons, du coup j’ai un peu bavé sur les soekris... Mais c’etais très agréable :)

La plupart des personnes rencontré était très sympa (elles le sont encore je pense :p). Y’a qu’un mec qui étais plus louche que les autres. Enfin non, illuminé serais le terme. Un vendeur de Baie informatique qui était venu voir des clients (dans la partie pro il y avait pas mal d’hébergeur). Il était paumé devant notre stand et nous avons du coup discuté. Il venais de découvrir ce qu’était le logiciel libre et je crois qu’il avait du mal à comprendre qu’autant de personnes puisse partager ainsi leur temps, leurs connaissances... Il n’en revenait pas. J’ai adoré cette rencontre avec un personnage ahuri devant l’ampleur d’une chose qu’il ne connaissait pas ! :D

A l’année prochaine surement.


