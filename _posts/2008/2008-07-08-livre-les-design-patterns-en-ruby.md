---
layout: post
title: Les design patterns en ruby
---

  C'est le titre de la traduction du livre "Design pattern in Ruby":http://www.amazon.fr/Design-Patterns-Ruby-Russ-Olsen/dp/0321490452/ref=sr_1_2?ie=UTF8&amp;s=english-books&amp;qid=1215551725&amp;sr=8-2 . J'en avais déjà parlé: "Ruby Design Pattern":http://www.typouype.org/articles/2008/01/13/ruby-design-pattern.

J'attendait de finir "L'art du beau code":http://www.amazon.fr/s/ref=nb_ss_eb?__mk_fr_FR=%C5M%C5Z%D5%D1&amp;url=search-alias%3Denglish-books&amp;field-keywords=l%27art+du+beau+code&amp;x=0&amp;y=0 un pavé magnifique, riche en information, pour le commander en Anglais, mais voilà, je n'aurais finalement pas à faire travailler mon cerveau dans la langue de Shakespear, un trio magique, bien connu des raillers surtout, j'ai nomé: Laurent Julliard, Mikhail Kachakhidze et Richard Piacentini ce sont occupé de la traduction !

Et bien messieurs, merci bien ! Je pré-commande la version française qui devrait être dispo d'ici la fin du mois de juillet, ROYAL !

!http://ecx.images-amazon.com/images/I/51vOlmC%2BWTL._SL500_AA240_.jpg!:http://www.amazon.fr/design-patterns-en-ruby/dp/2744022691/ref=sr_1_1?ie=UTF8&amp;s=books&amp;qid=1215552170&amp;sr=1-1


Les passionné de la "Programmation Orientée Objet":http://fr.wikipedia.org/wiki/Programmation_objet et/ou de "Ruby":http://www.ruby-lang.org seront comblé.


  