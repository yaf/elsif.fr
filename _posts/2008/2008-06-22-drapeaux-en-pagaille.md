---
layout: post
title: Drapeaux en pagaille
---

  En disant drapeau, je pense aux eternels *flags* qui font fureur dans beaucoup d'applications de gestion (et peut-être d'autres). On en met un par là pour dire que _ça c'est fait !_, un autre par ici pour signaler _c'est à faire_, un autre là bas pour un _peut-être qu'il faudrais s'en occuper_. J'avoue ne pas aimer du tout ce genre de donner. Et depuis longtemps une des question qui revient souvent dans mon esprit est *Mais pourquoi utiliser des flags ? Et comment pourrait-on s'en sortir sans ?*

Je pense que les flags sont là pour nous permettre de _mettre à plus tard_ un traitement, effectuer une sorte de désynchronisation. Soit pour dire _c'est à faire_, soit pour dire _c'est fait_. Alors pourquoi ne pas faire les choses tout de suite ?

Je pioche un exemple dans l'application sur laquelle je travail aujourd'hui: la facturation. Un facture est créée dans le système, on la stock dans la base. Jusqu'ici, c'est classique. Mais voilà, le système doit communiquer avec 2 voir 3 système externe (selon les filialles dans lesquels on installe l'application). Alors on lui colle des flags logique: _0_,_1_ ou carement des flags textuel _send_,_ready_,_send_. Le tout pour que lors de l'execution d'un batch, un peu plus tard dans la journée, voir à la fin du mois, le programme soit capable de savoir quelle facture il doit prendre en compte.

Mais finalement, pourquoi ne pas, au moment de la création de la facture, de son annulation ou tout autres évènement, créer des objets propre au batch devant s'executer plus tard ? Pourquoi ne pas faire les choses tout de suite ? On pourrais me dire: "Oui mais tu comprends ça fait créer une table pour chaque application externe et tout ça". Bah, aujourd'hui on se bat avec des flags à initialiser, à mettre à jour, à modifier sur chaque évènement, alors bon. Pourquoi ne pas travailler tout de suite sur une structure qui facilite l'execution du batch ? En plus cela découplerais la facture de notre système et l'image d'elle même que l'on doit envoyer aux autres (qui souvent n'est pas vraiment la même). On pourrais aussi du coup modifier, sans impacter le système courant, l'image que l'on doit envoyer quand l'application externe change de mode de fonctionnement.

Vous en pensez quoi vous ? Il y a beaucoup de flag chez vous ? Avez vous une autre idée pour s'en passer ?

_A chaque fois que ces flags sont sources de problème, je me lève dans l'openspace pour faire des signes, comme quand sur les portes avions, les petites mains font signe au avion avec des drapeau pour les remettre à l'horizontal :-), que je suis chiant des fois_


  