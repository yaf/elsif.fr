---
layout: post
title: Socket versus Port
---

  Quel est le plus performant ? Quel est le plus sécurisé ?

Aujourd'hui, nous utilisons principalement un frontal web qui redirige ensuite, au travers d'un bien souvent, les requêtes sur un serveur _d'application_. C'est le cas des techno Java avec les Tomcat et Glassfish, et par les techno Ruby avec Mongrels entre autres, mais même les petits nouveaux s'y mette: thin, ebb.

"Ebb":http://ebb.rubyforge.org/ justement est le serveur d'application auquel je m'interesse ces dernier temps, c'est apparement un des plus performants. Ce qui m'interesse également c'est la possibilité d'utiliser les _socket_ unix.

J'ai un peu de mal à mettre en place cette solution pour le moment, dès que c'est fait, je pourrais faire des test de performance.

Mais une question me harcèle: *Est-ce qu'il est _mieux_ d'utiliser les sockets ou bien les redirection de ports ?* En mettant à part cet histoire de chrootage.


  