---
layout: post
title: OpenBSD wicontrol
---

  _Pour vous evitez de vous prendre les pieds dans le tapis..._

Je ne suis pas un grand utilisateur de wifi... Mais j'apprend maintenant que j'ai un petit portable de moins de 2 kilos que j'enmène partout :D

Quand je cherche un peu de doc sur comment ocnfigurer le wifi pour OpenBSD, je tombe souvent sur des articles qui parlent d'une comande "@wicontrol(8)@":http://www.openbsd.org/cgi-bin/man.cgi?query=wicontrol&amp;apropos=0&amp;sektion=0&amp;manpath=OpenBSD+Current&amp;arch=i386&amp;format=html . Mais voilà, impossible de l'avoir sur ma machine. J'ai beau chercher dans les paquets, les ports, je ne trouve rien.

Finalement, a force de fouiller, je suis tombé sur une petite phrase dans la doc des nouveautés de la version 4.0 d'OpenBSD:

bq. La configuration sans-fil pour wi(4) est désormais totalement supportée par ifconfig(8). L'utilitaire wicontrol(8) est désormais obsolète et a été supprimé de l'arbre CVS.

("Mise à jour 3.9 -&gt; 4.0 : Application userland":http://www.openbsd.org/faq/fr/upgrade40.html#apps)

Ne cherchez plus, tout est sur votre machine :)

_ps: C'est quand même dommage que l'on ai toujours la commande disponible dans les pages "man" disponible en ligne_


  