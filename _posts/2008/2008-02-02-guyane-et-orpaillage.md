---
layout: post
title: Guyane et Orpaillage
---


Loin de mes sujets habituel, je vais quand même vous parler de [Guyane](http://fr.wikipedia.org/wiki/Guyane) et d’"orpaillage":http://fr.wikipedia.org/wiki/Orpaillage.

On n’etend pas souvent parler de la Guyane, sauf pour un lancement de fusée, et l’orpaillage, on a tout de suite une image du far-west en tête... Mais pourtant, ça se passe *en france* ou plutôt sur les territoires colonisé par la france il y a bien longtemps. C’est le livre d’une connaissance dont je veux vous parler aujourd’hui, c’est sont livre qui ma permis d’en apprendre plus sur un sujet que je ne connaissait pas, sur un sujet dont on ne parle pas, sur un sujet qui n’interesse pas (en métropole du moins): **L’orpaillage en Guyane**

[Axel May](http://guyaneetorpaillage.over-blog.com/) a ouvert un blog pour accompagner la sortie de sont livre sur le sujet. Un sujet qui lui tiens à coeur, ayant passé quelque année de ça vie dans la région.

Un livre bien écrit sur un sujet qui meriterais plus d’interêt de la part de beaucoup de personnes. C’est dur de *découvrir* à quel point certaines personnes, certains association de circonstances détruise petit à petit notre planête nouricière. Que se soit d’un point de vue écologique, d’un point de vue politique, d’un point de vue sociologique, le livre d’Axel nous ouvre les yeux sur un problème qui voit ces racines (en tout cas pour la Guyane) démarrer dans les année 1920, et qui ne fait que s’empirer depuis, et qui ne présente rien de rassurant pour l’avenir.

Plutôt que de vous recopier un extrait du livre ou le 4ieme de couverture, je vous invite à parcourir sont blog ainsi que l’article wikipedia traitant du sujet et apportant des informations plus *technique* (un bon complément au livre): [Orpaillage en Guyane](http://fr.wikipedia.org/wiki/Orpaillage_en_Guyane) (Axel fait parti des contributeurs de cet article d’ailleurs). Tout ceci vous donnera peut-être envie de vous procurer ce livre très interessant, qui bien que nous montrant les points négatifs de l’orpaillage en Guyane reste objectif et essaye de nous dresser au mieu l’état des lieux de ce phénomène.

Non mais vous y croyez vous des *cowboys en armes* et des histoires de *chercheurs d’or* à notre époque ? Moi qui pensais naÃ¯vement que le plus gros problème de l’amazonie c’etais la déforestation ayant pour but l’agriculture intensive et que le mercure n’était présent que dans nos vieux termomètres :-/

!(http://zone.typouype.org/axelmay_guyaneetorpaillage.jpg)

[Guyane Française L’or de la honte](http://www.editions-calmann-levy.com/livre/titre-249109-Guyane-francaise-l-or-de-la-honte.html) un livre à lire absolument !


