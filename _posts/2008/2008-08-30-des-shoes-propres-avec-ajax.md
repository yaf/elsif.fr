---
layout: post
title: Des shoes propres avec Ajax
---


Voilà que [Shoes](http://shoooes.net/) se pare d’une nouvelle fonctionnalitée: xmlHttpRequest. En gros, et pour reprendre sont exemple, vous pouvez lancer une opération de *download* tout en continuant à travailler...

Je vous laisse voir cela plus en détail sur le blog d'[hackety.org: threadedDownloadsInShoes](http://hackety.org/2008/08/15/threadedDownloadsInShoes.html)

Merci qui ? Merci [\_Why](http://whytheluckystiff.net/) !


