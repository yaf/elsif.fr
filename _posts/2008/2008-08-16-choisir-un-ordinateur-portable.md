---
layout: post
title: Choisir un ordinateur portable
---

  Quand on a l'habitude d'utiliser une bonne grosse tour avec un écran gigantesque, un bon clavier et de la place autour de tout ça, c'est dur de choisir un ordinateur portable correctement, voici quelque pièges dans lesquels je suis tombé, et comment j'ai eu l'occassion de rectifié le tir.

Il y a quelque mois, me voici dans _l'obligation_ de *choisir un ordinateur portable*. *Le contexte*, c'est important pour le choix d'une machine: un nouveau job dans une SSII qui m'amènera à me déplacer souvent.

Ok, alors il me faut un bon processeur et de la ram, il ne faut pas déconner, je vais quand même faire un peu de developpement sur cette machine ! Un bon disque dur aussi, les portable sont souvent équipé d'un disque qui tourne pas très vite, je vais prendre un 7200 tours/minutes, comme ça, ça va _envoyer la purée_. Il me faut un belle écran aussi, je vais me prendre un truc trop bien en utlra bright view wild très large avec tout plein de pixels affiché et l'écran qui brille ! Je me restreint à un beau 15 pouces en écran large quand même. Bien sur niveau connectique, il me faut tout, et le lecteur/graveur de cd/dvd aussi.

A tiens ils (dans mon cas, Dell) fournissent un sac à dos ? C'est original (_c'est là que ça aurait du me mettre la puce à l'oreille_).

Bon, je vous passe le prix de la configuration, et je vous montre ce que j'ai du coup (_fait_) acheter:

!http://farm3.static.flickr.com/2054/2146300114_15faecdfcb.jpg?v=0!:http://www.flickr.com/photos/yafra/2146300114/

_ps: C'est celui de droite bien sur_

Alors la super configue qui va bien, c'est sur, posé sur le bureau c'est top de chez top. Mais alors dès que je part en déplacement, c'est la misère. Je me retrouve avec une carapace de tortue géante sur le dos de *pas loin de 5 kilos*. Je ne parle même par de l'*autonomie, a peine plus de 2 heures*...

Au bout de 3 déplacements, j'ai commencé à raler sur mon choix... Une opportunité plus loin, et voilà que je peut changer de machine ! On me propose de refaire une commande !

Bien, cette fois je ne vais pas utiliser les même critères, mon contexte c'est le *mobilité*. Quitte à prendre moins puissant, avec un écran moins grand, il me faut *de la légèreté, un faible encombrement, de l'autonomie*

Me voilà cette fois dans le coin des 12 pouces de chez Dell, ou plutôt dans la catégorie des moins de 2 kilos. Voilà, je craque pour le petit là, oui oui le tout petit...

!http://farm4.static.flickr.com/3129/2762608879_7a0ea9d01c.jpg?v=0!:http://www.flickr.com/photos/yafra/2762608879/

_ps: toujours celui de droite hein :)_

Alors c'est sur j'ai pas un écran brillant machin truc qui fait 3 kilomètres de large. Jai pas de lecteur/graveur cd/dvd intégré, c'est un lecteur externe qui est fourni avec, d'ailleurs je le laisse à la maison en général. Mais le gros avantage, le grand _trip_ c'est quand je le glisse dans la house du MacBookAir (ben oui c'est ce que j'ai trouvé de mieux adapté) et que je met ça entre deux pochettes dans mon sac habituel. Avec ça et presque 4 heures d'autonomie, je suis tranquille, et la machine me suit partout ! :)

*Alors faite attention quand vous choississez un ordinateur portable. Prenez bien en considération le contexte d'utilisation de votre machine.* _Vous allez pas me dire qu'il n'y a que moi pour m'être planté de la sorte ?_

Pour information:

* La première machine que j'avais commandé est un "Dell Precision M4300":http://www1.euro.dell.com/content/products/productdetails.aspx/precn_m4300?c=ch&amp;l=fr&amp;s=bsd&amp;cs=chbsdt1 , une très belle machine, mais il ne faut pas avoir à trop ce déplacer. Pour l'heure elle fait le bonheur d'un collègue sédentaire :)
* La seconde est un "Dell Latitude D430":http://www.dell.com/content/products/productdetails.aspx/latit_d430?c=us&amp;l=en&amp;s=bsd&amp;cs=04 machine que j'utilise aujourd'hui depuis un gros mois et qui me convient très bien :)



  