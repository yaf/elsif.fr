---
layout: post
title: Yes! Another blogger
---

Et oui, voilà enfin quelque chose sur ce site.

J'ai mis le temps à me décider mais aprés tout un blog c'est sûrement le truc le plus facile pour faire vivre un site et partager certaines de mes humeurs / idées / points de vue et autres.

Alors petit à petit je vais venir raconter ma vie ;)

_A bientôt_

