---
layout: post
title: Wikimédia a besoin de votre aide
---

Et oui, apparement la Wikimedia Foundation lance un appel...

Tout le monde connais wikipédia, l'encyclopédie en ligne ouverte à tous, sans publicité. Que vous soyez contributeur ou simple utilisateur, cette encyclopédie mérite de pouvoir continuer à fournir un contenu toujours plus interessant.

Je ne m'étale pas plus étant un simple utilisateur occassionnel je ne maitrise pas les arcannes de ce projet, je vous renvoie donc sur la dites pages d'appel à l'aide : [Wikimédia a besoin de votre aide - Wikimedia Foundation](http://wikimediafoundation.org/wiki/Wikim%C3%A9dia_a_besoin_de_votre_aide)

