---
layout: post
title: Le calendrier libre
---

L'[April](http://www.april.org) à encore une bonne idée: la [création d’un calendrier](http://www.april.org/actions/calendrier2008/) :-)

C’est idiot comme ça, mais c’est une très bonne idée je trouve. Et comme c’est un calendrier libre, [les propositions d’illustrations](http://www.april.org/actions/calendrier2008/) pour chaque mois sont les bienvenues. J’ai hate de voir le resultat !

> Création du Calendrier Libre 2008
>
> L'APRIL vous invite à participer à la création d'un Calendrier libre 2008. Pourquoi un calendrier ? Parce que c’est un outil de communication sympathique, utile ("c’est quand les RMLL 2008 ?"), facile à décliner (impression, fond d’écran)...
> Pourquoi "libre" ? Parce qu’il aborde les thématiques qui comptent pour le logiciel libre, et parce que, étant sous licence libre, il peut être diffusé et partagé librement par tous et partout. Voir la suite de l’article pour savoir comment participer.

*source: [april.org](http://www.april.org/groupes/#ToC3)*

