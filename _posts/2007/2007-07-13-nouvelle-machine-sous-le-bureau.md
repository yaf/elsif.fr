---
layout: post
title: Nouvelle machine sous le bureau
---


Certain d’entre vous on déjà repéré les 7 différences [dans mon nouveau bureau-maison](http://www.typouype.org/articles/2007/07/11/nouveau-coin-bureau)

Et oui il y a bien deux écrans, et une nouvelle machine ! Et pour ceux qui se pose la question, non il n’y a pas de portable dans la sacoche a gauche (hein [jean-mi](http://www.lacomte.net) ;-) ).

J’ai donc craqué pour une machine en architecture x86. Plus personne ne vendant de PowerPC, je me suis rabattu là dessus.

Et dans la bête j’y ai mis quelque carte de ma selection, donc une carte réseau d-link. Ce n’est pas le plus important finalement, mais quelle fut ma surprise en découvrant ce dépliant dans la boiboite

![La boite dlink avec la version imprimé de la gnu gpl](/files/boite_dlink_gpl.jpg)

![Dlink fourni la GNU GPL en version imprimé](/files/dlink_gpl.jpg)

Oui oui ! C’est bien la <span class="caps">GPL</span> v2 ! C’est bon ça et on en redemande. Du coup j’ai fouillé les autres boites à la recherche d’une information du même tonneau, mais rien :(.

Et pour les plus curieux d’entre vous (hein jean-mi) le deuxième écran sert pour le mac mini qui est revenu sous mac os X en dépannage pour mon amie, sont portable aillant grillé. D’ailleurs si l’un d’entre vous sait comment je pourrais me procurer une carte mère pour un portable acer, on ne sait jamais, merci de me contacter (yannick *<span class="caps">DOT</span>* francois *AT* gmail *<span class="caps">DOT</span>* com).

*encore un billet bien en retard... je court, je court*


