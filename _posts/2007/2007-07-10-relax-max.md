---
layout: post
title: Relax max
---


*oÃ¹ quand un fan d’XML s’y remet enfin et découvre RelaxNg*

J’ai toujours aimé l’univers qui gravite autour d’"XML":http://www.w3.org/<span class="caps">XML</span>/ : [XPath](http://www.w3.org/TR/xpath20/), [XSLT](http://www.w3.org/Style/XSL/)... Mais je n’avais pas encore vraiment eu l’occassion de mis frotter plus que ça au travail. Enfin si, mais là ça atteint des sommets.

Je suis dans une boite dont le produit est basé sur <acronym title="eXtensible Markup Language"><span class="caps">XML</span></acronym>. Beaucoup, mais alors beaucoup de <acronym title="eXtensible Stylesheet Language Transformation"><span class="caps">XSLT</span></acronym>, du coup des XPath de folie, et bien sur une dose de Java/Hibernate/...

Malheureusement comme beaucoup de société dont j’ai croisé la route, personne ne prend le temps de faire les chose bien: la [DTD](http://fr.wikipedia.org/wiki/DTD) du fichier <span class="caps">XML</span> servant de base à tout un tas de chose dans l’application n’est pas à jour. Mais alors pas du tout. Du coup, je me suis plongé dans cette petite nouveauté (enfin pour moi) que j’avais vu arrivé, mais sur laquelle je ne m’etais pas encore penché: [RelaxNG](http://www.relaxng.org/)

Pour décrire la structure d’un document <span class="caps">XML</span> on a (classé par ordre d’apparition):

*
<acronym title="Document Type Definition"><span class="caps">DTD</span></acronym>. C’est un format interessant, mais non <span class="caps">XML</span>. Il a l’avantage d’être simple.


* XmlSchema. Une description de document au format <span class="caps">XML</span>. Là tout de suite ça deviens interessant: on peut le traité par le biais d’une machine. Pas mal de base de données et autres produit se sont lancé dans l’utilisation de ce genre de document pour la description de leur données. Les description sont très strict, et bonus par rapport au <span class="caps">DTD</span>: on peut typé les données.


* RelaxNG. Description de document en <span class="caps">XML</span> egalement, donc les même avantages que XmlSchema. Cependant c’est plus simple, et plus libre. On peut donc mieux décrire ces documents. En plus, pour ne rien gacher, on peut utiliser du XmlSchema dedans ! (ça peut être pratique)


Bon plutôt que de vous faire un tas d’exemple, je préfère vous faire lire ceux qui existe déjà et qui sont très bien: [RelaxNG Tutorial](http://relaxng.org/tutorial-20011203.html)

Si vous n’avez pas encore regardé XmlSchéma, sauté directement à RelaxNG, vous aller gagner du temps.


