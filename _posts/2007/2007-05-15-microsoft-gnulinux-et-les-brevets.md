---
layout: post
title: Microsoft, GNULinux et les brevets
---


Ou *quand microsoft fait de l’humour geek*

On a vu hier fleurir quelques news à propos de microsoft et des brevets que <span class="caps">GNU</span>/Linux et consorts violent.

[L’info sur le monde informatique.](http://www.lemondeinformatique.fr/actualites/lire-microsoft-veut-faire-payer-les-violations-de-brevets-par-l-open-source-22850.html)

Sans trop rentrer dans le détail, voici un petit topo. Apparemment 235 brevets serais violés. La firme de la fenêtre magique pousse même le vice à faire une répartition:

* 15 brevets violer par les application de messagerie
* 45 pour OpenOffice
* 65 pour l’interface utilisateur (alors par contre là il ne précise pas si c’est Gnome, <span class="caps">KDE</span>, twn, ion3, windowmaker ou autre flubox :-D )
* et (j’ai gardé le plus drôle pour la fin: **42 pour le noyau linux**.


Elle est pas bonne celle là ? Un bon poission d’avril en retard. Quelle bande de geekos chez Microsoft quand même, jusqu’aux avocats !

il est à noté que l’initiative [Show Us The Code](http://showusthecode.com/) qui a pour but de savoir quel morceau de code viole un quelconque brevet n’a toujours pas vu une ligne de code...

Un petit ajout pour la forme: j’espère que seul Novel et Dell signerons un accord avec Microsoft, c’est lui donner raison que de faire ça.

*Heureusement, je ne pense pas que [Theo de Raadt](http://fr.wikipedia.org/wiki/Theo_de_Raadt)  signe un accord entre [OpenBSD](http://www.openbsd.org) et Microsoft , ouf ;-)*


