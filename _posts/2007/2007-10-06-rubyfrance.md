---
layout: post
title: RubyFrance
---

  
[L’assemblée générale de RubyFrance](http://rubyfrance.org/articles/2007/10/04/assemble-gnrale-2007/) s’est bien déroulée.

Le CA accueille de nouvelles têtes, certaines ce sont éloignées (pas très loin). Nouveau président, nouveau vice-président, seul le trésorier reste accroché à ses sous :-p

Maintenant il faut aller les voir aux [JDLL](http://www.jdll.org/2007), mon petit doigt me dit qu’il y aura sûrement un stand RubyFrance là bas ;-)

  