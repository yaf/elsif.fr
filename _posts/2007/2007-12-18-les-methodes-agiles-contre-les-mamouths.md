---
layout: post
title: Les methodes agiles contre les mamouths
---

  
Dans mes reflexions sur les *méthodes* qu’il faudrais mettre en pratique dans bien des entreprises, je pense souvent aux [méthodes dites agiles](http://fr.wikipedia.org/wiki/M%C3%A9thode_agile).

Alors oui, c’est dur de mettre en place l’"extreme programming":http://fr.wikipedia.org/wiki/Extreme_programming,  plusieurs problèmes sont évidant: le code en binome par exemple. C’est pas facile, regardé autour de vous dans l’openspace qui sert de maison aux équipes de developpement: qui ne cherche pas a avoir la souris et/ou le clavier en main ? Vous en avez beaucoup des collègues qui quand ils viennent pour vous aider, ne vous prennent pas le clavier des mains ? Pas beaucoup pour moi, je dois pouvoir les compter sur les doigts de la main gauche (et ce sur plusieurs société).

Mais ça c’est juste une question d’habitude surement.

Le [Test Driven Development](http://fr.wikipedia.org/wiki/Test_Driven_Development) fais pourtant parti des base de l’extreme programming (à mes yeux en tout cas) mais même ça c’est pas facile à faire. Déjà il faut une technologie qui le facilite. Avec ma techno propriétaire en ce moment, ça serais pas facile à faire (mais j’essaie de trouver une idée pour quand même, na ! :p).

Pour toute ces raison, je regrette de ne pas avoir pu me rendre aux [rencontres agiles](http://www.lemondeinformatique.fr/actualites/lire-rencontres-agiles-la-preuve-par-l-exemple-24880.html) histoire d’entendre des retour d’expérience, de rencontrer des personnes ayant pu mettre en place de type de méthodologie de travail.

*Il faut que je surveille d’un peu plus prêt les activitées de l’association [eXtreme Programming France](http://xp-france.net/) moi.. Ca serais pas mal*

  