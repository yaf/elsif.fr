---
layout: post
title: OpenBSD Fondation
---

Quelle surprise ce matin en découvrant dans mes mails et dans mes fils [l’annonce de la création de l’OpenBSD Fondation](http://undeadly.org/cgi?action=article&sid=20070726015128).

Je n'avais aucune connaissance de la création de cette fondation. Mais c'est bien, c'est bon pour le futur d'[OpenBSD](http://www.openbsd.org) et de tout les projets qui en découle: [OpenSSH](http://www.openssh.org), [OpenBGPD](http://www.openbgpd.org/), [OpenNTPD](http://www.openntpd.org/) et [OpenCVS](http://www.opencvs.org) (que j’attend avec impatience !).

Voici l’annonce faite sur les listes OpenBSD:

> The OpenBSD Foundation is pleased to announce today it has completed its organization as a Canadian federal non-profit corporation and is ready for public interaction.
>
> The OpenBSD Foundation has been formed for the purpose of supporting the OpenBSD project, and related projects such as OpenSSH, OpenBGPD, OpenNTPD, and OpenCVS.
>
> In particular it will act as a single point of contact for persons and organizations requiring a legal entity to deal with when they wish to support OpenBSD in any way.
>
> The OpenBSD Foundation will initially concentrate on facilitating larger donations of equipment, funds, documentation and resources. Small scale donations should continue to be submitted through the existing mechanisms.
>

Que dire de plus si ce n’est: **Longue vie à OpenBSD !**

