---
layout: post
title: Le retour du retour du retour...
---


Finalement l’envie de bloguer me reprend. Après divers aventures je vais essayer de faire un résumé de ce qu’il c’est passé ces deux dernier mois *regarde la date du dernier billet affiché*  Ouch ! le 4 mars, oui c’est bien ça, même plus de deux mois.

* J’ai changé de boulot (je suis maintenant chez un petit éditeur de la région)
* La poumibox est devenue la bricabox et a maintenant son nom de domaine: [www.bricabox.info](http://www.bricabox.info). Elle héberge toujours le blog de Jean-mi [lacomte](http://www.lacomte.net) et le teaser qui n’en fini plus de Zifro avec son [zlab.fr](http://www.zlab.fr). D’ailleurs, y’a quelque éléments dont je reparlerais par rapport à cette box ;-)
* La bricabox toujours, esst enfin passé à la version [4.1 d’openbsd](http://www.openbsd.org/fr/41.html) qui apporte sont lot d’évolution. A noté l’exellent thème (pas trop dur de faire mieux que puffyx de la version 4.0): **Puffy baba and the 40 vendors** en référence aux éternels vendeurs de cartes et autres périphériques qui ne fournissent pas les spécifications matérielles permettant d’implémenter un pilote libre pour leur utilisation :(
* Nous avons aménagé la pièce qui nous servais de débaras pour y installer un bureau, un vrai :-)
* On a commencé à s’habituer doucement à la région (et à découvrir des coins sympa dont je reparlerais surement ici.
* Et j’en oublie surement (en 2 mois il s’en apsse des choses !) mais c’est déjà pas mal.


A bientôt surement, et désolé pour les quelques personnes (s’il y’en a) qui avait l’habitude de passer par ici de l’absence sans trop d’explication. J’ai eu un coup de démotivation pour ce blog...


