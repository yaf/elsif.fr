---
layout: post
title: Un parisien en province - Moins de stress
---


Cette fois je m’attaque à un autre point de la province... Je rappel que tout ceci est **mon** point de vue sur mon exode provincial. J’ai voulu partir de Paris et ça région pour voir, essayer, découvrir un autres univers. Certain partent plus loin, moi j’ai choisi le sud.

**Idée reçu: il y a moins de stress en province**

C’est vrai. Je ne sais pas pour les autres villes comme Toulouse, Lyon ou Marseilles, mais ici en tout cas, la vie est tranquille.

Mais quand on y regarde de plus prêt on comprend vite pourquoi.

J’habite en centre ville. A longueur de journée qui je vois dehors, en terrasse, oÃ¹ le soir à traÃ®ner dehors ? Des **étudiants** ! Je n’en ai jamais vu autant au mètre carré. Alors forcement pour eux la vie est cool, tranquille et quoiqu’on en dise sans stress.

Ensuite, y’a toute les personnes de la fonction publique pour faire tourner ces écoles, facs et autres lycée, et vu la quantité, il en faut. C’est bien simple, sur la dizaine de personnes que je connais ici, huit on leur conjoint dans la **fonction publique** (enseignant pour la plupart).

Ensuite il y a les **chercheurs**. <span class="caps">INRA</span>, <span class="caps">IRD</span>, <span class="caps">CNRS</span>, y’a de quoi faire sur Montpellier. Ils sont nombreux, sur la même dizaine de personne cité plus haut, 4 au moins travail dans le domaine de la recherche ici.

Il y a encore une catégorie d’habitant pas stressé de la région: les **chomeurs et autres rmistes**. Il me semble que l’hérault est une des régions les plus touché par le chomages. Et, petite anecdote au passage, on a dit à mon amie en entretien que recruter des commerciaux c’était dur car les chomeurs préfère rester au chomage ici. Un lien peut-être avec le salaire ridicule que les entreprises propose ici ? Sûrement.

Bref, toute ces personnes représente 75% de la population que je croise tout les jours (je pense), et leur *non-stress* est communicatif. Du coup personne n’est stressé ici. Ce n’est pas parce qu’il n’y a pas de transports (y’a de bon embouteillage ici, des bus qui n’arrive pas et il faut attendre le suivant, des trains qui ne passent pas non plus). Certains de mes collègues mettent une heure à venir au boulot.

**La contre partie que je n’aurais jamais imaginé**

C’est mou. C’est très très mou. On se fait vite prendre dans la spirale: envie de rien faire. La grosse flemme, et moins on en fait, moins on a envie d’en faire. Alors le week-end, le soir, c’est pas grave. Mais au boulot... quel manque d’efficacité ! Finalement, le *mouvement*, le *speed comme disent certain* que l’on a Paris me manque presque. Au moins ça bouge. Surtout que n’étant pas d’un naturel stressé, je n’ai jamais eu l’impression de l’être à Paris.

Ce qui est sur c’est que pour quelqu’un qui a envie de débrancher, de sortir du *circuit* , il faut partir en province.

Je crois que moi je préfère faire ça pour mes vacances, et pas être en vacances tout le temps. (oui ça fait un peut *teaser* c’est pas innocent)


