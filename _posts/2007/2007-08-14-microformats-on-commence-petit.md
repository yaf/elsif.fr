---
layout: post
title: "Microformats: on commence petit"
---


Depuis le temps que je les observent du coin de l’oeil, il fallait que je passe le pas.

Etant en train de travailler sur un nouveau design, j’en ai profité pour reprendre quelque éléments de la barre de menu.

**Mais les microformats c’est quoi ?**

D’autre l’expliquent déjà depuis bien longtemps. Je suis loin de connaitre parfaitement le sujet, mais je m’y met. En guise de point de départ pour en savoir plus, je vous colle ici la première phrase de l’"article de wikipedia sur les microformats":http://fr.wikipedia.org/wiki/Microformats

> Les microformats sont un langage de balisage qui permet l’expression de la sémantique dans une page web HTML (ou XHTML). Les programmes peuvent extraire du sens à partir d’une page web qui est balisée avec des microformats.

D’autres informations sont disponibles sur le site des [microformats](http://microformats.org/). Très enrichissant.

Plusieurs blogs que je lit régulièrement mon amené a réfléchir sur le sujet, même si je suis loin d’approcher à une conclusion complète. Je vous conseil leur lecture pour compléter tout ça.

* [Les petites cases](http://lespetitescases.net/index355)
* [Frederic de Villamil](http://fredericdevillamil.com/tag/microformats)
* [David Biologeek](http://www.biologeek.com/journal/index.php/Web-semantique)
* [Christian Faure](http://www.christian-faure.net/)


Il en existe sûrement plein d’autres.

Les microformats et le web sémantique sont-ils bon ou pas pour le web en général ? Je n’en sait rien pour le moment. Toujours est-il que faire une *hcard* dans la barre de menu ne m’a pas demandé beaucoup d’effort, et permet a divers outils de traité mes données personnels plus facilement.

Je commence petit, j’espère continuer tant que rien ne me fait penser que c’est idiots, stupide ou inutile.

*Il n’y a pas mon adresse complète, je préfère recevoir des mails plutôt que du courrier :-). C’est juste pour me situé géographiquement. De même je préférerais que l’on me contact via [Jaber](http://www.jabberfr.org/) ([article de wikipedia sur jabber](http://fr.wikipedia.org/wiki/Jabber) )*

