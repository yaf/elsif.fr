---
layout: post
title: "Reflexion : les bases de données"
---

Les bases de données et leurs gestionnaires (SGBD). Dans l’informatique de "gestion" c’est le commun des développeurs et autre administrateur. Une application sans base de donnée c’est rare, (disons que je n’en ai jamais vu). [MySQL](http://mysql.com/), [PostgreSQL](http://www.postgresql.org/) pour les bases les plus connu du monde libre, [Oracle](http://www.oracle.com), [Sybase](http://www.sybase.fr/) et [DB2](http://www-306.ibm.com/software/data/db2/) pour les commercials les plus connu.

Pour les applications de "gestion" je comprend l’interêt des base de données. Mais pourquoi doit-on utiliser une base de donnée pour un site web ? Après tout on fait du texte, on génère du HTML/XHTML. Et même en application de gestion, on map bien souvent les tables de la base de donnée avec des objets dit "metier". Alors à quoi ça sert une base ? C’est ridicule non.

On pourrais imaginer que les interfaces d’administrations des blogs génère directement une page, et qu’en cas de bessoin on puisse la modifier. Une étape de moins, page en static donc un poil plus rapide à obtenir. Bref, que des avantages, enfin une petite limitation pour la mise à jour, c’est surement un peu plus long de reparser une page que d’accéder à quelque données dans une base.

Pour des applications autres, ou des données autres, on pourrais même imaginer des sérialisation d’objet, plutôt que de les mettre dans une base de donné. Pour peut qu’on sérialisze en XML, l’objet sérializé dans ce format serais accéssible en lecture beaucoup plus rapidement, et surtout par n’importe quelle technologie.

En bref, je me pose la question: Pourquoi mettre en base de données derrière toute les données ?

Je reconnais quelque avantage tout de même. On stock une donnée brut et non ça forme (quoique, avec les chaset on peut avoir des surprises, mais passons outre en suppossant que tout est en UTF-8 par exemple :) ). On peut croiser les données plus facilement, plus rapidement (merci SQL). Peut-être un jour XQL permettras de stocker des données en XML plutôt qu’en base de donnée, il y a déjà XPath qui fait très bien l’affaire je pense... A tester.

Mais a coté de ça, les données sont physiquement stocké en format propriétaire   (enfin on sait que Mysql, quand on utilise le moteur MyIsam,stock les données en text clair). C’est donc un frein à l’intéropérabilité: on ne peut pas utilisé les Base de données contruite avec un [Système de Gestion de Base de Donnée](http://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_base_de_donn%C3%A9es) dans un autre. Genre on cré une base de donnée oracle et on s’y connecte avec PostgreSQL (Ou alors il faut m’expliqué, peut-être que j’ai raté une information quelque part).

Bref, pour le moment je continue à faire comme tout le monde, utiliser des bases de données pour stocker mes données, mais j’avoue que la question est pour moi en suspend depuis bientôt 2 ans...

*C’est une question qui me dérange beaucoup. Mon problème c’est que j’ai l’impression que peut de gens se la pose. Aurais-je tord ? C’est possible. Mais je vais continué à creuser... Pourquoi toute les techno XML (base de donnée XML, XSL) ne sont pas plus utilisé, pourquoi une base de donnée tel que [Xindice de la fondation Apache](http://xml.apache.org/xindice/) ne perce pas ? A voir...*

