---
layout: post
title: "Statelessness: mon job un point c'est tout"
---

Une évidence pour certain, et un point important mis de coté pour d’autres.

Un serveur web, appelons le Jo, est un peu égoÃ¯ste. En tout cas c’est ce que certains pensent.

Mais quand on y regarde de plus prêt, Jo n’est pas égoÃ¯ste, au contraire. Il nous sert sur un plateau d’argent tout les réponses à nos questions. Et il n’arrête pas, jamais. On lui en demande toujours plus !

> Donne moi ça (*From annonymous*)

> Comment ça tu ne trouve pas ça ! (*From annonymous*)

> Redonne moi celle-ci, et encore une fois, et encore une fois (*From Jean-louis l’agrégateur de flux qui veux tout savoir sur tout*)

Alors forcement, il a décidé de ne pas faire certaine chose: retenir le nom de tous ceux qui viennent lui demander quelque chose, retenir d’oÃ¹ ils viennent, à quel heure, comment étaient-ils habillé...

J’exagère, Jo fait une chose en plus. C’est surtout une protection pour lui, pour que l’on ne puisse pas l’accuser de ne rien faire de ces journées/nuits: Il note. Il note tout ce qui passe par là, tout ce qu’on lui  demande. Mais il y a tellement à noter...

Comme vous le voyez, Jo ne peut pas se souvenir de vous. Plusieurs personnes essaient de l’aider à garder la mémoire, un bijoutier, un éleveur de dromadaire, un charmeur de serpent, un conducteur d’éléphant, un cafetier, et un laveur de carreau. Mais Jo n’à pas appris à travailler comme cela. Et surtout Jo n’a jamais appris à discuter, juste à répondre aux questions. Alors a quoi bon vouloir faire de ces questions/réponses une discussion digne d’un salon branché ?

L’histoire est inspiré d’un billet bien réel publié par [Christian Fauré](http://www.christian-faure.net/) au sujet de l’aspect **statlessness du protocole http, et du web en général** intitulé [Pas de processus sur le Web](http://www.christian-faure.net/2007/08/10/pas-de-processus-sur-le-web/) . Je vous en conseil la lecture, surtout si vous n’avez rien compris à mon histoire d’indien... Mince, j’ai grillé Jo :-/ Enfin, ce qui est valable pour Jo l’est pour tout les autres travailleurs dans son genre.

*Je précise que je ne suis sous l’emprise de rien du tout, si ce n’est la fatigue peut-être...*


