---
layout: post
title: Cool URIs don't change
---

Et oui [Sunny](http://sunfox.org/) ce n’est pas

> “good links are eternal links” (ou qqchose du genre)

Mais effectivement quelque chose du genre [Cool URIs don’t change](http://www.w3.org/Provider/Style/URI)

Et c’est pour respecter cette règle que le sauveur d’URI(Uniform Resource Identifier) a encore frappé.

Il y a un moment maintenant, j’utilisais [Dotclear](http://www.dotclear.net/), un bon moteur de blog en [PHP](http://fr.wikipedia.org/wiki/PHP). Mais voilà. Depuis je suis passé à [Typo](http://www.typosphere.org) un moteur de blog en [Ruby](http://www.ruby-lang.org) on [Rails](http://www.rubyonrails.org). Les URI de l’ancien blog on été indexé, et du coup ne pointe plus sur rien. **Merci à Sunny de me l’avoir signalé**.

Même mieux que ça, ce sauveur d’URI perdu a poussé le vice jusqu’à me fournir l’[expression rationnelle](http://fr.wikipedia.org/wiki/Expressions_rationnelles) qui va bien pour rediriger les anciennes URI vers les nouvelles.

Du coup, les URI du genre :

[http://www.typouype.org/index.php/?2006/03/04/45-logo-debian-vs-logo-gnome](http://www.typouype.org/index.php/?2006/03/04/45-logo-debian-vs-logo-gnome)

(si vous en rencontré encore) sont maintenant redirigées au bon endroit :
[http://www.typouype.org/articles/2006/03/04/logo-debian-vs-logo-gnome](http://www.typouype.org/articles/2006/03/04/logo-debian-vs-logo-gnome)

Merci encore Sunny,

et n’oubliez pas **Cool URIs don’t change ! **

