---
layout: post
title: Happy 6th Birthday Haiku !
---


Cela fait un moment que j’en avais entendu parler. Ces dernières semaines j’avais passé un premier pas timide en récupérant la dernière image pour le tester dans Qemu.

Vous savez de quoi je parle ? Non ? Bon, un petit tour sur wikipedia, aller je vous aide: [Haiku OS](http://fr.wikipedia.org/wiki/Ha%C3%AFku_(syst%C3%A8me_d'exploitation)). Et oui, c’est un système d’exploitation. C’est la version libre de *feu* BeOS ([BeOS sur Wikipedia aussi](http://fr.wikipedia.org/wiki/BeOS)).

Ce système est intéressant sur plusieurs points.

* Il est libre. Pour moi c’est devenu un point important.


* Il est proche de ça première release officiel. Il y a donc pas mal de chose à faire encore, cela permet de pouvoir s’investir pourquoi pas, il y a de quoi faire


* Il a une approche original, reprise certes de BeOS, mais quand même: Il est orienté desktop ! Et ça, même si les Ubuntus et autres mandriva et OpenSuse n’ont plus rien à envié à Windows et MacOsX, ce sont des distribs Linux, plutôt conçu comme un serveur à la base.


Bon le troisième point est discutable, je le conçoit. M’enfin c’est toujours passionant de voir un OS nouveau (ou un nouvel OS ? A moins que ça ne soit SE nouveau :-p ). En plus j’aime bien le style :)

J’ai un handicap par rapport au fada de BeOS qui traÃ®ne autour du projet, forcement. Mais la communauté que j’ai rencontré jusqu’à présent me parait bien sympathique.

Bref, pour le moment je fais _mumuse avec. A suivre._

*Ca me branche plus qu’"OpenSolaris":http://www.opensolaris.org ;-)*

Ecran de démarrage:

![Ecran de démarrage](/files/haiku_start.jpg)

Haiku !

![Quelques fenêtre dans Haiku, la selection des apps](/files/haiku_screen_app_2.jpg)

Haiku encore :)

![Menu contextuel d'Haiku](/files/haiku_screen_apps.jpg)

Et surtout, quoi qu’on en pense, on reste polie et on souhaite tous un joyeux anniversaire à cette (autre) bande de fous qui on vu dans le libre un moyen de continuer à faire vivre leur passion !

![Bannière pour l'anniversaire du projet](/files/haiku_banner.jpg)

*[Jorge G. Mare (a.k.a. Koki).](http://myhaiku.org/) est à l’origine de mon information, il rappel les évènements dans son billet de [6ème anniversaire d’Haiku](http://myhaiku.org/2007/08/17/happy-6th-birthday-haiku/) . Merci à lui*


