---
layout: post
title: C'est pas de ma faute c'est historique
---


Combien de fois on entends cette phrase quand on est développeur: 1 fois ? 10 fois ? 100 fois ? trop souvent.

Il a bon dos l’historique hein, on lui en met plein la tronche. Alors oui, les absents ont toujours tord, et comme de par hazard, *historique* on le voit jamais lui. Mais il prend chère.

> Oui non, mais en fait tu as raison, mais c’est historique. *blablabla*
>
> Comment on fait avancer un soft si on traine ce boulet d’_historique_ ? Même absent on dirais qu’il fait peur.
>
> Tu comprends c’est *historique* alors je préfère pas changer ça.


> Attention c’est *historique* touche pas on ne sait jamais.

> Ah non c’est *historique* faut pas changer ça.

Stop.

Alors *historique* il faut le licencier lui, y’en a marre. On traine des boulettes d’architecture et c’est toujours à cause de lui. Alors table rase du passé, on met *historique* à la porte et je suis sur que l’on gagnera du temps.

Trop d’équipes sont pénalisé par lui (en plus il a plein de frère partout, car c’est pas la seule boite ou j’entends parlé de lui). Architecture bancale, code moisi, code procédurale écrit en avec un langage objet. Il nous fait toujours la totale lui.

Pourquoi les développeurs ont-ils peur de lui ? Sûrement à cause de ces anciens potes devenu DSI, Responsable technique ou autre *chef*. Hmm. Quel dommage. La culture agile n’est pas pour demain on dirais, on a besoin de sang neuf au poste de déscision, ou alors de gens qui on su couper les ponts avec *historique*.

Historique, c’est la plait du développeur.

*Je ne suis passé que dans très peu de boite oÃ¹ l’historique n’etais pas une plait car soit inexistant, soit mis de coté. Désolé pour le coup de sang, mais j’en ai marre de voir des aplpication qui pourrais être mille fois mieux faite, mille fois plus conviviale, mille fois plus pratique à utiliser pour les utilisateurs, mille fois plus en phase avec les besoins. Et en plus c’est même pas libre :’(*

