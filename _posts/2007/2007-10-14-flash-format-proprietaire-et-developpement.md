---
layout: post
title: Flash, format propriétaire et développement
---


Me voilà revenu avec [OpenBSD](http://www.openbsd.org). Je ne vais pas en parler en long et en large maintenant, un bon fond d’écran le fera pour moi :)

![Xfce openbsd](/files/xfce_openbsd_2.jpg)

Par contre, il faut savoir qu’Adobe ne fourni pas de player pour ce système d’exploitation...

Mais est-ce que j’en ai vraiment besoin ? On le sait flash, n’est pas bon pour l’accessibilité, le référencement, la légèreté... Alors c’est sur, on fait de jolie chose, toute brillante, avec de l’interactivité et tout ça, mais bon.

Alors qu’est-ce qu’il faut faire ? Aider le développement d’implémentation libre tel que [Gnash](http://www.gnu.org/software/gnash/) ou encore [Flash](http://www.swift-tools.net/Flash/) (Un projet disponible dans les *packages* [OpenBSD](http://www.openbsd.org)).

Mais est-ce qu’aider à la réalisation d’une implémentation libre ne conforte pas Adobe et les utilisateurs de flash dans le choix du format propriétaire, et surtout dans son **invasion du web** ? C’est sur, j’aimerais pouvoir accéder à tout le contenu web, et aujourd’hui, parce que je suis sur une plateforme non prise en compte par les propriétaires de flash je ne peut pas...

Cruel dilème pour moi. Je n’ai même pas installé une des deux solutions libre pour le moment, je crois que je le ferais quand je serais persuadé de leur *utilité*. J’irais même peut-être plus loin en essayant d’aider l’un de ces projets. Mais je ne sais pas... Est-ce *bien* ?

Si Adobe voulais bien libérer le format flash, je ne me poserais pas toutes ces questions. Si les utilisateurs de flash (je pense aux personnes réalisant des fichiers et autres sites flashouillés) arretaient de s’en servir je ne me poserais pas toutes ces questions non plus...

Dur dur la vie dans un univers vraiment :

> Free, Functional and secure

*devise d’OpenBSD*


