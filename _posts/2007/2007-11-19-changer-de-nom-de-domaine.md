---
layout: post
title: Changer de nom de domaine
---

  
Ces dernier temps, pas mal de changement ce sont produit dans ma vie. Une expérience en province: presque un an à Montpellier; mon retour sur Paris avec un cap à passer pour un nouveau boulot qui m’offre la possibilité de troquer mon status de développeur pour un autre (j’en reparlerais plus tard); l’envie de ne plus blogué en pseudo annonyme: je signe mes billets de mon nom.

Tout ça ressemble à ce que l’on appel souvent la crise de la trentaine. C’est bien possible. Toujours est-il que du coup, j’aimerais reléguer le pseudo *pouype* à des taches plus réduite (sur <span class="caps">IRC</span> entre autres). Du coup je me pose la question du changement de nom de domaine.

Cependant, comme les [cool uris don’t change](http://www.typouype.org/articles/2007/09/17/cool-uris-dont-change) je me demande si c’est bien raisonnable.

le nom de domaine [typouype.org](http://www.typouype.org) court jusqu’en avril 2008. Si je change maintenant, je pourrais faire tenir une redirection jusqu’à cette période, mais après ? Mes uris vont changer :-/ Cela me freine un peu dans ce changement. J’en ai envie, mais est-ce bien raisonnable ? Du coup j’aimerais bien avoir votre avis sur la question, oui oui, vous les quelques rare lecteurs de mes fautes d’orthographes et autres billet sur OpenBSD et Ruby. Alors exprimez vous, peut-être que cela m’aidera à me décider.

D’avance Merci

  