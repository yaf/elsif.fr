---
layout: post
title: Interview
---

Non non pas de moi, mais de [Christophe Porteneuve](http://www.tddsworld.com/blogs/eapc/index.php) est directeur de spécialisation SIGL à l’INSIA, c’est également un adorateur de [Ruby on Rails](http://www.rubyonrails.org/). Comment lui reprocher quelque chose en voyant ça, quelqu’un qui fait du [Ruby](http://www.ruby-lang.org) ne peut être mauvais, même si c’est via Ruby on Rails ;-). Cependant, j’ai découvert ce matin une [interview de lui sur le JDN](http://developpeur.journaldunet.com/itws/061213-itw-chat-web20-porteneuve.shtml) dont le titre ma fait tiqué:

> Le couple JavaScript/Ajax est une alternative souvent viable à ActionScript

*Le couple javascript/ajax *... Heu, AJAX ça ne signifie pas Asynchronous Javascript And Xml ? mince alors, on m’aurais menti ? Cependant je comprend, Ajax seul, ce n’est pas grand chose finalement, juste un mode de communication avec le serveur, il faut ajouter du javascript pour permettre une bonne utilisation d’une communication de type Ajax (notamment pour le traitement de la réponse).

Par contre pour ce qui est de l’alternative, je suis d’accord, bien que je pense qu’ActionScript ne serve aujourd’hui pas tout à fait au même chose qu’Ajax.

Bref, c’est une interview longue mais interessante, à lire donc.

*Aller aller, on ne se laisse pas aller, on repart sur la nouvelle année, au taquet :D*

