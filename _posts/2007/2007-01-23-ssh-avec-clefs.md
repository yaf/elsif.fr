---
layout: post
title: ssh avec clefs
---

Accéder à un serveur, avant ça se faisait par [telnet](http://fr.wikipedia.org/wiki/Telnet). Maintenant Telnet est utilisé pour tester les connexions d’un serveur HTTP ou SMTP (ou autres) mais surement pas pour faire de l’administration de machine, car telnet est simple, basique et transmet en clair toute les instruction tapé, y compris les mots de passe.

Pour un accès plus sécurisé, l’administration de serveur (ou la simple connexion) s’effectue par le biais d’[SSH](http://fr.wikipedia.org/wiki/SSH). SSH est un protocole de communication, [OpenSSH](http://fr.wikipedia.org/wiki/OpenSSH) (voir aussi le site officiel [OpenSSH.org](http://www.openssh.com/fr/)) est un ensemble d’outil libre permettant l’utilisation de ce protocole.

Bon on arrête là les liens vers wikipedia :p Le but c’est donc de ce connecté de manière sécurisé à une machine distante. Pour cela, la machine distante doit avoir un “serveur” (ou démon) ssh qui écoute sur un port (par défaut: le 22). La commande en question: `ssh login@machine`

Et oui, tout simplement aller voir la man page [ssh(1)](http://www.openbsd.org/cgi-bin/man.cgi?query=ssh) pour en savoir plus.

Ensuite il faut saisir un mot de passe. Quand c’est une fois de temps à autre, ça peut aller. Quand cela vous arrive souvent taper tout le temps des mot de passe peut devenir pénible. On peut y remédier simplement. Tout d’abord, il faut se créer une clé. La commande [ssh-keygen(1)](http://www.openbsd.org/cgi-bin/man.cgi?query=ssh-keygen) nous permet de créé une clé privée et une clé publique chiffré en utilisant soit un type [RSA](http://fr.wikipedia.org/wiki/Rivest_Shamir_Adleman) soit [DSA](http://fr.wikipedia.org/wiki/Digital_Signature_Algorithm)  (arf, encore des lien wikipédia :-p ). Prenons DSA qui apparement est plus sécurisé (je ne suis pas un expert en sécurité, si quelqu’un peut me confirmé ça ?)

<pre>
  {% highlight bash %}
    yafra@yeti:~$ ssh-keygen -t dsa
    Generating public/private dsa key pair.
    Enter file in which to save the key (/home/yafra/.ssh/id_dsa):
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /home/yafra/.ssh/id_dsa.
    Your public key has been saved in /home/yafra/.ssh/id_dsa.pub.
    The key fingerprint is:
    xx:33:xx:11:DD:aa:a1:33:dc:d1:ca:ef:09:fd:28:e6 yafra@yeti
  {% endhighlight %}
</pre>

En plus de préciser l’endroit où l’on souhaite placer les clefs, on peut préciser une phrase qui servira de confirmation de clef. Cette commande va donc créer une clé publique et une privée dans le répertoire .ssh (dans mon cas). On retrouve un fichier id_dsa pour la clé privée et un id_dsa.pub pour la clé publique.

Bien, maintenant on va ajouter la clé publique sur les serveur que l’on veux pouvoir contacter sans saisir de mot de passe. On vas utiliser la commande [scp(1)](http://www.openbsd.org/cgi-bin/man.cgi?query=scp) pour ça.

`scp .ssh/id_dsa.pub monlogin@monserveur:.ssh/`

On place notre clé publique dans le repertoire .ssh de notre utilisateur sur le serveur cible. Ensuite il faut se placer sur le serveur en question pour effectuer l’ajout de la clé dans la liste des cléfs autorisés:

<pre>
  {% highlight bash %}
    ssh monlogin@monserveur
    cat .ssh/id_dsa.pub &gt;&gt; .ssh/authorized_keys
  {% endhighlight %}
</pre>

Et voilà, le tour est joué. On peut maintenant se connecter en ssh sur cette machine en tapant simplement: `ssh monlogin@monserveur`

Et sans avoir à taper de mot de passe. La seul condition est d’avoir la clé privé correspondante placé dans le repertoire .ssh (ou autre en fonction de la configuration du client ssh, mais c’est une autres histoire).

*Ca faisait longtemps que j’avais pas posté. Faut dire qu’en se moment jesuis pas mal pris... tiens je vais d’ailleurs faire un petit top un de ces jours, dès que j’ai retouché quelque photos pour mettre avec :).*

*Pour ce qui est de ce billet, c’est pas bien compliqué, c’est des infos qu’on peut trouver sur pas mal de site, mais je pense que ça ne tueras personne que l’info soit disponible un peu plus :) et puis ça me fait plaisir de parler d’OpenSSH et de SSH en général, je m’en sert quotidiennement. Désolé pour ceux qui eventuellement ne comprendrais rien à tout ça (hein Terckan ;-) ).*

