---
layout: post
title: Avenir des applications web libre
---


En réponse au billet de David *Biologeek* sur [L’avenir des applications web libres](http://www.biologeek.com/journal/index.php/quel-avenir-pour-les-applications-web-libres) je vais ici essayer de poser mes idées, reflexions sur la question.

##Web et libre

Aujourd’hui, on utilise de plus en plus internet (ça c’est pas nouveau). Hier simple *présentation* de documents, aujourd’hui *participatif* qui sait ce que le web nous reserve demain ?

Bien souvent le web est associé au monde du logiciel libre. C’est justifié. Les logiciels libres se sont développés grace au web: La communication entre les équipes de dev, entre les utilisateurs, la rédaction de documentation et leurs mise à disposition, la distribution des logiciels et autres librairies.

De même qu’au début beaucoup de serveur était des unix propriétaires, puis des unix libres, le logiciel serveur de page était principalement Apache. Aujourd’hui microsoft et d’autre sont entré dans la danse et prolifèrent tranquillement. C’est normal, pourquoi pas après tout. Il faut du choix, c’est là la richesse d’internet.

Mais tout ceci n’est que plateforme matériel, qu’en est-il des services et des applications en ligne.

##Services et applications en ligne

Qui n’a pas déjà utilisé Wikipedia, gmail, flickr et j’en passe ? Toute ces applications/services en lignes sont accessible gratuitement (en tout cas dans un premier temps). Mais quid des licenses ?

La <span class="caps">GNU</span> Fondation a écrit une license pour ce type d’applications/services: la licence [GNU Affero General Public License](http://www.gnu.org/licenses/agpl.html) Mais qui l’utilise ?

Pour montrer leur ouverture, la plus part de ces services propose des APIs libre d’utilisation. Cela permet aux utilisateurs d’acceder à leurs données pour pouvoir les utiliser dans un autre cadre que celui proposé. Bien. C’est déjà ça.

Mais avez vous essayé de récupérer l’intégralité de vos mails placé sur les serveur google ? Pas évidant. A si, avec le protocole <span class="caps">POP</span>... Et qu’en est-il de votre carnet d’adresse ? Moi je n’ai pas réussi, j’ai tout recopié à la main. Et flickr ? Vous avez déjà tenté de récupérer toute vos photos d’un coup ? Je ne crois pas que ça soit faisable...

##Le libre

Les principes de bases du libre sont:

* Liberté d’utilisation
* Liberté d’observation
* Liberté de modification
* Liberté de distribution


C’est surement dans le désordre, mais les principes sont là. L’*utilisation* OK, sur internet, peu de personnes sont prêtes à payer un service (en général). **Observation** hmmm. Ok on peut voir le source de la page web, peut-être même récupérer quelque javascript et autres css, mais les scripts sur le serveur ? Celui par exemple qui retaille mes photos sur flickr ? Pourquoi par exemple il foire completement sur mes formats png ? (C’est peut-être réparé depuis mais bon...)

Avec le libre on gagne en fiabilité, en vitesse de correction. Car plusieurs yeux valent mieux qu’un. Pour l’exemple de *retaillage* des format png, peut-être que moi, ou un autre, en voyant ça nous aurions pu apporter une correction... Mais là, on ne peut pas.

## L’avenir du libre pour le web.

Les applications/services sous licence libre pour internet on tout interêt à exister, et ce pour plusieurs raisons.

D’abord pour les gens comme moi qui préfère, ou s’amuse à avoir leur propre serveur. Si j’accepte de payer l’infrastructure matériel (merci aussi au copropriétaires ;)), je ne me vois pas payer des licence pour l’utilisation d’un système d’exploitation, d’un serveur web et encore moi d’une application (de blog ou autres). Alors pour ce domaine, oui, il faut encore des Dotclear, Typo (de typoesphere, en ruby hein ;)), des awstats, et autres sendmail et radiant cms...

Ensuite, c’est pour une question de réelle liberté ainsi que l’augmentation du nombre de possibilité (le choix c’est une partie de la liberté non ? ). Aujourd’hui il existe des concurrents à gmail, flickr et consorts, mais qui propose les sources de son applications ? A part wikipedia, je n’en connais pas (j’ai pas beaucoup cherché non plus).

Pour ce développé un peu plus, les applications et services en ligne devrais être réalisé avec des fonctionnalité tel Jabber. Pour moi, le protocole Jabber et les implémentation libre de ce protocole (coté client mais surtout coté serveur) sont un des exemple à suivre pour la création d’applications/services en ligne libre.

J’entends par là des applications offrant en plus du services de messagerie, de partage de bookmarks, de partage de photos, une réelle possibilité de mise en relation de plusieurs serveurs.

Par exemple. J’ai en tête, et commencé à réaliser une application de gestion/lecture de mes flux rss/atom. Je ne suis pas entierement satisfait par celui que j’utilise actuellement: Netvibes. Je pourrais m’imaginer roi du monde et lancer mon système basé sur un business model publicitaire, ou bien à la flickr avec des fonctionnalité réduite poru la version gratuite et payante en location pour la version complète. Mais je crois que le mieux que j’ai a faire c’est une version libre, ouverte que chacun pourrais mettre en place sur son serveur, que chacun pourrais enrichir de fonctionnalité.

Mais surtout il faudrais que nos applications puissent communiquer entre elle pour se partager des informations. Attention, pas des informations sur les utilisateurs, mais plutôt sur un catalogue de flux rss disponible sur une même catégorie par exemple.

Imaginons encore une gallerie photos équipé d’un moteur de recherche de photos qui pourrais par un protocole simple et ouvert aller faire de la recherche sémantique sur tout les serveurs utilisant ce protocole !

Je pense qu’il faut batir des applications web libres communiquantes. Peut-être que cela ne s’applique pas à toutes les applications, mais le fait d’être libre permet au moins cela: avoir la possibilité de le faire, même après coup :)


