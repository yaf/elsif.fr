---
layout: post
title: Happy birthday !
---

  
Arf, je ne sais pas si c’est le bon format :-/

Quoiqu’il en soit, le blog [les formats ouverts](http://formats-ouverts.org/) fête ces [3 ans révolus](http://formats-ouverts.org/blog/2007/07/02/1252-3-ans-revolus). 3 ans que Thierry *il-voit-des-formats-partout* Stoehr nous en parle. Alors **bon anniversaire**, merci pour toute ces informations, anecdotes, décryptages. Et encore beaucoup des comme ça !

Merci.

*les formats commencent à m’obséder aussi, c’est grave ?*

  