---
layout: post
title: Changement de licence
---

  
Suite à la lecture de l’"article au sujet de l’utilisation des licenses Creative Commons sur le framablog":http://framablog.org/index.php/post/2007/06/30/Creative-Commons-statistiques j’ai décidé de modifier les licenses de mes photos sur [flickr](http://www.flickr.com/photos/yafra/).

Apparemment (et je le découvre aujourd’hui) il n’y a que deux version de license Creative Commons qui soit compatible avec le logiciel libre: la [Creative Commons BY](http://creativecommons.org/licenses/by/2.0/) et la [Creative Commons BY-SA](http://creativecommons.org/licenses/by-sa/2.0/) or moi j’avais opté pour la version avec un blocage pour l’utilisation commercial: [Creative Commons BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/2.0/deed.en).

Le temps de passer sur toute les photos dans la journée, et cela sera modifié.

  