---
layout: post
title: Nouveau coin bureau
---


En arrivant dans ce nouvel appart, on a *voulu* une pièce de plus. Forcement, n’ayant pas d’enfants à y installé, c’est devenu le coin bureau.

On a mis beaucoup de temps à l’installer. Au début débarras bien pratique, il a fallu vider les cartons quand même. Alors un peut de bricolage, un peu de ponsage, un peu de peinture, achat de chute de jonc de mer, et hop. Voilà le nouveau bureau !

![Nouvelle installation de bureau](/files/bureau_montpellier_2.jpg)

*billet bien en retard, ça fait quand même au moins 3 mois qu’on l’a fini ce bureau :-p*


