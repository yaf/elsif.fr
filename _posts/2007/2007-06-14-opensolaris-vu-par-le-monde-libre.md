---
layout: post
title: OpenSolaris vu par le monde libre
---


Libéré en parti et sous [CDLL](http://fr.wikipedia.org/wiki/Common_Development_and_Distribution_License) sur la fin 2004, la communautée [OpenSolaris](http://www.opensolaris.org/os/) grandi et ça commence à se voir.

Du coup, [Linus Torvalds](http://fr.wikipedia.org/wiki/Linus_Torvalds)  (Le papa de linux pour ceux qui eventuellement ne trainerais pas trop sur la planête libre) profite d’un fil au sujet des [license GPL V2 et V3 autour de linux](http://lkml.org/lkml/2007/6/9/11) (et justement de l’échange de code avec OpenSolaris) pour [casser un peu de sucre sur OpenSolaris](http://lkml.org/lkml/2007/6/12/232). Même si la fin du message est plutôt *positive*

> The *good* news is that Jonathan Schwartz actually does seem to have made a difference, and I hope to God he is really as serious about open-sourcing things as he says he is. And don’t get me wrong: I think a truly open-source GPLv3 Solaris would be a really really *good* thing, even if it does end up being a one-way street as far as code is concerned

[Jonathan Schwartz](http://en.wikipedia.org/wiki/Jonathan_I._Schwartz)  (Le patron de SunMicrosystem, OpenSolaris est issu des technos Sun et en fait parti) répond sur ce même fil à la première question. Cependant, il a utilisé sont [blog pour répondre à Linus](http://blogs.sun.com/jonathan/entry/one_plus_one_is_fifty), pour défendre l’idée qu’OpenSolaris et la politique de Sun autour de ce projet est bien orienté vers le libre, et ne surf pas sur une *mode* du logiciel libre.

Je ne partage pas forcement les critiques de Linus, a la limite je partage certain doute, mais je pense que Sun est de bonne foi. De plus j’avoue ne pas trouver ces arguments très fondé, ou seulement sur ces prédictions de madame soleil. De toute façon Linus n’est qu’un gros trolleur :-p

En parlant de trolleur...

Si je parle de toute ça aujourd’hui ce n’est pas seulement parce que je m’interesse au projet OpenSolaris (curiosité quand tu nous tiens...). Non c’est surtout à cause de l’apparition d’un gai luron, qui habituellement troll avec Linus. Un personnage dont j’apprecie beaucoup le travail: [Theo de Raadt](http://fr.wikipedia.org/wiki/Theo_de_Raadt).

Alors oui, bien souvent il troll avec Linus, oui bien souvent il n’est pas du tout diplomate, oui bien souvent il n’utilise pas les *formes* qu’il faudrais. Mais là, sur ce coup, il fait les choses bien, et j’ai été beaucoup surpris qu’il s’invite dans la discussion. Mais il la bien fait ! Il a fait un [commentaire au sujet des problème de documentation du matériel sun](http://blogs.sun.com/jonathan/entry/one_plus_one_is_fifty#comment-1181726726000).

Alors oui, ça n’a presque pas grand chose à voir. Mais en y cherchant bien... Je ne resiste pas à mettre quelque extrait commenté:

> oday I speak as the project leader for another set of open source projects — OpenBSD and OpenSSH. OpenSSH will be better known to your audience, as it is what they use daily to connect securely to and from their Solaris (or Linux) machines. OpenSSH killed telnet and rlogin, for those who still remember those mechanisms.

Là il troll bien fort, comme il sait le faire :). Mais il a raison, OpenSSH a bien tué telnet et rlogin (qui s’en sert encore ici ?)

> Two operating systems run on Sun’s latest PCI-e based (smallish) Ultrasparc-III machines, the v215/v245 — Solaris and OpenBSD. The latter system runs on those machines because the code to support the non-processor chips on the board had to be written after painstaking reverse engineering, because Sun refuses to make available documentation for how these chips are programmed.

Alors là, ça tue. Seul OpenBSD et Solaris fonctionne sur ces [sun Fire v215/v245](http://www.sun.com/servers/entry/v445/index.xml?intcmp=lnch09_v445_overview). Solaris c’est normal, c’est vendu avec je crois. Par contre OpenBSD fonctionne dessus grace au [reverse engineering](http://en.wikipedia.org/wiki/Reverse_engineering)  car Sun n’a jamais fourni la moindre documentation sur ces machines.

> There are two operating systems which surprisingly do not run on the Sun v215/v245 ( Linux and OpenSolaris. OpenSolaris?? Yes) Sun isn’t even open enough to give the OpenSolaris community enough documentation to support their new machines. So I think that Linus is right, and Sun has a long road ahead.

Le comble. OpenSolaris, le projet *d’ouverture* (c’est à la mode ce mot ;-)) de Solaris ne tourne PAS sur ces machines Sun !!! Et pour une fois on vois Theo en accord avec Linus ! Waaahooouu !

Le rapport est là. Sun souhaite et s’oriente (je veux le croire) vers le libre, mais doucement, trop doucement peut-être disent certains. C’est vrai qu’une chose qui pourrait être fait facilement et simplement c’est de fournir les specifications matériel à la communauté du libre. C’est facile et ça ajoute du crédit à l’action que mène Sun. J’espère que le commentaire de Theo  aidera a accélérer les choses (même si personnellement j’ai pas $15000 à mettre dans une machine Sun :-p)

Sacré Theo !

Même si je m’interesse à OpenSolaris, je dit: **Longue vie à [OpenBSD](http://www.openbsd.org)**

*Voilà un billet * people du libre *, j’espère que les non initié à cet univers auront appris quelque truc, hein Terckan :-p*


