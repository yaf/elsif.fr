---
layout: post
title: Standard ouvert pour l'europe
---

L’europe a, il y a quelque temps, mis en place un [cadre européen dâ€™interopérabilité des services paneuropéens dâ€™administration en ligne](http://europa.eu.int/idabc/en/document/3761) (Version 1.0, 2004, page 9) qui préconise les standards ouverts.

Cependant, certain organisme souhaite une révision de ce cadre et **c’est mal**.

Un petit extrait pour vous inciter à réagir:

> Cependant, Gartner recommende de ne pas se focaliser sur l’usage des formats ouverts pour eux même. Qu’ils soient ouverts ou non, les formats participent aux déploiement des services publics. EIF v2.0 devrait faciliter le(s) modèle(s) commercial(aux) générant le plus de profits financiers plutôt que ceux créant de la valeur publique en reconnaissant les droits de propriété intellectuelle si il y en a. Le support de formats multiples permet une migration vers les formats ouverts quand cela sera approprié sur le long terme.


Voilà pourquoi vous [Soutenir les formats ouverts en Europe](http://www.openstandards.eu/main-fr)

Et ça urge. Une consultation a été lancé, mais elle prend fin le **31 juillet**, faite vite.

