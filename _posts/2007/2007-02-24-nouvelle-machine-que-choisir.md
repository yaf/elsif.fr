---
layout: post
title: "Nouvelle machine: que choisir"
---

Oui j'ai beau [adorer mon mac mini powerpc](http://www.typouype.org/articles/2006/01/28/mac-world) j'ai quand même un problème pour le desktop. MacOsX est bien meilleur que windows, c'est sur. Mais j'aimerais être plus <b>libre</b> ;-). Cependant même si on peut trouver du [Debian PPC](http://www.debian.org/ports/powerpc/) qui fonctionne bien, du [NetBSD PPC](http://www.netbsd.org/Ports/macppc/) qui marche bien aussi (quoique, en essayant j'ai bousillé ma config d'openfirmware :-/ ) et surtout du [OpenBSD PPC](http://www.openbsd.org/macppc.html), le support pour les paquet applicatif laisse à désirer. Peu de paquets, mise à jour avec un à deux mois de retard... Bref, c'est pas la fête. Et depuis qu'Apple à décidé de ne plus distribuer ces machines avec des processeurs PowerPC ça vas en s'empirant... Même Ubuntu arrête de supporter officiellement cette architecture.



Du coup, pour ma machine "desktop" de tout les jour, j'aimerais passé à une architecture x86 qui me permettrais d'être <b>libre</b> tout en ne souffrant pas d'un manque d'application.


<b>
Une grande question cependant vient me freiner dans cette achat: Portable ou desktop ?</b>


Une chose est sur: si je part sur un desktop, ça sera pour un petit silencieux genre Shuttle, ou autres (macmini intel ?).



Quelques points clef de ma reflexion:


*
<u>Le prix</u>: je ne souhaite pas mettre plus de 700 euros. *Faut pas poussé mémé dans les orties*.
*
<u>La mémoire</u>: Il m'en faut 1g minimum.
*
<u>Mon écran</u>: J'ai actuellement un écran plat 17" donc :

* soit je le garde, avec une option portable, ça me fait un deuxième écran (mais bon c'est peut-être pas très propre, beau, pratique (rayez la mention inutile).
* soit je le garde, avec une option fixe, et ça me fait re-utiliser mon écran
* soit je le vend (avec option portable évidemment :-p), mais du coup je sais pas trop à qui, ni oÃ¹.


*
<u>Libre</u>: Peut importa l'option, il me faut un matériel supporté par tout les driver *libre*.
*
<u>Vente lié</u>: J'aimerais ne pas avoir à faire la démarche de récupération du prix d'un windows. Et puis vu le système DRMisé à fond, j'ai peur du maériel vendu avec :-(. Et là le problème c'est que les portables vendu par [Keynux](http://www.keynux.com/default_zone/fr/html/Prod_Notebook.php) par exemple, coute assez cher par rapport à mon budget.



Je n'ai pas encore fait mon choix, c'est délicat, pas facile. Je ne me déplace pas tant que ça, mais les rare fois ou je bouge, avoir un portable serais pratique (en plus je vais pas tarder à devenir [FONeros](http://www.fon.com/fr/) ça serais dommage de ne pas en profiter). Je dirais que la balance penche du coté du portable: après tout c'est quand même très pratique, et ça se répend de plus en plus. Et puis la classe de coller un sticker OpenBSD (ou FreeBSD ? ) sur un portable :-p.


