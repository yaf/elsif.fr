---
layout: post
title: OpenBSD 4.2 c'est déjà demain
---

L'équipe d'[OpenBSD](http://www.openbsd.org) sort une version tout les 6 mois. La version 4.2 sortira donc en Octobre 2007. Le developpement avance bien, et le  *freeze* de la branche ne devrais pas tarder. Je n’ai jamais vu de retard depuis que je m’interesse à OpenBSD, le 1er mai, le 1er octobre, la version est toujours dispo, bien fini.

Bon d’accord, Wim de [KD85](http://kd85.com/) (Fournisseur officiel de produit à base de poisson-qui-pique sur la Belgique et la France, voir plus ?) met un poil plus de temps à envoyer les CDs et autres bricole lié à une version d’OpenBSD. Mais on peut mettre à jour dès le 1er, à tout les coups.

Tout ça pour vous annoncé la disponibilité d’une [liste du nouveau matériel supporté par OpenBSD 4.2 chez les morts-vivants](http://undeadly.org/cgi?action=article&sid=20070824144802).

J’ai noté une novueauté qui pourrais faire un joyeux bricolage:

> [uts](http://www.openbsd.org/cgi-bin/man.cgi?query=uts&sektion=4) , a driver for USB touchscreens

Ca donne des idées non ? :-D

Enfin on pourra noter que l’équipe d’OpenBSD ne laisse pas tomber le PowerPC avec la 4.2  ! Contrairement à d’autre...

Enfin, je me répète, mais: **vive OpenBSD !**

![OpenBSD 4.2](http://www.openbsd.org/images/Marathon.jpg)

