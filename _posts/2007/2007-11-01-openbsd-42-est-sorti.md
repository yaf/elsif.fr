---
layout: post
title: OpenBSD 4.2 est sorti !
---


Ca y est, 1er novembre rime avec **OpenBSD release** (comment ça je suis un mauvais rimeur ? :-p).

A vos commandes !

[L’annonce sur undeadly canal historique](http://undeadly.org/cgi?action=article&sid=20071101030310)

La page officielle sur [www.openbsd.org/42.html](http://www.openbsd.org/42.html)

!(http://www.openbsd.org/images/Marathon.jpg)

Cette version est dédicacée à la mémoire d’un grand monsieur de l’IPV6: Jun-ichiro “itojun” Itoh Hagino [décédé le 29 octobre 2007](http://undeadly.org/cgi?action=article&sid=20071030220114)...

A noter dans cette version:

* toujours plus de drivers vraiment libre
* [cwm](http://www.openbsd.org/cgi-bin/man.cgi?query=cwm&sektion=1) rempalce wm2
* de grosses évolution sur [pkg_add](http://www.openbsd.org/cgi-bin/man.cgi?query=pkg_add&sektion=1)

La liste officiel de tout les changements: [openbsd.org/plus42.html](http://www.openbsd.org/plus42.html)

Bonne lecture et bonne installation !

