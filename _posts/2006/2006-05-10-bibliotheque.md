---
layout: post
title: Bibliothêque...
---

Juste un petit mot sur la pile d’arbres tranché qui traine sur le bureau et dans le salon...

Qui aurais put deviner qu’un jour j’ai autant de livres ? et qu’en plus c’est moi qui est décidé de les acheter, et de les lire ! Moi qui est tout fait pour éviter de lire, tout le temps, oui même au fameux bac français je n’avais pas lu les bouquins (bon j’ai eu 6 c’est peut-être pour ça aussi ;-) )

Je crois ne jamais avoir autant de livre avec des pages marqué (commencé à lire quoi) en même temps:

* Initiation à [OpenBSD](http://www.openbsd.org)
* Apprentissage de Rails via [RubyOnRails](http://www.amazon.fr/exec/obidos/ASIN/2212117469/qid=1147267212/sr=8-1/ref=sr_8_xs_ap_i1_xgl/402-6447091-8584914)
* Approfondissement de mes connaissance en “design pattern” via le bouquin [Design Pattenr tête la première](http://www.amazon.fr/exec/obidos/ASIN/2841773507/qid=1147267359/sr=8-1/ref=pd_ka_0/402-6447091-8584914) (Bon ça j’ai presque fini, de toute façon c’est histoire d’approfondir).
* Un bouquin sur l’économie des logiciel: [L’économie des logiciels](http://www.amazon.fr/exec/obidos/ASIN/2707138444/402-6447091-8584914?%5Fencoding=UTF8) histoire d’avoir un autre point de vue que celui que je me forge de l’intérieur. Celui là je l’ai commencé, mais c’est trés... comment dire, scolaire, non universitaire. Comme je n’y suis jamais aller...
* Et celui là m’attend mais bon... il prend la poussière: [Du bon usage de la piraterie](http://www.amazon.fr/exec/obidos/ASIN/291296959X/402-6447091-8584914?%5Fencoding=UTF8)

Sans compter que j’ai un bouquin sur les Unices libre, les BSD qui arrive d’ici la mi-mai.

Sachant que je n’arrive pas à lire dans les transports, du coup je me couche un peu tard en ce moment. *Comment ça j’ai une drole de tête avec des grosses cernes ? pfff n’importe quoi, je suis pas fatigué :-p*

