---
layout: post
title: OpenBSD 4.0 pre-commande
---

  

J'aurais pu faire un billet pour félicité [Larcenette](http://larcenette.free.fr/dotclear/index.php) d'être embauché par Lafraise, mais tout le monde en parle déjà :) (re-félicitation quand même). Mais je veux surtout vous annoncé (si vous le saviez pas déjà) qu'OpenBSD 4.0 est disponible en pré-commande !



Depuis longtemps OpenBSD utilise un rythme de livraison tout les 6 mois. On pouvais donc se préparer à cette disponibilité. Mais c'est toujours un evennement, même quand on le sait :D. Et oui, Ubuntu n'est pas le premier à s'imposer une livraison régulière :-p.



Par ici l'annonce officiel:
[OpenBSD 4.0](http://www.openbsd.org/40.html)


  