---
layout: post
title: Documentation d'Entreprise
---

Une petite reflexion comme ça, car je suis en train de poser en quelque mots une réflexion sur un problème soulevé dans mon entreprise, et je me pose la question des formats utilisé pour les divers documents dans l'entreprise: **Quel format utiliser ?**

Personnellement, même si sur la machine qu'on me fourni il y a la suite office, j'ai perdu l'habitude de l'utiliser. J'ai [Abiword](http://www.abisource.com/) d'installer, je préféèe donc utilisé ce traitement de texte simple, ne faisant qu'une chose mais le faisant bien !

Cependant, malgrés l'utilisation d'Abiword, je continue à utiliser le format propriètaire de word: .doc. Et ça, je n'aime pas du tout, abiword ou pas. Je préfèrerais largement utiliser des [format ouverts](http://formats-ouverts.org/) que je pense plus péreinne, plus sur, plus simple.

Finalement j'opte (et ce n'est pas la première fois) pour rédiger mon document au format [XHTML](http://fr.wikipedia.org/wiki/XHTML) que je pense ouvert (il l'est non ?) et qui surtout offre l'avantage d'être un langage de balise.

Pour moi il existe plusieurs avantage à utiliser ces formats dans l'entreprise, je vais essayer de lister ceux qui me viennent à l'esprit.

* **C'est un format portable** ceci implique qu'il fonctionne sur tout les machines, tout les systêmes. A priori toute les machines/systême on aujourd'hui un navigateur web, peu importe lequel d'ailleurs. Même les téléphone et autres PDA en sont équipé.
* **C'est un format ouvert**. On peut donc acceder à sont contenu sans s'acquiter d'une license.
* **C'est un format texte**: un simple éditeur de texte permete de le lire. C'est sur il n'y auras pas de mise en page, mais c'est faisable sans trop de difficulté
* On peut imaginer une feuille de style commune à tout les documents de l'entreprise, ainsi on à une **homogénisation de toute les présentations**.
* Il pourrais y avoir une DTD spécifique, qui enrichie la Dtd xhtml-strict (par exemple) pour y **ajouter des entité contenant certaines information de la société, ou bien des valeurs par défaut**.
* Le format est lisible très facilement par des progammes (parseur XML). On peut imaginer du coup que certaines documentation sur un produit puisse être **intégré directement dans le dit produit sous forme d'aide en ligne, d'assistance à la saisie**.
* Plusieurs possibilité pour la saisie du document. Les gens habitué à la création de page web pourrais faire ça via un éditeur de texte, les autres pourrais **utilisé un des nombreux outils de saisie qui existe sur le marché**.

Alors vous me direz: "mais il existe OpenDocument !", et vous auriez peut-être raison, je ne me suis pas encore penché sur ce format pourtant standardisé par l'[ISO](http://www.iso.org/iso/en/ISOOnline.frontpage) et l'[OASIS](http://www.oasis-open.org/home/index.php).
Il faut que j'y remédie, dès que j'aurais un peu de temps ;-)

Sur ceux, je retourne faire ma doc.

