---
layout: post
title: A propos du livre sur "l'économie des logiciels"
---

Ca fait un moment que je doit vous en parler. Ca tombe bien y'a une petite promo chez Amazon: [L'économie des logiciels](http://www.amazon.fr/gp/product/2707138444/sr=8-2/qid=1155245191/ref=sr_1_2/402-3957118-0140150?ie=UTF8&s=gateway), déjà qu'il n'est pas cher :-p

Pour commencer, on ma signalé que la collection Repères, dont fait parti ce livre, est une collection "étudiante". N'ayant jamais mis les pieds à la fac... Et effectivement, le style est très scolaire :). Ceci dit c'est globalement très interessant, surtout quand on est dans le millieu, mais je suis sur que des personnes s'interessant à l'économie en général, ou bien à l'informatique apprendras quelque truc ;-).

Plutôt que de me lancer dans un résumé, je vais vous citer quelque passage que j'ai trouvé interessant. Soit parce qu'il reflete une réalité que j'ai observé, soit simplement parce que le passage est interessant.


Voici pour commencer quelque chose que j'ai pu voir chez certains employeurs ou bien des clients:

> Sur des marchés où il est souvent crutial pour une entreprise de se positionner avant ses concurrents, la pression est très forte pour raccourcir la dernière étape du processus de developpement consacrée à la correction des erreurs, (...).

Il faut tout de même préciser que ça a tentance à disparaitre avec le temps. Mais en entreprise, tout est souvent plus lent.

La phase de test/homologation est souvent vue comme la cinquième roue du carrosse. Il m'est arrivé une fois d'être prété à l'équipe d'homologation. Je ne vais pas vous raconter l'histoire en détail, ça a durée un mois, et tout les jour apportais son lots de surprises :-/. Ce qui est important de savoir dans cette histoire c'est que l'application livré au client à la fin du mois de test n'etais même pas celle que nous avions trituré :-( Amusant non ?

Heuresement j'ai l'impression que ça change, les gens savent maintenant que livré un produit non fini, ou non testé est une source de problème énorme !

Une notion que j'ai apprise dans ce livre: l'occupation de terrain. En fait les companie vendent des logiciels non terminé pour occuper le terrain. Elles essaient de cette manière de prendre un monopole avant les autres. C'est ce qui c'est plus ou moins passé avec Windows, de Microsoft.

D'ailleurs il y a presque tout un chapitre sur Microsoft. Très interessant. En voici un petit extrait:

> La place acquise par Microsoft ne repose pas sur une supériorité qualitative de ses produits, mais sur le pouvoir, financier et technologique, que lui confère sa position monopollistique acquise sur les systèmes d'exploitation pour micro-ordinateurs.

Un autre petit extrait à propos de la réaction tardive de microsoft dans son positionnement sur le marché des navigateur

> Pour imposer son navigateur (Internet Explorer), Microsoft conclut des accords avec les fournisseurs d'accès à Internet et les constructeurs de micro-ordinateur, négocie avec les développeurs de sites pour qu'ils ajoutent des caractéristiques qui ne fonctionnent qu'avec son navigateur, puis fourni gratuitement Internet Explorer (...).

Nous savons bien ce qu'il en est à present. Internet Explorer a cessé d'évoluer, mais heuresement pour nous,la [fondation Mozilla](http://www.mozilla.org/foundation/) a réagi (je n'entre pas dans le détail, ce n'est pas le sujet), suvi par d'autres.

Un autre petit extrait à propos de Microsoft (je ne peut pas m'empecher :-p). Cet fois c'est au sujet des méthodes pour s'imposer sur des marchés dit de type *"Winner Take All"*

> La première de ces pratiques concerne la  manipulation des anticipations. De multiples moyens sont utilisés pour convaincre les utilisateurs du futur succès d'un progiciel, depuis des budgets de communication extrêmement élevés jusqu'à la pratique contestable du vaporware qui consiste à annoncer un produit qui ne voit pas le jour, ou à une date très éloignée de la date indiquée, afin de geler les ventes des concurrents. (...).

Il est tout de même précisé que ces technique ne sont pas l'apange de Microsoft, mais que Billou est ces compères on su les utiliser à merveille :D

Et on arrive enfin aux chapitres sur les logiciels libres. Et là, l'auteur cite un extrait d'un livre de Pierre Levy

> La création de logiciels peut-être considérée comme un art (...).

Et c'est vrai que chacun à son style, ça manière. Comme en dessin, ou en photo. Bref, peut-être une orientation pour uen prochaine lecture, j'aime bien cette petite phrase, surtout dans ce chapitre  ;-)

Quelques lignes plus loin une citation de l'auteur de [La Cathédrale et le Bazar](http://www.amazon.fr/gp/product/1565927249/sr=1-3/qid=1155248260/ref=sr_1_3/402-3957118-0140150?ie=UTF8&s=english-books) Raymond Eric:

> Le statut social n'est pas déterminé par ce que vous contrôlez, mais ce que vous donnez (...). L'abondance crée une situation où la seule évaluation possible de la réussite dans cette compétition est la réputation que chacun acquiert auprès de ses pairs (...). Les participants rivalisent pour le prestige en donnant du temps, de l'energie, et de la créativité

Bon je sais c'est une citation d'une citation d'un autre livre/auteur, mais je la trouve bien quand même :) Et de mon petit point de vue ça me semble assez vrai, non ?

Une petie dernière à propos de la situation actuellement modifié par le logiciel libre.

> En effet, le succès de certains logiciels libres a permis de rétablir une situation de concurrence sur des segments comme les systèmes d'exploitation ou les serveurs de base de données, où l'importance des rendements croissants d'adoption avait entraÃ®né une concentration monopolistique, avec des effets néfastes concernant la diversité et l'amélioration qualitative des progiciels. Tous les acteurs (y compris Microsoft !) reconnaissent que les logiciels libres contribuent à enrichir "l'écologie" du monde logiciel.

Je suis surpris, je ne saivais pas que Microsoft avais annoncé ce genre de chose. Ca à du être fait discretement peut-être ? :)

En gros un livre qui même s'il est orienté scolaire passe très bien. J'ai peut-être eu du mal à digéré le chapitre   3 qui est surement le plus économique. Mais ça vaut le coup de lire ce livre ;-)

J'espère que mon petit point de vue n'est pas trop indigeste :).

