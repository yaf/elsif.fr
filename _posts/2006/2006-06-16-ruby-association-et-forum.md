---
layout: post
title: "Ruby: Association et forum"
---

L'association Rubfr avance. Après les divers problème sur le Wiki, nous somme en train de migrer tout les articles (et d'en créer de nouveaux) toujours à la même adresse: [rubyfr.org](rubyfr.org).

En fait nous somme repassé sous [instiki](http://www.instiki.org/). Un wiki sous ruby évidemment ;-). Dans ça dernière version, le framework de mapping de base de donnée [activerecords](http://wiki.rubyonrails.com/rails/pages/ActiveRecord). Bien plus efficace. De plus nous sommes passé avec une base Postgres.

C'est toujours ne travaux, mais ça commence à devenir interessant ;-) Merci à [Fredix](http://geekoland.org/) qui est à l'initiative de cette migration.

D'un autres coté, c'est [Greg](http://greg.rubyfr.net/pub/) qui à mis en place [un forum](http://forum.rubyfr.net/) pour l'association et surtout pour tout les gens voulant converser autour de Ruby ;-). Un grand merci à lui également !

Ruby en france avance, RubyOnRails est une bonne locomotive pour le moment, mais Ruby avance aussi ;-)
  
