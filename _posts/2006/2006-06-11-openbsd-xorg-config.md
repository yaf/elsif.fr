---
layout: post
title: "OpenBSD: Xorg.config"
---


J’avais jeté l’éponge [et re-installé une debian](/2006/05/08/openbsd-debian) suite à des problème de configuration graphique principalement.

[Mais j’ai recement craqué](/2006/06/08/openbsd-installation-sur-mac-mini) et j’ai remis ça. Du coup j’ai passé un peu de temps sur ma configuration, un peu plus que la première fois finalement, et ça paie ! la preuve en image:

[![Screenshot du gestionnaire de bureau windowmaker](/files/windowmaker.jpg)](/files/windowmaker.jpg)

C’est que du bonheur, c’est bon pour apprendre, et windowmaker.. .j’en reparlerais surement ;-)

*pouype sous openbsd: To be continued.*

