---
layout: post
title: "Imaged-spam : help wanted"
---

Une bonne initiative qui ma fois devrais pouvoir nous débarraser d'un paquet de courriels "indésirables" ;-)

Snyff travail depuis un moment sur un plugin de spamassassin pour la détection de spam images (il y en a de plus en plus car c'est un des moyen trouvé par les spammeus pour contourner les outils anti-spam)


Sont projet est bien avancé, [mais maintenant il faut l'aider à remplir la base de connaissance de spam](http://bde.insa-rouen.fr/~lnyffene/blog/wordpress/?p=221) soit en faisant suivre les spams images reçu à l'adresse [imaged.spam.project@gmail.com](mailto:imaged.spam.project@gmail.com) soit (et c'est ce que je suis en train de faire) en laissant trainer cette adresse e-mail sur divers site (histoire qu'elle soit la cible de spam ;-) )

Faites un geste pour tous nous aidé, et merci à toi Snyff ;-)


