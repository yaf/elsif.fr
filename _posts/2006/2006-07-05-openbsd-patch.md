---
layout: post
title: "OpenBSD: Patch"
---

J'hésitais avec "Patcher n'est pas joué" comme titre mais bon.

Me voilà donc dans une nouvelle étape de ma découverte d'OpenBSD: la mise à jour. Contrairement à Debian et aux gnu/linux en général, pas de mise à jour de noyau en même temps que la mise à jour des applications. Pas de synaptic, .deb et autre rpm. Le système ressemblerais plus à Gentoo apparement, mais n'ayant jamais essayé Gentoo, je ne me permettrais pas de l'affirmer.

Donc, pour les BSD en général (je crois) et OpenBSD en particulier (j'en suis sur), le système devant passer un audit poussé, les applications sont mises à part, elles ne sont pas contrôlé par l'équipe d'OpenBSD. Elles ne bénéficie pas de la même niveau de sécurité garanti:

> we just do not have enough human resources to ensure the same level of robustness and security.

On en reparleras plus tard ;-)

Grace au site [www.openbsd101.com](http://www.openbsd101.com/) je me suis lancé en toute tranquillité dans la mise à jour du coeur de mon système. Il y a actuellement [3 patchs pour OpenBSD 3.9](http://openbsd.org/errata.html). J'ai donc suivi [les instructions d'OpenBSD101.com](http://www.openbsd101.com/patching.html), adapté à mon architecture PowerPC évidemment. Tout c'est déroulé à merveille. Xorg est un peu long à compiler sur un mac mini mais suffit de prévoir ;-)

Le patch, dit "manuel", est une des méthodes qui permet de garder son système à jour. [Cette page d'OpenBSD.org explique comment suivre Stable](http://openbsd.unixtech.be/fr/stable.html) mais outre le CVS, on peut très bien appliquer les patchs "à la main". Dans mon cas, c'est ce que j'ai préféré faire. Dans cahque patch est se trouve les instructions à effectuer pour appliquer le patch.

<pre>
  {% highlight bash %}
    # head 001_sendmail.patch
    Apply by doing:
        cd /usr/src
        patch -p0
  {% endhighlight %}
</pre>

Mais tout est très bien expliquer sur OpenBSD101 ;-)

Me voilà donc Stable ! Prochaine étape, bien comprendre le système applicatif (packages ET ports).

