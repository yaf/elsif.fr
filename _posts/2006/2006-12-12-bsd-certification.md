---
layout: post
title: BSD Certification
---


Si j’étais administrateur système et/ou réseau, si j’avais le temps, si j’étais bon en anglais...

Ca fait beaucoup de condition, mais une chose est sur, si je remplissais toute ces conditions, je me m’arrangerais sêrement pour passer une certification BSD.

J’ai découvert deux organismes (hum), disons une société et un organisme proposant des certifications BSD. La société, je dirais que c’est un peu douteux: [BSDCertification.com](http://www.bsdcertification.com). Cependant leurs buts affichés [sur la page mission](http://www.bsdcertification.com/mission.php) sont louable :).

Je préfère quand même l’aspect de l’organisme [BSDCertification.org](http://www.bsdcertification.org). Quand on voit les gens qui participent [sur la page people](http://www.bsdcertification.org/index.php?NAV=Meet%20Us), ça inspire un peu plus confiance en la qualité et le but de cette organisme.

*De toute façon je ne rempli aucune des condition citées en haut, donc je vais pas me torturer :p.*

