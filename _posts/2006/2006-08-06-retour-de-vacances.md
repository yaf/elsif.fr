---
layout: post
title: Retour de vacances
---


Et voilà, c’est terminé. C’était bien agréable, comme d’habitude.

Une première semaine en Croatie: voyage en groupe, nous étions 8 Français, et avons retrouvé sur place 7 Anglais. 2 guides pour nous accompagner, un Australien et un Croate polyglote. Ile de Lopud proche de Dubrovnik, le concept c’était de visiter les iles en kayak de mer. Très agréable, très joli, très original, mais bien sportif, surtout les passages d’ile en ile, dans une mer ouverte :) Faut avoir un peu de bras quand même.

Du coup la deuxième semaine c’était repos à Montpellier, comme d’habitude j’allais dire. On adore cette ville, d’ailleurs on va surement finir par aller y vivre.

Voici quelque photos, toutes ne sont pas de moi. Elles ne sont pas rangées ou classées, je vous laisse reconnaitre la Croatie et Montpellier ;-)

![Kayak en croatie, pause](/files/kayak_pause_croatia.jpg)

![Baignade](/files/kayak_plouf_croatia.jpg)

![Ile de Kolocep](/files/kolocep_croatia.jpg)

![Ile de Sipan](/files/sipan_croatia.jpg)

![Dubrovnik](/files/dubrovnik_croatia.jpg)

![Statue à Sète](/files/statue_set.jpg)

