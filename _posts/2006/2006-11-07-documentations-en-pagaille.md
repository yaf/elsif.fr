---
layout: post
title: Documentations en pagaille
---

Dans la série de lien qui traîne dans mes bookmarks au boulot, j’ai un petit blog anglophone trés intéressant : [smashing magazine](http://www.smashingmagazine.com/).

On y parle d’application web, mais coté construction. En fait je suis tombé sur ce site par le biais d’un billet qui [recense une série d’autres billets sous forme d’exemple ou de pense bête, classé par techno web](http://www.smashingmagazine.com/2006/10/30/cheat-sheet-round-up-ajax-css-latex-ruby/).

 C’est un blog nouveau pour moi, mais je vais l’ajouter à mes flux rss histoire de suivre un peu. On verra bien si c’est toujours aussi intéressant.

*Bon, j’ai bientôt rangé tout mes bookmarks @taf. Je ne me sert pas trop de [Del.icio.us/yafra](http://del.icio.us/yafra), je vais peut-être nettoyer aussi ceux que j’ai là bas.*

