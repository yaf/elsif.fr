---
layout: post
title: OpenBSD 4.0 released !
---

  

Ca y est ! Le grand jour est arrivé :). [OpenBSD 4.0](http://openbsd.org/40.html) est officiellement sorti. Cette nouvelle version apporte sont [lot de nouveauté](http://www.openbsd.org/plus40.html). A noter: toujours plus de driver libre, openSSH 4.4, [ftp(1)](http://www.openbsd.org/cgi-bin/man.cgi?query=ftp&apropos=0&sektion=0&manpath=OpenBSD+4.0&arch=macppc&format=html) supporte le https maintenant, quelques architectures supplémentaire (OpenBSD/armish, OpenBSD/sparc64, OpenBSD/zaurus), et bien d'autres chose encore.



Bon par contre je doit avouer que je suis pas fan du design de cette version, le style asterix, même si c'est pufferix ne me plait pas trop. Tant pis, j'acheterais un t-shirt openssh à la place :p.



*On souhaite bon courage à Wim Vandeputte et l'équipe de [kd85](http://kd85.com/) pour leur déménagement ET les envois de cette nouvelle merveille :p *


  