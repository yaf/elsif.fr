---
layout: post
title: Absence estivale
---

  

Suite à un petit pépin technique, je n'ai pas pu vous prévenir de mon absence de la semaine dernière. J'étais dans un endroit sans voiture, sans internet, sans pharmacie (pour dire) perdue au millieu de l'adriatique. Je vous en reparlerais plus tard :-)



Je ne suis que de passage, je repart pour le sud de la france pour la semaine. Je ferais un petit topo le week-end prochain, avec quelques photos ;-). Ou peut-être dans la semaine car cette fois c'est dans la civilisation que je vais, avec un hotel qui apparement bénéficie d'un accès wifi. On verras bien.



Je laisse tout ouvert, comme d'habitude, on verras bien en rentrant.
Bon courage à ceux qui travail.


  