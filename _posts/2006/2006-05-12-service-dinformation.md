---
layout: post
title: Service d'information
---

Et voilà encore un texte qui me fait réfléchir, et encore une fois ou je tombe d’accord avec l’auteur. C’est [Louis Naugès](http://nauges.typepad.com/) qui parle des [nouveau DSI](http://nauges.typepad.com/my_weblog/2006/05/le_dsi_nouveau_.html)

Il a raison, les DSI ne devrait plus être des Directeur des Systèmes d’Information, mais plutôt des Directeur des Services d’information. Le passif de la plus part des entreprise transforme le travail à effectuer en puzzle plus qu’en création. Les developpements sont maintenant plus réduit, et consiste en général à mettre en place des services simple en fonction des besoins exprimés.

Le problème viens peut-être du fait que changé la signification du sigle ne suffit pas, il faut aussi changer la façon de voir l’informatique d’entreprise. D’ailleurs, informatique d’entreprise ? mais pourquoi faire cette distinction ? Les contraintes ne sont pas les même ? Sur certain aspect oui. Mais avec les outils web disponible aujourd’hui, c’est cette informatique d’entreprise qui me donne l’impression d’être à la traine de beaucoup de wagon.

Personnellement je suis en train d’essayer de transformer une application existante pour justement la découper en petit bout. Pour donner un petit exemple, on trimbale aujourd’hui l’objet session (c’est du J2EE) jusqu’au couche d’abstraction de la base de donnée (des pseudo bean)... Elle est belle l’abstraction de couche. Mais ce n’est pas le sujet.

Je n’y avais jamais pensé mais je serais pour ce changement de signification, à condition (comme Louis le signal) que l’approche change avec.

Wait and see. My 2 cents.

