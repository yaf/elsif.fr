---
layout: post
title: Vim exploration de fichiers
---

  

Oui oui, entre [Vim](http://www.vim.org/) et [SciTE](http://www.scintilla.org/SciTE.html) mon coeur balance. L'un est d'une puissance exceptionnel et disponible par défaut (enfin en version VI simple) sur toutes les machines de type Unix, l'autre est simple, jolie et tout de même efficace. Mais aujourd'hui je vais parler d'un plugin de Vim que je trouve excellent: un l'explorateur de fichier



Ce qui est bien avec Vim (et les logiciels libre en général) c'est qu'il y a toujours une plusieurs personnes qui, pour notre plus grand plaisir, développent de tas de petites choses qui nous facilite la vie. Un explorateur de fichier dans Vim... Je ne pensais même pas que ça pouvait exister. Mais en fouillant dans les [tips de Vim](http://www.vim.org/tips/index.php) j'ai découvert ce [superbe plugins](http://www.vim.org/scripts/script.php?script_id=184).



C'est tout simple, il suffit de télécharger le tarball. Je vous conseil de prendre celui qui est disponible sur le site de Vim, y'a un suivi des versions. Cependant pour les gens pressé, j'ai mis à dispo une copie de la version que j'ai testé [dans la zone (vtreeexplorer-1.24.tar.gz)](http://zone.typouype.org/vtreeexplorer-1.24.tar.gz). Ensuite on décompresse le tout dans le répertoire .vim:

<blockquote>
$ cd ~/.vim
$ wget http://zone.typouype.org/vtreeexplorer-1.24.tar.gz
$ tar -xvzf vtreeexplorer-1.24.tar.gz
</blockquote>


Et voilà. Maintenant en ouvrant Vim, vous avez accès à la commande :Explore qui ouvrira dans la page courante la liste des fichiers contenu dans le répertoire courant. D'autres commande sont disponible, comme :Vexplore qui exécuteras la même chose mais en effectuant un vsplit auparavant. Les fans de la souris pourront naviguer avec elle, mais quelque raccourci clavier vous permettra d'aller plus vite :)



*Faut que je dépiote la doc maintenant, y'a pas mal de chose interessante... Puis me viens une idée aussi ou plutôt une question: des plugins Vim en ruby c'est faisable ? :D*


  