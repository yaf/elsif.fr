---
layout: post
title: Boulet Time
---

Voilà ce qui arrive quand on se lance tête  la première : une semaine sans pouvoir poster quoique se soit...

Passer sur un blog typo, à la limite, c'est pas génant, la partis admin est basé  toujours sur le même principe (avec quelque option en plus et "l'aspect" différent).

Par contre la techno utilisé derrière, ici Ruby on Rails ben forcement, quand on connais pas on fait des bétises. Alors Merci beaucoup à Arnaud de chez [typhon](typhon.net) qui me résout mes problèmes en un rien de temps.

Et promis Arnaud, j'ai commencé à lire le livre [Ruby on Rails](http://www.amazon.fr/exec/obidos/ASIN/2212117469/qid=1146216787/sr=1-1/ref=sr_1_10_1/402-6021202-3947362), j'espère qu'arrivé ) la fin je ne te dérangerais plus ;-)

