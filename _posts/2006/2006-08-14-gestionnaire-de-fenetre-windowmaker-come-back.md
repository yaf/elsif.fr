---
layout: post
title: "[Gestionnaire de fenêtre] WindowMaker come back !"
---

Site à mes [précédents problèmes avec Xfce](/articles/2006/08/13/xfce-consommation-cpu-dommage) j’ai donc basculer sous Windowmaker.

C’est tout le paradoxe des Unices libres (Gnu/linux et autres \*BSD). Y’a tellement de choix qu’il est dur d’en faire un. Dans le cas qui nous interesse, les gestionnaires de fenêtre: ils pullulent ! Pas un mois sans que j’en découvre un nouveau !

C’est le genre de discussion qui peut vite tourner au troll... :)

Déjà, il y a pour moi quelques grande familles: Les complets *clickodrome* qui sont très bien quand on à une bonne machine et que l’on ne cherche pas à savoir comment sont faites les choses en dessous. J’entends par là [Gnome](http://www.gnome.org/) et [KDE](http://www.kde.org/). Même si j’ai découvert linux sous KDE, j’ai une préférence pour Gnome aujourd’hui. Mais aucun des deux ne me convient car ils ne viennent jamais seul, regardez le nombre de Kmachintruc ou de Gbidulechose qui viennent avec ces deux là :-)

Ensuite y’a toutes les \*Box. Simple, leger, c’est surtout pour les passionnés de la console. [Fluxbox](http://fluxbox.sourceforge.net/), [Blackbox](http://blackboxwm.sourceforge.net/), [OpenBox](http://icculus.org/openbox/) et j’en oublie surement. C’est sympa, j’ai fait tourner un peu OpenBOX, j’avais trouvé ça agréable, simple. Mais ça ne me vas pas non plus...

Ensuite y’a les ovnis: [Ion](http://modeemi.fi/~tuomov/ion/) par exemple c’est un windowmanager qui n’utilise pas la souris. Bon j’ai pas vraiment essayer.

On pourrais les ranger dans les Ovnis, mais ils sont assez répendu, alors c’est plus des ovnis :-p. [Xfce](http://www.xfce.org/) j’en parles un peu dans mon précédent poste. J’avoue que dans les Gestionnaire un peu clickodrome, c’est mon préféré !.

L’autres Ovni c’est [WindowMaker](http://windowmaker.info) ! Basé sur GNUStep qui se veux l’implémentation libre de NextSTEP, base sur laquel Apple à pondu Mac OS X. Moi j’adore son coté décalé !. Je suis un fan, même si certaine fonctions *clickdromesque* me manque parfois (genre montage automatique de mon appareil photo). Mais  au vue de mes problèmes avec Xfce, j’ai décidé de m’y consacrer un peu plus !

[![Screenshot du gestionnaire de fenetre windowmaker](/files/windowmaker_green.jpg)](/files/windowmaker_green.jpg)

A vrai dire, j’ai déjà un petit quelque chose pour windowmaker en route, on reparleras très prochainement... ;-)

