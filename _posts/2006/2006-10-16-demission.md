---
layout: post
title: Démission
---


Je crois que je n'aurais jamais la [médaille du travail](http://vosdroits.service-public.fr/particuliers/F14.xhtml), vous savez, celle qu'on [donne aux personnes ayant passé plus de 20 ans dans la même boite](http://vosdroits.service-public.fr/particuliers/F10.xhtml).

Non c'est sur, 20 ans ça fait beaucoup. Je dit jamais, mais qui sais, peut-être qu'un jour je trouverais une boite qui me convient en tout point, me permet de m'épanouir et ce sur plus d'un an :).

En tout cas, ça ne sera pas pour cette fois. Je viens d'envoyer ma démission. J'ai déjà été licencié une fois (non pas pour insubordination ou autres, mais pour cessation de paiement delà part de la maison mère finnoise: découverte des prud homme), et démissionné deux fois pour changer d'entreprise. Mais cette troisième à un goêt particulier: je démissionne sans avoir de promesse d'embauche signé. Un risque, c'est sur, mais je suis sur de moi, enfin, j'ai assuré mes arrières (je vous en parlerais si j'ai besoin de faire jouer ce joker, mais pas avant ;-) ).

En plus du "risque" prit, cette démission sonne le début de la fin de ma vie <del>Parisienne</del> banlieusarde. Etant né ici, ayant grandi ici, ça me fait bizarre, même si je suis trés motivé pour tenter l'expérience. C'est une sensation trés étrange, mais un poids de moins sur mon estomac. Je m'engage enfin concrêtement sur la voie du départ.

*Bon maintenant il faut que ça bouge au niveau des propositions d'emploi, histoire de que j'arrive à manger avec appétit de nouveau :p.*

