---
layout: post
title: Aux utilisateurs de Flash
---

C'est beau le vectoriel, j'aime bien aussi cette techno. Mais pas avec Flash. Fermé, ce format ne me plait pas. D'autant plus que du coup il ne fonctionne pas sous toute les architecture. Par exemple, que ça soit avec mon ancienne Debian ou mon OpenBSD du moment, ben je peut pas le lire. Il n'existe pas de plugin flash pour firefox pour linux (ou bsd) pour les architecture powerpc. Alors si on prend un netbsd qui *marche même sur un grille pain* je vous raconte pas la faible couverture de l'engin.

Et pourtant on en vois de plus en plus. Le petit plugin pour écouter les podcasts, pour visualiser une vidéo. Le site [lafraise.com](http://lafraise.com) en est malheuresement équipé ce qui m'empêche de voter pour les visuels (malheuresement pour moi, mais heuresement pour mon vote, au boulot je suis sous windows.)

L'exellent site [qui parle des format ouverts](http://formats-ouverts.org/blog/) frappe encore une fois. Effectivement il y a encore une agence web qui à cru bon de faire un site tout en flash, [et les formats ouverts nous explique encore une fois pourquoi ce n'est pas bon du tout](http://formats-ouverts.org/blog/2006/06/19/848-ten-mobilecom-le-site-web-qui-n-existe-pas-a-cause-de-son-format). A noter la petit pointe d'humour puisque Ten-Mobile.com (puisque c'est d'eux dont il s'agit) a le bon gout d'utiliser la phrase:

> Si votre mobile ne fait que téléphoner, il ne fait pas grand chose.

Et comme le sougligne Thierry Stoehr, on pourrais ajouter:

> Comme votre site Web ne fait que du Flash, il ne fait pas grand chose.

J'adore !

Vive le monde libre (c'est à dire basé sur des formats ouverts) ;-)

