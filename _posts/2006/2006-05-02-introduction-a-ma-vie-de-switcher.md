---
layout: post
title: Introduction à ma vie de switcher
---

Un petit post d'explication sur la création de cette catégorie.

Je ne sais pas si ça pourras servir à quelqu'un mais, ça me permettras au moins de noter mes reflexions.

En effet, depuis mon introduction au monde informatique, j'ai découvert les système d'exploitation, et je continue d'en découvrir de nouveaux...

Certains propose plusieurs environnements graphique, d'autres sont figé.

J'ai aussi découvert les différentes couche d'un programme, d'un réseau. Les différents langages, les différents outils.

C'est très agréable d'avoir le choix dans ce domaine, ça me plait beaucoup. Mais ça m'apporte beaucoup de questions que je pourrais résumé à _Que choisir ?_

C'est un luxe dont je ne veux pas me priver. Cependant pour pouvoir continuer à travailler/m'amuser, il faut que je me stabilise. Je me demande si j'y arriverais un jour, en effet j'adore testé, découvrir. Je me rend compte que je suis un *"switcher"*

Je vais donc utilisé cette *catégorie* pour essayer de justifier certains choix que je fais, et parler de ces expériences.

J'ai quelque vieux billets que je vais mettre dans cette catégorie.

