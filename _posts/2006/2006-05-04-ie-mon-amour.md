---
layout: post
title: IE mon amour...
---

Oui ce navigateur est vieux, mais ce n’est pas pour ça que je vais le respecter...

C’est une grosse bouze, je viens de passer une bonne partie de l’après midi à mettre au point une page de l’application sur laquelle je bosse pour qu’elle soit compatible mozilla/ie.

C’est un problème dans mon entreprise, tout est fait pour IE... ça me fait rager.

Bref, tout ça pour noter ici un des fameux petit problème de l’après midi (ça pourras aidé d’autre personne peut-être et surtout ça me permettras de ne pas l’oublier ;-) )

Pour faire simple, je voici un petit bout de page:

Basique n’est-ce pas ? Et bien en executant ce code sous Firefox puis sous IE on trouveras respectivement 5 et 3 comme nombre d’enfant.

Effectivement, firefox, et je supose la pluspart des navigateur moderne (confirmation après rapide test sous opéra qui affiche 5 childNodes egalement), compte 2 childNodes de type element (un “checkbox” et un “button”). A la limite, IE aussi compte ces deux Elements (heuresement !). C’est sur la considération des elements de type text, c’est à dire les blancs avant et après chaque éléments. En XML il faut tenir compte de ces zones qui peuvent contenir du texte, et le fait de les différencier est important car elle donne plus de souplesse dans la gestion des ordonnancements d’enfants. IE lui ne considère pas le premier comme une zone de texte utilisable...

Et pourtant, et c’est là tout l’illogique de la situation, en modifiant le code de cette manière:

On se rend compte que maintenant IE compte comme Firefox et Opéra (surement d’autres) compte 5 elements enfants.

Pour résumer, il est délicat de prendre des raccourcis si on veux que l’application web fonctionne aussi sur les dinosaures comme IE.

Personnelement pour vérifier et modifier l’état de la checkbox je suis obligé de faire une boucle supplémentaire pour vérifier sur quelque type d’element je me trouve avant d’appliquer ma modification/lecture. Avec IE forcement je ne passe qu’une fois dans la boucle, avec Firefox 2 du coup... Mais c’est la faute à IE ;-).

Alors je suis peut-être passé à coté d’une astuce qui permet de signaler à firefox de ne pas considérer le premier blanc comme zone de texte possible, mais bon. Si un jour du texte fait sont apparition, je serais bien embété et obligé de modifier le script.

Merci IE pour cette folle après-midi ! (je garde mes autres problèmes pour d’autres jours... Sinon j’en ai pour la nuit :-p )

