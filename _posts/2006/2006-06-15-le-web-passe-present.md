---
layout: post
title: "Le web: passé, present"
---


Deux billets au sujet du Web en général mon marqué aujourd'hui. Honneur aux dames [avec la rencontre la lène avec la toile](http://www.jeuxdemaux.com/2006/06/15/944-le-jour-ou-jai-rencontre-la-toile). Une bien belle rencontre avec le web.
et ensuite un billet de [de Tristan Nitot à propos du futur du web](http://standblog.org/blog/2006/06/14/93114828-les-perspectives-du-web)



Pour suivre La Lène, j'ai découvert le web en même temps que l'informatique. Je ne me souviens pas avoir utilisé un ordinateur sans connexion, sauf le vieil amstrad, mais ça semble une autre époque, époque ou je ne faisait que jouer avec.


C'est donc chez un de mes premier client que j'ai utilisé le web pour la première fois, pour chercher de la documentation, faire tansiter des mise à jour logiciel, mes boites à outils (pas de clé usb à l'époque)


Pour mon usage personnel, à peine un ordinateur installé à la maison, j'avais déjà un compte free, avec un vieux modem 56K qui ma quand même permis d'une part d'apprendre plus sur les technologies qui n'etais pas pratiqué dans ma boite, et d'autres part de découvrir la joie des jeux en réseau, des MMORPG pour être plus précis, avec [Ultima Online](http://uo.com/) sur un serveur Américain !



Je suis là ou je suis actuellement grace à cette petite boite bleu qui m'a permis d'apprendre, apprendre encore, puis tester, essayer, et travailler ;-). Sans elle je serais surement resté dans ma branche technique, je serais à coup sur chez Peugot, à coté de chez moi, avec une blouse blanche dans le bureau des méthodes à préparer des gammes d'usinage.

*Non rien de rien, non je ne regrette rien...*

Pour ce qui est du billet de Tristan, je trouve que comme d'habitude, en plus de la belle plume, il a une superbe analyse. En fait je la trouve superbre peut-être parce que je suis entierement d'accord avec lui.

La toile est un aspect des plus beau de la "mondialisation". Mais pour le rester, il faut absoluement que les grande firme restent en dehors de toutça, ou alors respect l'essence même de cette ensemble. Il y a encore du travail pour que tous puisse y accéder, mais ça avance, on y arrivera ;-)

D'ailleurs, merci d'avoir utilisé la comparaisson avec les téléphones portables. Je ne savais pas comment convaincre mes connaissances d'utilisé des formats ouverts, des standards ouverts.

Je vais juste ajouter un grand merci à tout les gens qui font le web, à vous tous ! (enfin le peu qui arrive à me lire malgré les fautes. promis je fait des effort). Continuons à faire du web un endroit agréable et enrichissant POUR TOUS !

