---
layout: post
title: Fond d'écran pour BSD
---

Un site bien sympa qui propose une série de fond d’écran orienté <span class="caps">BSD</span>, l’unix libre (enfin surtout [openbsd](http://www.openbsd.org) :p ).

![Think correctly](http://www.bsdnexus.com/wallpapers/tehxaimus-thinkcorrectly1280.png)
![Simple bsd diable with background](http://www.bsdnexus.com/wallpapers/bpimg16.jpg)
![The power to server with diablotin](http://www.bsdnexus.com/wallpapers/freebsdpower.png)
![Old bsd, netbsd](http://www.bsdnexus.com/wallpapers/old-bsd.jpg)
![Freebsd blue](http://www.bsdnexus.com/wallpapers/fbsd-wall.png)
![Openbsd blue](http://www.bsdnexus.com/wallpapers/openbsd_image.png)

A voir :)

*Dernière semaine, dernière ligne droite. Je recommence à accumuler les bookmarks. Il faut que je nettoie *


