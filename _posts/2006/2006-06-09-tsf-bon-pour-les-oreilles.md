---
layout: post
title: "TSF: Bon pour les oreilles"
---

Pour quelqu'un qui passe un nombre d'heure incalculable avec des écouteurs sur les oreilles, je trouve que je ne parles pas assez de musique par ici.

Au travail, en transport en commun: port d'oreilles musicale obligatoire. A la maison, si c'est pas les infos ou la douce voix de ma compagne il faut de la musique. Chez les copains en général y'a toujours du son (au pire les commentaires du match de basket ;-) )

Musique un jour, musique toujours.

Je ne parlerais pas maintenant de l'offre en musique libre que l'on peut retrouver sur [jamendo.com](http://www.jamendo.com/fr/) ou encore de [musique-libre.org](http://www.musique-libre.org/) car d'autre comme [Fimagina](http://fimagina.jmtrivial.info/blog/) le font très bien :-). Non je voudrais parler de ce que j'ai dans les orielles là tout de suite maintenant.



Pour vous situer, je suis à mon bureau, je débogue une fonctionnalitée d'import de fichier XML avec interface graphique pour faire un mapping avec la base de donnée. Tranquille quoi. J'ai mes oreilles musicale. Et en général, pour travaillé, j'aime écouter du Jazz.



Mes gouts musicaux sont essentiellement orienté sur la "black musique" comme certains le dirait. J'avoue n'avoir jamais fait attention, j'écoute ce qui me plait. C'est vrai que ça va du Reggae (pour ma période estudiantine et jusqu'à maintenant) au Jazz, en passant par le Funk, le Hip-hop, la Soul. Quelque morceau ou artiste de la musique dites du monde et un poil d'électro (en général pour écouter quelque sample ils ont récupéré ;-) ).



Donc au boulot c'est plutôt Jazz. Ca me permet de bien m'isoler, pour me concentrer sur ma/mes tache/s. Et malgré une bonne discothèque, mes disques sont des vinyls, et je n'ai pas encore tout numérisé, je tourne donc vite en rond.



Du coup, j'écoute la radio, et quand il est question de Jazz à la radio, je n'ai que trois lettres à la bouche: [TSF](http://www.tsfjazz.com/) !.



Jazz en tout genre, peu de pub quelques infos, de bon animateurs. Très important aussi, une bonne playlist très fourni. J'entends peu de fois le même morceau dans une journée (est-ce déjà arrivé ???). Alors que d'autres radio sont coutumière du fait.



Bref, une très bonne radio que je vous conseil si vous aimez ou voulez découvrir le Jazz.



