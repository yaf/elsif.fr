---
layout: post
title: "Ecocircus: Eco festival CREABIO"
---


Ca y est ! ils sont enfin présent sur le web, je vais vous parlez d'eux, c'est l'occassion.

Dans un premier temps l'association **les Sabots d'Hélène** est né pour faire vivre l'amour d'un lieu: Villerest, ou plutot une maison bien particulière. Divers festivité autour du cirque et de la musique s'y succède pour le plaisir de tous. L'ensemble dans un eternel respect de l'environnement. C'est de cette ensemble que les **les Sabots d'Hélène** deviennent [Ecocircus](http://www.ecocircus.org/), une manière d'élargir l'action de l'asso.

Je vous laisse lire [la présentation de l'acco pour en savoir plus](http://www.ecocircus.org/index.php?2006/06/16/6-presentation).

Dans le cadre magnifique de Villerest (département de l'Eure) s'organise un festival: [le festival CREABIO](http://www.ecocircus.org/index.php?2006/06/16/10-ecofestival-creabio).
Je copie ici la présentation:

> C'est sur une initiative collective et bénévole que notre association a entrepris de se lancer dans l'organisation d'une manifestation unique en son genre dans le département de l'Eure.
>
> En effet, CREABIO est un salon de plein air qui a pour vocation de rassembler pendant deux journées un large public, autour de spectacles, ateliers, conférences et exposants sur les thèmes de l'ECOLOGIE et de la CREATIVITE.
>
> La partie festive de la manifestation sera animée par des spectacles de cirque et de musique car nous estimons que ces vecteurs culturels permettent de véhiculer des valeurs de solidarité et de respect de l'environnement.
>
> Au travers de cette manifestation, nous souhaitons sensibiliser les gens à une démarche responsable dans l'acte de consommer en respectant la nature et l'homme; c'est en rassemblant durant un week-end convivial et festif des professionnels et des bénévoles prêts à partager leurs expériences créatrices en faveur de l'environnement, de la culture et/ou des relations humaines que notre association souhaite participer à la nécessaire prise de conscience pour un monde durable.

A ne pas manquer !

