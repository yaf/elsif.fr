---
layout: post
title: Structure d'une page web
---

Eh bien voilà une trés interessante étude [reading pattern](http://www.useit.com/alertbox/reading_pattern.html). D’après une analyse de Jakob Nielsen (un guru du web entre autres) les lecteurs d’une page web ne lirais pas en Z comme pour un magasine ou un livre (il me semble) mais en F...

Pour moi ça signifie une chose simple... Les menu placé à droite permette l’originalité mais pas forcement une bonne lisibilité. Je pense donc que sur mon futur design pour ce site, je placerais le menu à gauche. Simplement.

![sens de lecture du page web, du coin nord ouest puis selon, horizontal si je minteresse, vertical si je cherche](/files/readibility-directions.gif)

Cela confirme également qu’il faut faire très attention à bien choisir le contenu d’un menu et les éléments qui y apparaissent pour faciliter une recherche.

Que du bon sens :-).

Merci à [Fred Cavazza](http://www.fredcavazza.net/index.php?2006/04/20/1138-nous-ne-lisons-pas-en-z-mais-en-f-) et à [Laurent Goffin via gwix.net](http://www.gwix.net/web_gwix/article.asp?filID=233) sans qui je n’aurais pas découvert cette étude fort interessante.
  
