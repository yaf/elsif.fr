---
layout: post
title: La Corée plus dur que l'Europe
---

Dans le même genre de processus que plusieurs pays à travers la planête, la Corée viens de confirmer que Microsoft doit absoluement séparé la vente de ces applications (Trouvé [via Gnunux](http://www.gnunux.info/dotclear/index.php?2006/05/24/169-ah-la-coree-du-sud)).

Explication plus détaillé sur [silicon.fr](http://www.silicon.fr/getarticle.asp?ID=15294)

Je doit avouer que c'est une bonne chose, de mon point de vue, même si je pense que Microsoft arriveras toujours à ces fins. Mais ça montre le chemin à suivre ;-)

C'est vrai c'est plus facile pour quelqu'un qui ne connais rien à l'informatique (même en temps qu'utilisateur). Pas de recherche pour trouver quel est le meilleur logiciel pour faire ce que l'on souhaite faire, pas de questions à se poser.

Mais justement ne pas se poser de questions c'est vraiment faire le "mouton". Je trouve ça dangereux, et surtout pas très interessant. Je ne sais pas combien de personne qui utilise leur machine se sont déjà  posé la question:

>
> Mais existe-t-il autres chose que Outlook ? mais existe-t-il autre chose que Msn ? mais existe-t-il autre chose que Word ?
>

De mon coté j'essaie de parler, d'expliquer (parfois maladroitement) au gens qui m'entoure qu'il existe d'autres outils pour faire tel ou tel chose. Des outils bien meilleur.

Ma plus grande fierté pour le moment c'est d'avoir [convaincu des amis proche à essayer gnu/linux](http://typouype.org/articles/2006/03/02/les-premiers), et surtout de voir qu'ils en sont content !

