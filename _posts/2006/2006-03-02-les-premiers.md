---
layout: post
title: Les premiers
---

Et voilà, a force d'en parler, j'arrive à mes fin on dirais (enfin c'est un début seulement j'espère): un couple de bon ami à re-installer son ordinateur en dual boot (ben oui je pouvais pas faire tourner le dernier jeu de foot-de-la-mort-qui-tue sous le pinguoin). C'est donc un autre ami qui leur à installer la partie windows, en me laissant 8 Go (sur un disque de 120 c'est pas très sympa mais bon) pour que j'installe gnu/linux.

Forcement mon choix c'est porter sur une distribution [Ubuntu](http://www.ubuntu.com/). Depuis ça sortie je pense que c'est surement la meilleur distribution "Grand Public". C'est le système de paquet "à la debian" qui me fait préférer Ubuntu.

La première étape, pour moi, ça a été de bien observer leur matériel. Et déjà la première chose que j'ai faite c'est remplacer la connexion à leur freeBox pour la passer de l'USB à l'Ethernet. Ils m'avaient signalé des problème de deconnexion (sous windows), je pense que c'etais du à ça. Depuis ça vas mieux d'ailleurs (sous windows ou sous linux).

Bien, ensuite j'ai donc installé la dites distribution, sans aucun problème, surtout qu'ils n'ont pas de matériel exotique, ou vieux.

Finalement le plus long a été (et seras ? ) l'aprentissage. En fait ce ne sont pas des grands habitué de l'ordinateur, enfin pas encore. Ils possèdent une machine chez eux depuis un an seulement. Mais j'ai eu la sensation que ça se passais bien. J'ai laissé faire la madame pour voir s'il elle si retrouvais, et je n'ai eu à intervenir que très peu. Enfin quand je dit très peu... Il a quand même fallu que j'aille dans les dépots universe pour récupérer quelque bricole qu'ils ont pris l'habitude d'utiliser, et j'ai essayer de lire leur partition NTFS qui sert de stockage de données sous windows, mais la manipulation (bien que graphique) ne me permettais pas d'écouter leur musique. Bref, de toute façon, 8Go sur 120, je pense que je recommencerais cette installation une fois leur donnée déplacées, et la partition de données passée en Ext3 (il faudra que je jete un oeil au script/logiciel dispo en libre sous windows pour communiquer avec ce genre de partition) et après ça sera nickel ;-)

Bienvenue à eux dans le monde libre ;-)

