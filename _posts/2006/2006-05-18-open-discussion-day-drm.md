---
layout: post
title: Open Discussion Day, DRM
---

Aucun rapport me direz vous ? Et bien quelque part si. Tout les deux sont lié à des problèmes de format ouvert. Au passage je vous invite, si vous ne connaissez pas déjà, à visiter régulièrement [un trés bon site sur les formats ouverts](http://formats-ouverts.org/)

## Open Discussion Day

Ce vendredi 19 mai seras donc le jour de la discussion ouverte.

*Je copie/colle le texte fait sur [linuxfr.org par ploum](http://linuxfr.org/2006/05/18/20828.html). Il à une meilleur plume que moi ;-)*

> Avez-vous déjà imaginé ce que serait Internet sans un protocole de courriel standard ouvert ? Chaque fournisseur disposerait de son propre protocole fermé et de son logiciel client officiel. Les personnes avec les adresses en @machin.fr ne pourrait communiquer qu’avec les adresses en @machin.fr et ainsi de suite.
>
>
> Et bien c’est malheureusement le cas en ce qui concerne la messagerie instantanée : MSN Messenger, Yahoo! Messenger, ICQ, AIM, Gadu-Gadu, Skype - pour ne citer qu’eux - ne peuvent pas se parler entre eux. Actuellement, la messagerie instantanée est particulièrement fragmentée.
>
>
> C’est pour cette raison que le 19 mai a été instauré Open Discussion Day dont la première édition aura lieu cette année. Durant cette journée, nous vous invitons à ne pas utiliser les protocoles propriétaires (Yahoo! Messenger, MSN Messenger, ICQ, AIM, etc.). Prévenez vos contacts le jour avant et proposez leur d’ouvrir un compte Jabber sur Google Talk ou n’importe quel serveur Jabber disponible (si possible géographiquement proche).
>
>
> Beaucoup d’entre nous ont aussi choisi cette date pour définitivement abandonner les messageries propriétaires. Certains serveurs Jabber (dont fritalk) couperont les passerelles vers les réseaux propriétaires durant 24h afin de marquer l’évènement. On se reparle sur Jabber ?

Voilà, je pense donc que je couperais toute messagerie non libre ce jour là.

## DRM

C’est voté, c’est trop tard. voici un pot pourri de lien interessant toujours sur [linuxfr.org compilé par Clem](http://linuxfr.org/2006/05/18/20825.html).

Ou va-t-on ? Je crois que je vais acheter de moins en moins de disques/DVDs. Etant donnée que [je ne plus plus voir de film](http://typouype.org/articles/2006/02/23/cest-mon-tour), j’irais quelque fois au cinéma, peut-être un peu plus qui sais. Quoique, vu le prix :-/.

Pour ce qui est des CDs, ben je vais recommencer à acheter uniquement des vinyls. Le seul problème c’est que j’avais arreté par manque de place. Et je n’ai toujours pas déménagé :’(

Pourquoi toujours plus de profit ?

