---
layout: post
title: "OpenBSD: Nouveau Thême pour Windowmaker"
---

Petit à petit je prend mes quartier avec OpenBSD :-). Je commence à bien aprécier cet unix libre. J’apprend énormément ! beaucoup plus qu’avec Gnu/linux qui finalement nous facilite bien les choses mais en nous les cachants sous des couches et des couches de script/soft.

Bref, après une petite webrando ayant pour thème: “trouver un fond d’écran à partir du quel je travaillerais un thème pour mon bureau”, et une petite demi heure à travailler sur les couleurs, voici le resultat:

![screenshot du gestionnaire de fenetre windowmaker](/files/green-windowmaker.jpg)

Bon il faut que je travail maintenant sur les icones et sur les thèmes de <span class="caps">ROX</span> par exemple.

