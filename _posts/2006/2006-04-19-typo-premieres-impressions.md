---
layout: post
title: "Typo: Premières impressions"
---

Et bien voilà, après avoir repris la pluspart des articles pour vérifier que rien ne c'étais perdu en route, après avoir explorer la face administration de ce blogue, c'est l'heure du premier bilan.

Commençons par les points négatifs. Ajax à gogo. J'avoue ne pas trop en voir l'utilité. Je me demande même si ce n'est pas un "frein".

L'interface d'administration est assez haustère. Bon je sais que c'est pas très génant, mais certains outils de blog ou autre CMS font des efforts au niveau présentation sur ces parties là, c'est un plus que n'a pas Typo.

Pour le moment c'est tout ce qui me gène.

Pour les cotés positif. Je ne ferais pas un article sur ruby et/ou rails, car c'est une question de gout, ou alors il faudrais en faire un article complet...

Par contre j'aime bien le concept des catégories qui sont plutôt des tags que des catégories. C'est très agréable. Il vas falloir que je range un peu d'ailleurs ;-) .

La série de plugin permettant une jonction avec Flickr et Del.icio.us sont aussi des plus non négligeable (dans mon cas puisque j'utilise les deux ;-) ).

Globalement je ne suis pas mécontent d'avoir changé. Je vais pouvoir parler de Ruby et faire un peu de Rails du cout ;-) Surtout qu'il me faut refaire absoluement le thèmes. Celui par défaut n'est pas exceptionnel (question de gout peut-être).


