---
layout: post
title: "Trinity rescue : A l'aide !"
---

Et bien voilà, ça devais arriver, les parents d'un ami à qui j'avais installé leur premier portable (et expliquer les bases de l'utilisation d'un ordinateur) sont revenus vers moi il y a une semaine. Leur vieux portable (5 ans d'age) ne démarrais plus. Du coup il en on acheter un autre (ce qui n'est pas une mauvaise chose en soit) pour pouvoir continuer à travailler, surfer, communiquer.

Alors après l'installation de la nouvelle machine nous avons parlé un peu de l'ancienne, et apparement y'a quelque données qu'ils aimeraient bien pouvoir récupérer. Je pense qu'il n'y croit pas trop mais bon.

Je me suis dit "Chic ! une occassion de tester un kit de survie Linux me permettant de récupérer des données (entre autres) sur un ordinateur malade". Après une rapide recherche je tombe sur [Trynity Rescue Kit](http://trinityhome.org/trk/weavernews/viewnews.php?news_id=51). Bon c'est basé sur Mandriva mais je vais quand même essayer. Ne serais-ce que pour ce petit script de rien du tout mais qui vas m'être util: mountallfs ;-)

Tranquille à la maison, une soirée dégagée, bien je me lance. Insertion du CD, démarrage de la bête. Aucun problème, la plus part des élément sont reconnus. Comme je m'y attendais, et c'est assez logique et finalement très pratique, je me retrouve dans un mode console, embellie par un fond noir décoré du logo orange flouté de ce kit de survie.

Un petit `mounallfs`, parfait, j'ai toute les partition, ma clé usb (ben oui pas de réseau sur cette vieille machine, faudras ajouter une carte PCMCIA pour bien faire). Visite des repertoire windows (c'est toujours plus drole à partir d'une console linux je trouve ;-)), je récupére une série de .doc ( erf ), et je supprime au fur et à mesure pour pas m'embrouiller dans les repertoire déjà parcouru et récupéré. Et là je tombe sur un nid de .exe dont les noms ressemble fort à des virus :-) Ha ben voilà d'où viens le problème de démarrage !. Bon finissons la  récupération de données, tranquillement.

Pour finir je dirais que malgrés le fait que je ne suis pas un grand fan des distribution Mandriva, j'avoue que ce kit est de trés bonne facture. Un problème seulement (si on peut appeler ça un problème) j'etais en qwerty...


J'avoue que j'aurais beaucoup aimé tester [Timo's Rescue](http://rescuecd.sourceforge.net/) qui lui es basé sur une debian (dont vous l'aurez compris je suis fan). Mais je l'ai découvert après coup, dommage. Ca seras pour la prochaine fois. Je pense que tant que j'aurais des proches sous windows, j'aurais besoin de ce genre d'outils ;-).


