---
layout: post
title: "Format ouvert: OpenDocument"
---

Et bien en voilà une bonne nouvelle !

Non pas le soleil, ça c'est normal :-p, je voulais parler du format ouvert OpenDocument. On apprend par le biais du site parlant des format ouvert [qu'OpenDocument est officialisé par l'ISO](http://formats-ouverts.org/tb.php?id=795). Après l'oasis, cela confirme la validité du format

Et dire que je viens de demandé au DSI nouvellement arrivé de ma boite dans quel format il voulais qu'on stock les documents, et qu'il ma répondu qu'on avais Microsoft Office sur tout les postes et donc que... Bref, je suis un peu déçu, mais tout ça vas me permettre d'argumenter un peu plus !

*to be continued...*

