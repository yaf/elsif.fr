---
layout: post
title: "OpenBSD/PowerPC: mapping clavier mac usb"
---

Et bien voilà, il manquait encore un petit quelque chose de génant: le mapping du clavier !

Je suis parti du principe que ça devait surement être fait un peu comme sous Debian. Effectivement, un petit: `cd /etc/X11/xkb`

Et nous voilà dans l'antre de la configuration clavier sous unix.

Même genre de fichier, je vais donc aller chercher sur le site qui m'a beaucoup aidé pour mes premiers pas debianeux sur machine PowerPC: [Linux-france.org/macintosh](http://www.linux-france.org/macintosh/).

De là je récupère un paquet contenant le fichier qui m'interesse: [le fichier de mapping du clavier Mac USB](http://zone.typouype.org/fr_new)

Je relis vite fais les instructions puis je me lance: `cp fr_new /etc/X11/xkb/symbols/macintosh/`

puis, histoire de vérifier le bon fonctionnement: `setxkbmap fr_new`

Ca marche !

Bien, maintenant, mise à jour du fichier xorg.conf pour une prise au prochain redémarrage de X.

<pre>
  {% highlight bash %}
    Section "InputDevice"
           Identifier  "Keyboard0"
           Driver      "keyboard"
           Option      "Protocol"  "standard"
           Option      "XkbRules"  "xorg"
           Option      "XkbModel"  "macintosh"
           Option      "XkbLayout" "fr_new"
    EndSection
  {% endhighlight %}
</pre>

Petit remplacement du `"XkbLayout" "fr"` en `"XkbLayout" "fr_new"`.

Je pense que je reviendrais sur le contenu de ces fichiers. Histoire d'en apprendre plus, et peut-être de modifier un peu le mapping à ma convenance.

Le principal c'est que je peut faire {\[|\]} !

