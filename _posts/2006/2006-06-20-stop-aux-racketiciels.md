---
layout: post
title: Stop aux racketiciels !
---

Je n’en ai pas parlé pour le moment mais en voyant ce matin [que Silvyn en parle](http://www.silvyn.net/blog/index.php?2006/06/19/278-halte-aux-racketiciels) je me suis dit que plus on en parle, plus on peut toucher de personnes.

Alors oui il faut signer la pétition [Halte au racketiciels](http://www.racketiciel.info/) car la vente forcé n’est pas une bonne chose pour qui que se soit (sauf les industriels qui en profitent). Il faut être **libre de choisir** ce que l’on souhaite.

[![Racketiciel](http://aful.org/media/image/racketiciel/average5.png)](http://www.racketiciel.info/)

