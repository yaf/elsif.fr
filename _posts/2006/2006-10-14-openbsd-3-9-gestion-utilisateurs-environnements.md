---
layout: post
title: "OpenBSD 3.9 : gestion utilisateurs environnements"
---

Dans le cadre d'un serveur, distant ou non, il est bon de prendre certaines précaution, et de s'aménager un environnements agréable. On vas essayer de faire ça.

Pour commencer simple et efficace, il est recommandé un peu partout, par un peu tout le monde de ne pas se connecter avec l'utilisateur "administrateur": root en terme nixien (de la planête \*nix/\*bsd, aucun rapport avec nixon ou qui que se soit :p). C'est dangereux pour les données et le systèmes. Un tel utilisateur a plein pouvoir sur l'ensemble et peu par faut d'attention tout mettre en l'air. Comme tout le monde le sait: l'erreur et humaine et en général les problèmes viennent de l'humain :p. En *gros (et en gras) pas de connexion avec l'utilisateur root*.

En général, sur un serveur on se connect e en utilisant le protocole [ssh](http://fr.wikipedia.org/wiki/Ssh), dont 'implémentation la plus répendu est [OpenSSH](http://www.openssh.com/). Cette implémentation réalisé par les soins de l'équipe d'OpenBSD est evidemment incluse dans leur système, ainsi que dans beaucoup d'autres (plus ou moins officiellement). Le démon (serveur) OpenSSH est également le seul à être lancé dans une installation par défaut dans OpenBSD. Ce démon dispose d'un fichier de configuration qui vas nous permettre de limiter l'accès à notre serveur.

C'est assez simple, une fois connecté avec son utilisateur habituel sur le serveur, il faut passer en root pour pouvoir modifier le fichier (j'ai dit connexion en root, pas utilisation de root :p). Pour passer en root, utilisez la commande [su(1)](http://www.openbsd.org/cgi-bin/man.cgi?query=su&apropos=0&sektion=0&manpath=OpenBSD+3.9&arch=macppc&format=html) nous permet de devenir root le temps de la manipulation (on peut aussi utiliser [sudo(8)](http://www.openbsd.org/cgi-bin/man.cgi?query=sudo&apropos=0&sektion=0&manpath=OpenBSD+3.9&arch=macppc&format=html) mais comme je n'en ai pas encore parlé, on vas faire sans).

Ensuite une édition du fichier */etc/ssh/sshd_config* nous permettras de modifier la seul ligne qui pour le moment nous interesse : *#PermitRootLogin yes*. En fait la plupart des options en commentaire (je n'ai pas vérifié pour toute) sont affiché avec leur valeurs par défaut. Il nous suffit juste d'enlever le commentaire et de modifier le yes en no pour obtenir ceci: *PermitRootLogin no*. Facile non ? Et ça évite bien des problèmes. Fini les erreurs de manip alors que l'on c'est connecté avec root (vécu inside), et surtout la personne voulant attaquer le serveur devras commencer par trouver un login...

*Et blablabla et blablabla. Normalement je voulais aussi parler du fichier .profile et de sudo, mais j'ai trop blablaté sur ssh là, j'ai pas envie de faire un post trop long avec plein de truc dedans. Là y'a que PermitRootLogin no, au moins c'est simple :p. Je tacherais de faire plus concis la prochaine fois quand même :)*

