---
layout: post
title: Nouveau Thême
---

Rapidement, mon premier thème pour Typo. Je l'ai baptisé discouype, vue que les couleurs vive me font penser aux années disco, les grosses bordures aussi d'ailleurs.

J'ai encore deux ou trois retouches à faire surement, mais j'en ai profité pour enlever les commentaires, les trackbacks, comme ça pas de problème de spam. Comme avec l'armée qui ne met pas sont réseau interne en contact avec le réseau web mondial pour être sur de ne pas être touché de l'extérieur. Ben voilà, la meilleur protection c'est l'abstention (attention pas dans tout les cas ;-) ).

Il faut que je vois aussi si des couleurs comme ça ne vont pas me tuer à force...

*A suivre*

ps: je mettrais en ligne un tarball contenant le thèmes quand je l'aurais validé ;-)

