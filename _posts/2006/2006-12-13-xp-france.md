---
layout: post
title: XP France
---

XP, eXtreme Programming (oui oui , on vas quand même pas parler du système d’exploitation à base de fenêtre ici ! Vous croyez quoi :) ). Pour ceux qui ne connaissent pas, c’est une méthode de travail. Dans mon ancienne boite il parait qu’on faisait de l’XP. Mais quelque personnes (dont moi) voyais bien que ça ne ressemblais pas à de l’XP comme on doit la faire, du coup c’étais rebaptisé Ultima Programming :p

Pour revenir sur l’eXtreme Programming, la vrai. C’est donc une méthode de développement basé sur quelque principe de base, simple, qui semble une évidence. Je vais vous parler des points dont je me souviens, je ne suis pas un expert, et j’ai jamais eu l’occasion de pratiquer cette méthode au sein d’une équipe, d’un projet, pourtant c’est pas l’envie qui me manque :p.

## Test unitaire

C’est une des bases importantes de l’XP. Dans le livre qui ma permis d’en apprendre plus sur cette méthode, c’étais traité comme le pilier de l’XP. Les [tests unitaires \[wikipedia\]](http://fr.wikipedia.org/wiki/Test_unitaire)  sont une série de petite vérification. Pour faire simple, et par l’exemple imaginons un objet *HexaColor* qui a une méthode *getColor(color)*. Une série de test unitaire de cet objet pourrais ressembler à :

<pre>
  {% highlight java %}
    assert(Color.getColor('blue'), '#0000ff')
    assert(Color.getColor('white'), '#ffffff')
  {% endhighlight %}
</pre>

C’est simple non ? Les tests unitaires permette de bien préciser ce que l’on attend d’une méthode, d’un objet. Le développeur de l’objet les fournis, cela remplace bien souvent les spécifications fonctionnelle car on peut, en lisant les test unitaires comprendre ce que fait chaque méthode, chaque objet. De plus, en rejouant les test à chaque modification dans le code, on vérifie qu’il n’y a aucun effet de bord et/ou aucune régression dans le projet.

Une chose simple que je ne n’ai encore jamais vu appliquer...

## Documentation

Je l’ai effleuré dans le bloc test unitaire, en XP il n’y a pas ou peu de documentation. Les tests unitaires servent de documentation fonctionnel, et la documentation technique ne sert à rien. Le code est sensé être simple, clair, limpide et donc se suffire à lui même.

A la limite, c’est ici un point discutable pour moi. Le problème viens du fait que nous ne sommes pas tous égaux en programmation, et chacun applique sont style. On dit bien “écrire du code”. C’est donc comme pour toute les écriture, chacun à ça façon de formuler. Maintenant, dans l’XP il y a un point qui peut permettre de palier à ce problème.

## Coder en binôme

Et oui, c’est surprenant, pour beaucoup de manager c’est une perte de temps (comme les tests unitaires), mais dans l’XP c’est un des point clé (d’après moi): Tout code s’effectue en binôme. Plusieurs

avantages sont apportés par ce point.

### La communication, le partage.

Point ultra important. On peut dans ce cas partager ça façon de voir les chose et la confronté à une autre. Ca permet de faire évoluer les façon de faire, et permet de faire évoluer plus vite ceux qui débute en les mettant en contact avec des codeurs plus expérimenté, mais ça permet aussi aux plus expérimenté de continuer à ce remettre en cause, de ne pas rester assis sur des acquis (c’est jamais bon ça).

### Le contrôle, l’analyse

Il n’y a quand même qu’un seul clavier sur le poste de développement. Donc pendant qu’un écrit l’autre regarde, réfléchi, affine l’algorithme mis en place. On peut corriger les erreurs tout de suite. On le sais: 4 yeux valent mieux que 2 (surtout que y’en a 2 qui sont plus ou moins concentré sur la frappe, même si bon, c’est discutable :p ). Si un des deux ne sais pas comment faire une partie, l’autre peut prendre la relève.

### L’ennui

C’est bon pour le moral aussi de travailler à deux. Surtout que dans l’histoire, on doit changer de binôme toute les demi journée... Oui oui, un binôme n’est valable qu’un demi journée :). Ca bouge, ça casse la routine.


## Itérations courtes

Comme les binômes doivent changer toute les demi journées (enfin c’est le système optimal d’après ce que j’ai retenu). Il faut se tenir à des “taches”, des “itérations” courtes. Une méthode, un besoin doit être construit en une demi journée, sinon ce n’est pas la peine de le faire, il faut le découper en plusieurs petits morceaux. Le but est de tester toute les demi journée l’intégration de nouveauté, de voir le projet avancé toute les demi journée. Avec l’XP on ne doit pas perdre de temps.

## Réunions

Le contact avec le client ou un de ces représentant est permanant. Il voit le projet avancé rapidement par petit morceau et peut revenir dessus si un point flou commence à s’éclaircir au fur et à mesure que le code avance. Par contre fini les réunions qui n’aboutissent à rien. L’auteur du bouquin parle même d’un minuteur de cuisson. Il le mettais à 15 minutes, une fois sonner, fini la réunion, tant pis si tout les points n’ont pas été abordé. Une réunion qui dur embrouille les esprits, c’est contre productif.

## Code jetable

Il ne faut pas hésiter à jeter le code fait la veille, le code qui finalement ne correspond pas à ce que le client souhaite. Jeter un code qui a pris une demi journée à faire est toujours plus facile que jeter un projet réalisé dans l’ombre pendant 6 mois. Je suis gentil sur les 6 mois, j’ai vu des projet de 2 ou 3 années mis aux oubliettes sans même avoir tourné en production... Un beau gÃ¢chis de ressources et d’argent.

## Conclusion

C’est une bonne méthode sur le papier, après dans les fait, la difficulté c’est de l"appliquer. Comme pour la sécurité informatique, le problème vient de l’humain. Il est apparemment très dur d’avoir des responsables, des développeurs près et capable de passer à l’action.

Tout ça pour vous mettre un petit lien (oui je nettoie toujours mes favoris :p). Il existe une association française de gens qui développe ou s’intéresse à cette méthode de travail [xp-france](http://xp-france.net/). Je n’ai pris contact avec eux, je ne suis pas inscrit. En gros je ne les connais pas vraiment, mais je crois qu’il faut que je le fasse. C’est dans ma monstrueuse TODOListe :).  Le bouquin dont je parle doit être [Extreme Programming : La référence \[Amazon\]](http://www.amazon.fr/Extreme-Programming-r%C3%A9f%C3%A9rence-Kent-Beck/dp/2744014338/sr=8-2/qid=1166002858/ref=pd_ka_2/171-6036956-2893829?ie=UTF8&s=books) par Kent Beck.

*Erf ! encore un billet énorme, je voulais arrêter de faire ça. Pfff. Tout ça pour un lien en plus :p*

