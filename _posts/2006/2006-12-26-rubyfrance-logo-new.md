---
layout: post
title: RubyFrance.logo.new
---


C’est une étape importante pour l’identitée d’une association: le logo. Un appel avait été lancé pour nous proposer des idées (c’est vrai que la plupart des rubyiste ne sont pas vraiment graphiste :-D ). J’aime beaucoup celui qui a été retenu, mais il faut remercier toute les personnes qui nous on envoyé une proposition !

Pour en savoir plus:
[RubyFrance](http://rubyfrance.org/articles/2006/12/23/nouveau-logo/)

*Bouh le tout petit post... :)*

