---
layout: post
title: Pourquoi choisir OpenBSD
---

Je ne vais pas m'étaler tout de suite, j'en ferais un autres billet plus fourni. Cependant, je suis tombé ce matin sur une série de billet annonçant l'interview d'un des developpeur d'OpenBSD. [Le billet du GCU me parait un bon résumé  ](http://www.gcu.info/2058/2006/06/01/kernel-trap-recoit-lhomme-aux-3000-doigts/)

En gros, OpenBSD, malgrès sa license permissive, fait plus pour le libre que gnu/linux. Effectivement, OpenBSD livre une bataille contre les blobs. Les blobs sont en fait les driver binaires (compilé) fourni par les fabricants (de cartes réseau, graphiques, et autres) peut-être gratuitement, mais donc sans les sources.

OpenBSD supporte peut-étre moins de matériel que gnu/linux (et encore, c'est à vérifier) mais au moins tout le code est libre, completement libre !

J'ajouterais que j'aime à pensé qu'il est aussi de meilleur qualité, mais ça je n'ai pas vérifié (et qui le peut ? :-) )

Vive OpenBSD !
  
