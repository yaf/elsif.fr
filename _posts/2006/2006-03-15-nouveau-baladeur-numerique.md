---
layout: post
title: Nouveau baladeur numérique
---

Cap franchi !

Depuis le temps que ça me travail, je suis aller m'acheter un nouveau baladeur numérique pouvant lire le [format ouvert](http://formats-ouverts.org/) [ogg vorbis](http://www.vorbis.com/).

Au début j'etais parti pour acheter un samsung, j'hésitais entre:

* [le YP_t7fz](http://www.samsung.com/fr/products/portableaudio/pocket/video/yp_t7fzsels.asp)
* [le YP_t8z](http://www.samsung.com/fr/products/portableaudio/pocket/video/yp_t8zels.asp)
* [et le YP_t8x](http://www.samsung.com/fr/products/portableaudio/pocket/video/yp_t8xels.asp)

En gros, ces trois modèle permette la lecture du OGG et, petite option bien interessante, me permettrais de numériser mes vinyls très simplement, sans avois à approcher ma platine de ma machine (ou inversement), puisque ils embarquent un encodeur. Malgrés le fait que le format de l'encodeur soit le Mp3, je me disais que j'aurais pu à postériori re-transformer tout ça en ogg sur ma machine...

Et finalement à l'arrivé, je regarde l'ensemble des baladeur, prend en main le yp_t8x, m'extasie devant les nouveau lecteur avec ecran tactile enorme, permettant de voir des films.

Puis je tombe finalement sur un baladeur qui ressemble étrangement [à l'i-bead](http://www.ism-technologies.com/fr/ibead.php) que j'utilise actuellement, qui me convient très très bien, sauf qu'il ne lit pas le ogg.

En regardant bien, ce [ndeo](http://www.ndeo.fr/) me conviendrais tout à fait, voyons:

* Lit le ogg: Le but ultime, le critère déterminant
* Radio: ben oui c'est pratique des fois
* Dictaphone: bon je m'en sert pas mais au moins y'en a un
* Interface usb 2 intégré: ah ! voilà un petit plus par rapport à mon i-bead qui lui est en usb 1, oula mais c'est même un gros plus par rapport au samsung qui m'obligerais à trinballer un cable pour le relier à un pc... hmmm
* Encodeur mp3: Bon encore mp3, mais y'a aussi un encodeur...
* 512 mo: bon ça fait petit par rapport au taille du marché, mais le reste me vas tellement bien...

Aller c'est décidé, d'autant plus que ça coute bien moins cher ! zou ! je repose le samsung, je prend ce ndeo (voici un lien où la description est un peu plus accessible : [sur clubic](http://www.clubic.com/article-18713-1-ndeo-equinox.html), ben oui chez ndeo on veux faire moderne avec du flash partout mais, encore un inconvénient, du coup y'a pas de lien direct au produit...

Me voila donc en possession d'un baladeur qui fait la même taille que mon i-bead (cool du coup je re-utilise la house que j'avais acheter) qui est aussi pratique, avec un belle écran bi couleur (jaune et bleu, sur fond noir ça fait pas mal, mais c'est une histoire de gout), avec des plus vraiment interessant, j'adore. Et tout ça pour 50 euros de moins !

Du coup j'ai acheter quelque disque en bonus, ben oui j'en profite avant que tous passe en format-vérouillé-que-je-pourrais-plus-lire-comme-je-veux ;).

Une marque je connaissais pas, qui à un site que j'aime pas non plus mais qui fait un bon produit, c'est nickel ;-)

