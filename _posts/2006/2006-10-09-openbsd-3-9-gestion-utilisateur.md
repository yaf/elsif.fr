---
layout: post
title: OpenBSD 3.9 gestion utilisateur
---

Voilà donc le système d’exploitation [OpenBSD 3.9](http://openbsd.org) installé sur une dédibox. Après avoir envoyé le resultat de la commande [dmesg](http://www.openbsd.org/cgi-bin/man.cgi?query=dmesg&apropos=0&sektion=0&manpath=OpenBSD+Current&arch=i386&format=html) à l’adresse dmesg_AT_openbsd.org, on vas maintenant mettre en place certaines petites bricole bien pratique (voir indispensable :p )

La première chose à faire c’est de lancer la commande [afterboot](http://www.openbsd.org/cgi-bin/man.cgi?query=afterboot&apropos=0&sektion=0&manpath=OpenBSD+3.9&arch=powerpc&format=html).

Celle ci nous informe de plusieurs manipulation conseillé pour le bon fonctionnement du système. On y retrouve notamment l’adresse de la [page d’errata](http://www.openbsd.org/errata.html) listant les correctifs à appliquer pour suivre la branche stable-current. Aujourd’hui, 11 correctifs sont disposition. Dont un à propos du serveur X dont on à pas besoin (sur un serveur).

Pour commencer, hors de question d’utiliser l’utilisateur root , créons un nouvel utilisateur.
[adduser](http://www.openbsd.org/cgi-bin/man.cgi?query=adduser&apropos=0&sektion=0&manpath=OpenBSD+3.9&arch=macppc&format=html) est la commande qu’il nous faut :). Sous OpenBSD et sans paramètres, celle ci permet d’ajouter un utilisateur de façon assité, vraiment très pratique et très clair. Quelque conseil apparaissent tout de même sur la fin de la page man:

* pour le *username*(nom d’utilisateur / Login)
* Utilisez plutôt des *minuscules*
* Utilisez des caractères *alphabétique* (pas de ponctuation)
* Pas plus de *31 caractères*
* Le *fullname* (nom complet)
* sans le caractère *:*

Pour le reste, les sélections par défaut sont très bien (enfin moi c’est ce que j’ai utilisé :p). Juste pour l’utilisateur qui seras en charge de l’administration du système, il faut penser à le mettre dans le groupe **wheel** on verra plus tard que ça vas nous servir pour le [sudo](http://www.openbsd.org/cgi-bin/man.cgi?query=sudo&apropos=0&sektion=0&manpath=OpenBSD+3.9&arch=macppc&format=html). Voici ce que ça donne en image:

Pour la suite on explorera le fichier sudoer pour le paramétrage de la commande [sudo](http://www.openbsd.org/cgi-bin/man.cgi?query=sudo&apropos=0&sektion=0&manpath=OpenBSD+3.9&arch=macppc&format=html).

