---
layout: post
title: Nouvelle pétition anti DADVSI
---

Il y a peu de temps je lisais [sur le glazblog](http://www.glazman.org/weblog/dotclear/index.php?2006/03/21/1655-dadvsi-petition). C’est vrai que moi aussi je me demandais ce qu’on allais pouvoir faire maintenant...

Et bien voilà, une nouvelle pétition viens de voir le jour, je vous invite à aller la signer !

>
> Je réalise des copies à usage privé de mes DVD, je craque les DRM de mes fichiers pour pouvoir les passer sur mon lecteur MP3, je télécharge depuis Internet. J’ai une copie d’un code DeCSS. J’ai un logiciel de P2P. Et je n’ai pas l’intention d’arréter parce que rien de tout cela n’était jusqu’à présent illégal.
>

Ca se passe [par ici](http://www.petitiononline.com/nodadvsi/petition.html)

