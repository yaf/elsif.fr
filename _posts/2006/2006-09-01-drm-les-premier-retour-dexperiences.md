---
layout: post
title: DRM les premier retour d'expériences
---

  

Et bien voilà, premier retour d'expériences sur l'utilisation massive de DRM: [Un responsable de rayon carrefour interviewé par un membre du collectif stopDRM](http://stopdrm.info/index.php?2006/08/30/103-interview-du-responsable-dun-rayon-cd-a-carrefour).



Morceaux choisis:

<blockquote>
On a de plus en plus de retours de clients qui ne peuvent pas transférer le disque qu'ils ont acheté vers leur baladeur.
</blockquote>
<blockquote>
En tout cas, en ce qui concerne les CD protégés contre la copie, honnêtement, je ne pense pas que les maisons de disques s'y retrouvent...
</blockquote>



Comme quoi, on en as parlé, on avais prévenu, maintenant les grosses major vont faire du [téléchargement gratuit](http://fr.theinquirer.net/2006/08/30/universal_se_lance_dans_le_tel.html). On vas finir par voir arriver la license globale :D


C'est R-I-D-I-C-U-L-E ! Je me marre...



Comme d'hab', je ne dirais qu'une chose de plus: Vive les vinyls !


  