---
layout: post
title: Zone de turbulences
---


Juste pour signaler que mon site/blog vas entrer dans une période de forte turbulence à durée indeterminé (non merci pas de cpe ;-) ).

Effectivement je suis sensé changer d'hebergeur, mettre typo (blog basé sur ruby) et re-injecter mon historique... Ca fait un moment que ça me pend au nez, mais comme d'habitude, j'attend le dernier moment pour le faire.

Donc à partir de ce soir minuit, je ne répond plus de rien en ce qui concerne [typouype.org](www.typouype.org).  J'espère que ça ne dureras pas trop longtemps et surtout que je n'aurais pas de problème durant cette période ;-)

A trés vite 
