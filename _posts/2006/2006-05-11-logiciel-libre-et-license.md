---
layout: post
title: Logiciel libre et license
---

Je profite d'un billet de [Greg au sujet](http://greg.rubyfr.net/pub/?p=95) d'un vieille [article d'agoraVox au sujet de l'illusion des logiciel libre](http://www.agoravox.fr/article.php3?id_article=8970) pour rebondir tardivement moi aussi sur ce dernier

Je suis tout à fait d'accord avec Greg sur le fait que le logiciel libre ne signifie pas logiciel gratuit. La langue française permet cette distinction contrairement à la langue anglaise. Je suis surpris que des francophone (comme l'auteur de l'article apparement) fasse l'erreur. Un anglophone encore, je pourrais comprendre mais là !

C'est un sujet à troll, je le conçoit, et en lisant quelque commentaire sous l'article, je me suis même demandé si ce n'etais pas un article "provocation"

Je voulais juste signaler que dès les premier jours ou j'ai commencé à m'interesser au monde du libre, j'ai mis la main au portefeuille. Effectivement j'ai commencé par acheter une RedHat (à l'époque à la fnac ! ). Et dernièrement j'ai acheter une OpenBSD.

Alors oui on peut dire tout ce qu'on veux, mais sans même parler de contribuer, ou regarder le code en détail, le fait de savoir que c'est faisable me rassure sur la stabilité du produit. Quand on peut voir par dessus votre épaule on s'applique toujours plus, et si vous faites une faute, quelqu'un corrigeras à coup sur !

Alors n'en déplaisse à certain, mais Vive le libre !
  
