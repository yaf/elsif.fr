---
layout: post
title: "JDLL: feed back"
---



Mince c'est vrai qu'on est déjà jeudi et je n'ai toujours pas fait mon débrif de JDLL.



Ca à mal commencé en fait. J'ai un peu merdé (disons le clairement) sur mes horaires de RER, du coup j'ai raté mon train, et je ne suis arrivé sur Lyon qu'a 11h :-/. Heureusement pour moi, un Rubyiste/Openbsdiste ([Zifro](http://hazzard.free.fr/zifro/) pour ne pas le nommer :p ) trés sympa qui m'avais déjà attendu au train de 9 heure est revenue pour celui de 11 heure pour m'emmener à bon port :).



J'ai réussi à voir quand même l'excellente présentation de Christophe Espern à propos de DADVSI.



Forcement en entrant dans la salle, rencontre rapide avec le président de l'association Rubyfr : [fredix](http://fredix.freemonk.org/). En charge avec le [G](http://www.myreseau.org/) d'enregistré/transmettre en direct les conférences.



Petit sandwich le midi avec le trésorier de l'association [RubyFrance](http://rubyfr.org): Alex qui est passé rapidement faire un coucou.



Petit tour des stands également, je ne détaillerais pas tout ce que j'ai vu, mais entre autres, le stand [OpenBSD](http://openbsd.org) avec les t-shirt de la version 4.0 qui ne devrais plus tardé, et un boitier contenant de vrai CD de la 4.0 ... mais pas à vendre :'(. Tant pis on attendra encore un peu.



Juste pour ajouter mon grain de sel à *l'affaire* Mozilla/Debian: je n'ai vu aucun projectile passé à travers l'allé qui les séparait, aucune insulte non plus, que de la bonne ambiance, trés agréable et sympathique :) .



L'après midi, Zifro ma fait changé d'avis quand à mon planning prévisionnel, je suis aller avec lui voir la conférence sur XUL, plutôt que l'intrigante conférence sur * Linux Ecology Howto*. Je ne regrette pas, même si Paul n'avais pas l'air au mieux de ça forme, le tour d'horizon de l'architecture reptilienne était très instructif. En plus [Fraifrai ](http://www.fraifrai.net/) qui lui est allé à la conf * Linux Ecology Howto* n'en est pas ressorti trés enthousiaste.



Tiens en parlant de [Fraifrai](http://www.fraifrai.net/) que j'avais déjà croisé sur #libre-attitude@irc.freenode.net ma bien présenté le travail effectué par l'[april](http://www.april.org/), lui même en étant membre. J'avoue que je connaissait, mais ne mesurait pas l'importance de leurs actions. Après, il a essayer de <s>m'enroler de force</s> me convaincre d'adhérer. Je ne l'ai pas fait sur le coup, mais je pense que ça vas être réglé dans peu de temps (ce week-end ?)



Pour revenir aux conférences, la présentation des clients légers (matériel) était très enrichissante. Les lightnings talks on vu leur lots de surprise, un bon gros troll accompagnant une présentation de Ruby, comment aider le projet Gnome, une présentation du FAI associatif [FDN](http://www.fdn.fr/)...



En conclusion, une journée trés bien rempli, trés agréable. J'ai suis trés heureux d'avoir pu rencontré tout ce beau monde, et surtout les rubyistes que je côtoie depuis quelque temps en ligne.



Vivement l'année prochaine :)



