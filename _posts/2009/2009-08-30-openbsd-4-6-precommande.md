---
layout: post
title: OpenBSD 4.6 Précommande
---

Certains diront que c’est tôt, plus tôt que d’habitude... C’est possible, mais on peut depuis quelque jour pré-commander la futur version d’[OpenBSD](http://openbsd.org) qui sortira le 1er novembre en
[version 4.6](http://www.openbsd.org/46.html).

[![Planet Users](http://www.openbsd.org/images/PlanetUsers.jpg)](http://www.openbsd.org/46.html)

Il y a tout plein de chose déjà dedans, mais d’ici novembre, il y en aura surement encore plus :-)

A noter, le style du tshirt qui change par rapport à d’habitude, mais qui me parait sympa :)

*source : le journal officiel d’OpenBSD* [OpenBSD 4.6 Pre-Orders Online!](http://undeadly.org/cgi?action=article&amp;sid=20090809155554)

*les deux sont commandé :)*


