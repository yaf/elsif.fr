---
layout: post
title: Ruby et les frameworks web
---

Pour noël on a eu le droit à une annonce très bruyante (mais plus très fraïche maintenant).

*[Rails 3](http://rubyonrails.org) intègrera [Merb](http://merbivore.com/)*

Personnellement, je trouve que c'est une bonne chose. Ces deux framework apporte la même chose aux développeurs: un confort de développement pour des applications web. Et on juste quelques divergence de point de vue qui explique l'existance des deux frameworks. Et pour des nouveaux venu, quelque semaine de tests sur l'un, puis sur l'autres pour vraiment savoir lequel choisir... Cette fusion devrait permettre d'apporter les bonnes idées de Merb dans Rails et inversement. De plus il semblerais que les développeurs de Merb travail beaucoup plus à l'écoute de la communauté d'utilisateur, si Rails peut s'enrichir de ce mode de fonctionnement, ça permettra surement à tout le monde d'en profiter. Nous verrons bien ce que cela peut donner.

De la même manière, j'ai l'impression que [Sinatra](http://sinatra.rubyforge.org/) et [Camping](http://camping.rubyforge.org/files/README.html) marche dans la même direction: faire un framework web ultra léger. C'est dommage de ne pas joindre leurs force.

Avant de juger trop vite il faut que j'étudie ce Sinatra... _Fly me to the moon_

