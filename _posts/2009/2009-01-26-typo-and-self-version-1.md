---
layout: post
title: Typo and self version +1
---

Quel beau hasard, l'équipe de [typo sort la version 5.2](http://blog.typosphere.org/2009/01/25/typo-5-2-helmut-newton-for-rails-2-2-released) le jour de mon anniversaire :-) Amusant non ?

Ce blog tourne donc avec la version 5.2 du moteur, et un humain plus vieux d'un an aux manettes.

Très sympa cette nouvelle interface d'admin.

