---
layout: post
title: HaikuOS alpha1
---


Lentement mais surement l’équipe du projet [Haiku](http://www.haiku-os.org/) avance. Et nous arrivons là à une grande étape pour ce projet d’OS alternatif.

Pour rappel, HaikuOS est un projet visant à faire revivre (sous licence libre) BeOS.

Cela fait un moment que je n’avais pas testé un autre OS qu’OpenBSD, mais je crois que là, c’est l’occasion.

[![Haiku en alpha 1](http://www.haiku-os.org/files/images/news/2009-09-14_r1a1-cd-mockup.thumb.png)](http://www.haiku-os.org/news/2009-09-13_haiku_project_announces_availability_haiku_r1alpha_1)

Si vous aussi l’expérience vous tente: [haiku_r1alpha_1](http://www.haiku-os.org/news/2009-09-13_haiku_project_announces_availability_haiku_r1alpha_1)


