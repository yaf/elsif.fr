---
layout: post
title: Le code et les commentaires
---


Je viens de lire la [traduction d’un article parlant des commantaires dans le code source](http://www.framablog.org/index.php/post/2009/11/21/commentaires-code-source-programmation) sur le [Framablog](http://www.framablog.org) . C’est un article écrit par Esther Schindler que je ne connais pas. Son article original titré [If the comments are ugly, the code is ugly](http://www.itworld.com/development/84780/if-comments-are-ugly-code-ugly) parle donc de code source, de code source avec de vilain commentaires.

Elle signale que les commentaires doivent être irréprochable, sans faute d’orthographe,  sans erreur de grammaire, à jour par rapport aux codes sources commenté...

Et bien j’ai trouvé une solution encore plus simple: **je supprime les commentaires** et j’essaie de rendre le code plus lisible. Les langages de programmations actuel sont assez expressif, utilise des mots anglais clair, certain langage apporte une structure permettant même d’écrire des presques phrases ! **Un code propre n’est pas un code avec des commentaires à jour, mais un code lisible, clair et concis**

De plus, tout les langages modernes (enfin j’espère) bénéficie d’une [librairie de test unitaire](http://fr.wikipedia.org/wiki/Test_unitaire), ce qui veut dire que l’on peut écrire un bout de code très simple qui montre l’intention du programme réel, qui montre comment il fonctionne, **qui le documente en quelque sorte**. Alors pourquoi écrire en plus des commentaires ? Je préfère voir un code lisible, et au pire devoir aller lire des tests unitaires pour comprendre son fonctionnement (voir ajouter des tests pour vérifier que j’ai bien compris).

Alors je ne sais pas qui est cette dame, mais, même si elle a en partie raison, pitié, éviter de faire du code illisible et sans test unitaire programmé sous pretexte que vous avez écrit des commentaire digne d’un grand roman.


