---
layout: post
title: Nouvelle affiche au bureau
---

  
Je viens d’accrocher une nouvelle affiche au bureau:

![your programm with interface, logic and data, for users it's interface with magic](http://zone.elsif.fr/programs-as-seen-by-users.jpg)

Qui a dit que j’ai fait ça pour que l’équipe apprenne à mieux travailler ces interfaces utilisateurs ? :-D

*merci à celui dont j’ai oublié le nom qui a fait un lien qui pointe là dessus!*

  