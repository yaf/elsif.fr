---
layout: post
title: OpenBSD 4.5 et PickAxe 1.9
---


Deux annonce pour ce 1er mai, l’une attendu l’autres, presque, mais pas forcement aujourd’hui.

## OpenBSD 4.5

C’est un classique du 1er mai, une nouvelle version d’[OpenBSD](http://www.openbsd.org) voit le jour. Cette version est associé à un thème [Tron](http://fr.wikipedia.org/wiki/Tron), très réussi je trouve.

[![Pufftron](http://openbsd.org/images/Pufftron.jpg)](http://openbsd.org/45.html)

Dans la liste des améliorations et nouveautés de cette version on pourra noter:

* Amélioration du support wifi (les cartes d’intel posait des problème par exemple)
* Ajout de fonctionnalité de composite au serveur X d’openbsd Xenocara [xcompmgr](http://www.openbsd.org/cgi-bin/man.cgi?query=xcompmgr&sektion=1&format=html)
* [OpenSSH 5.2](http://www.openssh.org)

et tout un tas d’autres choses que je vous propose d’aller lire sur [le journal officiel d’openbsd](http://undeadly.org/cgi?action=article&sid=20090430171848)

## PickAxe Ruby 1.9

Le pickaxe est LE livre de référence du langage de programmation [Ruby](http://ruby-lang.org). Son nom viens de la couverture qui, vous l’aviez deviné, représente une pioche. Et bien une version à jour, c’est à dire parlant de la version 1.9 de ruby est en cours d’impression. On attend avec impatience cette nouvelle référence du langage. Je vous laisse lire la nouvelle sur le [Ruby Inside, Ruby 1.9 pickaxe](http://www.rubyinside.com/programming-ruby-19-the-new-pickaxe-now-in-print-1739.html)

*Pffiiu, on a de quoi s’occuper ce week-end ;-)*

