---
layout: post
title: OpenBSD binary upgrade
---

Le site qui héberge les infos du script que j'utilise pour [suivre la version -current d'OpenBSD](http://www.typouype.org/2008/02/02/openbsd-current) a déménagé.

Vous pourrez retrouver toute les infos sur le script [OpenBSD binary upgrade](http://www.han.dds.nl/software/OpenBSD-binary-upgrade/) dans la rubrique _software_ du site de [Han Boetes](http://www.han.dds.nl/).

