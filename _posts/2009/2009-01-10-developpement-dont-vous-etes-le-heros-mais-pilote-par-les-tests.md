---
layout: post
title: Développement dont vous êtes le héros mais piloté par les tests
---

Un soir au [Dojo](http://wiki.agile-france.net/cgi-bin/wiki.pl?DojoDeveloppement), après avoir fait un [Bowling en Ruby en mode Randori](http://wiki.agile-france.net/cgi-bin/wiki.pl?DojoDeveloppement/Lundi15Decembre2008) [Emmanuel](http://emmanuelgaillot.blogspot.com/) nous a parlé d'un papier qu'il avait rédigé sur ce sujet, en déroulant cette exercice en _Kata dont vous êtes le héros_. Après quelque discussion et autres découverte, il a décidé de publié ce texte.

Je vous invite donc à dérouler ce "Kata Bowling en Ruby" dont vous êtes le héros [Reader driven](http://sites.google.com/site/emmanuelgaillot/). Bon test !

_Ce format est très interessant, je pense qu'il pourrais être adapté et pourrais donner des idées aux participants du Dojo d'ici ou d'ailleurs_

