---
layout: post
title: Agile Tour 2009 - Paris
---

Demain ce déroule l’étape Parisienne de l’[agile tour 2009](http://www.agiletour.org/fr/at2009_paris.html) . Un [jolie programme de conférence/retour d’expérience et autre atelier](http://www.agiletour.org/fr/at2009_paris_programme.html) qui promet de nous faire passer une bonne journée.

L’évènement aura lieu à la Fac de Nanterre... Ca rappelera des souvenir à certain, et donnera peut-être de bonne orientation à d’autres, déjà sur place.

A demain donc ;-)


