---
layout: post
title: Haiku R1/Alpha 1
---


Ca y est ! L’héritier du [BeOS](http://fr.wikipedia.org/wiki/Beos) , mais en version libre, sort en version R1 (notation hérité du vieux système disparu) Alpha 1.

[HaikuOS](http://www.haiku-os.org/)

C’est un cap important pour l’équipe car c’était une cible de compatibilité avec la dernière version existante de BeOS !.

Ils doivent avoir besoin d’un peu de sous, mais ça pourra également faire plaisir aux nostalgique, bref, un CD est quelques goodies sont disponible sur [cafepress.com/haiku_os.](http://www.cafepress.com/haiku_os.408438071)

L’annonce sur [Haiku-os.org/news](http://www.haiku-os.org/news/2009-10-28_r1alpha_1_commemorative_cd_now_available)


