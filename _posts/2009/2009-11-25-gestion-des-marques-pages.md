---
layout: post
title: Gestion des marques pages
---


Apres avois utilisé les del.icio.us et autres ma.gniolia, après avois lu Frederic faire tant de bonne pub pour diigo, je me suis ouvert un compte, et ma fois, je ne suis pas trop déçu.

Toutes les fonctionnalitées dont j’ai besoin sont là:

* plugin firefox efficace,
* gestion du privée/publique,
* état “non-lu”

Changement de cap: Utilisation de Diigo...

Ou pas.

Après discussion avec un certain [David](http://blog.javabien.net/) , je me rend compte que je ne me sert de mes marques pages partagé qu’uniquement en tampon... En gros, j’y place tout ce qui m’interesse, mais aussi tout ce qu’il faut que je lise.

Puis je dépiote de temps à autre ce qui traine dans mes lectures, mais voilà, après j’en fait quoi ? Deux options en général, soit je supprime le marque page après l’avoir lu, soit je le modifie pour marquer un sujet plus général.

Donc au final, je me retrouve avec un paquet de lien que finalement je n’utilise pas, car je passe bien souvent par un moteur de recherche de retrouver ce genre de site.

Tout ça pour dire que finalement, j’arrête d’utiliser ces outils. Je fais un tampon dans les marques page de mon navigateur, en local, et puis c’est tout.

