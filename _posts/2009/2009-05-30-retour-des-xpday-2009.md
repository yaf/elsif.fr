---
layout: post
title: Retour des XPDay 2009
---


Lundi et mardi dernier je me suis rendu au [XPDay2009](http://xpday.fr/) . J’y ai trainé un associé de [Kantena](kantena.com), j’y ai également retrouvé quelques têtes connu du dojo bien sur, mais également de l’association [RubyFrance](http://rubyfrance.org).

Le cadre était magnifique: [Le chalet de la porte jaune](http://xpday.fr/lieu-et-acces) est un endroit exellent pour ce genre de chose. Mise à part effectivement des salles parfois difficile d’accès ;-).

Nous avons entendu beaucoup de chose interessante, mais aussi beaucoup échangé, et c’est ce qui fait beaucoup dans ce genre d’évènement: les rencontres. Le planning et le cadre étaient propice à ce genre de chose.

Ce que j'attend de ces deux journée:

* Rencontrer des gens qui partage un interêt pour ces bonne pratique
* Avoir quelque retour d'expérience
* Apprendre et passer un bon moment.

Et bien mission accompli !

J’ai passé un bon moment, j’ai rencontré des agilistes convaincu, et moins convaincu, et les retour d’expérience on fini de me convaincre.

Ce que je retient principalement c’est que l’agilité, mais surtout le projet informatique sont une histoire d’Homme en premier lieu. C’est pas forcement nouveau, mais on le sent vraiment dans ce genre de contexte. Et c’est surement cette première valeur  du [manifeste agile](http://fr.wikipedia.org/wiki/Manifeste_Agile) : *L'interaction avec les personnes plutôt que les processus et les outils.* qui est le plus dur à faire comprendre au réticent. C’est dailleurs un des points point commun avec le [Lean](http://fr.wikipedia.org/wiki/Lean) (ou [TPS](http://fr.wikipedia.org/wiki/Syst%C3%A8me_de_production_de_Toyota) . C’est un point commune à la fois sur la forme, car le Lean se concentre sur les Hommes , mais également sur le  fond (du problème) car c’est aussi un des aspects les plus dur à faire comprendre dans une entreprise voulant adopter des pratiques *Lean*

Pour ce qui est de mon objectif professionel: *avoir une vision plus clair de ce que je peut proposer sous forme d'un contrat ou non, autour de projet au forfait, avec un client distant.* je n’ai pas de réponse toute faite. Mais je m’y attendait. C’est normal, toute ces pratiques ne peuvent s’appliquer de la même manière dans tout les contexte. Au risque de me répéter, l’agilité, le lean, sont des pratiques de bon sens, et le bon sens ne peut s’appliquer de la même manière à tout les projets. J’ai même entendu qu’un projet mené en mode [cascade](http://fr.wikipedia.org/wiki/Développement_en_cascade) avec les bonnes personnes au bon endroit, avec des une équipe motivé peu faire mieux qu’une équipe mauvaise utilisant Scrum ou une autre méthodes agiles... Et je suis assez d’accord avec cela !

Bref, pour une première, je suis ravi. Je compte bien renouveller l’expérience l’année prochaine, et cette fois, j’essayerais d’amener avec moi plus de personnes de Kantena !

