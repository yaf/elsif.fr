---
layout: post
title: Apéro Ruby de mai 2009
---


Pour ceux qui ne serait pas encore au courant, [Cyril ‘shingara’ Mougel](http://blog.shingara.fr) organise au nom de l’association [RubyFrance](http://rubyfrance.org) le 12ième apéro Ruby de Paris.rb (la communauté parisienne de Ruby). Si cela vous tente, rendez-vous le 20 mai à partir de 20h au Dune (ça devient une habitude ;-)) 18 avenue Claude Vellfaux 75010 Paris (Métro Colonel Fabien ou Goncourt: [e-dune/acces](http://www.e-dune.fr/acces.php)).

> Petit extrait des activités de la soirée (à part boire un coup et discuter):
>
> * Rails 3 étant annoncé en Alpha pour très bientôt, nous pourrons tous en parler entre nous.
> * RailsConf 09 qui s’est terminé le 10 Mai

Et Cyril nous précise les non-modalités

> Aucun enregistrement préalable n’est nécessaire. mais vous pouvez toujours m’indiquer votre venue par email. Si vous souhaitez réaliser une présentation, n’hésitez pas à me l’indiquer pour qu’on puisse l’organiser.
>
> En espérant vous voir à nouveau, bonne soirée

Je pense que j’y ferais un tour, j’espère vous y voir ;-)

