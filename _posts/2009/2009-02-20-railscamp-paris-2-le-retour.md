---
layout: post
title: RailsCamp Paris 2 le retour
---

Fin d'hiver chargé coté évènement autour de Ruby:

* un [RubyCamp](http://barcamp.org/RubyCampLyon) ce week-end,
* un [RailsCamp](http://barcamp.org/RailsCampParis2) le samedi 7 mars

Et oui, encore une fois Jean-Francois fait des étincelles, il a trouvé le moyen d'organiser un RailsCamp dans les locaux de Sun, avenue Iéna, avant qu'ils déménage !

Ce RailsCamp va ce dérouler sur un fond de merge entre Rails et Merb. Comme l'année dernière c'est encore un super moyen de ce rencontrer, de coder, de discuter, de partager, d'apprendre, de découvrir...

Cet fois, j'espère pourvoir y passer la journée (et pourquoi pas être présent au mashpit du lendemain, si mashpit il y a).

Par contre dépéchez vous de vous inscrire, il n'y a déjà plus beaucoup de place.

[RubyFrance vous donne rendez-vous pour le RailsCamp Paris 2](http://rubyfrance.org/articles/2009/02/17/rendez-vous-au-railscamp-paris-2-chez-sun/)

*hmmm Ce thème de blog est sympa, mais je n'aime pas trop l'organisation de la typo dans les billets... Il va falloir relever les manches*



