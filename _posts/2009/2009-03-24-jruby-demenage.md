---
layout: post
title: Jruby déménage
---

[Charles Nutter, dit Headius](http://blog.headius.com/) l'a annoncé par un _twit_: le projet JRuby déménage:

> I'm so excited to move everything to kenai.com + git + github. We'll really be dogfooding JRuby on Rails then (kenai is JRoR-based).

_source: [https://twitter.com/headius/status/1378984561](https://twitter.com/headius/status/1378984561)_

Le projet JRuby est donc en train de déménagé de la plateforme [Codehaus](http://codehaus.org/) à [Kenai](http://kenai.com/). Ce gestionnaire de projet est, comme l'indique Charles, basé sur [JRuby](http://kenai.com/projects/jruby) _(je vous donne la nouvelle adresse ;-))_. C'est très bon tout cela.

Ce site héberge déjà le projet [ActiveRecord-JDBC](http://kenai.com/projects/activerecord-jdbc) qui est maintenant traité comme un projet _enfant_ de JRuby.

Et pour reprendre encore une fois Charles, ils ont besoin d'aide pour transferer le wiki:

> Help JRuby move to http://kenai.com Copy wiki pages from http://wiki.jruby.org/wiki/Main_Page to http://kenai.com/projects/jruby/pages/Home, then tell me so I can lock the original.

_source: [https://twitter.com/headius/status/1374505769](https://twitter.com/headius/status/1374505769)_

N'hésitez pas ;-)

