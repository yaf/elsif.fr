---
layout: post
title: BSD ressources
---

Pour ceux qui s’interesse aux monde des \*BSD, voici les quelques ressources qui traine dans mes favoris (surtout autour d’OpenBSD).

* [BSDAnywhere](http://bsdanywhere.org/) : Un liveCD basé sur OpenBSD
* [DragonFlyBSD](http://www.dragonflybsd.org/) : Un système <span class="caps">BSD</span> qui fait de plus en plus parlé de lui, surtout connu pour sont système de fichier [hammer](http://www.dragonflybsd.org/hammer/)
* [OpenBSDsupport](http://www.openbsdsupport.org/) : une sources de documentation (en anglais) pour OpenBSD
* [Undeadly](http://undeadly.org/) : on ne présente plus le journal officiel d’OpenBSD :)
* [OpenBSD 101](http://www.openbsd101.com/) : Les bases, une fois qu’on a installé OpenBSD
* [OpenBSD France](http://www.openbsd-france.org/) : le site de la communauté française autour d’openbsd
* [Le wiki d’OpenBSD France](http://wiki.openbsd-france.org/) : un wiki rempli d’info pour OpenBSD (en français)
* [le jardin magique](http://www.gcu-squad.org/) : Qui s’interesse aux systèmes \*BSD connait forcement le <span class="caps">GCU</span>-squad
* [Le wiki du GCU](http://wiki.gcu.info/doku.php) : et son wiki, grande source d’informations.

Je suis loin d’avoir une liste complète, j’ajoute quoi ?

