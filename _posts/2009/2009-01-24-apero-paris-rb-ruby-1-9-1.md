---
layout: post
title: Apero Paris.rb - Ruby 1.9.1
---

Lundi 26 janvier aura lieu le [10ième apéro Ruby](http://rubyfrance.org/articles/2009/01/24/dixieme-apero-ruby-de-paris-rb-ruby-france-special-ruby-1-9-1/) organisé à Paris par l'association RubyFrance, et surtout le GO(Gentil Organisateur) Jean-François ! C'est à *20 h* au *Dune* (18, avenue Claude Vellefaux 75010 Paris, Métro Colonel Fabien ou Métro Goncourt).

Cette apéro est organisé autour de la sortie de la version 1.9.1 de [Ruby](http://ruby-lang.org) qui devrait sortir, est sorti, enfin, on est dedans là :-)

_Je ne serait pas à cette apéro, je préfère aller au [Dojo de développement](http://xp-france.net/cgi-bin/wiki.pl?DojoDeveloppement) qui à lieu tout les lundi (ou presque). Ceci dit, si vous n'êtes pas au dojo, j'espère que vous serez à l'apéro !_

_note: Je pourrais peut-être venir après remarque..._

