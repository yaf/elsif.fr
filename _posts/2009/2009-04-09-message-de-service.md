---
layout: post
title: Message de service
---

Depuis ce week-end, il y a quelque soucis sur la [Bricabox](http://bricabox.info)...

Le ping réponds, mais les sites et le ssh non. Une surcharge ? Ca m'étonnerais. Des attaques ? Pas plus que d'habitude, quoique, une bonne série de tentative d'accès en [brut force](http://fr.wikipedia.org/wiki/Recherche_par_force_brute).

Je ne sais pas si cela vous a géné, moi pas trop :-). Cependant, je vais tacher d'être vigilant ces prochain jour, et étudier la possibilité de changer de serveur web (aujourd'hui [Lighttpd](http://www.lighttpd.net/), passé à [Nginx](http://nginx.net/) ou l'[Apache Chrooté d'OpenBSD](http://www.openbsd.org/faq/faq10.html#httpdchroot)), ou bien changer de serveur d'application (passer de [mongrel](http://mongrel.rubyforge.org/) à [Thin](http://code.macournoyer.com/thin/), [ebb](http://ebb.rubyforge.org/) ou carrement [ModRails](http://www.modrails.com/) (avec Apache)).

Je vous tiendrais au courant si je trouve un quelque chose d'interessant.



