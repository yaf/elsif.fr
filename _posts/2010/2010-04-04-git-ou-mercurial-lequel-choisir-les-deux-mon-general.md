---
layout: post
title: Git ou Mercurial lequel choisir ? Les deux mon général !
---

  
![Git Logo](http://elsif.fr/files/git-logo.png)


Voici le nième billet que l’on peut trouver sur [Git](http://git-scm.com/) et [Mercurial](http://mercurial.selenic.com/).


On pourrais énumérer les différences entre les deux pour pouvoir choisir, mais [Why Git Is Better Than X](http://whygitisbetterthanx.com/) le fait assez bien (quoique, on peut douter de l’impartialité ;-)).


On pourrais aussi choisir mercurial pour l’executable en deux lettres (`hg`) au lieu de git qui en contient trois (`git`)...


Un petit plus pour Mercurial, en tant que client en tout cas. effectivement, grace à ce plugin [hg-git](http://hg-git.github.com/) un utilisateur de mercurial pourra partager avec des utilisateurs de Git. Je en suis pas sur que l’inverse existe.


Un autre point me dit que finalement on peut très bien ce servir des deux dans un même projet. C’est une discussion dans un TGV de retour de Strasbourg et le billet [MercurialSquashCommit de Martin Fowler](http://www.martinfowler.com/bliki/MercurialSquashCommit.html) qui eveil ma curiosité. J’ai l’impression que travailler par patch permet également d’utiliser n’importe quelle DVCS, à condition bien sur que celui-ci propose une création de patch facilité. Il me semble que Git propose avec la commande [git-diff](http://www.ru.kernel.org/pub/software/scm/git/docs/git-diff.html) voir [git-format-patch](http://www.kernel.org/pub/software/scm/git/docs/git-format-patch.html) permet une gestion très interessante des patchs. Mercurial a également une gestion interessante par le biais de l’[extension MQ](http://mercurial.selenic.com/wiki/MqExtension).


Mais j’ai besoin de faire quelque essais avant d’être persuadé (pour l’un et/ou l’autre). Promis, je vous en reparle quand c’est fait.


Alors utiliser, Git, Mercurial ou un autre, peut importe tant que l’équipe arrive à trouver un rythme et un processus qui lui convient ;-)


![Mercurial Logo](http://elsif.fr/files/mercurial-logo1.png)

  
