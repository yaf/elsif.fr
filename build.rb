#!/usr/bin/env ruby
# encoding: utf-8

require 'fileutils'
require 'haml'
require 'date'
require 'yaml'

WEEK_DAYS = %w{ Lundi Mardi Mercredi Jeudi Vendredi Samedi Dimanche}
MONTH = %w{ '' Janvier Février Mars Avril Mai Juin Juillet Août Septembre Octobre Novembre Décembre }

def month_in_letter(date)
  month = date.is_a?(Fixnum) ? date : date.month
  MONTH[month]
end

def dow_in_letter(date)
  day = date.is_a?(Fixnum) ? date : date.day
  WEEK_DAYS[day]
end

def year_of_posts
  years = Dir['_posts/*'].map{|f| f.split('/')[1].to_i}
  years.sort.reverse
end

def posts_for(year)
  post_class = Struct.new(:title, :url, :date, :month)
  posts = []
  Dir["_posts/#{year}/*.md"].each do |d|
    post = post_class.new(title: d)
    year, month, day, *title = d.split('/').last.gsub(/\.md/,'').split("-")
    file = YAML.load_file(d)
    post.title = title.join(' ')
    post.title = file['title']

    post.date = Date.new(year.to_i, month.to_i, day.to_i)
    post.month = month.to_i
    post.url = "/#{year}/#{month}/#{day}/#{title.join('-')}.html"

    posts << post
  end
  posts.sort_by{|p| p.date}.reverse
end

def posts_for_month(year_param, month_param)
  post_class = Struct.new(:title, :url, :date, :month)
  posts = []
  Dir["_posts/#{year_param}/*.md"].each do |d|
    year, month, day, *title = d.split('/').last.gsub(/\.md/,'').split("-")
    next if month.to_i != month_param

    post = post_class.new(title: d)
    file = YAML.load_file(d)
    post.title = title.join(' ')
    post.title = file['title']

    post.date = Date.new(year.to_i, month.to_i, day.to_i)
    post.month = month.to_i
    post.url = "/#{year}/#{month}/#{day}/#{title.join('-')}.html"

    posts << post
  end
  posts.sort_by{|p| p.date}.reverse
end

def posts_for_day(year_param, month_param, day_param)
  post_class = Struct.new(:title, :url, :date, :month)
  posts = []
  Dir["_posts/#{year_param}/*.md"].each do |d|
    year, month, day, *title = d.split('/').last.gsub(/\.md/,'').split("-")
    next if month.to_i != month_param
    next if day.to_i != day_param

    post = post_class.new(title: d)
    file = YAML.load_file(d)
    post.title = title.join(' ')
    post.title = file['title']

    post.date = Date.new(year.to_i, month.to_i, day.to_i)
    post.month = month.to_i
    post.url = "/#{year}/#{month}/#{day}/#{title.join('-')}.html"

    posts << post
  end
  posts.sort_by{|p| p.date}.reverse
end

FileUtils.mkdir_p '_site'

Dir['**/*.haml'].each do |f|
  next if f =~ /^_.*$/
  puts "generate #{f}"
  dest_dir = File.join("_site", File.dirname(f))
  FileUtils.mkdir_p dest_dir unless File.exist?(dest_dir)
  engine = Haml::Engine.new(File.read(f))
  File.write("_site/#{f.gsub(/haml/,'html')}", engine.render)
end

year_index_template = Haml::Engine.new(File.read("_year_index.haml"))
month_index_template = Haml::Engine.new(File.read("_month_index.haml"))
day_index_template = Haml::Engine.new(File.read("_day_index.haml"))
post_template = Haml::Engine.new(File.read("_post.haml"))

Dir['_posts/*'].each do |y|

  puts "work on year: #{y}"
  year = y.split('/')[1]

  posts_for_year = posts_for(year)
  File.write("_site/#{year}/index.html", year_index_template.render(Object.new, posts: posts_for_year, months: posts_for_year.map(&:month).uniq.sort, year: year))

  Dir["#{y}/*.md"].each do |d|
    puts "work on posts: #{d}"
    year, month, day, *title = d.split('/').last.gsub(/\.md/,'').split("-")
    FileUtils.mkdir_p "_site/#{year}/#{month}/#{day}"

    File.write("_site/#{year}/#{month}/index.html", month_index_template.render(Object.new, posts: posts_for_month(year.to_i, month.to_i), month: month.to_i, year: year))

    File.write("_site/#{year}/#{month}/#{day}/index.html", day_index_template.render(Object.new, posts: posts_for_day(year.to_i, month.to_i, day.to_i), day: day.to_i, month: month.to_i, year: year))

    y = YAML.load_file(d)
    File.write("_site/#{year}/#{month}/#{day}/#{title.join('-')}.html", post_template.render(Object.new, post: File.read(d).gsub(/---(.*)---/im, ''), date: DateTime.new(year.to_i, month.to_i, day.to_i), title: y['title']))
  end
end

FileUtils.cp 'elsif.css', '_site/elsif.css'
FileUtils.cp 'humans.txt', '_site/humans.txt'

