#!/bin/sh

SOURCE_PATH=$PWD
SITE_PATH="$PWD/../yaf.github.com"

cd $SITE_PATH
git pull
cd $SOURCE_PATH
ruby build.rb
rsync -avz $SOURCE_PATH/_site/ $SITE_PATH
cd $SITE_PATH

git add .
git ci -m "$1 update site `date`"
git push

cd $SOURCE_PATH
