programmation

- avoir executé dix programmes
- executé un programme interprété
- compilé un programme
- executé un programme compilé
- comprendre le paradigme objet
- comprendre le paradigme impératif
- comprendre le paradigme functionnel
- comprendre au moins deux paradigmes
- j'ai un langage de prédilection
- je sais me servir d'un gestionnaire de bibliothèque
- je sais me servir d'au moins 3 gestionnaires de bibliothèques (sur 3 langages différents)
- savoir faire une boucle
- savoir faire une boucle de 2 façon différentes
- savoir faire une boucle de 3 façon différentes
- savoir faire un kata
- savoir faire une structure conditionée
- savoir faire une structure conditionée de 2 façon différente
- savoir faire une structure conditionée de 3 façon différente
- savoir comibné 2 programme
- savoir écrire une bibliothèque
- savoir reconnaitre les mots clefs d'un langages
- savoir faire du réfactoring
- avoir faire deux kata
- être à l'aise sur un langage
- être à l'aise sur deux lanage
- avoir corrigé un bug dans le code d'une autre personne
- tdd
- coder à plusieurs sur le même projet
- coder à plusieurs en même temps sur le même projet
- coder à plusieur avec un seul ordi
- expliquer mon code à une personne tech
- expliquer mon code à une personne non tech
- esprit critique du code
- 

Git

- avoir initié un projet
- avoir cloné un projet
- avoir fait 10 commit
- avoir fait 1 commit
- avoir poussé du code
- avoir récupéré du code
- avoir résolu un conflit
- avoir utilisé une branche
- savoir la différence entre merge et rebase
- avoir utilisé la commande merge
- avoir utilisé la commande rebase
- avoir utilisé la commande checkout
- avoir utilisé 5 commandes différentes
- je sais aller sur n'importe quel commit
- je sais modifier la configuration du git projet
- j'ai utilisé un hook


Système & réseau & sécurite

- comprendre le protocole http
- intégration continue
- déploiement
- sécurité
- intrusion
- gestion mot de passe

UX/design

- Entretien utilisateur
- test utilisateur
- tester une idée sans code
- déssiner des écrans
- accessibilité


web

- j'ai une idée de comment fonctionne le navigateur
- je connais la console développeur
- je sais regarder le code source d'une page
- je sais analysé l'utilisation réseau
- css
- javascript
- html

Projet

- connais le concept de licence
- connais une licence
- connais plusieurs licence
- sais choisir une licence
- découper un problème en petit morceau
- trouver la priorisation
- organiser la boucle de feedback
- prendre du recul poru s'améliorer
- schema urbanisation du logiciel


Base de données

- SQL
- requete select
- select avec where
- insert
- delete
- update
- insert avec condition
- delete avec condition
- update avec condition
- creation de table
- mise à jour de table
- sauvegarde
- administraiton de compte
- sécurité des données

